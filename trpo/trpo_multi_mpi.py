from baselines.common import explained_variance, zipsame, dataset
from baselines import logger
import baselines.common.tf_util as U
import tensorflow as tf, numpy as np
import time
from baselines.common import colorize
from mpi4py import MPI
from collections import deque
from mpi_adam import MpiAdam
from baselines.common.cg import cg
from contextlib import contextmanager
import os
import matplotlib.pyplot as plt
import datetime

from trpo.trpo_cnn import TrpoCnn

from trpo_mpi import learn, flatten_lists, build_equal_cnn
import trpo_mpi


def traj_segment_generator(pi, env, horizon, stochastic, cnn=None):
    # Initialize state variables
    t = 0
    ac1 = env.action_space.sample()
    ac2 = env.action_space.sample()
    new = True
    rew1, rew2 = 0.0, 0.0
    ob1, ob2 = env.reset()
    speed1 = env.sample_speed()
    speed2 = np.flip(speed1, axis=1)
    if cnn:
        speed1 = cnn([speed1])[0, :]
        speed2 = cnn([speed2])[0, :]

    cur_ep_ret = 0
    cur_ep_len = 0
    ep_rets = []
    ep_lens = []

    # Initialize history arrays
    obs1 = np.array([ob1 for _ in range(horizon)])
    speeds1 = np.array([speed1 for _ in range(horizon)])
    rews1 = np.zeros(horizon, 'float32')
    vpreds1 = np.zeros(horizon, 'float32')
    acs1 = np.array([ac1 for _ in range(horizon)])
    prevacs1 = acs1.copy()

    obs2 = np.array([ob2 for _ in range(horizon)])
    speeds2 = np.array([speed2 for _ in range(horizon)])
    rews2 = np.zeros(horizon, 'float32')
    vpreds2 = np.zeros(horizon, 'float32')
    acs2 = np.array([ac2 for _ in range(horizon)])
    prevacs2 = acs2.copy()

    news = np.zeros(horizon, 'int32')

    while True:
        prevac1 = ac1
        prevac2 = ac2
        if cnn:
            ac1, vpred1 = pi.act(stochastic, ob1, speed)
            ac2, vpred2 = pi.act(stochastic, ob2, speed)
        else:
            ac1, vpred1 = pi.act(stochastic, ob1, None)
            ac2, vpred2 = pi.act(stochastic, ob2, None)
        
        # Slight weirdness here because we need value function at time T
        # before returning segment [0, T-1] so we get the correct
        # terminal value
        if t > 0 and t % horizon == 0:
            yield {"ob": np.vstack((obs1, obs2)), "rew": np.append(rews1, rews2), "vpred": np.append(vpreds1, vpreds2), "new": np.append(news, news),
                   "ac": np.vstack((acs1, acs2)), "prevac": np.vstack((prevacs1, prevacs2)), "nextvpred": np.append(vpred1 * (1 - new), vpred2 * (1 - new)),
                   "ep_rets": ep_rets, "ep_lens": ep_lens, 'speed': np.append(speed1, speed2)}
            if cnn:
                _, vpred1 = pi.act(stochastic, ob1, speed1)
                _, vpred2 = pi.act(stochastic, ob2, speed2)
            else:
                _, vpred1 = pi.act(stochastic, ob1, None)
                _, vpred2 = pi.act(stochastic, ob2, None)
            
            # Be careful!!! if you change the downstream algorithm to aggregate
            # several of these batches, then be sure to do a deepcopy
            ep_rets = []
            ep_lens = []

        i = t % horizon
        obs1[i] = ob1
        vpreds1[i] = vpred1
        acs1[i] = ac1
        prevacs1[i] = prevac1
        obs2[i] = ob2
        vpreds2[i] = vpred2
        acs2[i] = ac2
        prevacs2[i] = prevac2
        news[i] = new
        speeds1[i] = speed1
        speeds2[i] = speed2

        ob1, ob2, rew1, rew2, new, info = env.step([ac1, ac2])
        rews1[i], rews2[i] = rew1, rew2
        speed1 = info['speed']
        speed2 = np.flip(speed1, axis=1)
        if cnn:
            speed1 = cnn([speed1])[0, :]
            speed2 = cnn([speed2])[0, :]

        cur_ep_ret += rew1
        cur_ep_len += 1
        if new:
            ep_rets.append(cur_ep_ret)
            ep_lens.append(cur_ep_len)
            cur_ep_ret = 0
            cur_ep_len = 0
            ob1, ob2 = env.reset()
            if cnn:
                speed1 = cnn([speed1])[0, :]
                speed2 = cnn([speed2])[0, :]
        t += 1


def add_vtarg_and_adv(seg, gamma, lam):
    new = np.hstack((np.split(seg["new"], 2)[0], 0, np.split(seg["new"], 2)[1], 0))  # last element is only used for last vtarg, but we already zeroed it if last new = 1
    vpred = np.hstack((np.split(seg["vpred"], 2)[0], seg["nextvpred"][0], np.split(seg["vpred"], 2)[1], seg["nextvpred"][1]))
    T = len(seg["rew"])
    seg["adv"] = gaelam = np.empty(T, 'float32')
    rew = seg["rew"]
    lastgaelam = 0
    for t in reversed(range(T//2)):
        nonterminal = 1 - new[t + 1]
        delta = rew[t] + gamma * vpred[t + 1] * nonterminal - vpred[t]
        gaelam[t] = lastgaelam = delta + gamma * lam * nonterminal * lastgaelam
    lastgaelam = 0
    for t in reversed(range(T//2, T)):
        nonterminal = 1 - new[t + 1]
        delta = rew[t] + gamma * vpred[t + 1] * nonterminal - vpred[t]
        gaelam[t] = lastgaelam = delta + gamma * lam * nonterminal * lastgaelam

    seg["tdlamret"] = seg["adv"] + seg["vpred"]


trpo_mpi.traj_segment_generator = traj_segment_generator
trpo_mpi.add_vtarg_and_adv = add_vtarg_and_adv
