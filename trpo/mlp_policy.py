from baselines.ppo1.mlp_policy import MlpPolicy
from baselines.common.mpi_running_mean_std import RunningMeanStd
import baselines.common.tf_util as U
import tensorflow as tf
import gym
from baselines.common.distributions import make_pdtype


def _init(self, ob_space, ac_space, policy_hid_size, vf_hid_size, activation_policy, activation_vf, gaussian_fixed_var=True, cnn_input=False):
    assert isinstance(ob_space, gym.spaces.Box)
    code_len = 32

    self.pdtype = pdtype = make_pdtype(ac_space)
    sequence_length = None

    ob = U.get_placeholder(name="ob", dtype=tf.float32, shape=[sequence_length] + list(ob_space.shape))
    if cnn_input:
        ob_cnn = U.get_placeholder(name="ob_cnn", dtype=tf.float32, shape=[None, code_len])

    with tf.variable_scope("obfilter"):
        self.ob_rms = RunningMeanStd(shape=ob_space.shape)

    if cnn_input:
        with tf.variable_scope("cnnobfilter"):
            self.ob_cnn_rms = RunningMeanStd(shape=code_len)
        ob_cnnz = tf.clip_by_value((ob_cnn - self.ob_cnn_rms.mean) / self.ob_cnn_rms.std, -5.0, 5.0)

    obz = tf.clip_by_value((ob - self.ob_rms.mean) / self.ob_rms.std, -5.0, 5.0)

    last_out = obz
    for i, x in enumerate(vf_hid_size):
        last_out = tf.layers.dense(last_out, x, name="vffc%i" % (i + 1), kernel_initializer=U.normc_initializer(1.0))
        if cnn_input and i == 0:
            cnn_units = tf.layers.dense(ob_cnnz, x, name="vfcnnfc1", kernel_initializer=U.normc_initializer(1.0), use_bias=False)
            last_out = last_out + cnn_units
        last_out = activation_vf(last_out)
    self.vpred = tf.layers.dense(last_out, 1, name="vffinal", kernel_initializer=U.normc_initializer(1.0))[:,0]

    last_out = obz
    for i, x in enumerate(policy_hid_size):
        last_out = tf.layers.dense(last_out, x, name="polfc%i" % (i + 1), kernel_initializer=U.normc_initializer(1.0))
        if cnn_input and i == 0:
            cnn_units = tf.layers.dense(ob_cnnz, x, name="polcnnfc1", kernel_initializer=U.normc_initializer(1.0), use_bias=False)
            last_out = last_out + cnn_units
        last_out = activation_vf(last_out)

    if gaussian_fixed_var and isinstance(ac_space, gym.spaces.Box):
        mean = tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name="polfinal", kernel_initializer=U.normc_initializer(0.01))
        logstd = tf.get_variable(name="logstd", shape=[1, pdtype.param_shape()[0]//2], initializer=tf.zeros_initializer())
        pdparam = tf.concat(values=[mean, mean * 0.0 + logstd], axis=1)
    else:
        pdparam = tf.layers.dense(last_out, pdtype.param_shape()[0], name="polfinal", kernel_initializer=U.normc_initializer(0.01))

    self.pd = pdtype.pdfromflat(pdparam)

    self.state_in = []
    self.state_out = []

    stochastic = tf.placeholder(dtype=tf.bool, shape=())
    ac = U.switch(stochastic, self.pd.sample(), self.pd.mode())
    if cnn_input:
        self._act = U.function([stochastic, ob, ob_cnn], [ac, self.vpred])
    else:
        self._act = U.function([stochastic, ob], [ac, self.vpred])


def act(self, stochastic, ob, ob_cnn=None):
    if ob_cnn is None:
        ac1, vpred1 = self._act(stochastic, ob[None])
    else:
        ac1, vpred1 = self._act(stochastic, ob[None], ob_cnn[None])
    return ac1[0], vpred1[0]


MlpPolicy._init = _init
MlpPolicy.act = act