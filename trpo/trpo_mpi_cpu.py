from baselines.common import explained_variance, zipsame, dataset
from baselines import logger
import baselines.common.tf_util as U
import tensorflow as tf, numpy as np
import time
from baselines.common import colorize
from mpi4py import MPI
from collections import deque
from mpi_adam import MpiAdam
from baselines.common.cg import cg
from contextlib import contextmanager
import os
import matplotlib.pyplot as plt
import datetime

from trpo.trpo_cnn import TrpoCnn
from baselines.trpo_mpi.trpo_mpi import add_vtarg_and_adv, flatten_lists

import sys
sys.path.append('../gp')
# import gp_env

RESULT_PATH = '../results/debug/'


def traj_segment_generator(pi, env, horizon, stochastic, cnn=None, if_surrenv=False):
    # Initialize state variables
    t = 0
    ac = env.action_space.sample()
    new = True
    rew = 0.0
    ob = env.reset()
    field = env.sample_speed()
    if cnn:
        speed = cnn([field])[0, :]

    cur_ep_ret = 0
    cur_ep_len = 0
    ep_rets = []
    ep_lens = []

    # Initialize history arrays
    obs = np.array([ob for _ in range(horizon)])
    speeds = np.array([speed for _ in range(horizon)])
    fields = np.array([field for _ in range(horizon)])
    rews = np.zeros(horizon, 'float32')
    vpreds = np.zeros(horizon, 'float32')
    news = np.zeros(horizon, 'int32')
    acs = np.array([ac for _ in range(horizon)])
    prevacs = acs.copy()

    if if_surrenv:
        surr_env = None

    max_step = 100
    half_step = max_step/2
    surr_count = 0

    index = [ob.shape[0],
             ob.shape[0] + 1,
             ob.shape[0] + 2,
             ob.shape[0] + 2 + 32,
             ob.shape[0] + 2 + 32 + ac.shape[0]]
    while True:
        prevac = ac
        if cnn and not if_surrenv:
            ac, vpred = pi.act(stochastic, ob, speed)
        else:
            ac, vpred = pi.act(stochastic, ob, None)
        # Slight weirdness here because we need value function at time T
        # before returning segment [0, T-1] so we get the correct
        # terminal value
        if t > 0 and t % horizon == 0:
            if if_surrenv:
                # stack X and Y
                stack = np.hstack((obs, rews[:, None], news[:, None], speeds, acs))
                X = stack[: horizon - 1, :]
                Y = stack[1: horizon, : index[3]]
                if surr_env is None:
                    print ('creating surr env')
                    surr_env = gp_env.Surr_env(X, Y, M=500, disp=False)
                    print(surr_env.svgp_model.as_pandas_table())
                else:
                    print('updating surrenv')
                    surr_env.update(X, Y)
                    print(surr_env.svgp_model.as_pandas_table())


            yield {"ob": obs, "rew": rews, "vpred": vpreds, "new": news,
                   "ac": acs, "prevac": prevacs, "nextvpred": vpred * (1 - new),
                   "ep_rets": ep_rets, "ep_lens": ep_lens,
                   "speed": speeds, "field": fields}
            if cnn and not if_surrenv:
                _, vpred = pi.act(stochastic, ob, speed)
            else:
                _, vpred = pi.act(stochastic, ob, None)
            # Be careful!!! if you change the downstream algorithm to aggregate
            # several of these batches, then be sure to do a deepcopy
            ep_rets = []
            ep_lens = []

        i = t % horizon
        obs[i] = ob
        vpreds[i] = vpred
        news[i] = new
        acs[i] = ac
        prevacs[i] = prevac
        speeds[i] = speed
        fields[i] = field

        if not if_surrenv or surr_env is None or surr_count < half_step:
            ob, rew, new, info = env.step(ac)
            # speed = 10 * info['speed']  # not sure, check here
            field = info['speed']  # not sure, check here
            if cnn:
                speed = cnn([field])[0, :]  # not sure, check here
        else:
            stack_in = np.hstack((ob, rew, new, speed, ac))
            stack_m, _ = surr_env.step(stack_in[None, :])
            ob = stack_m[0, :index[0]]
            rew = stack_m[0, index[0]: index[1]][0]
            new = stack_m[0, index[1]: index[2]][0]
            speed = stack_m[0, index[2]: index[3]]
            new = 1 if new > 0.5 or surr_count == (max_step-1) else 0
            # print('ob: '+str(ob))
            # print('rew: '+str(rew))
            # print('new:'+str(new))


        rews[i] = rew

        cur_ep_ret += rew
        cur_ep_len += 1
        if new:
            ep_rets.append(cur_ep_ret)
            ep_lens.append(cur_ep_len)
            cur_ep_ret = 0
            cur_ep_len = 0
            ob = env.reset()
            field = env.sample_speed()
            if cnn:
                speed = cnn([field])[0, :]
        t += 1
        surr_count += 1
        surr_count %= max_step


def learn(env, policy_func, *,
          timesteps_per_batch,  # what to train on
          max_kl, cg_iters,
          gamma, lam,  # advantage estimation
          entcoeff=0.0,
          cg_damping=1e-2,
          vf_stepsize=3e-4,
          vf_iters=3,
          max_timesteps=0, max_episodes=0, max_iters=0,  # time constraint
          callback=None, identifier='', load_ckpt=False,
          reward_list=[],
          finetune=False,
          if_test=False, test_stepsize=None,
          test_env=None,
          cnn_iters=10,
          cnn_stepsize=1e-5,
          if_buildcnn=False,
          if_surrenv=False
          ):
    nworkers = MPI.COMM_WORLD.Get_size()
    rank = MPI.COMM_WORLD.Get_rank()
    np.set_printoptions(precision=3)
    board_save_flag = True
    # Setup losses and stuff
    # ----------------------------------------
    ob_space = env.observation_space
    ac_space = env.action_space
    with tf.device('/cpu:0'):
        pi = policy_func("pi", ob_space, ac_space)
        oldpi = policy_func("oldpi", ob_space, ac_space)
        atarg = tf.placeholder(dtype=tf.float32, shape=[None])  # Target advantage function (if applicable)
        ret = tf.placeholder(dtype=tf.float32, shape=[None])  # Empirical return
        ob = U.get_placeholder_cached(name="ob")

    if finetune:
        ob_cnn = U.get_placeholder_cached(name="ob_cnn")
    if if_buildcnn and rank == 0:
        with tf.device('/gpu:0'):
            cnn = TrpoCnn(env.field_size)
            cnn_var_list = tf.global_variables(scope='cnn')
            # cnn_var_list = [v for v in cnn_var_list if v.name.split('/'[0]).startwith('cnn')]
    if if_surrenv:
        with tf.device('/cpu:0'):
            cnn = TrpoCnn(env.field_size)
            cnn_var_list = tf.global_variables(scope='cnn')

    with tf.device('/cpu:0'):
        ac = pi.pdtype.sample_placeholder([None])

        kloldnew = oldpi.pd.kl(pi.pd)
        ent = pi.pd.entropy()
        meankl = tf.reduce_mean(kloldnew)
        meanent = tf.reduce_mean(ent)
        entbonus = entcoeff * meanent

        vferr = tf.reduce_mean(tf.square(pi.vpred - ret))

        ratio = tf.exp(pi.pd.logp(ac) - oldpi.pd.logp(ac))  # advantage * pnew / pold
        surrgain = tf.reduce_mean(ratio * atarg)

        optimgain = surrgain + entbonus
        losses = [optimgain, meankl, entbonus, surrgain, meanent]
        loss_names = ["optimgain", "meankl", "entloss", "surrgain", "entropy"]

        dist = meankl

        all_var_list = pi.get_trainable_variables()

    if finetune:
        var_list = [v for v in all_var_list if v.name.split("/")[1].startswith(("pol", "polcnn"))]
        vf_var_list = [v for v in all_var_list if v.name.split("/")[1].startswith(("vf", "vfcnn"))]
        vfadam = MpiAdam(vf_var_list)
        # if rank==0:
        #     with tf.device('/gpu:0'):
        #         cnn = TrpoCnn(env.field_size)
        #         cnn_var_list = tf.global_variables(scope='cnn')
        #         cnnadam = MpiAdam(cnn_var_list)
        # else:
        with tf.device('/cpu:0'):
            cnn = TrpoCnn(env.field_size)
            cnn_var_list = tf.global_variables(scope='cnn')
            cnnadam = MpiAdam(cnn_var_list)
    else:
        var_list = [v for v in all_var_list if v.name.split("/")[1].startswith("pol")]
        vf_var_list = [v for v in all_var_list if v.name.split("/")[1].startswith("vf")]
        vfadam = MpiAdam(vf_var_list)

    # if (if_buildcnn or finetune) and rank == 0:
    #     with tf.device('/gpu:0'):
    #         cnnerr = tf.reduce_mean(tf.square(cnn.field - cnn.logits))
    #         corr = tf.reduce_mean(cnn.field)
    #         cnn_extra_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    #         train_cnn_op = tf.train.AdamOptimizer(cnn_stepsize).minimize(cnnerr, var_list=cnn_var_list)
    #         # compute_cnnlossandgrad = U.function([cnn.field],
    #         #                                     [cnnerr, corr, U.flatgrad(cnnerr, cnn_var_list), cnn_extra_update_ops])
    #         cnn_update = U.function([cnn.field], [train_cnn_op, cnnerr, corr])
    if finetune:
        cnnerr = tf.reduce_mean(tf.square(cnn.field - cnn.logits))
        corr = tf.reduce_mean(cnn.field)
        cnn_extra_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        # train_cnn_op = tf.train.AdamOptimizer(cnn_stepsize).minimize(cnnerr, var_list=cnn_var_list)
        compute_cnnlossandgrad = U.function([cnn.field],
                                            [cnnerr, corr, U.flatgrad(cnnerr, cnn_var_list), cnn_extra_update_ops])

    with tf.device('/cpu:0'):
        get_flat = U.GetFlat(var_list)
        set_from_flat = U.SetFromFlat(var_list)
        klgrads = tf.gradients(dist, var_list)
        flat_tangent = tf.placeholder(dtype=tf.float32, shape=[None], name="flat_tan")
        shapes = [var.get_shape().as_list() for var in var_list]
        start = 0
        tangents = []

        for shape in shapes:
            sz = U.intprod(shape)
            tangents.append(tf.reshape(flat_tangent[start:start + sz], shape))
            start += sz
        gvp = tf.add_n([tf.reduce_sum(g * tangent) for (g, tangent) in zipsame(klgrads, tangents)])  # pylint: disable=E1111
        fvp = U.flatgrad(gvp, var_list)

        assign_old_eq_new = U.function([], [], updates=[tf.assign(oldv, newv)
            for (oldv, newv) in zipsame(oldpi.get_variables(), pi.get_variables())])

        compute_inputs = [[ob, ac, atarg], [ob, ac, atarg], [flat_tangent, ob, ac, atarg], [ob, ret], [ob, ret]]
        if finetune:
            for inputs in compute_inputs:
                inputs.append(ob_cnn)

        compute_losses = U.function(compute_inputs[0], losses)
        compute_lossandgrad = U.function(compute_inputs[1], losses + [U.flatgrad(optimgain, var_list)])
        compute_fvp = U.function(compute_inputs[2], fvp)
        compute_vflosses = U.function(compute_inputs[3], vferr)
        compute_vflossandgrad = U.function(compute_inputs[4], U.flatgrad(vferr, vf_var_list))
    
    @contextmanager
    def timed(msg):
        if rank == 0:
            print(colorize(msg, color='magenta'))
            tstart = time.time()
            yield
            print(colorize("done in %.3f seconds" % (time.time() - tstart), color='magenta'))
        else:
            yield

    def allmean(x):
        assert isinstance(x, np.ndarray)
        out = np.empty_like(x)
        MPI.COMM_WORLD.Allreduce(x, out, op=MPI.SUM)
        out /= nworkers
        return out

    saver = tf.train.Saver(all_var_list)

    # saver = tf.train.Saver()
    if (if_buildcnn or finetune) and rank == 0:
        cnn_saver = tf.train.Saver(cnn_var_list)
    if if_surrenv:
        cnn_saver = tf.train.Saver(cnn_var_list)

    if load_ckpt:
        saver.restore(tf.get_default_session(), RESULT_PATH + 'saved_model/model_data/' + identifier + '.ckpt')
        vfadam.load_params(RESULT_PATH + 'saved_model/adam/' + identifier + '.json')
    else:
        with tf.device('/cpu:0'):
            U.initialize()
            if if_surrenv:
                cnn_saver.restore(tf.get_default_session(), '../results/build_cnn/saved_model/cnn_data/cnn')

    th_init = get_flat()
    MPI.COMM_WORLD.Bcast(th_init, root=0)
    set_from_flat(th_init)
    vfadam.sync()

    if finetune:
        cnnadam.sync()
    # Prepare for rollouts
    # ----------------------------------------
    seg_gen = traj_segment_generator(pi, env, timesteps_per_batch, stochastic=True, 
        cnn=cnn.get_smart_logits if finetune or if_surrenv else None, if_surrenv=if_surrenv)

    episodes_so_far = 0
    timesteps_so_far = 0
    iters_so_far = 0
    tstart = time.time()
    lenbuffer = deque(maxlen=40)  # rolling buffer for episode lengths
    rewbuffer = deque(maxlen=40)  # rolling buffer for episode rewards
    test_rewbuffer = deque(maxlen=40)

    assert sum([max_iters > 0, max_timesteps > 0, max_episodes > 0]) == 1



    mpi_size = MPI.COMM_WORLD.Get_size()
    save_count = 0
    while True:
        if callback: callback(locals(), globals())
        if max_timesteps and timesteps_so_far >= max_timesteps:
            break
        elif max_episodes and episodes_so_far >= max_episodes:
            break
        elif max_iters and iters_so_far >= max_iters:
            break
        logger.log("********** Iteration %i ************" % iters_so_far)

        with timed("sampling"):
            seg = seg_gen.__next__()

        add_vtarg_and_adv(seg, gamma, lam)

        # ob, ac, atarg, ret, td1ret = map(np.concatenate, (obs, acs, atargs, rets, td1rets))
        ob, ac, atarg, tdlamret = seg["ob"], seg["ac"], seg["adv"], seg["tdlamret"]
        if finetune:
            ob_cnn = seg['speed']
        vpredbefore = seg["vpred"]  # predicted value function before udpate
        atarg = (atarg - atarg.mean()) / atarg.std()  # standardized advantage function estimate

        if hasattr(pi, "ret_rms"): pi.ret_rms.update(tdlamret)
        if hasattr(pi, "ob_rms"): pi.ob_rms.update(ob)  # update running mean/std for policy
        if finetune:
            if hasattr(pi, "ob_cnn_rms"): pi.ob_cnn_rms.update(ob_cnn)

        args = seg["ob"], seg["ac"], seg["adv"]
        if finetune:
            args += (seg['speed'], )
        fvpargs = [arr[::5] for arr in args]

        def fisher_vector_product(p):
            return allmean(compute_fvp(p, *fvpargs)) + cg_damping * p

        assign_old_eq_new()  # set old parameter values to new parameter values
        with timed("computegrad"):
            *lossbefore, g = compute_lossandgrad(*args)
        lossbefore = allmean(np.array(lossbefore))
        g = allmean(g)
        if np.allclose(g, 0):
            logger.log("Got zero gradient. not updating")
        else:
            with timed("cg"):
                stepdir = cg(fisher_vector_product, g, cg_iters=cg_iters, verbose=False)
            assert np.isfinite(stepdir).all()
            shs = .5 * stepdir.dot(fisher_vector_product(stepdir))
            lm = np.sqrt(shs / max_kl)
            # logger.log("lagrange multiplier:", lm, "gnorm:", np.linalg.norm(g))
            fullstep = stepdir / lm
            expectedimprove = g.dot(fullstep)
            surrbefore = lossbefore[0]
            stepsize = 1.0
            thbefore = get_flat()
            for _ in range(10):
                thnew = thbefore + fullstep * stepsize
                set_from_flat(thnew)
                meanlosses = surr, kl, *_ = allmean(np.array(compute_losses(*args)))
                improve = surr - surrbefore
                logger.log("Expected: %.3f Actual: %.3f" % (expectedimprove, improve))
                if not np.isfinite(meanlosses).all():
                    logger.log("Got non-finite value of losses -- bad!")
                elif kl > max_kl * 1.5:
                    logger.log("violated KL constraint. shrinking step.")
                elif improve < 0:
                    logger.log("surrogate didn't improve. shrinking step.")
                else:
                    logger.log("Stepsize OK!")
                    break
                stepsize *= .5
            else:
                logger.log("couldn't compute a good step")
                set_from_flat(thbefore)
            if nworkers > 1 and iters_so_far % 20 == 0:
                paramsums = MPI.COMM_WORLD.allgather((thnew.sum(), vfadam.getflat().sum()))  # list of tuples
                assert all(np.allclose(ps, paramsums[0]) for ps in paramsums[1:])

        # for (lossname, lossval) in zip(loss_names, meanlosses):
        #     logger.record_tabular(lossname, lossval)

        with timed("vf"):
            vfloss = []
            for _ in range(vf_iters):
                if finetune:
                    for (mbob, mbret, mbobcnn) in dataset.iterbatches((seg["ob"], seg["tdlamret"], seg["speed"]),
                                                             include_final_partial_batch=False, batch_size=64):
                        vfloss.append(allmean(np.array(compute_vflosses(mbob, mbret, mbobcnn))))
                        g = allmean(compute_vflossandgrad(mbob, mbret, mbobcnn))
                        vfadam.update(g, vf_stepsize)
                else:
                    for (mbob, mbret) in dataset.iterbatches((seg["ob"], seg["tdlamret"]),
                                                             include_final_partial_batch=False, batch_size=64):
                        vfloss.append(allmean(np.array(compute_vflosses(mbob, mbret))))
                        g = allmean(compute_vflossandgrad(mbob, mbret))
                        vfadam.update(g, vf_stepsize)
        if if_buildcnn or finetune:
            # if mpi_size > 1:
            #     gather_speed = np.array(MPI.COMM_WORLD.gather(seg['field'], root=0), 'float32')
            # else:
            #     gather_speed = seg['field']
            # if rank == 0:
            #     if mpi_size > 1:
            #         gather_speed = np.concatenate([i for i in gather_speed], axis=0)
            with timed("cnn"):
                cnn_loss = np.zeros((cnn_iters,), dtype=np.float32)
                cnn_corr = np.zeros((cnn_iters,), dtype=np.float32)
                for i in range(cnn_iters):
                    ite_loss = []
                    ite_corr = []
                    for mbspeed in dataset.iterbatches([seg['field']], include_final_partial_batch=False, batch_size=64):
                        # print(str(mbspeed[0].shape()))
                        # cnn_l_and_g = cnn_update(mbspeed[0])
                        # l = cnn_l_and_g[1]
                        # c = cnn_l_and_g[2]
                        cnnlossandgrad = compute_cnnlossandgrad(mbspeed[0])
                        g = cnnlossandgrad[2]
                        l = np.array(cnnlossandgrad[0])
                        c = np.array(cnnlossandgrad[1])
                        cnnadam.update(g, cnn_stepsize)
                        ite_loss.append(l)
                        ite_corr.append(c)
                    cnn_loss[i] = np.mean(ite_loss)
                    cnn_corr[i] = np.mean(ite_corr)
                    logger.log("cnn_loss: %f, cnn_corr: %f"%(cnn_loss[i], cnn_corr[i]))
                    # if rank == 0:
                    #     print("%dth loss: %f err: %f" % (i, cnn_loss[i], cnn_corr[i]), end='\r')
            # cnnadam.sync()

        test_episode_reward = 0
        test_episode_rewards = []
        if if_test:
            if iters_so_far%5==0:
                test_ob = test_env.reset()
                for test_t in range(test_stepsize):
                    ac, vpred = pi.act(False, test_ob, None)
                    test_ob, rew, new, info = test_env.step(ac)
                    test_episode_reward += rew
                    if new:
                        break
            if mpi_size>1:
                test_episode_rewards = np.array(MPI.COMM_WORLD.gather(test_episode_reward, root=0), 'float32')
            else:
                test_episode_rewards = np.array([test_episode_reward])
            # test_rewbuffer.extend(test_episode_rewards)
            # logger.log("test_episode_rewards")
            # logger.log(test_episode_rewards)
            # logger.log("reward_buffer")
            # logger.log(test_rewbuffer)

        logger.record_tabular("VfLoss", np.mean(vfloss))

        # logger.record_tabular("ev_tdlam_before", explained_variance(vpredbefore, tdlamret))

        lrlocal = (seg["ep_lens"], seg["ep_rets"])  # local values
        listoflrpairs = MPI.COMM_WORLD.allgather(lrlocal)  # list of tuples
        lens, rews = map(flatten_lists, zip(*listoflrpairs))
        lenbuffer.extend(lens)
        rewbuffer.extend(rews)


        logger.record_tabular("EpLenMean", np.mean(lenbuffer))
        mean_reward = np.mean(rewbuffer)
        logger.record_tabular("EpRewMean", mean_reward)
        logger.record_tabular("EpThisIter", len(lens))
        episodes_so_far += len(lens)
        timesteps_so_far += sum(lens)
        iters_so_far += 1

        logger.record_tabular("EpisodesSoFar", episodes_so_far)
        logger.record_tabular("TimestepsSoFar", timesteps_so_far)

        if if_test:
            logger.record_tabular("TestEpRewMean", np.mean(test_episode_rewards))

        logger.record_tabular("TimeElapsed", time.time() - tstart)
        if if_buildcnn and rank == 0:
            logger.record_tabular("cnn_loss", np.mean(cnn_loss))
            logger.record_tabular("cnn_corr", np.mean(cnn_corr))


        if rank == 0:
            reward_list.append(mean_reward)
            logger.dump_tabular()
            # save_path = RESULT_PATH + 'saved_model/adam/'
            # os.makedirs(save_path, exist_ok=True)
            # vfadam.save_params(save_path + identifier + '.json')
            if save_count % 10 == 0:
                saver.save(tf.get_default_session(), RESULT_PATH+'saved_model/model_data/model')
                if if_buildcnn or finetune:
                    cnn_saver.save(tf.get_default_session(), RESULT_PATH+'saved_model/cnn_data/cnn')
                    if board_save_flag:
                        board_save_flag = False
                        writer = tf.summary.FileWriter(RESULT_PATH + 'tensorboard/', U.get_session().graph)
                        writer.close()
        save_count += 1


def build_equal_cnn(
        env, policy_func, *,
        timesteps_per_batch,  # what to train on
        cnn_iters=50, cnn_stepsize=1e-5,
        max_timesteps=0, max_episodes=0, max_iters=0,  # time constraint
        callback=None, identifier='', record_video=False, cnn=None,
        cont=True, reward_list=[]
        ):
    nworkers = MPI.COMM_WORLD.Get_size()
    rank = MPI.COMM_WORLD.Get_rank()
    np.set_printoptions(precision=3)
    # Setup losses and stuff
    # ----------------------------------------
    ob_space = env.observation_space
    ac_space = env.action_space
    pi = policy_func("pi", ob_space, ac_space)

    ob = U.get_placeholder_cached(name="ob")
    ac = pi.pdtype.sample_placeholder([None])

    cnnerr = tf.reduce_mean(tf.square(cnn.field - cnn.logits))
    corr = tf.reduce_mean(cnn.field)

    all_var_list = tf.global_variables()
    cnn_var_list = [v for v in all_var_list if v.name.split("/")[0].startswith("cnn")]
    if not cont:
        var_list = [v for v in all_var_list if v.name.split("/")[0].startswith("pi")]
        mlp_saver = tf.train.Saver(var_list)
        cnn_saver = tf.train.Saver(cnn_var_list)
        mlp_saver.restore(tf.get_default_session(), RESULT_PATH + 'saved_model/model_data/' + identifier[:-4] + '.ckpt')
        U.ALREADY_INITIALIZED = U.ALREADY_INITIALIZED.union(set(var_list))
    # if cont:
    #     cnn_saver.restore(tf.get_default_session(), "./state_of_art/smoke_cnn_equal.ckpt")
    #     U.ALREADY_INITIALIZED = U.ALREADY_INITIALIZED.union(set(cnn_var_list))

    cnnadam = MpiAdam(cnn_var_list)
    extra_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    get_flat = U.GetFlat(cnn_var_list)
    set_from_flat = U.SetFromFlat(cnn_var_list)

    compute_cnnlossandgrad = U.function([cnn.field],
                                        [cnnerr, corr, U.flatgrad(cnnerr, cnn_var_list), extra_update_ops])

    @contextmanager
    def timed(msg):
        if rank == 0:
            print(colorize(msg, color='magenta'))
            tstart = time.time()
            yield
            print(colorize("done in %.3f seconds" % (time.time() - tstart), color='magenta'))
        else:
            yield

    def allmean(x):
        assert isinstance(x, np.ndarray)
        out = np.empty_like(x)
        MPI.COMM_WORLD.Allreduce(x, out, op=MPI.SUM)
        out /= nworkers
        return out

    def plot(rewards):
        x = range(0, len(rewards))
        curr_time = datetime.datetime.now().strftime("%y%m%d%H%M%S")
        plt.figure('Training Result')
        plt.plot(x, rewards, 'k', lw=3)
        plt.scatter(x, rewards)

        save_path = RESULT_PATH + "saved_model/cnn_images/"
        os.makedirs(save_path, exist_ok=True)
        plt.savefig(save_path + identifier[:-4] + '_' + curr_time + ".png")
        plt.gcf().clear()

    if cont:
        saver = tf.train.Saver()
        saver.restore(tf.get_default_session(), RESULT_PATH + 'saved_model/model_data/' + identifier + '.ckpt')
    else:
        U.initialize()
    th_init = get_flat()
    MPI.COMM_WORLD.Bcast(th_init, root=0)
    set_from_flat(th_init)
    cnnadam.sync()

    # Prepare for rollouts
    # ----------------------------------------
    seg_gen = traj_segment_generator(pi, env, timesteps_per_batch, stochastic=False, cnn=None)

    episodes_so_far = 0
    timesteps_so_far = 0
    iters_so_far = 0

    assert sum([max_iters > 0, max_timesteps > 0, max_episodes > 0]) == 1

    while True:
        if callback: callback(locals(), globals())
        if max_timesteps and timesteps_so_far >= max_timesteps:
            break
        elif max_episodes and episodes_so_far >= max_episodes:
            break
        elif max_iters and iters_so_far >= max_iters:
            break
        logger.log("********** Iteration %i ************" % iters_so_far)

        with timed("sampling"):
            seg = seg_gen.__next__()
        
        with timed("cnn"):
            cnn_loss = np.zeros((cnn_iters,), dtype=np.float32)
            cnn_corr = np.zeros((cnn_iters,), dtype=np.float32)
            for i in range(cnn_iters):
                ite_loss = []
                ite_corr = []
                for mbspeed in dataset.iterbatches([seg['speed']], include_final_partial_batch=False, batch_size=64):
                    cnnlossandgrad = compute_cnnlossandgrad(mbspeed[0] * 10)
                    g = allmean(cnnlossandgrad[2])
                    l = allmean(np.array(cnnlossandgrad[0]))
                    c = allmean(np.array(cnnlossandgrad[1]))
                    # cnnadam.sync()
                    cnnadam.update(g, cnn_stepsize)
                    ite_loss.append(l)
                    ite_corr.append(c)
                cnn_loss[i] = np.mean(ite_loss)
                cnn_corr[i] = np.mean(ite_corr)
                if rank == 0:
                    print("%dth loss: %f err: %f" % (i, cnn_loss[i], cnn_corr[i]), end='\r')
            if rank == 0:
                plot(cnn_loss)

        logger.record_tabular("cnn_loss", np.mean(cnn_loss))
        reward_list.append(np.mean(cnn_loss))

        lrlocal = (seg["ep_lens"], seg["ep_rets"])  # local values
        listoflrpairs = MPI.COMM_WORLD.allgather(lrlocal)  # list of tuples
        lens, rews = map(flatten_lists, zip(*listoflrpairs))

        episodes_so_far += len(lens)
        timesteps_so_far += sum(lens)
        iters_so_far += 1

        if rank == 0:
            logger.dump_tabular()
            saver.save(tf.get_default_session(), RESULT_PATH+'saved_model/model_data/model')
