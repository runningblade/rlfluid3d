import tensorflow as tf
import baselines.common.tf_util as U
import numpy as np


class TrpoCnn(object):

    def __init__(
            self, field_size,
            filter_sizes=None,
            strides=None,
            filter_nums=None,
            pads=None,
            pre_code_len=128,
            code_len=32
    ):
        if pads is None:
            pads = ["SAME", "SAME", "SAME", "VALID", "VALID"]
        if filter_nums is None:
            filter_nums = [64, 64, 128, 128, 128]
        if strides is None:
            strides = [2, 2, 2, 1, 1]
        if filter_sizes is None:
            filter_sizes = [7, 7, 5, 5, 5]
        assert len(pads) == len(filter_nums) == len(strides) == len(filter_sizes)
        self.field_size = field_size
        self.h = field_size[0]
        self.w = field_size[1]
        self.c = field_size[2]
        self.filter_sizes = filter_sizes
        self.strides = strides
        self.filter_nums = filter_nums
        self.pads = pads
        self.pre_code_len = pre_code_len
        self.code_len = code_len
        self.keep_prob = tf.constant(value=0.5, dtype=tf.float32)
        self.build_cnn()

    def build_cnn(self):
        with tf.variable_scope("cnn"):
            self.field = U.get_placeholder(
                name="field",
                dtype=tf.float32,
                shape=[None] + list(self.field_size)
            )
            hidden = self.field

            filter_sizes = self.filter_sizes
            strides = self.strides
            filter_nums = self.filter_nums
            pads = self.pads

            # (, 96, 160, 2)
            # (, 48, 80, 64)
            # (, 24, 40, 64)
            # (, 12, 20, 128)
            # (, 8, 16, 128)
            # (, 4, 12, 128) 6144

            for i, (k, s, n, p) in enumerate(zip(filter_sizes, strides, filter_nums, pads)):
                hidden = U.conv2d(hidden, n, "cnn_l%d" % i, [k, k], [s, s], pad=p)
                hidden = tf.layers.batch_normalization(hidden)
                hidden = tf.nn.relu(hidden)

            last_cnn_shape = list(hidden.shape[1:])

            hidden = U.flattenallbut0(hidden)

            hidden = U.dense(
            # self.pre_code = U.dense(
                hidden,
                self.pre_code_len,
                "cnn_pre_logits",
                weight_init=tf.contrib.layers.xavier_initializer()
            )
            hidden = tf.layers.batch_normalization(hidden)
            hidden = tf.nn.relu(hidden)

            self.smart_logits = U.dense(
                hidden,
                self.code_len,
                "cnn_smart_logits",
                weight_init=tf.contrib.layers.xavier_initializer()
            )
            hidden = tf.layers.batch_normalization(self.smart_logits)
            hidden = tf.nn.relu(hidden)

            # hidden = tf.layers.batch_normalization(hidden)
            # # hidden = tf.layers.batch_normalization(self.pre_code)
            # hidden = tf.nn.relu(hidden)

            hidden = U.dense(
                hidden,
                self.pre_code_len,
                "decnn_post_logits",
                weight_init=tf.contrib.layers.xavier_initializer()
            )
            hidden = tf.layers.batch_normalization(hidden)
            hidden = tf.nn.relu(hidden)

            hidden = U.dense(
                hidden,
                np.prod(last_cnn_shape),
                "decnn_l0",
                weight_init=tf.contrib.layers.xavier_initializer()
            )
            hidden = tf.layers.batch_normalization(hidden)
            hidden = tf.nn.relu(hidden)

            hidden = tf.reshape(hidden, [-1] + last_cnn_shape)

            for i, (k, s, p, n) in enumerate(zip(reversed(filter_sizes[1:]), reversed(strides[1:]), reversed(pads[1:]), reversed(filter_nums[:-1]))):
                hidden = tf.layers.conv2d_transpose(
                    hidden,
                    n,
                    (k, k),
                    strides=(s, s),
                    padding=p,
                    kernel_initializer=tf.contrib.layers.xavier_initializer(),
                    name="decnn_l%d" % (i + 1),
                )
                hidden = tf.layers.batch_normalization(hidden)
                hidden = tf.nn.relu(hidden)

            self.logits = tf.layers.conv2d_transpose(
                hidden,
                self.c,
                (filter_sizes[0], filter_sizes[0]),
                strides=(strides[0], strides[0]),
                padding=pads[0],
                kernel_initializer=tf.contrib.layers.xavier_initializer(),
                name="decnn_logits",
            )

            smart_logits_stopped = tf.stop_gradient(self.smart_logits)
            self.get_smart_logits = U.function([self.field], smart_logits_stopped)
            # smart_logits_stopped = tf.stop_gradient(self.pre_code)
            # self.get_smart_logits = U.function([self.field], smart_logits_stopped)

    # def build_equal_cnn(self):
    #     with tf.variable_scope("cnn"):
    #         self.field = U.get_placeholder(
    #             name="field",
    #             dtype=tf.float32,
    #             shape=[None] + list(self.field_size)
    #         )
    #         hidden = self.field
    #
    #         kernel = [7, 7]
    #         stride = [4, 4]
    #         num = [128, 128]
    #         pad = ["SAME", "SAME"]
    #
    #         for i, (k, s, n, p) in enumerate(zip(kernel, stride, num, pad)):
    #             hidden = U.conv2d(hidden, n, "cnn_l%d" % i, [k, k], [s, s], pad=p)
    #             hidden = tf.layers.batch_normalization(hidden)
    #             hidden = tf.nn.relu(hidden)
    #
    #         hidden = U.flattenallbut0(hidden)
    #
    #         self.smart_logits = U.dense(
    #             hidden,
    #             32,
    #             "cnn_smart_logits",
    #             weight_init=tf.contrib.layers.xavier_initializer()
    #         )
    #         hidden = tf.layers.batch_normalization(self.smart_logits)
    #         hidden = tf.nn.relu(hidden)
    #
    #         hidden = U.dense(
    #             hidden,
    #             6 * 10 * 128,
    #             "decnn_l0",
    #             weight_init=tf.contrib.layers.xavier_initializer()
    #         )
    #         hidden = tf.layers.batch_normalization(hidden)
    #         hidden = tf.nn.relu(hidden)
    #
    #         hidden = tf.reshape(hidden, [-1, 6, 10, 128])
    #
    #         kernel = [7]
    #         stride = [4]
    #         num = [128]
    #         pad = ["SAME"]
    #
    #         for i, (k, s, p, n) in enumerate(zip(kernel, stride, pad, num)):
    #             hidden = tf.layers.conv2d_transpose(
    #                 hidden,
    #                 n,
    #                 (k, k),
    #                 strides=(s, s),
    #                 padding=p,
    #                 kernel_initializer=tf.contrib.layers.xavier_initializer(),
    #                 name="decnn_l%d" % (i + 1),
    #             )
    #             hidden = tf.layers.batch_normalization(hidden)
    #             hidden = tf.nn.relu(hidden)
    #
    #
    #         self.logits = tf.layers.conv2d_transpose(
    #             hidden,
    #             2,
    #             (7, 7),
    #             strides=(4, 4),
    #             padding="SAME",
    #             kernel_initializer=tf.contrib.layers.xavier_initializer(),
    #             name="decnn_logits",
    #         )
