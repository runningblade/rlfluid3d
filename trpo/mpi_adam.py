from baselines.common.mpi_adam import MpiAdam
import json
import numpy as np

def save_params(self, fname):
    with open(fname, 'w') as file:
        json.dump([self.m.tolist(), self.v.tolist(), self.t], file)

def load_params(self, fname):
    try:
        with open(fname, 'r') as file:
            params = json.load(file)
        self.m, self.v, self.t = np.array(params[0]), np.array(params[1]), params[2]
    except:
        print('No saved adam params!')

MpiAdam.save_params = save_params
MpiAdam.load_params = load_params