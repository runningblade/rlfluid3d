import sys
sys.path.append("../envs/FluidOpt-build/")
import os
import shutil
import pyFluidOpt as fo
import numpy as np

def set_trans(t, v):
    for i in range(3):
        t(i, 3, v[i])

def set_rot(t, r):
    for i in range(3):
        for j in range(3):
            t(i, j, r(i, j))


class RgdSol(object):
    def __init__(self, dim, sz, x, y, z=0):
        self.rgd_sol = None
        self._get_rgd_sol(dim, sz, x, y, z)
        self.add_rgd_func = {
            'ball':  self.rgd_sol.addBall3,
            'cross': self.rgd_sol.addCross2,
            'box':   self.rgd_sol.addBox2
        }

    def add_rgd(self, rgd_type, ext, pos, rot):
        assert rgd_type in self.add_rgd_func
        self.add_rgd_func[rgd_type](
            fo.Vec3(*ext),
            fo.Vec3(*pos),
            fo.Vec3(*rot)
        )
        return self

    def add_solid(self, tran, dim, ext, depth, inside_out):
        self.rgd_sol.addSolid(fo.BoxGeomCell(
            tran,
            dim,
            fo.Vec3(*ext),
            depth,
            inside_out
        ))
        return self

    def config(self, rho, init_vel):
        self.rgd_sol.getBody(0).setP(fo.Vec3(*init_vel))
        self.rgd_sol.getBody(0).rho = rho
        return self

    def get_rgd_body(self, id=0):
        return self.rgd_sol.getBody(id)

    def _get_rgd_sol(self, dim, sz, x, y, z=0):
        self.rgd_sol = fo.RigidSolver(dim, sz)
        self.rgd_sol.BB = fo.BBox(
            fo.Vec3(sz, sz, sz),
            fo.Vec3(x - sz, y - sz, max(z - sz, sz))
        )


class SrcSol(object):
    def __init__(self, *args, **kwargs):
        self.s = fo.RandomSource()
        self.sf = None
        self.sig = 1
        self.get_src(*args, **kwargs)

    def get_src(self, *args, **kwargs):
        raise NotImplementedError

    def config(self, rho, T):
        self.s.T = T
        self.s.rho = rho
        return self

    def _reset(self):
        raise NotImplementedError

    def update_h(self, deg, pos, spd):
        self.sf._vel = fo.Vec3(spd * np.cos(deg), spd * np.sin(deg), 0)
        self.sf._ctr[0] = pos
        self._reset()

    def update_v(self, deg, pos, spd):
        self.sf._vel = fo.Vec3(spd * np.cos(deg), spd * np.sin(deg), 0)
        self.sf._ctr[1] = pos
        self._reset()

    def update_3D(self, pos, spd):
        self.sf._vel = fo.Vec3(0, self.sig * spd, 0)
        self.sf._rectBB._minC[2] = pos
        self.sf._rectBB._maxC[2] = pos
        self._reset()

    def _reset(self):
        self.sf.reset()


class SphereSrcSol(SrcSol):
    def get_src(self, ctr, rad, dim, res, vel=(0, 0, 0), last_for=0.0, src=True, ins=False):
        assert len(ctr) == len(vel) == 3
        self.sf = fo.ImplicitSphere(fo.Vec3(*ctr), rad, dim, res, fo.Vec3(*vel), last_for, src, ins)
        self.s.addSource(self.sf)
        return self

class RectSrcSol(SrcSol):
    def get_src(self, min_c, max_c, dim, res, sig=1, vel=(0, 0, 0), last_for=0.0, src=True, ins=False):
        assert len(min_c) == len(max_c) == len(vel) == 3
        self.sig = sig
        self.sf = fo.ImplicitRect(fo.Vec3(*min_c), fo.Vec3(*max_c), dim, res, fo.Vec3(*vel), last_for, src, ins)
        self.s.addSource(self.sf)
        return self

class Sink2DSrcSol(SrcSol):
    def get_src(self, sol, w, h, res, coef, dim=2, vel=(0, 0, 0), last_for=0.0, src=False, ins=False):
        self.sf = fo.ImplicitRectNeg(
            fo.Vec3(2 * coef * res, 2 * coef * res, 0),
            fo.Vec3(w - 2 * coef * res, h - 2 * coef * res, 0),
            dim, res, fo.Vec3(*vel), last_for, src, ins
        )
        self.s.addSource(self.sf)
        self.s.setSource(False)
        sol.addSource(self.s)
        return self

class Sink3D2WaySrcSol(SrcSol):
    def get_src(self, sol, w, h, res, sink_h, dim=3, vel=(0, 0, 0), last_for=0.0, src=False, ins=False):
        # left sink
        self.sf = fo.ImplicitRect(
            fo.Vec3(w - res, res, sink_h),
            fo.Vec3(w, h - res, sink_h + res),
            dim, res, fo.Vec3(*vel), last_for, src, ins
        )
        self.s.addSource(self.sf)
        self.s.setSource(False)
        sol.addSource(self.s)
        # right sink
        self.s = fo.RandomSource()
        self.sf = fo.ImplicitRect(
            fo.Vec3(0, res, sink_h),
            fo.Vec3(res, h - res, sink_h + res),
            dim, res, fo.Vec3(*vel), last_for, src, ins
        )
        self.s.addSource(self.sf)
        self.s.setSource(False)
        sol.addSource(self.s)
        return self

class Sink3D1WaySrcSol(SrcSol):
    def get_src(self, sol, w, h, res, sink_h, dim=3, vel=(0, 0, 0), last_for=0.0, src=False, ins=False):
        # oppsite sink
        self.sf = fo.ImplicitRect(
            fo.Vec3(0, h - res, sink_h),
            fo.Vec3(w, h, sink_h + res),
            dim, res, fo.Vec3(*vel), last_for, src, ins
        )
        self.s.addSource(self.sf)
        self.s.setSource(False)
        sol.addSource(self.s)
        return self

class FluidSol(object):
    def __init__(self):
        self.srcs = []
        self.rgd = None
        self.SPARSITY = 1
        self.sol = None

    def add_src(self, src):
        self.srcs.append(src)

    def _get_sol(self):
        raise NotImplementedError

    def update_sol(self):
        self.sol = self._get_sol()

    def init_domain(self, warp_on, sz, dim, w, h, d=0):
        sz = 1 / sz
        self.sol.initDomain(
            fo.BBox(
                fo.Vec3(0, 0, 0),
                fo.Vec3(w, h, d)
            ),
            fo.Vec3(sz, sz, 0 if d == 0 else sz),
            fo.Vec3i(1, 0, 0) if warp_on else None
        )
        self.rgd = RgdSol(dim, sz, w, h, d)

    def init_sol(self):
        self.sol.globalId = 0
        self.sol.globalTime = 0

    def init_solid(self):
        self.sol.initSolid(self.rgd.rgd_sol)

    def add_rgd(self, rgd_type, ext, pos, rho, rot=(0, 0, 0), init_vel=(0, 0, 0)):
        assert len(ext) == len(pos) == len(rot) == 3
        self.rgd.add_rgd(rgd_type, ext, pos, rot).config(rho, init_vel)
        return self

    def add_bottom(self, w, h):
        self.add_solid((w/2, h/2, 0), (0, 0, 0), 3, (w, h, 0.1))

    def add_trap(self, trap_h, trap_t, w, h):
        self.add_solid(
            (w/2, h/2 - trap_t/2, 0),
            (np.pi/4, 0, 0),
            3,
            (w, np.sqrt(2) * trap_h / 2, np.sqrt(2) * trap_h / 2)
        )
        self.add_solid(
            (w/2, h/2 + trap_t/2, 0),
            (np.pi/4, 0, 0),
            3,
            (w, np.sqrt(2) * trap_h / 2, np.sqrt(2) * trap_h / 2)
        )
        self.add_solid(
            (w/2, h/2, 0),
            (0, 0, 0),
            3,
            (w, trap_t/2, trap_h)
        )

    def add_solid(self, tran, rot, dim, ext, depth=0, inside_out=False):
        assert len(ext) == len(tran) == len(rot) == 3
        T = fo.Mat4()
        R = fo.Mat3()
        T.setIdentity()
        set_trans(T, fo.Vec3(*tran))
        R.fromLog(fo.Vec3(*rot))
        set_rot(T, R)
        self.rgd.add_solid(T, dim, ext, depth, inside_out)
        return self

    def get_rgd_pos(self, dim=2):
        assert dim == 2 or dim == 3
        rgd = self.rgd.get_rgd_body()
        pos = rgd.pos()
        if dim == 2:
            return pos[0], pos[1]
        return pos[0], pos[1], pos[2]

    def get_rgd_rot(self):
        rgd = self.rgd.get_rgd_body()
        rot = rgd.rot()
        return (rot(y, x) for x in range(0, 2) for y in range(0, 2))

    def get_rgd_theta(self):
        rgd = self.rgd.get_rgd_body()
        return rgd.rot().toLog()[2]

    def get_rgd_vel(self, dim=2):
        assert dim == 2 or dim == 3
        rgd = self.rgd.get_rgd_body()
        vel = rgd.linSpd()
        if dim == 2:
            return vel[0], vel[1]
        return vel[0], vel[1], vel[2]

    def get_rgd_omega(self):
        rgd = self.rgd.get_rgd_body()
        rot_vel = rgd.rotSpd()
        return rot_vel[2]

    def get_rgd_frc(self, dim=2):
        assert dim == 2 or dim == 3
        frc = self.sol.getForceOnBody(0)
        if dim == 2:
            return frc[0], frc[1]
        return frc[0], frc[1], frc[2]

    def get_rgd_trq(self):
        trq = self.sol.getTorqueOnBody(0)
        return trq[2]

    def get_rgd_bbox(self):
        rgd = self.rgd.get_rgd_body()
        bb = fo.BBox()
        rgd.getWorldBB(bb)
        return (bb._minC[0], bb._minC[1]), (bb._maxC[0], bb._maxC[1])

    def add_sink_2D(self, w, h, sz):
        sz = 1.0 / sz
        coef = 1 << self.SPARSITY
        sink = Sink2DSrcSol(self.sol, w, h, sz, coef)

    def add_sink_3D_2Way(self, w, h, sink_h, sz):
        sz = 1.0 / sz
        sink = Sink3D2WaySrcSol(self.sol, w, h, sz, sink_h)

    def add_sink_3D_1Way(self, w, h, sink_h, sz):
        sz = 1.0 / sz
        sink = Sink3D1WaySrcSol(self.sol, w, h, sz, sink_h)

    def add_srcs(self, srcs):
        self.srcs.extend(srcs)
        for src in self.srcs:
            self.sol.addSource(src.s)

    def config_srcs(self, *args):
        for src in self.srcs:
            src.config(*args)

    def get_dx(self):
        return self.sol.getGrid0().getDx()

    def get_width(self):
        return self.sol.getGrid0().getWidth()

    def update_src_v(self, degs, poses, jets, spd):
        assert len(self.srcs) == len(degs) == len(poses) == len(jets)
        for deg, pos, jet, src in zip(degs, poses, jets, self.srcs):
            src.update_v(deg, pos, spd)
            self.sol.addSource(src.s) if jet else self.sol.removeSource(src.s)

    def update_src_h(self, degs, poses, jets, spd):
        assert len(self.srcs) == len(degs) == len(poses) == len(jets)
        for deg, pos, jet, src in zip(degs, poses, jets, self.srcs):
            src.update_h(deg, pos, spd)
            self.sol.addSource(src.s) if jet else self.sol.removeSource(src.s)

    def update_3Dsrc(self, poses, spds):
        assert len(self.srcs) == len(poses) == len(spds)
        for pos, spd, src in zip(poses, spds, self.srcs):
            src.update_3D(pos, spd)

    def advance(self, dt):
        self.sol.focusOn2(0, 1)
        self.sol.advance(dt)

    def get_ft_sz(self, sz):
        ft = fo.VectorField()
        self.sol.getFeature(ft, sz)
        return ft.getNrPoint()[0], ft.getNrPoint()[1]

    def get_ft(self, sz=-1):
        ft = fo.VectorField()
        self.sol.getFeature(ft, sz)
        x, y = ft.getNrPoint()[0], ft.getNrPoint()[1]
        rho = np.zeros((x, y, 2))
        for i in range(x):
            for j in range(y):
                for k in range(2):
                    rho[i][j][k] = ft[i * x + j][k]
        return rho

    def is_ct(self):
        rgd = self.rgd.get_rgd_body()
        return rgd.hasContactFlag()

    def focus_on1(self, w, h):
        self.sol.focusOn1(fo.BBox(
            fo.Vec3(0, 0, 0),
            fo.Vec3(w, h, 0)
        ))

    def write_vtk(self, path, epi):
        raise NotImplementedError


class LiquidSol(FluidSol):
    def _get_sol(self):
        return fo.SolverFLIP2DConstant1()

    def get_mesh(self):
        mesh = self.sol.getSurfaceMesh(True, 6)
        mesh_v = []
        mesh_i = []
        for i in range(0, mesh.nrV()):
            mesh_v.append(mesh.getV(i)[0])
            mesh_v.append(mesh.getV(i)[1])
            mesh_v.append(0)
        for i in range(0, mesh.nrI()):
            mesh_i.append(mesh.getI(i)[0])
            mesh_i.append(mesh.getI(i)[1])
            mesh_i.append(mesh.getI(i)[2])
        return mesh_v, mesh_i

    def init_sol(self, unilateral=True, variational=False, BOTE=True):
        self.sol.setUseUnilateral(True)
        self.sol.setUseVariational(False)
        self.sol.setUseBOTE(True)
        super().init_sol()

    def get_pt(self):
        width = self.get_width()
        nrPt = self.sol.getPSet().size()
        xys = []
        for i in range(0, nrPt):
            xys.append(self.sol.getPSet()[i]._pos[0])
            xys.append(self.sol.getPSet()[i]._pos[1])
        return xys, nrPt

    def write_vtk(self, path, epi=0):
        full_path = path + '/liquid_' + str(epi) + '/'
        if os.path.exists(full_path):
            shutil.rmtree(full_path)
        os.mkdir(full_path)
        self.sol.getPSet().writeVTK(full_path + 'pSet.vtk')
        feature = fo.VectorField()
        i = 0
        while True:
            self.sol.getFeature(feature, 1)
            feature.writeVTK(full_path + 'feature%d.vtk' % i)
            self.sol.getPSet().writeVTK(full_path + 'pset%d.vtk' % i)
            self.sol.getRigid().writeMeshVTK(full_path + 'rigid%d.vtk' % i)
            self.sol.getSurfaceMesh(True, 6).writeVTK(full_path + 'surface%d.vtk' % i)
            yield


class SmokeSol(FluidSol):
    def _get_sol(self):
        return fo.SolverSmoke2DConstant1()


class SWESol(FluidSol):
    def _get_sol(self):
        return fo.SolverSWE2DConstant1()

    def init_sol(self, water_d, const_d=0, const_dl=5, const_l=0):
        self.sol.initLiquidLevel(water_d, 1)
        self.sol.CDrag = const_d
        self.sol.CDragLiquid = const_dl
        self.sol.CLift = const_l
        super().init_sol()

    def write_vtk(self, path, epi):
        full_path = path + '/vtk/SWE_' + str(epi) + '/'
        if os.path.exists(full_path):
            shutil.rmtree(full_path)
        os.mkdir(full_path)
        i = 0
        while True:
            self.sol.getRigid().writeMeshVTK(full_path + 'rigid%d.vtk' % i)
            ws = self.sol.getWS()
            wsObj = ws.toObj(True)
            wsObj.subdivide(1)
            wsObj.writeVTK(full_path + 'W%d.vtk' % i)
            bs = self.sol.getBS()
            bs.writeVTK(full_path + 'B%d.vtk' % i, True)
            i += 1
            yield

    def write_pov(self, path, epi):
        full_path = path + '/pov/mesh/' + str(epi) + '/'
        if os.path.exists(full_path):
            shutil.rmtree(full_path)
        os.makedirs(full_path, exist_ok=True)
        i = 0
        while True:
            ws = self.sol.getWS()
            wsObj = ws.toObj(True)
            wsObj.subdivide(1)
            wsObj.writePov(full_path + 'W%d.pov' % i, True)
            i += 1
            yield

class BaseReward(object):
    def __call__(self, *args, **kwargs):
        raise NotImplementedError

class BalanceReward(BaseReward):
    def __call__(self, *args, **kwargs):
        reward = 0
        reward += self._dist(self.c_pos, self.w_pos, args[0], self.ctr)
        reward += self._dist(self.c_vel, self.w_vel, args[1])
        reward += (1 - args[2]) / 2 * self.w_sh
        return reward

    def __init__(self, w_pos, w_vel, w_sh, ctr, c_pos=3, c_vel=3):
        self.w_pos = w_pos
        self.w_vel = w_vel
        self.w_sh  = w_sh
        self.ctr   = ctr
        self.c_pos = c_pos
        self.c_vel = c_vel

    def _dist(self, coe, w, vec1, vec2=(0, 0)):
        return w * np.exp(-coe * np.sqrt(
            (vec1[0] - vec2[0]) ** 2 +
            (vec1[1] - vec2[1]) ** 2
        ))

class SWE2DReward(BaseReward):
    '''
    based on x error when reached target_y
    '''
    def __init__(self, *args, **kwargs):
        pass

    def __call__(self, *args, **kwargs):
        obs = kwargs['obs']
        if obs['rgd_pos_y'] >= obs['target_y']:
            return 1 / (np.abs(obs['rgd_pos_x'] - obs['target_x']) + 0.1)
        else:
            return 0

class SWE3DReward(BaseReward):
    def __init__(self, *args, **kwargs):
        pass

    def __call__(self, *args, **kwargs):
        obs = kwargs['obs']
        if obs['rgd_pos_y'] >= obs['target_y']:
            return 1 / (np.sqrt((obs['rgd_pos_x'] - obs['target_x']) ** 2 + (obs['rgd_pos_z'] - obs['target_z']) ** 2) + 0.1)
        else:
            return 0

class SWE2DReward2(BaseReward):
    '''
    based on whether reached target bounding box and the distance to the bounding box
    '''
    def __init__(self, *args, **kwargs):
        self.target_threshold = kwargs['target_threshold']
        self.win_factor = kwargs['win_factor']
        self.lose_factor = kwargs['lose_factor']

    def __call__(self, *args, **kwargs):
        obs = kwargs['obs']
        if max(abs(obs['rgd_pos_x'] - obs['target_x']), \
            abs(obs['rgd_pos_y'] - obs['target_y'])) < self.target_threshold:
            # within bounding box
            return self.win_factor
        else:
            return -self.lose_factor * np.sqrt((obs['rgd_pos_x'] - obs['target_x']) ** 2 + \
                (obs['rgd_pos_y'] - obs['target_y']) ** 2)

class SWE3DReward2(BaseReward):
    def __init__(self, *args, **kwargs):
        self.target_threshold = kwargs['target_threshold']
        self.win_factor = kwargs['win_factor']
        self.lose_factor = kwargs['lose_factor']

    def __call__(self, *args, **kwargs):
        obs = kwargs['obs']
        if max(abs(obs['rgd_pos_x'] - obs['target_x']), \
            abs(obs['rgd_pos_y'] - obs['target_y']), \
            abs(obs['rgd_pos_z'] - obs['target_z'])) < self.target_threshold:
            # within bounding box
            return self.win_factor
        else:
            return -self.lose_factor * np.sqrt((obs['rgd_pos_x'] - obs['target_x']) ** 2 + \
                (obs['rgd_pos_y'] - obs['target_y']) ** 2 + \
                (obs['rgd_pos_z'] - obs['target_z']) ** 2)


class SWE2DReward3(BaseReward):
    '''
    based on whether reached target bounding box and the time when reached
    '''
    def __init__(self, *args, **kwargs):
        self.target_threshold = kwargs['target_threshold']
        self.win_factor = kwargs['win_factor']
        self.lose_factor = kwargs['lose_factor']

    def __call__(self, *args, **kwargs):
        obs = kwargs['obs']
        if max(abs(obs['rgd_pos_x'] - obs['target_x']), \
            abs(obs['rgd_pos_y'] - obs['target_y'])) < self.target_threshold:
            # within bounding box
            return self.win_factor / (kwargs['time'] + 1)
        else:
            return 0

class SWE3DReward3(BaseReward):
    def __init__(self, *args, **kwargs):
        self.target_threshold = kwargs['target_threshold']
        self.win_factor = kwargs['win_factor']
        self.lose_factor = kwargs['lose_factor']

    def __call__(self, *args, **kwargs):
        obs = kwargs['obs']
        if max(abs(obs['rgd_pos_x'] - obs['target_x']), \
            abs(obs['rgd_pos_y'] - obs['target_y']), \
            abs(obs['rgd_pos_z'] - obs['target_z'])) < self.target_threshold:
            # within bounding box
            return self.win_factor / (kwargs['time'] + 1)
        else:
            return 0

class BaseGameOver(object):
    def __call__(self, *args, **kwargs):
        raise NotImplementedError

class SWE2DGameOver(BaseGameOver):
    def __init__(self, *args, **kwargs):
        pass
        
    def __call__(self, *args, **kwargs):
        obs = kwargs['obs']
        return obs['rgd_pos_y'] >= obs['target_y']

SWE3DGameOver = SWE2DGameOver

class SWE2DGameOver2(BaseGameOver):
    def __init__(self, *args, **kwargs):
        self.target_threshold = kwargs['target_threshold']

    def __call__(self, *args, **kwargs):
        obs = kwargs['obs']
        return max(abs(obs['rgd_pos_x'] - obs['target_x']), \
            abs(obs['rgd_pos_y'] - obs['target_y'])) < self.target_threshold

class SWE3DGameOver2(BaseGameOver):
    def __init__(self, *args, **kwargs):
        self.target_threshold = kwargs['target_threshold']

    def __call__(self, *args, **kwargs):
        obs = kwargs['obs']
        return max(abs(obs['rgd_pos_x'] - obs['target_x']), \
            abs(obs['rgd_pos_y'] - obs['target_y']), \
            abs(obs['rgd_pos_z'] - obs['target_z'])) < self.target_threshold