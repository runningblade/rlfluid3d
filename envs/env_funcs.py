IDENTIFIER = ['lppsc']
reward_list = []
song = []

from envs import \
    liquid_cross_single_continuous as lcsc, \
    liquid_music_single_discrete as lmsd, \
    liquid_music_triple_discrete as lmtd, \
    liquid_pingpong_battle as lppb, \
    liquid_pingpong_bottom_battle as lppbb, \
    liquid_pingpong_single_continuous as lppsc, \
    liquid_pingpong_single_continuous_mpi as lppsc_mpi, \
    smoke_pingpong_single_continuous as sppsc, \
    smoke_cross_single_continuous as scsc, \
    liquid_cross_rotate_single_continuous as lcrsc

func = {
    'lcsc': lcsc.LiquidCrossSingleContinuous, \
    'lmsd': lmsd.LiquidMusicSingleDiscrete, \
    'lmtd': lmtd.LiquidMusicTripleDiscrete, \
    'lppb': lppb.LiquidPingPongBattle, \
    'lppbb': lppbb.LiquidPingPongBottomBattle, \
    'lppsc': lppsc.LiquidPingPongSingleContinuous, \
    'lppsc_mpi': lppsc_mpi.LiquidPingPongSingleContinuous, \
    'sppsc': sppsc.SmokePingPongSingleContinuous, \
    'scsc': scsc.SmokeCrossSingleContinuous, \
    'lcrsc': lcrsc.LiquidCrossRotateSingleContinuous
}