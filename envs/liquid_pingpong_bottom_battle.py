import math
import numpy as np
import ctypes as ct
import os

from pynput.keyboard import Listener
from env_utils import *

import gym
from gym import spaces
from gym.utils import seeding
from gym.utils import reraise
from fluidRender import *
from recorder import GameSingleRunner, GameDoubleRunner

import sys
sys.path.append('..')
from baselines.ppo1.mlp_policy import MlpPolicy
import tensorflow as tf
import baselines.common.tf_util as U

FPS = 50


class LiquidPingPongBottomBattle(gym.Env):
    # implementation
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': FPS
    }

    def __init__(self, w=5, h=3, radpp=0.15, radsh=0.12, radbar=0.1,  # geometry
                 fsh=40, fbb=50, max_omega=250, rhopp=1500,  # physics	# Yunsheng: fsh(10->30), fbb(10->30), max_omega(100->250), rhopp(1500->1000)
                 max_step=1000, win_reward=-1, cross_reward=5, crossing_width=5, shoot_punish=0.05, # reward weights
                 resf=32, res=100,  # render resolution
                 mag_charge_vel=1, mag_jet_vel=2, mag_size=10,
                 debug=False, render_option='p', **kwargs):  # render when training
        # the rest
        self.seed()
        self.viewers  = []
        self.now_step = 0
        self.max_step = max_step
        self.res      = res
        self.debug    = debug
        # Magazine
        self.mag_charge_vel = mag_charge_vel
        self.mag_jet_vel = mag_jet_vel
        self.mag_size = mag_size
        self.mag_loaded1 = self.mag_size
        self.mag_loaded2 = self.mag_size
        self.crossing = False
        self.l_in = False
        self.r_in = False
        self.crossing_width = crossing_width
        self.shoot_punish = shoot_punish
        # geometry
        self.w     = w
        self.h     = h
        self.radpp = radpp
        self.radsh = radsh
        self.resf  = resf
        self.hbar  = h / 3
        self.radbar = radbar
        # physics
        self.possh1     = w / 6
        self.velsh1     = 0.0
        self.theta1     = 90.0
        self.possh2     = w / 6
        self.velsh2     = 0.0
        self.theta2     = 90.0
        self.fsh       = fsh
        self.fbb       = fbb
        self.rhopp     = rhopp
        self.max_omega = max_omega
        self.pospp_init = w / 4
        # reward
        self.game_over  = False
        self.game_win = 0
        self.now_step   = 0
        self.win_reward = win_reward
        self.cross_reward = cross_reward
        # rendering
        self.bg_obj = Background(self.w, self.h, '../envs/texs/grey.jpg', (0, 0, 0))
        self.rigid_obj = Ball(self.radpp * 2, self.radpp * 2, '../envs/texs/tennisball.png')
        self.sh_obj = Ball(self.radsh * 2, self.radsh * 2, '../envs/texs/pipe.png', (0.5, 0.4, 0.9))
        self.bar_obj = Rectangle(radbar * 2, self.hbar, '../envs/texs/bar.png', (1, 1, 1))
        # fluid mesh info
        self.mesh_v_size = 0
        self.mesh_i_size = 0
        self.mesh_v = []
        self.mesh_i = []
        # state: pp x, pp y, pp velx, pp vely,
        # state: sh x, sh a, sh velx, shooter vela
        high = np.array([np.inf] * 7)
        self.observation_space = spaces.Box(-high, high, dtype=np.float32)
        # action: nop, shoot, shoot left, shoot right, move left, move right
        # self.bb_speed_range  = np.array([0, 1])  # Pingchuan: speed of liquid
        self.bb_volume_range = np.array([-1, 1])  # Pingchuan: volume of liquid
        self.sh_degree_range = np.array([-1, 1])  # Pingchuan: degree of shooter
        self.sh_force_range  = np.array([-1, 1])  # Pingchuan: moving force of shooter
        action_range = np.array([self.bb_volume_range, self.sh_degree_range, self.sh_force_range])
        action_range = action_range.T
        self.action_space = spaces.Box(action_range[0, :], action_range[1, :], dtype=np.float32)
        # c interface
        lib_path = os.path.join(os.path.split(os.path.realpath(__file__))[0], "FluidOpt-build/libFluid.so")
        self.fluid = ct.cdll.LoadLibrary(lib_path)
        liquid_func_declare(self.fluid)
        self.renderer = BattleRenderer(render_option, 'liquid', self.fluid, self.w, self.h, self.res, self.bg_obj, self.rigid_obj, self.sh_obj, self.bar_obj)
        self.reset()

        self.keymap = [{'w': 1, 'else': -1}, {'q': 1, 'e': -1}, {'d': 1, 'a': -1},
                {'i': 1, 'else': -1}, {'o': 1, 'u': -1}, {'j': 1, 'l': -1}]

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def destroy(self):
        self.fluid.destroyLiquid()

    def reset(self):
        self.destroy()
        self.fluid.createLiquidBottomBattle(self.w, self.h, self.resf, self.radsh, (ct.c_float*4)(1,1,1,1))
        self.fluid.addLiquidSolid(self.w/2, self.hbar/2, self.radbar/2, self.hbar/2, True)
        self.fluid.setRigidLiquid(self.pospp_init, self.h * 2 / 3, self.radpp, self.rhopp, GRAVITY)
        self.game_over  = False
        self.crossing   = False
        self.l_in       = False
        self.r_in       = False
        self.now_step   = 0
        self.possh1     = self.w / 6
        self.velsh1     = 0.0
        self.theta1     = 90.0
        self.possh2     = self.w / 6
        self.velsh2     = 0.0
        self.theta2     = 90.0
        self.mag_loaded1 = self.mag_size
        self.mag_loaded2 = self.mag_size
        # field size
        nrPoint = (ct.c_int * 2)() # w, h
        self.fluid.getFeatureLiquidSimple(ct.c_float(-1), ct.POINTER(ct.c_float)(), ct.byref(nrPoint))
        self.field_size = [nrPoint[1], nrPoint[0], 2] # input: h, w
        new_step = self.step(self.action_space.sample())
        return new_step[0], new_step[1]

    def sample_speed(self):
        nrPoint = (ct.c_int * 2)()
        self.fluid.getFeatureLiquidSimple(ct.c_float(-1), ct.POINTER(ct.c_float)(), ct.byref(nrPoint))
        rho = (ct.c_float * (nrPoint[0] * nrPoint[1] * 2))()
        self.fluid.getFeatureLiquidSimple(ct.c_float(-1), ct.byref(rho), ct.byref(nrPoint))
        speed = [rho[i] for i in range(nrPoint[0] * nrPoint[1] * 2)]
        return preprocess(speed, nrPoint[0], nrPoint[1])

    def step(self, action):
        # ensure its a valid action
        low = self.action_space.low
        high = self.action_space.high
        action1, action2 = action[0], action[1]
        action1 = np.clip(action1, low, high)
        action2 = np.clip(action2, low, high)
        assert self.action_space.contains(action1), "%r (%s) invalid " % (action1, type(action1))
        assert self.action_space.contains(action2), "%r (%s) invalid " % (action2, type(action2))
        # record the steps
        self.now_step += 1
        # shoot_A = False
        # shoot_B = False
        if self.debug is True:  # in case u wanna c the training process visually
            self.render()
        # Action/Engine
        # action_MOVE     = action // 4
        # action_SHOOT    = action % 4
        bb_speed   = 1
        bb_volume1  = action1[0]
        sh_omega1   = action1[1]
        sh_force1   = action1[2]
        shoot1      = False
        last_possh1 = self.possh1
        bb_volume2  = action2[0]
        sh_omega2   = action2[1]
        sh_force2   = action2[2]
        shoot2      = False
        last_possh2 = self.possh2
        # Pingchuan: update velocity of shooter
        self.velsh1 += sh_force1 * self.fsh / FPS
        self.possh1 += self.velsh1 / FPS
        self.theta1 += sh_omega1 * self.max_omega / FPS
        self.velsh2 += sh_force2 * self.fsh / FPS
        self.possh2 += self.velsh2 / FPS
        self.theta2 += sh_omega2 * self.max_omega / FPS

        pospp_x = (ct.c_float)()
        pospp_y = (ct.c_float)()
        self.fluid.getRigidPosLiquid(pospp_x, pospp_y)
        velpp_x = (ct.c_float)()
        velpp_y = (ct.c_float)()
        self.fluid.getRigidVelLiquid(velpp_x, velpp_y)

        # print("mag1: %f, mag2: %f" % (self.mag_loaded1, self.mag_loaded2))

        # print(self.theta)
        if np.random.rand() * 2 - 1 < bb_volume1:
            if pospp_x.value < self.w / 2:
                # if self.mag_loaded1 - self.mag_jet_vel > 0:
                shoot1 = True
                    # self.mag_loaded1 -= self.mag_jet_vel
        # else:
        #     self.mag_loaded1 = np.minimum(self.mag_size, self.mag_loaded1 + self.mag_charge_vel)
        if np.random.rand() * 2 - 1 < bb_volume2:
            if pospp_x.value > self.w / 2:
                # if self.mag_loaded2 - self.mag_jet_vel > 0:
                shoot2 = True
                    # self.mag_loaded2 -= self.mag_jet_vel
        # else:
        #     self.mag_loaded2 = np.minimum(self.mag_size, self.mag_loaded2 + self.mag_charge_vel)
        possh1_c    = (ct.c_float)(self.possh1)
        theta1_c    = (ct.c_float)(self.theta1)
        possh2_c    = (ct.c_float)(self.w - self.possh2)
        theta2_c    = (ct.c_float)(180.0-self.theta2)
        bb_speed_c = (ct.c_float)(self.fbb * bb_speed)
        mesh_v_size_c = (ct.c_int)(self.mesh_v_size)
        mesh_i_size_c = (ct.c_int)(self.mesh_i_size)
        contact_c = (ct.c_bool)()
        self.fluid.transferLiquidBottomBattle(1 / FPS, shoot1, shoot2, theta1_c, theta2_c, \
            possh1_c, possh2_c, bb_speed_c, mesh_v_size_c, mesh_i_size_c, self.radsh, contact_c)
        self.possh1 = possh1_c.value
        self.theta1 = theta1_c.value
        self.velsh1 = (self.possh1 - last_possh1) * FPS
        self.possh2 = self.w - possh2_c.value
        self.theta2 = 180.0-theta2_c.value
        self.velsh2 = (self.possh2 - last_possh2) * FPS

        self.mesh_v_size = mesh_v_size_c.value
        self.mesh_i_size = mesh_i_size_c.value

        # State
        state1 = np.array([pospp_x.value, pospp_y.value, velpp_x.value, velpp_y.value, self.possh1, self.velsh1, self.theta1])
        state2 = np.array([self.w - pospp_x.value, pospp_y.value, -velpp_x.value, velpp_y.value, self.possh2, self.velsh2, self.theta2])
        assert self.observation_space.contains(state1), "%r (%s) invalid " % (state1, type(state1))
        assert self.observation_space.contains(state2), "%r (%s) invalid " % (state2, type(state2))
        
        speed = self.sample_speed()

        def win1():
            self.pospp_init = self.w - init_dist
            return self.win_reward, -1

        def win2():
            self.pospp_init = init_dist
            return -1, self.win_reward

        def lose():
            self.pospp_init = self.w - self.pospp_init
            return -1, -1

        def restart():
            self.pospp_init = self.w - self.pospp_init
            return 0, 0

        # Reward
        reward1 = 0
        reward2 = 0 

        eds = 3   # Edge detect sensitivity
        init_dist = self.w / 4

        # Time out
        if self.now_step > self.max_step:
            self.game_over = True
            reward1, reward2 = restart()

        if contact_c.value:

            self.game_over = True

            # Ceiling
            if self.h - self.radpp * eds - pospp_y.value < 2 / self.resf:
                reward1, reward2 = lose()

            # Floor
            elif pospp_y.value - self.radpp * eds < 2 / self.resf:
                if pospp_x.value < self.w / 2:
                    reward1, reward2 = win2()
                else:
                    reward1, reward2 = win1()

            # Left edge
            elif pospp_x.value - self.radpp * eds < 2 / self.resf:
                reward1, reward2 = win2()

            # Right edge
            elif self.w - self.radpp * eds - pospp_x.value < 2 / self.resf:
                reward1, reward2 = win1()

            # Net
            else:
                if pospp_x.value < self.w / 2:
                    reward1, reward2 = win2()
                else:
                    reward1, reward2 = win1()

        else:
            # if pospp_y.value > self.hbar and self.w / 2 - 2 * self.radpp < pospp_x.value < self.w / 2 + 2 * self.radpp:
            #     if not self.crossing:
            #         self.crossing = True
            #         if velpp_x.value > 0:
            #             reward1 = self.cross_reward
            #         else:
            #             reward2 = self.cross_reward
            # else:
            #     self.crossing = False
            if self.w / 2 - self.crossing_width * self.radpp < pospp_x.value <= self.w / 2:
                if not self.crossing:
                    self.crossing = True
                    if velpp_x.value > 0.0:
                        self.l_in = True
            elif self.w / 2 < pospp_x.value <= self.w / 2 + self.crossing_width * self.radpp:
                if not self.crossing:
                    self.crossing = True
                    if velpp_x.value < 0.0:
                        self.r_in = True
            elif pospp_x.value <= self.w / 2 - self.crossing_width * self.radpp:
                if self.crossing and self.r_in:
                    reward1 += self.cross_reward
                    reward2 += self.cross_reward
                self.r_in = False
                self.l_in = False
                self.crossing = False
            else:
                if self.crossing and self.l_in:
                    reward1 += self.cross_reward
                    reward2 += self.cross_reward
                self.r_in = False
                self.l_in = False
                self.crossing = False

        # print(reward1, reward2)
        if not self.game_over:
            if shoot1:
                reward1 -= self.shoot_punish
            if shoot2:
                reward2 -= self.shoot_punish

        # print(reward1, reward2)

        # Return
        return state1, state2, reward1, reward2, self.game_over, {'speed': speed}

    def render(self, mode='human', close=False):
        self.viewers, result = self.renderer.render(self.viewers, mode, close, 
            [(self.possh1, self.radsh / 2), (self.w - self.possh2, self.radsh / 2)], 
            [self.theta1, 180 - self.theta2],
            self.mesh_v_size, self.mesh_i_size)
        return result


if __name__ == "__main__":
    env = LiquidPingPongBottomBattle()
    runner = GameDoubleRunner(env)
    runner.run('h')
