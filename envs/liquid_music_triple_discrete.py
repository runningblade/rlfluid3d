import math
import numpy as np
import ctypes as ct
import os

from pynput.keyboard import Listener
from env_utils import *
from music_params import *

import gym
from gym import spaces
from gym.utils import seeding
from gym.utils import reraise
from fluidRender import *
from recorder import GameSingleRunner

import sys
sys.path.append('..')
import time
import json

FPS = 50


class LiquidMusicTripleDiscrete(gym.Env):
    # implementation
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': FPS
    }

    def __init__(self, w=5, h=3, radpp=0.15, radsh=0.12,  # geometry
                 fsh=FSH, fbb=FBB, frt=FRT, rhopp=RHOPP,  # physics
                 nkey=7, max_step=1000, timeout=TIMEOUT, # reward weights
                 resf=32, res=100,  # render resolution
                 music=None, recording=None, fshratio=FSHRATIO, gratio=GRATIO, bouncevel=BOUNCEVEL,
                 debug=False, render_option='p', **kwargs):  # render when training
        # the rest
        self.seed()
        self.viewers  = []
        self.now_step = 0
        self.max_step = max_step
        self.res      = res
        self.debug    = debug
        # geometry
        self.w     = w
        self.h     = h
        self.radpp = radpp
        self.radsh = radsh
        self.resf  = resf
        self.nkey  = nkey
        self.hkey  = h / 8
        # rendering
        self.bg_obj = Background(self.w, self.h, '../envs/texs/grey.jpg', (0, 0, 0))
        self.rigid_obj = Ball(self.radpp * 2, self.radpp * 2, '../envs/texs/yellowball.png')
        self.sh_obj = Ball(self.radsh * 2, self.radsh * 2, '../envs/texs/pipe.png', (0.5, 0.4, 0.9))
        # physics
        self.possh_b    = w / 2
        self.possh_l    = h / 2
        self.possh_r    = h / 2
        self.velsh_b    = 0.0
        self.velsh_l    = 0.0
        self.velsh_r    = 0.0
        self.theta_b    = 90.0
        self.theta_l    = 0.0
        self.theta_r    = 180.0
        self.fsh        = fsh
        self.fbb        = fbb
        self.rhopp      = rhopp
        self.frt        = frt
        self.fshratio   = fshratio
        self.gratio     = gratio
        self.bouncevel  = bouncevel
        self.mesh_v_size = 0
        self.mesh_i_size = 0
        self.mesh_v = []
        self.mesh_i = []
        # reward
        self.last_hit_time = time.time()
        # target
        self.target = 0 # [0, nkey-1]
        self.next_target = np.random.randint(self.nkey)
        self.time = 1
        self.next_time = TBASE * 2 ** np.random.randint(1)
        self.last_hit = 0
        self.hit_right = True
        self.timeout = timeout
        # music
        self.music = None
        if music:
            with open(music, 'r') as f:
                self.music = json.load(f)
            self.recording = recording
            self.note_len = len(self.music)
            self.note_id = self.note_len - 1
            self.note_time = 0
            if self.recording is not None:
                os.makedirs('../results/saved_recording/jsons/', exist_ok=True)
                with open(self.recording, 'w') as f:
                    f.write('[')
            self.next_target, self.next_time = self.music[self.note_id][0], self.music[(self.note_id - 1) % self.note_len][1]
        # state
        high = np.array([np.inf] * 19)
        self.observation_space = spaces.Box(-high, high, dtype=np.float32)
        # action
        # mov_left, no_mov, mov_right = -1, 0, 1
        # rot_left, no_rot, rot_right = -1, 0, 1
        self.action_space = spaces.MultiDiscrete([3, 3, 3, 3, 3, 3, 2, 2, 2])
        # c interface
        lib_path = os.path.join(os.path.split(os.path.realpath(__file__))[0], "FluidOpt-build/libFluid.so")
        self.fluid = ct.cdll.LoadLibrary(lib_path)
        liquid_func_declare(self.fluid)
        self.renderer = MusicRenderer(render_option, 'liquid', self.fluid, self.w, self.h, self.res, self.bg_obj, self.rigid_obj, self.sh_obj, self.nkey, self.hkey)
        self.reset()

        self.keymap = [{'f': -1, 'h': 1}, {'w': -1, 's': 1}, {'i': -1, 'k': 1}, 
                {'r': -1, 'y': 1}, {'q': -1, 'e': 1}, {'u': -1, 'o': 1},
                {'t': 1}, {'d': 1}, {'j': 1}]

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def destroy(self):
        self.fluid.destroyLiquid()

    def reset(self):
        self.destroy()
        self.fluid.createLiquidTriple(self.w, self.h, self.resf, self.radsh, self.fbb, (ct.c_float*4)(1,1,1,1))
        for i in range(self.nkey):
            self.fluid.addLiquidSolid((i + 0.5) * self.w / self.nkey,
                self.h - self.hkey / 2, 0.5 * self.w / self.nkey, self.hkey / 2, False)
        self.fluid.setRigidLiquid(self.w / 2, self.h / 2, self.radpp, self.rhopp, self.gratio * GRAVITY)
        self.possh_b    = self.w / 2
        self.possh_l    = self.h / 2
        self.possh_r    = self.h / 2
        self.velsh_b    = 0.0
        self.velsh_l    = 0.0
        self.velsh_r    = 0.0
        self.theta_b    = 90.0
        self.theta_l    = 0.0
        self.theta_r    = 180.0
        self.now_step   = 0
        self.hit_right  = True
        if self.music is None:
            self.target = self.next_target
            self.next_target = np.random.randint(self.nkey)
            self.time = self.next_time
            self.next_time = TBASE * 2 ** np.random.randint(1)
        else:
            self.target, self.time = self.next_target, self.next_time
            self.next_target, self.next_time = self.music[(self.note_id + 1) % self.note_len][0], self.music[self.note_id][1]
            self.note_id = (self.note_id + 1) % self.note_len
            self.note_time = 0
        self.game_over = False
        # field size
        nrPoint = (ct.c_int * 2)() # w, h
        self.fluid.getFeatureLiquidSimple(ct.c_float(-1), ct.POINTER(ct.c_float)(), ct.byref(nrPoint))
        self.field_size = [nrPoint[1], nrPoint[0], 2] # input: h, w
        return self.step(self.action_space.sample())[0]

    def sample_speed(self):
        nrPoint = (ct.c_int * 2)()
        self.fluid.getFeatureLiquidSimple(ct.c_float(-1), ct.POINTER(ct.c_float)(), ct.byref(nrPoint))
        rho = (ct.c_float * (nrPoint[0] * nrPoint[1] * 2))()
        self.fluid.getFeatureLiquidSimple(ct.c_float(-1), ct.byref(rho), ct.byref(nrPoint))
        speed = [rho[i] for i in range(nrPoint[0] * nrPoint[1] * 2)]
        return preprocess(speed, nrPoint[0], nrPoint[1])

    def step(self, action):

        self.now_step += 1
        # ensure its a valid action
        assert self.action_space.contains(action), "%r (%s) invalid " % (action, type(action))

        shoot_b = False
        shoot_l = False
        shoot_r = False

        if self.debug is True:  # in case u wanna c the training process visually
            self.render()

        pospp_x = (ct.c_float)()
        pospp_y = (ct.c_float)()
        self.fluid.getRigidPosLiquid(pospp_x, pospp_y)

        # Action/Engine
        action_move_b = action[0] - 1
        action_move_l = action[1] - 1
        action_move_r = action[2] - 1

        action_rotate_b = action[3] - 1
        action_rotate_l = action[4] - 1
        action_rotate_r = action[5] - 1

        action_shoot_b = action[6]
        action_shoot_l = action[7]
        action_shoot_r = action[8]

        self.velsh_b += action_move_b * self.fsh / FPS
        self.velsh_l -= action_move_l * self.fshratio * self.fsh / FPS
        self.velsh_r -= action_move_r * self.fshratio * self.fsh / FPS

        self.theta_b -= action_rotate_b * self.frt / FPS
        self.theta_l -= action_rotate_l * self.frt / FPS
        self.theta_r -= action_rotate_r * self.frt / FPS

        shoot_b = int(action_shoot_b)
        shoot_l = int(action_shoot_l and pospp_x.value < 0.5 * self.w)
        shoot_r = int(action_shoot_r and pospp_x.value > 0.5 * self.w)

        last_possh_b = self.possh_b
        last_possh_l = self.possh_l
        last_possh_r = self.possh_r

        self.possh_b += self.velsh_b / FPS
        self.possh_l += self.velsh_l / FPS
        self.possh_r += self.velsh_r / FPS

        shoot = (ct.c_bool*3)(shoot_l, shoot_r, shoot_b)
        possh = (ct.c_float*3)(self.possh_l, self.possh_r, self.possh_b)
        theta = (ct.c_float*3)(self.theta_l, self.theta_r, self.theta_b)

        mesh_v_size_c = (ct.c_int)(self.mesh_v_size)
        mesh_i_size_c = (ct.c_int)(self.mesh_i_size)

        contact_c = (ct.c_bool)()

        self.fluid.transferLiquidTriple(1 / FPS, shoot, possh, theta, self.hkey, self.radsh, self.fbb, mesh_v_size_c, mesh_i_size_c, contact_c)

        self.possh_l, self.possh_r, self.possh_b = list(possh)
        self.theta_l, self.theta_r, self.theta_b = list(theta)

        self.velsh_b = (self.possh_b - last_possh_b) * FPS
        self.velsh_l = (self.possh_l - last_possh_l) * FPS
        self.velsh_r = (self.possh_r - last_possh_r) * FPS

        self.time -= 1 / FPS
        self.time = round(self.time, 2)
        if self.music:
            self.note_time += 1 / FPS
            self.note_time = round(self.note_time, 2)

        self.mesh_v_size = mesh_v_size_c.value
        self.mesh_i_size = mesh_i_size_c.value

        # State
        velpp_x = (ct.c_float)()
        velpp_y = (ct.c_float)()
        self.fluid.getRigidVelLiquid(velpp_x, velpp_y)

        forcepp = (ct.c_float * 2)()
        self.fluid.getRigidForceLiquid(forcepp)
        forcepp_x, forcepp_y = forcepp[0] / 5, forcepp[1] / 5
        
        state = np.array([self.target, self.next_target, self.time, self.next_time,
            pospp_x.value, pospp_y.value, velpp_x.value, velpp_y.value, forcepp_x, forcepp_y, 
            self.possh_b, self.possh_l, self.possh_r,
            self.velsh_b, self.velsh_l, self.possh_r,
            self.theta_b, self.theta_l, self.theta_r])

        assert self.observation_space.contains(state), "%r (%s) invalid " % (state, type(state))
        
        speed = self.sample_speed()

        # Reward
        reward = 0
        eds = 2

        if self.now_step > self.max_step:
            self.game_over = True

        if self.time < self.timeout:
            reward = music_calc_reward(True)
            self.game_over = True
            
        if contact_c.value:

            # Floor
            if pospp_y.value < eds * self.radpp:
                reward = music_calc_reward(True)
                self.game_over = True

            # Key
            if pospp_y.value > self.h - self.hkey - eds * self.radpp and time.time() - self.last_hit_time > 0.2:
                key_touched = np.floor(pospp_x.value * self.nkey / self.w)

                reward = music_calc_reward(False, key_touched, self.target, self.time)

                bounce_back_vel = (ct.c_float)(self.bouncevel)
                self.fluid.setRigidVelLiquid(velpp_x, bounce_back_vel)
                self.last_hit_time = time.time()

                self.last_hit = key_touched
                self.hit_right = (key_touched == self.target)

                if self.music is None:
                    self.target = self.next_target
                    self.next_target = np.random.randint(self.nkey)
                    self.time = self.next_time
                    self.next_time = TBASE * 2 ** np.random.randint(1)
                else:
                    if self.recording is not None:
                        with open(self.recording, 'a') as f:
                            json.dump(music_note(key_touched, self.note_time), f)
                            f.write(',')
                    self.target, self.time = self.next_target, self.next_time
                    self.next_target, self.next_time = self.music[(self.note_id + 1) % self.note_len][0], self.music[self.note_id][1]
                    self.note_id = (self.note_id + 1) % self.note_len
                    self.note_time = 0

        # Return
        return state, reward, self.game_over, {"speed": speed}

    def render(self, mode='human', close=False):
        self.viewers, result = self.renderer.render(self.viewers, mode, close, 
            [(self.possh_b, self.radsh / 2), (self.radsh / 2, self.possh_l), (self.w - self.radsh / 2, self.possh_r)], 
            [self.theta_b, self.theta_l, self.theta_r],
            self.target, self.hit_right, self.last_hit, self.time, self.mesh_v_size, self.mesh_i_size)
        return result


if __name__ == "__main__":
    env = LiquidMusicTripleDiscrete()
    runner = GameSingleRunner(env)
    runner.run('h')
