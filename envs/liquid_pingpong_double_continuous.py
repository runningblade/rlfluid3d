import math
import numpy as np
import ctypes as ct
import os

from pynput.keyboard import Listener
from env_utils import *

import gym
from gym import spaces
from gym.utils import seeding
from gym.utils import reraise
from gym.envs.classic_control.rendering import Geom, _add_attrs

try:
    import pyglet
except ImportError as e:
    reraise(
        suffix="HINT: you can install pyglet directly via 'pip install pyglet'. But if you really just want "
               "to install all Gym dependencies and not have to think about it, 'pip install -e .[all]' or "
               "'pip install gym[all]' will do it.")

try:
    from pyglet.gl import *
except ImportError as e:
    reraise(prefix="Error occured while running `from pyglet.gl import *`",
            suffix="HINT: make sure you have OpenGL install. On Ubuntu, you can run 'apt-get install "
                   "python-opengl'. If you're running on a server, you may need a virtual frame buffer; "
                   "something like this should work: 'xvfb-run -s \"-screen 0 1400x900x24\" "
                   "python <your_script.py>'")


class Points(Geom):
    def __init__(self, cache):
        Geom.__init__(self)
        self.cache = cache

    def render1(self):
        glPointSize(2)
        glEnable(GL_POINT_SMOOTH)
        glBegin(GL_POINTS)
        for i in range(len(self.cache) // 2):
            glVertex2f(self.cache[i * 2], self.cache[i * 2 + 1])
        glEnd()


FPS = 50


class LiquidPingPongDoubleContinuous(gym.Env):
    # implementation
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': FPS
    }

    def __init__(self, w=5, h=3, radpp=0.15, radsh=0.12,  # geometry
                 fsh=30, fbb=20, rhopp=1500,  # physics	# Yunsheng: fsh(10->30), fbb(10->30), max_omega(100->250), rhopp(1500->1200)
                 wp=100, wv=10, wsh=0.5, wl=1, max_step=1000,  # reward weights
                 resf=32, res=100,  # render resolution
                 debug=False):  # render when training
        # the rest
        self._seed()
        self.viewer   = None
        self.now_step = 0
        self.max_step = max_step
        self.res      = res
        self.debug    = debug
        # geometry
        self.w     = w
        self.h     = h
        self.radpp = radpp
        self.radsh = radsh
        self.resf  = resf
        # physics
        self.possh     = w / 2
        self.velsh     = 0.0
        self.fsh       = fsh
        self.fbb       = fbb
        self.rhopp     = rhopp
        # reward
        self.wp    = wp
        self.wv    = wv
        self.wsh   = wsh
        self.wl    = wl
        # background
        self.background = [(0, 0), (self.w, 0), (self.w, self.h), (0, self.h)]
        # state: pp x, pp y, pp velx, pp vely,
        # state: sh x, sh a, sh velx, shooter vela
        high = np.array([np.inf] * 6)
        self.observation_space = spaces.Box(-high, high)
        # action: nop, shoot, shoot left, shoot right, move left, move right
        self.bb1_volume_range = np.array([-1, 1])  # Pingchuan: volume of liquid
        self.bb2_volume_range = np.array([-1, 1])
        self.sh_force_range   = np.array([-1, 1])  # Pingchuan: moving force of shooter
        action_range = np.array([self.bb1_volume_range, self.bb2_volume_range, self.sh_force_range])
        action_range = action_range.T
        self.action_space = spaces.Box(action_range[0, :], action_range[1, :])
        # c interface
        lib_path = os.path.join(os.path.split(os.path.realpath(__file__))[0], "FluidOpt-build/libFluid.so")
        self.fluid = ct.cdll.LoadLibrary(lib_path)
        liquid_func_declare(self.fluid)
        self._reset()

    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _destroy(self):
        self.fluid.destroyLiquid()

    def _reset(self):
        self._destroy()
        self.fluid.createLiquid(self.w, self.h, self.resf, self.radsh, (ct.c_float*4)(1,1,1,1))
        self.fluid.setRigidLiquid(self.w / 2, self.h / 2, self.radpp, self.rhopp, GRAVITY)
        self.game_over  = False
        self.now_step   = 0
        self.possh      = self.w / 2
        self.velsh      = 0.0
        # field size
        nrPoint = (ct.c_int * 2)() # w, h
        self.fluid.getFeatureLiquidSimple(ct.c_float(-1), ct.POINTER(ct.c_float)(), ct.byref(nrPoint))
        self.field_size = [nrPoint[1], nrPoint[0], 2] # input: h, w
        return self._step(self.action_space.sample())[0]

    def sample_speed(self):
        nrPoint = (ct.c_int * 2)()
        self.fluid.getFeatureLiquidSimple(ct.c_float(-1), ct.POINTER(ct.c_float)(), ct.byref(nrPoint))
        rho = (ct.c_float * (nrPoint[0] * nrPoint[1] * 2))()
        self.fluid.getFeatureLiquidSimple(ct.c_float(-1), ct.byref(rho), ct.byref(nrPoint))
        speed = [rho[i] for i in range(nrPoint[0] * nrPoint[1] * 2)]
        return preprocess(speed, nrPoint[0], nrPoint[1])

    def _step(self, action):
        # ensure its a valid action
        action = np.clip(action, self.action_space.low, self.action_space.high)
        assert self.action_space.contains(action), "%r (%s) invalid " % (action, type(action))
        # record the steps
        self.now_step += 1
        shoot_A = False
        shoot_B = False
        if self.debug is True:  # in case u wanna c the training process visually
            self.render()
        # Action/Engine
        bb1_volume = action[0]
        bb2_volume = action[1]
        sh_force   = action[2]
        last_possh = self.possh
        # Pingchuan: update velocity of shooter
        self.velsh += self.fsh * (sh_force - 0.5) * 2 / FPS
        self.velsh = np.clip(self.velsh, -3, 3 )     # Yunsheng: velsh clip
        self.possh += self.velsh / FPS
        # print(self.theta)
        if np.random.rand(1) < bb1_volume:
            shoot_A = True
        if np.random.rand(1) < bb2_volume:
            shoot_B = True
        possh_c = (ct.c_float)(self.possh)
        self.fluid.transferLiquid(1 / FPS, shoot_A, shoot_B, possh_c, self.w/11, self.fbb, (ct.c_bool)())
        self.possh = possh_c.value
        self.velsh = (self.possh - last_possh) * FPS

        # State
        pospp_x = (ct.c_float)()
        pospp_y = (ct.c_float)()
        self.fluid.getRigidPosLiquid(pospp_x, pospp_y)
        velpp_x = (ct.c_float)()
        velpp_y = (ct.c_float)()
        self.fluid.getRigidVelLiquid(velpp_x, velpp_y)
        state = np.array([pospp_x.value, pospp_y.value, velpp_x.value, velpp_y.value, self.possh, self.velsh])
        assert self.observation_space.contains(state), "%r (%s) invalid " % (state, type(state))

        speed = self.sample_speed()

        # Reward
        reward = -self.wp * np.sqrt((state[0] - self.w / 2) * (state[0] - self.w / 2) +
                                    (state[1] - self.h / 2) * (state[1] - self.h / 2)) - \
                  self.wv * np.sqrt(state[2] * state[2] +
                                    state[3] * state[3]) + \
                  self.wl

        if shoot_A is True:
            reward = reward - self.wsh
        if shoot_B is True:
            reward = reward - self.wsh

        # Yunsheng: stopped_here

        if state[1] - self.radpp < 2 / self.resf or \
           state[0] - self.radpp < 2 / self.resf or \
           self.h - self.radpp - state[1] < 2 / self.resf or \
           self.w - self.radpp - state[0] < 2 / self.resf:
            self.game_over = True
            # pass

        if self.now_step > self.max_step:
            self.game_over = True

        # Return
        return state, reward, self.game_over, {'speed': speed}

    def _render(self, mode='human', close=False):
        if close:
            if self.viewer is not None:
                self.viewer.close()
                self.viewer = None
            return
        # renderer setup
        from gym.envs.classic_control import rendering
        if self.viewer is None:
            self.viewer = rendering.Viewer(self.w * self.res, self.h * self.res)
            self.viewer.set_bounds(0, self.w, 0, self.h)
        # background
        self.viewer.draw_polygon(self.background, color=(0, 0, 0))
        # liquid particles
        nr_particle_c = (ct.c_int)()
        null_ptr = ct.POINTER(ct.c_float)()
        self.fluid.getParticlesLiquid(null_ptr, nr_particle_c)
        particle_cache_c = (ct.c_float * (nr_particle_c.value * 2))()
        self.fluid.getParticlesLiquid(particle_cache_c, nr_particle_c)
        pts = Points(particle_cache_c)
        _add_attrs(pts, {'color': (0.5, 0.9, 0.4)})
        self.viewer.add_onetime(pts)
        # for i in range(nr_particle_c.value):
        #    t = rendering.Transform(translation = (particle_cache_c[i*2+0], particle_cache_c[i*2+1]))
        #    self.viewer.draw_circle(0.01, 20, color=(0.5, 0.9, 0.4)).add_attr(t)
        # pingpong
        pospp_x = (ct.c_float)()
        pospp_y = (ct.c_float)()
        self.fluid.getRigidPosLiquid(pospp_x, pospp_y)
        t = rendering.Transform(translation=(pospp_x.value, pospp_y.value))
        self.viewer.draw_circle(self.radpp, 20, color=(1, 1, 1)).add_attr(t)
        self.viewer.draw_circle(self.radpp, 20, color=(0.9, 0.9, 0.9), filled=False, linewidth=2).add_attr(t)
        # shooterA
        t = rendering.Transform(translation=(self.possh - self.w/11/2, self.radsh))
        self.viewer.draw_circle(self.radsh, 20, color=(0.5, 0.4, 0.9)).add_attr(t)
        self.viewer.draw_circle(self.radsh, 20, color=(0.3, 0.3, 0.5), filled=False, linewidth=2).add_attr(t)
        # shooterB
        t = rendering.Transform(translation=(self.possh + self.w/11/2, self.radsh))
        self.viewer.draw_circle(self.radsh, 20, color=(0.5, 0.4, 0.9)).add_attr(t)
        self.viewer.draw_circle(self.radsh, 20, color=(0.3, 0.3, 0.5), filled=False, linewidth=2).add_attr(t)
        # return
        return self.viewer.render(return_rgb_array=mode == 'rgb_array')

    def run_game(self):
        Listener(
            on_press=on_press,
            on_release=on_release).start()
        s = self.reset()
        total_reward = 0
        steps = 0
        bb1_volume = 0
        bb2_volume = 0
        sh_force = 0.5
        while True:

            if keys['left'] is True:
                sh_force = 0
            elif keys['right'] is True:
                sh_force = 1
            else:
                sh_force = 0.5

            if keys['1'] is True:
                bb1_volume = 1
            else:
                bb1_volume = 0

            if keys['2'] is True:
                bb2_volume = 1
            else:
                bb2_volume = 0

            action = np.array([bb1_volume, bb2_volume, sh_force])

            s, r, done, info = self.step(action)
            self.render()
            total_reward += r
            if done:
                print(["{:+0.2f}".format(x) for x in s])
                print("step {} total_reward {:+0.2f}".format(steps, total_reward))
            steps += 1
            if done:
                self.reset()
                total_reward = 0
                steps = 0
                bb1_volume = 0
                bb2_volume = 0
                sh_force = 0.5


if __name__ == "__main__":
    env = LiquidPingPongDoubleContinuous()
    env.run_game()
