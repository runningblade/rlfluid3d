import math
import numpy as np
import ctypes as ct
import os

from pynput.keyboard import Listener
from env_utils import *
from music_params import *

import gym
from gym import spaces
from gym.utils import seeding
from gym.utils import reraise
from fluidRender import *
from recorder import GameSingleRunner

import sys
sys.path.append('..')
import time
import json

FPS = 50


class LiquidMusicSingleDiscrete(gym.Env):
    # implementation
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': FPS
    }

    def __init__(self, w=5, h=3, radpp=0.15, radsh=0.12,  # geometry
                 # fsh=60, fbb=25, frt=600, rhopp=1500,  # physics
                 fsh=50, fbb=15, frt=600, rhopp=1200,
                 nkey=7, max_step=1000, timeout=-3, # reward weights
                 resf=32, res=150,  # render resolution
                 music=None, recording=None,
                 warp=True, max_speed=10, bouncelvel=BOUNCEVEL,
                 debug=False, render_option='p', **kwargs):  # render when training
        # the rest
        self.seed()
        self.viewers  = []
        self.now_step = 0
        self.max_step = max_step
        self.step_cnt = 0
        self.res      = res
        self.debug    = debug
        # geometry
        self.w     = w
        self.h     = h
        self.radpp = radpp
        self.radsh = radsh
        self.resf  = resf
        self.nkey  = nkey
        self.hkey  = h / 8
        # rendering
        self.bg_obj = Background(self.w, self.h, '../envs/texs/grey.jpg', (0, 0, 0))
        self.rigid_obj = Ball(self.radpp * 2, self.radpp * 2, '../envs/texs/yellowball.png')
        self.sh_obj = Ball(self.radsh * 2, self.radsh * 2, '../envs/texs/pipe.png', (0.5, 0.4, 0.9))
        # physics
        self.possh     = w / 2
        self.velsh     = 0.0
        self.theta     = 90.0
        self.fsh       = fsh
        self.fbb       = fbb
        self.rhopp     = rhopp
        self.frt       = frt
        self.mesh_v_size = 0
        self.mesh_i_size = 0
        self.mesh_v = []
        self.mesh_i = []
        # reward
        self.last_hit_time = time.time()
        # target
        self.target = 0
        self.next_target = np.random.randint(self.nkey)
        self.time = 1
        self.next_time = TBASE * 2 ** np.random.randint(3)
        self.last_hit = 0
        self.hit_right = True
        self.timeout = timeout
        self.warp = warp
        self.max_speed = max_speed
        # music
        self.key_map = {0: 'Do', 1: 'Re', 2: 'Mi', 3: 'Fa', 4: 'Sol', 5: 'La', 6: 'Ti'}
        self.music = None
        if music:
            with open(music, 'r') as f:
                self.music = json.load(f)
            self.recording = recording
            self.note_len = len(self.music)
            self.note_id = self.note_len - 1
            self.note_time = 0
            if self.recording is not None:
                os.makedirs('../results/saved_recording/jsons/', exist_ok=True)
                with open(self.recording, 'w') as f:
                    f.write('[')
            self.next_target, self.next_time = self.music[self.note_id][0], self.music[(self.note_id - 1) % self.note_len][1]
        # state: target, time, pp x, pp y, pp velx, pp vely,
        # state: sh x, sh a, sh velx, shooter vela
        high = np.array([np.inf] * 13)
        self.observation_space = spaces.Box(-high, high, dtype=np.float32)
        # action: nop, shoot, rotate left, rotate right, move left, move right
        self.action_space = spaces.MultiDiscrete([3, 3, 2])
        # c interface
        lib_path = os.path.join(os.path.split(os.path.realpath(__file__))[0], "FluidOpt-build/libFluid.so")
        self.fluid = ct.cdll.LoadLibrary(lib_path)
        liquid_func_declare(self.fluid)
        self.renderer = MusicRenderer(render_option, 'liquid', self.fluid, self.w, self.h, self.res, self.bg_obj, self.rigid_obj, self.sh_obj, self.nkey, self.hkey)
        self.reset()

        self.keymap = [{'a': -1, 'd': 1}, {'left': -1, 'right': 1}, {'w': 1}]

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def destroy(self):
        self.fluid.destroyLiquid()

    def reset(self):
        self.destroy()
        self.fluid.createLiquidSingle(self.w, self.h, self.resf, self.radsh, (ct.c_float*4)(1,1,1,1), self.warp)
        for i in range(self.nkey):
            self.fluid.addLiquidSolid((i + 0.5) * self.w / self.nkey,
                self.h - self.hkey / 2, 0.5 * self.w / self.nkey, self.hkey / 2, False)
        self.fluid.setRigidLiquid(self.w / 2, self.h / 2, self.radpp, self.rhopp, GRAVITY)
        self.possh      = self.w / 2
        self.velsh      = 0.0
        self.theta      = 90.0
        self.now_step = 0
        self.hit_right = True
        self.game_over  = False
        if self.music is None:
            self.target = self.next_target
            self.next_target = np.random.randint(self.nkey)
            self.time = self.next_time
            self.next_time = TBASE * 2 ** np.random.randint(3)
        else:
            self.target, self.time = self.next_target, self.next_time
            self.next_target, self.next_time = self.music[(self.note_id + 1) % self.note_len][0], self.music[self.note_id][1]
            self.note_id = (self.note_id + 1) % self.note_len
            self.note_time = 0
        # field size
        nrPoint = (ct.c_int * 2)() # w, h
        self.fluid.getFeatureLiquidSimple(ct.c_float(-1), ct.POINTER(ct.c_float)(), ct.byref(nrPoint))
        self.field_size = [nrPoint[1], nrPoint[0], 2] # input: h, w
        return self.step(self.action_space.sample())[0]

    def sample_speed(self):
        nrPoint = (ct.c_int * 2)()
        self.fluid.getFeatureLiquidSimple(ct.c_float(-1), ct.POINTER(ct.c_float)(), ct.byref(nrPoint))
        rho = (ct.c_float * (nrPoint[0] * nrPoint[1] * 2))()
        self.fluid.getFeatureLiquidSimple(ct.c_float(-1), ct.byref(rho), ct.byref(nrPoint))
        speed = [rho[i] for i in range(nrPoint[0] * nrPoint[1] * 2)]
        return preprocess(speed, nrPoint[0], nrPoint[1])
  
    def step(self, action):
        self.now_step += 1
        self.step_cnt += 1
        # ensure its a valid action
        assert self.action_space.contains(action), "%r (%s) invalid " % (action, type(action))
        shoot = False
        if self.debug is True:  # in case u wanna c the training process visually
            self.render()

        # Action/Engine
        action_move = action[0] - 1
        action_rotate = action[1] - 1
        action_shoot = action[2]

        self.velsh += action_move * self.fsh / FPS
        self.velsh = max(min(self.velsh, self.max_speed), -self.max_speed)
        self.theta -= action_rotate * self.frt / FPS
        shoot = bool(action_shoot)

        last_possh = self.possh
        self.possh += self.velsh / FPS

        possh_c    = (ct.c_float)(self.possh)
        theta_c    = (ct.c_float)(self.theta)
        bb_speed_c = (ct.c_float)(self.fbb)

        mesh_v_size_c = (ct.c_int)(self.mesh_v_size)
        mesh_i_size_c = (ct.c_int)(self.mesh_i_size)

        contact_c = (ct.c_bool)()

        self.fluid.transferLiquidSingle(1 / FPS, shoot, theta_c, possh_c, bb_speed_c, mesh_v_size_c, mesh_i_size_c, self.warp, contact_c)
        
        self.possh = possh_c.value
        self.theta = theta_c.value
        pos_change = self.possh - last_possh
        if pos_change > self.w / 2:
            pos_change -= self.w
        if pos_change < -self.w / 2:
            pos_change += self.w
        self.velsh = pos_change * FPS

        self.time -= 1 / FPS
        self.time = round(self.time, 2)
        if self.music:
            self.note_time += 1 / FPS
            self.note_time = round(self.note_time, 2)

        self.mesh_v_size = mesh_v_size_c.value
        self.mesh_i_size = mesh_i_size_c.value

        # State
        pospp_x = (ct.c_float)()
        pospp_y = (ct.c_float)()
        self.fluid.getRigidPosLiquid(pospp_x, pospp_y)
        velpp_x = (ct.c_float)()
        velpp_y = (ct.c_float)()
        self.fluid.getRigidVelLiquid(velpp_x, velpp_y)
        forcepp = (ct.c_float * 2)()
        self.fluid.getRigidForceLiquid(forcepp)
        forcepp_x, forcepp_y = forcepp[0] / 5, forcepp[1] / 5
        state = np.array([self.target, self.next_target, self.time, self.next_time, pospp_x.value, pospp_y.value, velpp_x.value, velpp_y.value, forcepp_x, forcepp_y, self.possh, self.velsh, self.theta])
        assert self.observation_space.contains(state), "%r (%s) invalid " % (state, type(state))
        
        speed = self.sample_speed()

        # Reward
        reward = 0
        eds = 2

        if self.now_step > self.max_step:
            self.game_over = True

        if self.time < self.timeout:
            reward = music_calc_reward(True)
            self.game_over = True

        if contact_c.value:

            # Floor
            if pospp_y.value < eds * self.radpp:
                reward = music_calc_reward(True)
                self.game_over = True

            # Key
            if pospp_y.value > self.h - self.hkey - eds * self.radpp and time.time() - self.last_hit_time > 0.1: # hit key
                key_touched = np.floor(pospp_x.value * self.nkey / self.w)

                reward = music_calc_reward(False, key_touched, self.target, self.time)

                bounce_back_vel = (ct.c_float)(-4)
                self.fluid.setRigidVelLiquid(velpp_x, bounce_back_vel)
                self.last_hit_time = time.time()

                self.last_hit = key_touched
                self.hit_right = (key_touched == self.target)

                if self.music is None:
                    self.target = self.next_target
                    self.next_target = np.random.randint(self.nkey)
                    self.time = self.next_time
                    self.next_time = TBASE * 2 ** np.random.randint(3)
                else:
                    if self.recording is not None:
                        with open(self.recording, 'a') as f:
                            json.dump(music_note(key_touched, self.note_time), f)
                            f.write(',')
                    self.target, self.time = self.next_target, self.next_time
                    self.next_target, self.next_time = self.music[(self.note_id + 1) % self.note_len][0], self.music[self.note_id][1]
                    self.note_id = (self.note_id + 1) % self.note_len
                    self.note_time = 0

        # Return
        return state, reward, self.game_over, {"speed": speed}

    def render(self, mode='human', close=False):
        self.viewers, result = self.renderer.render(self.viewers, mode, close, 
            [(self.possh, self.radsh / 2)], 
            [self.theta],
            self.target, self.hit_right, self.last_hit, self.time, self.mesh_v_size, self.mesh_i_size)
        return result


if __name__ == "__main__":
    env = LiquidMusicSingleDiscrete()
    runner = GameSingleRunner(env)
    runner.run('h')
