from pyglet.gl import *
from pyglet.window import key
from .camera import *


class Viewer3D(pyglet.window.Window):

    def __init__(self, camera_pos, world_up, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.keys = key.KeyStateHandler()
        self.push_handlers(self.keys)
        self.camera = Camera(pos=camera_pos, world_up=world_up)
        # self.shadows = True
        glClearColor(1.0, 1.0, 1.0, 1.0)
        # glEnable(GL_CULL_FACE)
        # glCullFace(GL_BACK)

    def on_draw(self):
        self.clear()
        self.switch_to()
        self.dispatch_events()

    def on_resize(self, width, height):
        glViewport(0, 0, width, height)

    def on_self_resize(self):
        glViewport(0, 0, self.width, self.height)

    def on_mouse_press(self, x, y, button, modifiers):
        self.camera.process_mouse_press(x, y, self.width, self.height)

    def on_mouse_motion(self, x, y, dx, dy):
        self.camera.process_mouse_move(dx, dy)

    def on_mouse_scroll(self, x, y, scroll_x, scroll_y):
        self.camera.process_mouse_scroll(scroll_y)

    def on_key_press(self, symbol, modifiers):
        if symbol == key.ESCAPE:
            self.close()
        # elif symbol == key.SPACE:
        #     self.shadows = not self.shadows
        #     print('shadow mode changed')

    def on_keyboard_activity(self, dt):
        if self.keys[key.W]:
            self.camera.process_keyboard(Movement.forward, dt)
        if self.keys[key.S]:
            self.camera.process_keyboard(Movement.backward, dt)
        if self.keys[key.A]:
            self.camera.process_keyboard(Movement.left, dt)
        if self.keys[key.D]:
            self.camera.process_keyboard(Movement.right, dt)
