import numpy as np
from enum import Enum
from .utils import *


Movement = Enum('Movement', ('forward', 'backward', 'left', 'right'))


def normalize(vector):
    return vector / np.linalg.norm(vector)


class Camera:

    def __init__(self, pos=(0.0, 6.0, 3.0), world_up=(0.0, 1.0, 0.0), yaw=-60.0, pitch=30.0,
        move_speed=0.5, mouse_sensitivity=0.1, zoom=45.0):
        self.pos = np.array(pos)
        self.world_up = np.array(world_up)
        self.yaw = yaw
        self.pitch = pitch
        self.move_speed = move_speed
        self.mouse_sensitivity = mouse_sensitivity
        self.zoom = zoom
        self.update_camera_vectors()

    def update_camera_vectors(self):
        front = (np.cos(np.radians(self.yaw)) * np.cos(np.radians(self.pitch)),
            np.sin(np.radians(self.pitch)),
            np.sin(np.radians(self.yaw)) * np.cos(np.radians(self.pitch)))
        # front = np.array([1.5, 1.5, 0.5]) - self.pos
        self.front = normalize(front)
        self.right = normalize(np.cross(self.front, self.world_up))
        self.up = normalize(np.cross(self.right, self.front))

    def process_keyboard(self, direction, dt):
        vel = self.move_speed * dt
        if direction == Movement.forward:
            self.pos += self.front * vel
        elif direction == Movement.backward:
            self.pos -= self.front * vel
        elif direction == Movement.left:
            self.pos -= self.right * vel
        elif direction == Movement.right:
            self.pos += self.right * vel

    def process_mouse_press(self, x, y, width, height):
        if x < 0.1 * width:
            self.yaw -= self.mouse_sensitivity * 100
        if x > 0.9 * width:
            self.yaw += self.mouse_sensitivity * 100
        if y < 0.1 * height:
            self.pitch -= self.mouse_sensitivity * 10
        if y > 0.9 * height:
            self.pitch += self.mouse_sensitivity * 10
        self.update_camera_vectors()

    def process_mouse_move(self, offset_x, offset_y, constrain_pitch=True, constrain_yaw=False):
        self.yaw += offset_x * self.mouse_sensitivity
        self.pitch += offset_y * self.mouse_sensitivity
        if constrain_pitch:
            self.pitch = np.clip(self.pitch, -89.0, 89.0)
        if constrain_yaw:
            self.yaw = np.clip(self.yaw, -179.0, -1.0)
        self.update_camera_vectors()

    def process_mouse_scroll(self, offset_y):
        if self.zoom >= 1.0 and self.zoom <= 45.0:
            self.zoom -= offset_y * self.mouse_sensitivity
        self.zoom = np.clip(self.zoom, 1.0, 45.0)

    def get_view_matrix(self):
        return look_at(self.pos, self.pos + self.front, self.up)

    def print_debug(self):
        print('pos', self.pos)
        print('up', self.up)
        print('yaw', self.yaw)
        print('pitch', self.pitch)
        print('front', self.front)
        print('right', self.right)
        print('zoom', self.zoom)
        print('--------------------------')