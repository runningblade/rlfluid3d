import numpy as np
import ctypes as ct
import os

from pynput.keyboard import Listener
from env_utils import *

import gym
from gym import spaces
from gym.utils import seeding
from gym.utils import reraise
from gym.envs.classic_control.rendering import Geom

try:
    import pyglet
except ImportError as e:
    reraise(suffix="HINT: you can install pyglet directly via 'pip install pyglet'. But if you really just want to install all Gym dependencies and not have to think about it, 'pip install -e .[all]' or 'pip install gym[all]' will do it.")

try:
    from pyglet.gl import *
except ImportError as e:
    reraise(prefix="Error occured while running `from pyglet.gl import *`",suffix="HINT: make sure you have OpenGL install. On Ubuntu, you can run 'apt-get install python-opengl'. If you're running on a server, you may need a virtual frame buffer; something like this should work: 'xvfb-run -s \"-screen 0 1400x900x24\" python <your_script.py>'")

class DensityField(Geom):
    def __init__(self, x, y, rhoss, posxss, posyss, sz, stride, nrRho):
        Geom.__init__(self)
        k=0
        self.quadColors = (GLfloat*(3 * sz[0] * sz[1]))()
        self.quadCoords = (GLfloat*(3 * sz[0] * sz[1]))()
        self.quadIndices = (GLuint *((sz[0] - 1) * (sz[1] - 1) * 4))()
        for xx in range(sz[0]):
            for yy in range(sz[1]):
                off = xx * sz[1]+yy
                offS = xx * stride[0]+yy * stride[1]
                self.quadColors[off * 3+0]=self.quadColors[off * 3+1]=self.quadColors[off * 3+2]=1-max(min(rhoss[offS],1),0)
                self.quadCoords[off * 3+0]=posxss[xx]
                self.quadCoords[off * 3+1]=posyss[yy]
                self.quadCoords[off * 3+2]=0
                if xx<sz[0]-1 and yy<sz[1]-1:
                    self.quadIndices[k]=xx * sz[1]+yy
                    k=k+1
                    self.quadIndices[k]=(xx+1) * sz[1]+yy
                    k=k+1
                    self.quadIndices[k]=(xx+1) * sz[1]+(yy+1)
                    k=k+1
                    self.quadIndices[k]=xx * sz[1]+(yy+1)
                    k=k+1
    def render1(self):
        glVertexPointer(3,GL_FLOAT,0,self.quadCoords)
        glColorPointer(3,GL_FLOAT,0,self.quadColors)
        glEnableClientState(GL_VERTEX_ARRAY)
        glEnableClientState(GL_COLOR_ARRAY)
        glDrawElements(GL_QUADS,len(self.quadIndices),GL_UNSIGNED_INT,self.quadIndices)
        glDisableClientState(GL_VERTEX_ARRAY)
        glDisableClientState(GL_COLOR_ARRAY)

FPS = 50

class SmokePingPongDoubleSync(gym.Env):
    # implementation
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': FPS
    }

    def __init__(self, w=5, h=3, radpp=0.2, radsh=0.1,  # geometry
                 fsh=20, fbb=15, fade=1, rhopp=1500,  #physics
                 wp=100, wv=10, wsh=0.5, wl=1, max_step=1000,  # reward weights
                 resf=32, res=100,  # render resolution
                 debug=False):  # render when training
        #c interface
        lib_path = os.path.join(os.path.split(os.path.realpath(__file__))[0], "FluidOpt-build/libFluid.so")
        self.fluid = ct.cdll.LoadLibrary(lib_path)
        smoke_func_declare(self.fluid)
        #the rest
        self._seed()
        self.viewer    = None
        self.now_step  = 0
        self.max_step  = max_step
        self.res       = res
        self.debug     = debug
        # geometry
        self.w         = w
        self.h         = h
        self.radpp     = radpp
        self.radsh     = radsh
        self.resf      = resf
        # physics
        self.possh     = w/2
        self.velsh     = 0.0
        self.fsh       = fsh
        self.fbb       = fbb
        self.fade      = fade
        self.rhopp     = rhopp
        # reward
        self.wp        = wp
        self.wv        = wv
        self.wsh       = wsh
        self.wl        = wl
        # background
        self.background = [(0, 0), (self.w, 0), (self.w, self.h), (0, self.h)]
        # state: pp x, pp y, pp velx, pp vely,
        # state: sh x, sh a, sh velx, shooter vela
        high = np.array([np.inf] * 6)
        self.observation_space = spaces.Box(-high, high)
        # action: nop, shoot, shoot left, shoot right, move left, move right
        self.NO_MOVE, self.MOVE_LEFT, self.MOVE_RIGHT = 0, 1, 2
        self.NO_SHOOT, self.SHOOT_LEFT, self.SHOOT_RIGHT, self.SHOOT_BOTH = 0, 1, 2, 3
        self.action_space = spaces.Discrete(12)
        self._reset()

    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _destroy(self):
        self.fluid.destroySmoke()

    def _reset(self):
        self._destroy()
        self.fluid.createSmoke(self.w,self.h,self.resf,self.radsh)
        self.fluid.setRigidSmoke(self.w/2,self.h/2,self.radpp,self.rhopp)
        self.fluid.setFade(1)
        self.game_over = False
        self.now_step = 0
        self.possh     = self.w/2
        self.velsh     = 0.0
        
        # field size
        nrPoint = (ct.c_int * 2)() # w, h
        self.fluid.getFeatureSmokeSimple(ct.c_float(-1), ct.POINTER(ct.c_float)(), ct.byref(nrPoint))
        self.field_size = [nrPoint[1], nrPoint[0], 2] # input: h, w
        return self._step(self.action_space.sample())[0]

    def sample_speed(self):
        nrPoint = (ct.c_int * 2)()
        self.fluid.getFeatureSmokeSimple(ct.c_float(-1), ct.POINTER(ct.c_float)(), ct.byref(nrPoint))
        rho = (ct.c_float * (nrPoint[0] * nrPoint[1] * 2))()
        self.fluid.getFeatureSmokeSimple(ct.c_float(-1), ct.byref(rho), ct.byref(nrPoint))
        speed = [rho[i] for i in range(nrPoint[0] * nrPoint[1] * 2)]
        return preprocess(speed, nrPoint[0], nrPoint[1])

    def _step(self, action):
        # ensure its a valid action
        assert self.action_space.contains(action), "%r (%s) invalid " % (action, type(action))
        # record the steps
        self.now_step += 1
        shoot_A = False
        shoot_B = False
        if self.debug == True:  # in case u wanna c the training process visually
            self.render()
        # Action/Engine
        action_MOVE = action // 4
        action_SHOOT = action % 4

        last_possh = self.possh
        if action_MOVE == self.MOVE_LEFT:
            self.velsh = self.velsh - self.fsh / FPS
        elif action_MOVE == self.MOVE_RIGHT:
            self.velsh = self.velsh + self.fsh / FPS
        self.possh = self.possh + self.velsh / FPS
        if action_SHOOT == self.SHOOT_LEFT or action_SHOOT == self.SHOOT_BOTH:
            shoot_A = True
        if action_SHOOT == self.SHOOT_RIGHT or action_SHOOT == self.SHOOT_BOTH:
            shoot_B = True
        possh_c = (ct.c_float)(self.possh)
        self.fluid.transferSmoke(1/FPS,shoot_A,shoot_B,possh_c,self.w/11,self.fbb)
        self.possh = possh_c.value
        self.velsh = (self.possh - last_possh) * FPS

        # State
        pospp_x = (ct.c_float)()
        pospp_y = (ct.c_float)()
        self.fluid.getRigidPosSmoke(pospp_x,pospp_y)
        velpp_x = (ct.c_float)()
        velpp_y = (ct.c_float)()
        self.fluid.getRigidVelSmoke(velpp_x,velpp_y)
        state = np.array([pospp_x.value, pospp_y.value, velpp_x.value, velpp_y.value, self.possh, self.velsh])
        assert self.observation_space.contains(state), "%r (%s) invalid " % (state, type(state))
        
        speed = self.sample_speed()

        # Reward
        reward = -self.wp * np.sqrt((state[0] - self.w / 2) * (state[0] - self.w / 2) + \
                                    (state[1] - self.h / 2) * (state[1] - self.h / 2)) - \
                  self.wv * np.sqrt(state[2] * state[2] + \
                                    state[3] * state[3]) + \
                  self.wl
        if shoot_A == True:
            reward = reward - self.wsh
        if shoot_B == True:
            reward = reward - self.wsh
        if state[1] - self.radpp < 2 / self.resf or \
                self.h - self.radpp - state[1] < 2 / self.resf or \
                state[0] - self.radpp < 2 / self.resf or \
                self.w - self.radpp - state[0] < 2 / self.resf:
            self.game_over = True
        if self.now_step > self.max_step:
            self.game_over = True
            
        # Return
        return state, reward, self.game_over, {'speed': speed}

    def _render(self, mode='human', close=False):
        if close:
            if self.viewer is not None:
                self.viewer.close()
                self.viewer = None
            return
        # renderer setup
        from gym.envs.classic_control import rendering
        if self.viewer is None:
            self.viewer = rendering.Viewer(self.w * self.res, self.h * self.res)
            self.viewer.set_bounds(0, self.w, 0, self.h)
        # background
        self.viewer.draw_polygon(self.background, color=(1, 1, 1))
        # Smoke densities
        null_ptr = ct.POINTER(ct.c_float)()
        sz_c = (ct.c_int * 2)()
        stride_c = (ct.c_int * 2)()
        nrRho_c = (ct.c_int)()
        self.fluid.getRhoSmoke(null_ptr, null_ptr, null_ptr, sz_c, stride_c, nrRho_c)
        rho_cache_c = (ct.c_float * nrRho_c.value)()
        posx_cache_c = (ct.c_float * sz_c[0])()
        posy_cache_c = (ct.c_float * sz_c[1])()
        self.fluid.getRhoSmoke(rho_cache_c, posx_cache_c, posy_cache_c, sz_c, stride_c, nrRho_c)
        dens = DensityField(self.w, self.h, rho_cache_c, posx_cache_c, posy_cache_c, sz_c, stride_c, nrRho_c)
        self.viewer.add_onetime(dens)
        # pingpong
        pospp_x = (ct.c_float)()
        pospp_y = (ct.c_float)()
        self.fluid.getRigidPosSmoke(pospp_x, pospp_y)
        t = rendering.Transform(translation = (pospp_x.value, pospp_y.value))
        self.viewer.draw_circle(self.radpp, 20, color=(1, 1, 1)).add_attr(t)
        self.viewer.draw_circle(self.radpp, 20, color=(0.9, 0.9, 0.9), filled=False, linewidth=2).add_attr(t)
        # shooterA
        t = rendering.Transform(translation = (self.possh-self.w/11/2, self.radsh))
        self.viewer.draw_circle(self.radsh, 20, color=(0.5, 0.4, 0.9)).add_attr(t)
        self.viewer.draw_circle(self.radsh, 20, color=(0.3, 0.3, 0.5), filled=False, linewidth=2).add_attr(t)
        # shooterB
        t = rendering.Transform(translation = (self.possh+self.w/11/2, self.radsh))
        self.viewer.draw_circle(self.radsh, 20, color=(0.5, 0.4, 0.9)).add_attr(t)
        self.viewer.draw_circle(self.radsh, 20, color=(0.3, 0.3, 0.5), filled=False, linewidth=2).add_attr(t)
        # return
        return self.viewer.render(return_rgb_array=mode == 'rgb_array')
    # human play

    def run_game(self):
        Listener(
            on_press=on_press,
            on_release=on_release).start()
        s = self.reset()
        total_reward = 0
        steps = 0
        while True:
            action = 0

            if keys['left'] == True and keys['right'] == False:
                action = self.MOVE_LEFT
            elif keys['left'] == False and keys['right'] == True:
                action = self.MOVE_RIGHT
            else:
                action = self.NO_MOVE

            action *= 4

            if keys['1'] == True:
                if keys['2'] == True:
                    action += self.SHOOT_BOTH
                else:
                    action += self.SHOOT_LEFT
            elif keys['2'] == True:
                action += self.SHOOT_RIGHT
            else:
                action += self.NO_SHOOT

            s, r, done, info = self.step(action)
            env.render()
            total_reward += r
            if steps % 20 == 0 or done:
                print(["{:+0.2f}".format(x) for x in s])
                print("step {} total_reward {:+0.2f}".format(steps, total_reward))
            steps += 1
            if done:
                self.reset()

if __name__ == "__main__":
    env = SmokePingPongDoubleSync()
    env.run_game()
