import pyFluidOpt as fo
import numpy as np
import math,random,os,shutil

def setTrans(T, v):
    for i in range(3):
        T(i,3,v[i])
def setRot(T, r):
    for i in range(3):
        for j in range(3):
            T(i,j,r(i,j))
if __name__ == '__main__':
    #setup
    sol=fo.RigidSolver(3,1.0/32.0)
    sol.BB=fo.BBox(fo.Vec3(0,0,0),fo.Vec3(2,2,1))
    sol.setWarpMode(fo.Vec3i(1,0,0))
    sol.gravity=fo.Vec3(0,-9.81,0)
    #add solid
    T=fo.Mat4()
    T.setIdentity()
    R=fo.Mat3()
    R.fromLog(fo.Vec3(0,0,math.pi/4))
    setTrans(T,fo.Vec3(1,0.5,0.5))
    setRot(T,R)
    cell=fo.BoxGeomCell(T,3,fo.Vec3(0.5,0.1,0.5),0.0,False)
    sol.addSolid(cell)
    #add rigid
    NR_RIGID=1000
    ALL_BOX=True
    for i in range(NR_RIGID):
        rad=random.uniform(0.1,0.3)
        pos=fo.Vec3(random.uniform(sol.BB._minC[0]+rad,sol.BB._maxC[0]-rad),
                    random.uniform(sol.BB._minC[1]+rad,sol.BB._maxC[1]-rad),
                    random.uniform(sol.BB._minC[2]+rad,sol.BB._maxC[2]-rad))
        if not sol.intersect2(rad,pos,False):
            if ALL_BOX:
                sol.addBox2(fo.Vec3(rad/2,rad/4,rad/2),pos,fo.Vec3(random.random(),random.random(),random.random()))
            else:
                if i < NR_RIGID/2:
                    sol.addCross2(fo.Vec3(rad/2,rad/4,rad/2),pos,fo.Vec3(random.random(),random.random(),random.random()))
                else: sol.addBall(rad,pos)
    sol.writeMeshVTK('mesh.vtk')
    #simulate
    dt=3E-3
    gTime=0.0
    I=10
    if os.path.exists('rigid'):
        shutil.rmtree('rigid')
    os.mkdir('rigid')
    sol.writeMeshVTK('rigid/mesh.vtk')
    sol.writePSetVTK('rigid/pset.vtk')
    for i in range(5000):
        print('Simulating frame: %d!'%i)
        sol.advance(dt,gTime)
        if (i%I)==0:
            sol.writeMeshVTK('rigid/mesh'+str(int(i//I))+'.vtk')
        gTime+=dt