#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include <Fluid/RigidSolver.h>
#include <Fluid/LiquidSolver.h>
#include <Fluid/SmokeSolver.h>
#include <Fluid/ShallowWaterSolver.h>
#include <Fluid/VariableSizedGridOp.h>
#include <Fluid/ScriptedSource.h>
#include <CommonFile/ParticleSet.h>
#include <CommonFile/RotationUtil.h>
#include <boost/lexical_cast.hpp>
#include "Fluid/ScriptedSource.h"

USE_PRJ_NAMESPACE
namespace py=pybind11;
PYBIND11_DECLARE_HOLDER_TYPE(T,boost::shared_ptr<T>);
#define PYTHON_FUNC(C,NAME) .def(STRINGIFY_OMP(NAME),&C::NAME)
#define PYTHON_FUNC_REF(C,NAME) .def(STRINGIFY_OMP(NAME),&C::NAME,py::return_value_policy::reference)
#define PYTHON_FUNC_OR(C,NAME) .def(STRINGIFY_OMP(NAME),C##_##NAME)
#define PYTHON_FUNC_OR_REF(C,NAME) .def(STRINGIFY_OMP(NAME),C##_##NAME,py::return_value_policy::reference)
#define PYTHON_MEMBER_READ(C,NAME) .def_readonly(STRINGIFY_OMP(NAME),&C::NAME)
#define PYTHON_MEMBER_READWRITE(C,NAME) .def_readwrite(STRINGIFY_OMP(NAME),&C::NAME,&C::NAME)
#define PYTHON_FIELD_READWRITE(C,NAME) .def_readwrite(STRINGIFY_OMP(NAME),&C::NAME)
#define EIGEN_OP_VEC(T) \
.def("__add__",[](const T& v,const T& w){return T(v+w);},py::is_operator()) \
.def("__sub__",[](const T& v,const T& w){return T(v-w);},py::is_operator()) \
.def("setZero",[](T& v){v.setZero();})  \
.def("setOnes",[](T& v){v.setOnes();})  \
.def("setConstant",[](T& v,T::Scalar val){v.setConstant(val);})  \
.def("dot",[](const T& v,const T& w){return v.dot(w);}) \
.def("__getitem__",[](const T& v,sizeType i){return v[i];}) \
.def("__setitem__",[](T& v,sizeType i,T::Scalar val){v[i]=val;})   \
.def("__repr__",[](const T& v){ostringstream os;os<<"["<<v.transpose()<<"]^T";return os.str();});
#define EIGEN_OP_MAT(T) \
.def("setZero",[](T& v){v.setZero();})  \
.def("setOnes",[](T& v){v.setOnes();})  \
.def("setIdentity",[](T& v){v.setIdentity();})  \
.def("setConstant",[](T& v,T::Scalar val){v.setConstant(val);})  \
.def("__call__",[](const T& v,sizeType i,sizeType j){return v(i,j);})   \
.def("__call__",[](T& v,sizeType i,sizeType j,T::Scalar val){v(i,j)=val;}) \
.def("__repr__",[](const T& v){ostringstream os;os<<"["<<v<<"]";return os.str();});
typedef RigidBody::Quat Quat;

template <typename Encoder>
struct EncoderName {
  static string name() {
    return "";
  }
};
template <int D>
struct EncoderName<EncoderOctree<D> > {
  static string name() {
    return "Octree"+boost::lexical_cast<string>(D);
  }
};
template <int D>
struct EncoderName<EncoderConstant<D> > {
  static string name() {
    return "Constant"+boost::lexical_cast<string>(D);
  }
};
template <int DIM,typename Encoder>
struct pyFluidSolver {
  typedef LiquidSolverPIC<DIM,Encoder> SolverPIC;
  typedef LiquidSolverFLIP<DIM,Encoder> SolverFLIP;
  typedef SmokeSolver<DIM,Encoder> SolverSmoke;
  typedef ShallowWaterSolver<DIM,Encoder> SolverSWE;
  typedef VariableSizedGrid<scalar,DIM,Encoder> GridType;
  template <typename MODULE>
  static void declare(MODULE m) {
    string str=boost::lexical_cast<string>(DIM)+"D"+EncoderName<Encoder>::name();
    //-----------------------------------------------------------------variable sized grid
    py::class_<GridType>(m,("VariableSizedGrid"+str).c_str())
    PYTHON_MEMBER_READ(GridType,_nrPointCellCenter)
    PYTHON_MEMBER_READ(GridType,_strideCellCenter)
    .def("getDx", [](const GridType& grd) {
      return grd._cellSz0.maxCoeff();
    })
    .def("getWidth", [](const GridType& grd) {
      return grd.getBBNodal().getExtent()[0];
    })
    .def("writeCellCenterVTK",[](const GridType& grd,const CellCenterData<scalar>& data,const string& path,bool extrude) {
      VariableSizedGridOp<scalar,DIM,Encoder>::template writeGridCellCenterVTK<scalar>(path,grd,data,extrude);
    })
    .def("writeNodalVTK",[](const GridType& grd,const NodalData<scalar>& data,const string& path,bool extrude) {
      VariableSizedGridOp<scalar,DIM,Encoder>::template writeGridNodalVTK<scalar>(path,grd,data,extrude);
    });
    //-----------------------------------------------------------------pic solver
    void (SolverPIC::*SolverPIC_focusOn1)(const BBox<scalar>& region)=&SolverPIC::focusOn;
    void (SolverPIC::*SolverPIC_focusOn2)(sizeType bodyId,scalar enlargedEps)=&SolverPIC::focusOn;
    RigidSolver& (SolverPIC::*SolverPIC_getRigid)()=&SolverPIC::getRigid;
    py::class_<SolverPIC,boost::shared_ptr<SolverPIC> >(m,("SolverPIC"+str).c_str())
    .def(py::init<>())
    .def_property("globalTime",&SolverPIC::getGlobalTime,&SolverPIC::setGlobalTime)
    .def_property("globalId",&SolverPIC::getGlobalId,&SolverPIC::setGlobalId)
    PYTHON_FUNC(SolverPIC,setCFLMode)
    PYTHON_FUNC(SolverPIC,initSolid)
    PYTHON_FUNC(SolverPIC,initLiquid)
    PYTHON_FUNC(SolverPIC,initDomain)
    PYTHON_FUNC_OR(SolverPIC,focusOn1)
    PYTHON_FUNC_OR(SolverPIC,focusOn2)
    PYTHON_FUNC(SolverPIC,getFeature)
    PYTHON_FUNC(SolverPIC,setUseUnilateral)
    PYTHON_FUNC(SolverPIC,getBB)
    PYTHON_FUNC(SolverPIC,getRadius)
    PYTHON_FUNC(SolverPIC,getNrCell)
    PYTHON_FUNC_REF(SolverPIC,getGrid)
    PYTHON_FUNC_REF(SolverPIC,getGrid0)
    PYTHON_FUNC_REF(SolverPIC,getPSet)
    PYTHON_FUNC_OR_REF(SolverPIC,getRigid)
    PYTHON_FUNC(SolverPIC,advance)
    PYTHON_FUNC(SolverPIC,addSource)
    PYTHON_FUNC(SolverPIC,removeSource)
    PYTHON_FUNC_REF(SolverPIC,getSurfaceMesh);
    //-----------------------------------------------------------------flip solver
    py::class_<SolverFLIP,SolverPIC,boost::shared_ptr<SolverFLIP> >(m,("SolverFLIP"+str).c_str())
    .def(py::init<>())
    PYTHON_FUNC(SolverFLIP,setUseVariational)
    PYTHON_FUNC(SolverFLIP,setUseBOTE)
    PYTHON_FUNC(SolverFLIP,getForceOnBody)
    PYTHON_FUNC(SolverFLIP,getTorqueOnBody);
    //-----------------------------------------------------------------smoke solver
    py::class_<SolverSmoke,SolverFLIP,boost::shared_ptr<SolverSmoke> >(m,("SolverSmoke"+str).c_str())
    .def(py::init<>())
    PYTHON_FUNC_REF(SolverSmoke,getT)
    PYTHON_FUNC_REF(SolverSmoke,getRho)
    PYTHON_FUNC(SolverSmoke,setVelDamping)
    PYTHON_FUNC(SolverSmoke,setDamping);
    //-----------------------------------------------------------------SWE solver
    py::class_<SolverSWE,SolverPIC,boost::shared_ptr<SolverSWE> >(m,("SolverSWE"+str).c_str())
    .def(py::init<>())
    PYTHON_FUNC(SolverSWE,getWS)
    PYTHON_FUNC(SolverSWE,getBS)
    PYTHON_FUNC_REF(SolverSWE,getW)
    PYTHON_FUNC_REF(SolverSWE,getB)
    .def_property("CDrag",&SolverSWE::getCDrag,&SolverSWE::setCDrag)
    .def_property("CDragLiquid",&SolverSWE::getCDragLiquid,&SolverSWE::setCDragLiquid)
    .def_property("CLift",&SolverSWE::getCLift,&SolverSWE::setCLift)
    .def_property("theta",&SolverSWE::getTheta,&SolverSWE::setTheta)
    PYTHON_FUNC_REF(SolverSWE,initLiquidLevel)
    PYTHON_FUNC(SolverSWE,getForceOnBody)
    PYTHON_FUNC(SolverSWE,getTorqueOnBody);
  }
};
PYBIND11_MODULE(pyFluidOpt,m)
{
  //-----------------------------------------------------------------basic types
  //Vec2
  py::class_<Vec2>(m,"Vec2")
  .def(py::init<>())
  .def(py::init<scalar,scalar>())
  EIGEN_OP_VEC(Vec2)
  //Vec3
  py::class_<Vec3>(m,"Vec3")
  .def(py::init<>())
  .def(py::init<scalar,scalar,scalar>())
  .def("cross",[](const Vec3& v,const Vec3& w){return v.cross(w);})
  EIGEN_OP_VEC(Vec3)
  //Vec2i
  py::class_<Vec2i>(m,"Vec2i")
  .def(py::init<>())
  .def(py::init<sizeType,sizeType>())
  EIGEN_OP_VEC(Vec2i)
  //Vec3i
  py::class_<Vec3i>(m,"Vec3i")
  .def(py::init<>())
  .def(py::init<sizeType,sizeType,sizeType>())
  EIGEN_OP_VEC(Vec3i)
  //Mat3
  py::class_<Mat2>(m,"Mat2")
  .def(py::init<>())
  EIGEN_OP_MAT(Mat2)
  //Mat3
  py::class_<Mat3>(m,"Mat3")
  .def(py::init<>())
  .def("fromLog",[](Mat3& m,const Vec3& v) {
    m=expWGradV<scalar>(v,NULL,NULL,NULL,NULL);
  })
  .def("toLog",[](const Mat3& m) {
    return invExpW<scalar>(m);
  })
  EIGEN_OP_MAT(Mat3)
  //Mat4
  py::class_<Mat4>(m,"Mat4")
  .def(py::init<>())
  EIGEN_OP_MAT(Mat4)
  //Quat
  py::class_<Quat>(m,"Quat")
  .def(py::init<>())
  .def(py::init<scalar,scalar,scalar,scalar>())
  .def(py::init<const Mat3&>())
  PYTHON_FUNC(Quat,toRotationMatrix)
  .def("__getitem__",[](const Quat& v,sizeType i) {
    return i==0?v.x():i==1?v.y():i==2?v.z():v.w();
  }) \
  .def("__setitem__",[](Quat& v,sizeType i,Quat::Scalar val) {
    (i==0?v.x():i==1?v.y():i==2?v.z():v.w())=val;
  })   \
  .def("__repr__",[](const Quat& v) {
    ostringstream os;
    os<<"["<<v.x()<<" "<<v.y()<<" "<<v.z()<<" "<<v.w()<<"]";
    return os.str();
  });
  //bounding box
  py::class_<BBox<scalar> >(m,"BBox")
  .def(py::init<>())
  .def(py::init<const Vec3&,const Vec3&>())
  PYTHON_MEMBER_READ(BBox<scalar>,_minC)
  PYTHON_MEMBER_READ(BBox<scalar>,_maxC);
  //-----------------------------------------------------------------ParticleSet
  //particle
  py::class_<ParticleN<scalar> >(m,"Particle")
  .def(py::init<>())
  PYTHON_MEMBER_READ(ParticleN<scalar>,_pos)
  PYTHON_MEMBER_READ(ParticleN<scalar>,_vel)
  PYTHON_MEMBER_READ(ParticleN<scalar>,_normal);
  //particle set
  void (ParticleSetN::*ParticleSetN_writeVTK)(const std::string& path) const=&ParticleSetN::writeVTK;
  const ParticleN<scalar>& (ParticleSetN::*ParticleSetN_get)(const sizeType& i) const=&ParticleSetN::get;
  py::class_<ParticleSetN>(m,"PSet")
  .def(py::init<>())
  PYTHON_FUNC(ParticleSetN,size)
  PYTHON_FUNC_OR(ParticleSetN,writeVTK)
  PYTHON_FUNC_OR_REF(ParticleSetN,get)
  .def("__getitem__",[](const ParticleSetN& v,sizeType i) {
    return v[i];
  });
  //-----------------------------------------------------------------ObjMesh
  const Vec3& (ObjMesh::*ObjMesh_getV)(int i) const=&ObjMesh::getV;
  const Vec3i& (ObjMesh::*ObjMesh_getI)(int i) const=&ObjMesh::getI;
  const Vec3& (ObjMesh::*ObjMesh_getN)(int i) const=&ObjMesh::getN;
  py::class_<ObjMesh>(m,"ObjMesh")
  .def(py::init<>())
  .def("nrV",[](const ObjMesh& mesh) {
    return mesh.getV().size();
  })
  .def("nrI",[](const ObjMesh& mesh) {
    return mesh.getI().size();
  })
  PYTHON_FUNC_OR_REF(ObjMesh,getV)
  PYTHON_FUNC_OR_REF(ObjMesh,getI)
  PYTHON_FUNC_OR_REF(ObjMesh,getN)
  PYTHON_FUNC(ObjMesh,smooth)
  .def("subdivide",[](ObjMesh& obj,sizeType nr) {
    obj.subdivide(nr);
  })
  .def("writePov",[](const ObjMesh& obj,const string& path,bool normal) {
    obj.writePov(path,normal);
  })
  .def("writeVTK",[](const ObjMesh& obj,const string& path) {
    obj.writeVTK(path,true);
  });
  //-----------------------------------------------------------------VectorField
  py::class_<VectorField>(m,"VectorField")
  .def(py::init<>())
  PYTHON_FUNC_REF(VectorField,getNrPoint)
  PYTHON_FUNC_REF(VectorField,getStride)
  PYTHON_FUNC(VectorField,getPt)
  .def("__getitem__",[](const VectorField& v,sizeType i) {
    return v[i];
  },py::return_value_policy::reference)
  .def("writeVTK",[](const VectorField& v,const string& path) {
    if(v.getDim() == 2)
      GridOp<scalar,scalar>::write2DVectorGridVTK(path,v);
  });
  //-----------------------------------------------------------------ScalarField
  py::class_<ScalarField>(m,"ScalarField")
  .def(py::init<>())
  PYTHON_FUNC_REF(ScalarField,getNrPoint)
  PYTHON_FUNC_REF(ScalarField,getBB)
  PYTHON_FUNC_REF(ScalarField,getCellSize)
  PYTHON_FUNC_REF(ScalarField,getStride)
  PYTHON_FUNC(ScalarField,getPt)
  .def("__getitem__",[](const ScalarField& v,sizeType i) {
    return v[i];
  })
  .def("__getitem__",[](const ScalarField& v,const Vec3i& id) {
    return v[id];
  })
  .def("toObj",[](const ScalarField& v,bool moveVertex) {
    ObjMesh obj;
    GridOp<scalar,scalar>::write2DScalarGridObj(obj,v,moveVertex);
    return obj;
  })
  .def("writeVTK",[](const ScalarField& v,const string& path,bool moveVertex) {
    if(v.getDim() == 2)
      GridOp<scalar,scalar>::write2DScalarGridVTK(path,v,moveVertex);
    else GridOp<scalar,scalar>::write3DScalarGridVTK(path,v);
  });
  //-----------------------------------------------------------------rigid body solver
  //rigid body
  void (RigidBody::*RigidBody_writeMeshVTK)(const std::string& str) const=&RigidBody::writeMeshVTK;
  void (RigidBody::*RigidBody_writePSetVTK)(const std::string& str) const=&RigidBody::writePSetVTK;
  py::class_<RigidBody,boost::shared_ptr<RigidBody> >(m,"RigidBody")
  .def(py::init<>())
  PYTHON_FUNC(RigidBody,setPos)
  PYTHON_FUNC(RigidBody,setRot)
  PYTHON_FUNC(RigidBody,setP)
  PYTHON_FUNC(RigidBody,setL)
  PYTHON_FUNC(RigidBody,pos)
  PYTHON_FUNC(RigidBody,rot)
  PYTHON_FUNC(RigidBody,q)
  PYTHON_FUNC(RigidBody,p)
  PYTHON_FUNC(RigidBody,L)
  PYTHON_FUNC(RigidBody,linSpd)
  PYTHON_FUNC(RigidBody,rotSpd)
  PYTHON_FUNC(RigidBody,I)
  PYTHON_FUNC(RigidBody,invI)
  PYTHON_FUNC(RigidBody,vel)
  PYTHON_FUNC(RigidBody,valid)
  PYTHON_FUNC(RigidBody,freeze)
  PYTHON_FUNC(RigidBody,applyTorque)
  PYTHON_FUNC(RigidBody,applyForce)
  PYTHON_FUNC(RigidBody,applyAngularImpulse)
  PYTHON_FUNC(RigidBody,applyImpulse)
  PYTHON_FUNC(RigidBody,getWorldBB)
  PYTHON_FUNC(RigidBody,applyWorldForce)
  PYTHON_FUNC(RigidBody,applyWorldImpulse)
  PYTHON_FUNC(RigidBody,toWorld)
  PYTHON_FUNC(RigidBody,toLocal)
  PYTHON_FUNC(RigidBody,dist)
  PYTHON_FUNC_REF(RigidBody,getPSet)
  PYTHON_FUNC_OR(RigidBody,writeMeshVTK)
  PYTHON_FUNC_OR(RigidBody,writePSetVTK)
  PYTHON_FUNC(RigidBody,getM)
  PYTHON_FUNC(RigidBody,getIB)
  PYTHON_FUNC(RigidBody,getInvIB)
  .def_property("rho",[](const RigidBody& b) {
    return b.rho();
  },[](RigidBody& b,scalar rho) {
    b.rho()=rho;
  })
  .def_property("restitute",[](const RigidBody& b) {
    return b.restitute();
  },[](RigidBody& b,scalar restitute) {
    b.restitute()=restitute;
  })
  PYTHON_FUNC(RigidBody,clearForceAndTorque)
  PYTHON_FUNC(RigidBody,clearContactFlag)
  PYTHON_FUNC(RigidBody,setContactFlag)
  PYTHON_FUNC(RigidBody,hasContactFlag);
  //rigid solver
  void (RigidSolver::*RigidSolver_writeMeshVTK)(const std::string& str) const=&RigidSolver::writeMeshVTK;
  void (RigidSolver::*RigidSolver_writePSetVTK)(const std::string& str) const=&RigidSolver::writePSetVTK;
  void (RigidSolver::*RigidSolver_addBall1)(const scalar rad,const Vec3& pos)=&RigidSolver::addBall;
  void (RigidSolver::*RigidSolver_addBall2)(const Vec3& ext,const Vec3& pos,const Quat& rot)=&RigidSolver::addBall;
  void (RigidSolver::*RigidSolver_addBall3)(const Vec3& ext,const Vec3& pos,const Vec3& rot)=&RigidSolver::addBall;
  void (RigidSolver::*RigidSolver_addBox1)(const Vec3& ext,const Vec3& pos,const Quat& rot)=&RigidSolver::addBox;
  void (RigidSolver::*RigidSolver_addBox2)(const Vec3& ext,const Vec3& pos,const Vec3& rot)=&RigidSolver::addBox;
  void (RigidSolver::*RigidSolver_addCross1)(const Vec3& ext,const Vec3& pos,const Quat& rot)=&RigidSolver::addCross;
  void (RigidSolver::*RigidSolver_addCross2)(const Vec3& ext,const Vec3& pos,const Vec3& rot)=&RigidSolver::addCross;
  RigidBody& (RigidSolver::*RigidSolver_getBody)(const sizeType& i)=&RigidSolver::getBody;
  StaticGeomCell& (RigidSolver::*RigidSolver_getSolid)(const sizeType& i)=&RigidSolver::getSolid;
  bool (RigidSolver::*RigidSolver_intersect1)(const Vec3& pos,bool solidOnly) const=&RigidSolver::intersect;
  bool (RigidSolver::*RigidSolver_intersect2)(scalar r,const Vec3& pos,bool solidOnly) const=&RigidSolver::intersect;
  py::class_<RigidSolver,boost::shared_ptr<RigidSolver> >(m,"RigidSolver")
  .def(py::init<>())
  .def(py::init<sizeType,scalar>())
  PYTHON_FUNC_OR(RigidSolver,writeMeshVTK)
  PYTHON_FUNC_OR(RigidSolver,writePSetVTK)
  PYTHON_FUNC(RigidSolver,nrBody)
  PYTHON_FUNC(RigidSolver,nrSolid)
  PYTHON_FUNC(RigidSolver,setGravity)
  PYTHON_FUNC_OR(RigidSolver,addBall1)
  PYTHON_FUNC_OR(RigidSolver,addBall2)
  PYTHON_FUNC_OR(RigidSolver,addBall3)
  PYTHON_FUNC_OR(RigidSolver,addBox1)
  PYTHON_FUNC_OR(RigidSolver,addBox2)
  PYTHON_FUNC_OR(RigidSolver,addCross1)
  PYTHON_FUNC_OR(RigidSolver,addCross2)
  PYTHON_FUNC(RigidSolver,addSolid)
  PYTHON_FUNC_OR_REF(RigidSolver,getBody)
  PYTHON_FUNC_OR_REF(RigidSolver,getSolid)
  PYTHON_FUNC(RigidSolver,clearContactFlag)
  PYTHON_FUNC(RigidSolver,advance)
  .def_property("BB",&RigidSolver::getBB,&RigidSolver::setBB)
  .def_property("gravity",&RigidSolver::getGravity,&RigidSolver::setGravity)
  PYTHON_FUNC_OR(RigidSolver,intersect1)
  PYTHON_FUNC_OR(RigidSolver,intersect2)
  PYTHON_FUNC_REF(RigidSolver,getPSet)
  PYTHON_FUNC(RigidSolver,setWarpMode);
  //solid body
  py::class_<StaticGeomCell,boost::shared_ptr<StaticGeomCell> >(m,"StaticGeomCell");
  py::class_<BoxGeomCell,StaticGeomCell,boost::shared_ptr<BoxGeomCell> >(m,"BoxGeomCell")
  .def(py::init<const Mat4&,sizeType,const Vec3&,scalar,bool>());
  py::class_<SphereGeomCell,StaticGeomCell,boost::shared_ptr<SphereGeomCell> >(m,"SphereGeomCell")
  .def(py::init<const Mat4&,sizeType,scalar,scalar,bool>());
  //-----------------------------------------------------------------liquid/smoke solver
  pyFluidSolver<2,EncoderConstant<1> >::declare(m);
  pyFluidSolver<3,EncoderConstant<1> >::declare(m);
  //-----------------------------------------------------------------implicit func
  //ImplicitFunc
  py::class_<ImplicitFunc<scalar>,boost::shared_ptr<ImplicitFunc<scalar> > >(m,"ImplicitFunc");
  //SourceImplicitFunc
  py::class_<SourceImplicitFunc,ImplicitFunc<scalar>,boost::shared_ptr<SourceImplicitFunc> >(m,"SourceImplicitFunc")
  PYTHON_FIELD_READWRITE(SourceImplicitFunc,_bb)
  PYTHON_FIELD_READWRITE(SourceImplicitFunc,_vel)
  PYTHON_FIELD_READWRITE(SourceImplicitFunc,_dim)
  PYTHON_FIELD_READWRITE(SourceImplicitFunc,_res)
  PYTHON_FIELD_READWRITE(SourceImplicitFunc,_lastFor)
  PYTHON_FIELD_READWRITE(SourceImplicitFunc,_source)
  PYTHON_FIELD_READWRITE(SourceImplicitFunc,_instant);
  //ImplicitRect
  py::class_<ImplicitRect,SourceImplicitFunc,boost::shared_ptr<ImplicitRect> >(m,"ImplicitRect")
  .def(py::init<>())
  .def(py::init<const Vec3&,const Vec3&,const sizeType&,const scalar&,const Vec3&,const scalar&,bool,bool>())
  PYTHON_FIELD_READWRITE(ImplicitRect,_rectBB)
  .def("reset",[](ImplicitRect& v) {
    v.reset(v._rectBB._minC, v._rectBB._maxC, v._dim, v._res, v._vel, v._lastFor, v._source, v._instant);
  });
  //ImplicitRectNeg
  py::class_<ImplicitRectNeg,ImplicitRect,boost::shared_ptr<ImplicitRectNeg> >(m,"ImplicitRectNeg")
  .def(py::init<>())
  .def(py::init<const Vec3&,const Vec3&,const sizeType&,const scalar&,const Vec3&,const scalar&,bool,bool>());
  //ImplicitCylinder
  py::class_<ImplicitCylinder,SourceImplicitFunc,boost::shared_ptr<ImplicitCylinder> >(m,"ImplicitCylinder")
  .def(py::init<>())
  .def(py::init<const Vec3&,const Vec3&,const scalar&,const scalar&,const sizeType&,const scalar&,const Vec3&,const scalar&,bool,bool>())
  PYTHON_FIELD_READWRITE(ImplicitCylinder,_ctr)
  PYTHON_FIELD_READWRITE(ImplicitCylinder,_dir)
  PYTHON_FIELD_READWRITE(ImplicitCylinder,_rad)
  PYTHON_FIELD_READWRITE(ImplicitCylinder,_thickness);
  //ImplicitSphere
  py::class_<ImplicitSphere,SourceImplicitFunc,boost::shared_ptr<ImplicitSphere> >(m,"ImplicitSphere")
  .def(py::init<>())
  .def(py::init<const Vec3&,const scalar&,const sizeType&,const scalar&,const Vec3&,const scalar&,bool,bool>())
  PYTHON_FIELD_READWRITE(ImplicitSphere,_ctr)
  PYTHON_FIELD_READWRITE(ImplicitSphere,_rad)
  .def("reset",[](ImplicitSphere& v) {
    v.reset(v._ctr, v._rad, v._dim, v._res, v._vel, v._lastFor, v._source, v._instant);
  });
  //-----------------------------------------------------------------source/sink
  //SourceRegion
  py::class_<SourceRegion,boost::shared_ptr<SourceRegion> >(m,"SourceRegion")
  .def_property("rho",&ImplicitFuncSource::PARENT::getRho,&ImplicitFuncSource::PARENT::setRho)
  .def_property("T",&ImplicitFuncSource::PARENT::getT,&ImplicitFuncSource::PARENT::setT);
  //ImplicitFuncSource
  py::class_<ImplicitFuncSource,SourceRegion,boost::shared_ptr<ImplicitFuncSource> >(m,"ImplicitFuncSource")
  .def("addSource",[](ImplicitFuncSource& s,boost::shared_ptr<SourceImplicitFunc> f) {
    s._sources.push_back(f);
  })
  .def("setSource", [](ImplicitFuncSource &s, bool isSource) {
    s.setSource(isSource);
  });
  //RandomSource
  py::class_<RandomSource,ImplicitFuncSource,boost::shared_ptr<RandomSource> >(m,"RandomSource")
  .def(py::init<>());
  //TokenRingSource
  py::class_<TokenRingSource,ImplicitFuncSource,boost::shared_ptr<TokenRingSource> >(m,"TokenRingSource")
  .def(py::init<>());
  //VariableTimeTokenRingSource
  py::class_<VariableTimeTokenRingSource,ImplicitFuncSource,boost::shared_ptr<VariableTimeTokenRingSource> >(m,"VariableTimeTokenRingSource")
  .def(py::init<>());
  //-----------------------------------------------------------------cell center data
  py::class_<CellCenterData<scalar> >(m,"CellCenterData")
  .def("size",[](const CellCenterData<scalar>& v) {
    return v._data.size();
  })
  .def("__getitem__",[](const CellCenterData<scalar>& v,sizeType i) {
    return v._data[i];
  });
  //-----------------------------------------------------------------nodal data
  py::class_<NodalData<scalar> >(m,"NodalData")
  .def("size",[](const NodalData<scalar>& v) {
    return v._data.size();
  })
  .def("__getitem__",[](const NodalData<scalar>& v,sizeType i) {
    return v._data[i];
  });
}
