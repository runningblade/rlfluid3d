#ifndef MATH_BASIC_H
#define MATH_BASIC_H

#include "Config.h"
#include <stdlib.h>
#include <vector>
#include <cmath>
#include <omp.h>
#include <float.h>
#include <Eigen/Eigen>

//convert function for IO.h
namespace std
{
template <typename T>
struct convert {
  template <typename T2>
  FORCE_INLINE T operator()(T2 d) const {
    return (T)d;
  }
};
}

#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif
#ifdef M_PI
#undef M_PI
#endif

PRJ_BEGIN

using namespace std;

#ifndef DOUBLE_PRECISION
#define SCALAR_MAX FLT_MAX
#define SCALAR_EPS FLT_EPSILON
#define M_PI 3.14159265358979323846f
#define EPS 1E-5f
typedef scalarF scalar;
#else
#define SCALAR_MAX DBL_MAX
#define SCALAR_EPS DBL_EPSILON
#define M_PI scalarD(3.14159265358979323846)
#define EPS scalarD(1E-9)
typedef scalarD scalar;
#endif

//define all eigen special sized matrices
#include "BeginAllEigen.h"
NAME_EIGEN_ROWCOL_ALLTYPES_SPECIALSIZE()
NAME_EIGEN_MAT_ALLTYPES_SPECIALSIZE()
NAME_EIGEN_SPECIAL_ALLTYPES_SPECIALSIZE(,f,scalarF)
NAME_EIGEN_SPECIAL_ALLTYPES_SPECIALSIZE(,d,scalarD)
#undef NAME_EIGEN_ALLTYPES
#define NAME_EIGEN_ALLTYPES(NAME,size1,size2) NAME_EIGEN(T,NAME,size1,size2)
template <typename T>
struct ScalarUtil;
template <>
struct ScalarUtil<float> {
  typedef float T;
  NAME_EIGEN_ROWCOL_ALLTYPES_SPECIALSIZE(Scalar)
  NAME_EIGEN_MAT_ALLTYPES_SPECIALSIZE(Scalar)
  NAME_EIGEN_SPECIAL_ALLTYPES_SPECIALSIZE(Scalar,,T)
  static T scalar_max;
  static T scalar_eps;
  static T scalar_inf;
  static T scalar_nanq;
};
template <>
struct ScalarUtil<double> {
  typedef double T;
  NAME_EIGEN_ROWCOL_ALLTYPES_SPECIALSIZE(Scalar)
  NAME_EIGEN_MAT_ALLTYPES_SPECIALSIZE(Scalar)
  NAME_EIGEN_SPECIAL_ALLTYPES_SPECIALSIZE(Scalar,,T)
  static T scalar_max;
  static T scalar_eps;
  static T scalar_inf;
  static T scalar_nanq;
};
#if defined(QUADMATH_SUPPORT) && defined(__GNUC__)
template <>
struct ScalarUtil<__float128> {
  typedef __float128 T;
  NAME_EIGEN_ROWCOL_ALLTYPES_SPECIALSIZE(Scalar)
  NAME_EIGEN_MAT_ALLTYPES_SPECIALSIZE(Scalar)
  NAME_EIGEN_SPECIAL_ALLTYPES_SPECIALSIZE(Scalar,,T)
  static T scalar_max;
  static T scalar_eps;
  static T scalar_inf;
  static T scalar_nanq;
};
#endif
template <>
struct ScalarUtil<int> {
  typedef int T;
  NAME_EIGEN_ROWCOL_ALLTYPES_SPECIALSIZE(Scalar)
  NAME_EIGEN_MAT_ALLTYPES_SPECIALSIZE(Scalar)
  NAME_EIGEN_SPECIAL_ALLTYPES_SPECIALSIZE(Scalar,,T)
  static T scalar_max;
  static T scalar_eps;
};
template <>
struct ScalarUtil<unsigned int> {
  typedef unsigned int T;
  NAME_EIGEN_ROWCOL_ALLTYPES_SPECIALSIZE(Scalar)
  NAME_EIGEN_MAT_ALLTYPES_SPECIALSIZE(Scalar)
  NAME_EIGEN_SPECIAL_ALLTYPES_SPECIALSIZE(Scalar,,T)
  static T scalar_max;
  static T scalar_eps;
};
template <>
struct ScalarUtil<int64_t> {
  typedef int64_t T;
  NAME_EIGEN_ROWCOL_ALLTYPES_SPECIALSIZE(Scalar)
  NAME_EIGEN_MAT_ALLTYPES_SPECIALSIZE(Scalar)
  NAME_EIGEN_SPECIAL_ALLTYPES_SPECIALSIZE(Scalar,,T)
  static T scalar_max;
  static T scalar_eps;
};
template<typename T> sizeType getEigenStride()
{
  vector<T,Eigen::aligned_allocator<T> > tester(2);
  return (sizeType)(((char*)&(tester[1]))-((char*)&(tester[0])));
}
#include "EndAllEigen.h"

//basic elementwise operation
template <typename Derived>
bool isFinite(const Eigen::MatrixBase<Derived>& x)
{
  //for(sizeType i=0; i<x.rows(); i++)
  //  for(sizeType j=0; j<x.cols(); j++)
  //    if(isnan(x(i,j)) || isinf(x(i,j)))
  //      return false;
  //return true;
  return ((x-x).array()==(x-x).array()).all();
}
template <typename Derived,typename Derived2>
bool compL(const Eigen::MatrixBase<Derived>& a,const Eigen::MatrixBase<Derived2>& b)
{
  return a.array().binaryExpr(b.array(),std::less<typename Derived::Scalar>()).all();
}
template <typename Derived,typename Derived2>
bool compLE(const Eigen::MatrixBase<Derived>& a,const Eigen::MatrixBase<Derived2>& b)
{
  return a.array().binaryExpr(b.array(),std::less_equal<typename Derived::Scalar>()).all();
}
template <typename Derived,typename Derived2>
bool compG(const Eigen::MatrixBase<Derived>& a,const Eigen::MatrixBase<Derived2>& b)
{
  return a.array().binaryExpr(b.array(),std::greater<typename Derived::Scalar>()).all();
}
template <typename Derived,typename Derived2>
bool compGE(const Eigen::MatrixBase<Derived>& a,const Eigen::MatrixBase<Derived2>& b)
{
  return a.array().binaryExpr(b.array(),std::greater_equal<typename Derived::Scalar>()).all();
}
scalarD compMax(scalarD a,scalarD b);
scalarF compMax(scalarF a,scalarF b);
sizeType compMax(sizeType a,sizeType b);
scalarD compMin(scalarD a,scalarD b);
scalarF compMin(scalarF a,scalarF b);
sizeType compMin(sizeType a,sizeType b);
template <typename Derived,typename Derived2>
Eigen::Matrix<typename Derived::Scalar,Derived::RowsAtCompileTime,Derived::ColsAtCompileTime>
compMax(const Eigen::MatrixBase<Derived>& a,const Eigen::MatrixBase<Derived2>& b)
{
  return a.cwiseMax(b);
}
template <typename Derived,typename Derived2>
Eigen::Matrix<typename Derived::Scalar,Derived::RowsAtCompileTime,Derived::ColsAtCompileTime>
compMin(const Eigen::MatrixBase<Derived>& a,const Eigen::MatrixBase<Derived2>& b)
{
  return a.cwiseMin(b);
}
template <typename Derived>
Eigen::Matrix<sizeType,Derived::RowsAtCompileTime,Derived::ColsAtCompileTime>
ceilV(const Eigen::MatrixBase<Derived>& a)
{
  return a.array().unaryExpr([](typename Derived::Scalar elem) {
    return std::ceil(elem);
  }).matrix().template cast<sizeType>();
}
template <typename Derived>
Eigen::Matrix<sizeType,Derived::RowsAtCompileTime,Derived::ColsAtCompileTime>
floorV(const Eigen::MatrixBase<Derived>& a)
{
  return a.array().unaryExpr([](typename Derived::Scalar elem) {
    return std::floor(elem);
  }).matrix().template cast<sizeType>();
}
template <typename Derived>
Derived roundV(const Eigen::MatrixBase<Derived>& a)
{
  return floorV((a.array()+0.5f).matrix());
}
//warping
template <typename T>
T handleScalarWarp(T& x,T minC,T maxC,sizeType* warpTimeRet)
{
  T warpRange=maxC-minC;
  sizeType warpTime=(sizeType)-std::floor((x-minC)/warpRange);
  if(warpTimeRet)
    *warpTimeRet=warpTime;
  x+=(T)warpTime*warpRange;
  return warpRange;
}
template <typename T>
T handleScalarWarpDiff(T& x,T warpRange,sizeType* warpTimeRet)
{
  sizeType warpTime=(sizeType)-std::floor((x+warpRange*0.5f)/warpRange);
  if(warpTimeRet)
    *warpTimeRet=warpTime;
  x+=(T)warpTime*warpRange;
  return warpRange;
}
template <typename T>
T handleScalarWarpDiff(T& x,T minC,T maxC,sizeType* warpTimeRet)
{
  return handleScalarWarpDiff<T>(x,maxC-minC,warpTimeRet);
}
//fetch sign
template <typename T>
T sgn(T val)
{
  return val < 0.0f ? -1 : val > 0.0f ? 1 : 0;
}
template <typename Derived>
Derived sgn(const Eigen::MatrixBase<Derived>& a)
{
  return a.array().unaryExpr([](typename Derived::Scalar elem) {
    return sgn(elem);
  }).matrix();
}
//align power of two
template <typename T>
T makePOT(T val,T decimate)
{
  ASSERT(decimate > 1)
  T ret=1;
  while(ret < val)
    ret*=decimate;
  return ret;
}
template <typename Derived>
Derived makePOTV(const Eigen::MatrixBase<Derived>& a,sizeType decimate)
{
  return a.array().uniaryExpr(makePOT).matrix();
}
//minmod clamp function
template <typename T>
T minmod(T a,T b,T c)
{
  if(a > 0.0f && b > 0.0f && c > 0.0f)
    return min<T>(min<T>(a,b),c);
  else if(a < 0.0f && b < 0.0f && c < 0.0f)
    return max<T>(max<T>(a,b),c);
  else return 0.0f;
}
template <typename Derived>
Eigen::Matrix<typename Derived::Scalar,Derived::RowsAtCompileTime,Derived::ColsAtCompileTime>
minmodV(const Eigen::MatrixBase<Derived>& a,
        const Eigen::MatrixBase<Derived>& b,
        const Eigen::MatrixBase<Derived>& c)
{
  Eigen::Matrix<typename Derived::Scalar,Derived::RowsAtCompileTime,Derived::ColsAtCompileTime> ret;
  ret.resize(a.rows(),a.cols());
  for(sizeType R=0; R<a.rows(); R++)
    for(sizeType C=0; C<a.cols(); C++)
      ret(R,C)=minmod(a(R,C),b(R,C),c(R,C));
  return ret;
}
//bounding box
template <typename T,int DIM=3>
struct BBox {
  static const int dim=DIM;
  typedef Eigen::Matrix<T,DIM,1> PT;
  typedef Eigen::Matrix<T,2,1> PT2;
  BBox() {
    reset();
  }
  virtual ~BBox() {}
  BBox(const PT& p):_minC(p),_maxC(p) {}
  BBox(const PT& minC,const PT& maxC):_minC(minC),_maxC(maxC) {}
  template <typename T2>
  BBox(const BBox<T2,DIM>& other) {
    copy(other);
  }
  template <typename T2>
  BBox& operator=(const BBox<T2,DIM>& other) {
    copy(other);
    return *this;
  }
  static BBox createMM(const PT& minC,const PT& maxC) {
    return BBox(minC,maxC);
  }
  static BBox createME(const PT& minC,const PT& extent) {
    return BBox(minC,minC+extent);
  }
  static BBox createCE(const PT& center,const PT& extent) {
    return BBox(center-extent,center+extent);
  }
  BBox getIntersect(const BBox& other) const {
    return createMM(compMax(_minC,other._minC),compMin(_maxC,other._maxC));
  }
  BBox getUnion(const BBox& other) const {
    return createMM(compMin(_minC,other._minC),compMax(_maxC,other._maxC));
  }
  BBox getUnion(const PT& point) const {
    return createMM(compMin(_minC,point),compMax(_maxC,point));
  }
  BBox getUnion(const PT& ctr,const T& rad) const {
    return createMM(compMin(_minC,ctr-PT::Constant(rad)),compMax(_maxC,ctr+PT::Constant(rad)));
  }
  void setIntersect(const BBox& other) {
    _minC=compMax(_minC,other._minC);
    _maxC=compMin(_maxC,other._maxC);
  }
  void setUnion(const BBox& other) {
    _minC=compMin(_minC,other._minC);
    _maxC=compMax(_maxC,other._maxC);
  }
  void setUnion(const PT& point) {
    _minC=compMin(_minC,point);
    _maxC=compMax(_maxC,point);
  }
  void setUnion(const PT& ctr,const T& rad) {
    _minC=compMin(_minC,ctr-PT::Constant(rad));
    _maxC=compMax(_maxC,ctr+PT::Constant(rad));
  }
  void setPoints(const PT& a,const PT& b,const PT& c) {
    _minC=compMin(compMin(a,b),c);
    _maxC=compMax(compMax(a,b),c);
  }
  PT minCorner() const {
    return _minC;
  }
  PT maxCorner() const {
    return _maxC;
  }
  void enlargedEps(T eps) {
    PT d=(_maxC-_minC)*(eps*0.5f);
    _minC-=d;
    _maxC+=d;
  }
  BBox enlargeEps(T eps) const {
    PT d=(_maxC-_minC)*(eps*0.5f);
    return createMM(_minC-d,_maxC+d);
  }
  void enlarged(T len,const sizeType d=DIM) {
    for(sizeType i=0; i<d; i++) {
      _minC[i]-=len;
      _maxC[i]+=len;
    }
  }
  BBox enlarge(T len,const sizeType d=DIM) const {
    BBox ret=createMM(_minC,_maxC);
    ret.enlarged(len,d);
    return ret;
  }
  PT lerp(const PT& frac) const {
    return _maxC*frac-_minC*(frac-1.0f);
  }
  bool empty() const {
    return !compL(_minC,_maxC);
  }
  template <int DIM2>
  bool containDim(const PT& point) const {
    for(int i=0; i<DIM2; i++)
      if(_minC[i] > point[i] || _maxC[i] < point[i])
        return false;
    return true;
  }
  bool contain(const BBox& other,const sizeType d=DIM) const {
    for(int i=0; i<d; i++)
      if(_minC[i] > other._minC[i] || _maxC[i] < other._maxC[i])
        return false;
    return true;
  }
  bool contain(const PT& point,const sizeType d=DIM) const {
    for(int i=0; i<d; i++)
      if(_minC[i] > point[i] || _maxC[i] < point[i])
        return false;
    return true;
  }
  bool contain(const PT& point,const T& rad,const sizeType d=DIM) const {
    for(int i=0; i<d; i++)
      if(_minC[i]+rad > point[i] || _maxC[i]-rad < point[i])
        return false;
    return true;
  }
  void reset() {
    _minC=PT::Constant( ScalarUtil<T>::scalar_max);
    _maxC=PT::Constant(-ScalarUtil<T>::scalar_max);
  }
  PT getExtent() const {
    return _maxC-_minC;
  }
  T distTo(const BBox& other,const sizeType d=DIM) const {
    PT dist=PT::Zero();
    for(sizeType i=0; i<d; i++) {
      if (other._maxC[i] < _minC[i])
        dist[i] = other._maxC[i] - _minC[i];
      else if (other._minC[i] > _maxC[i])
        dist[i] = other._minC[i] - _maxC[i];
    }
    return dist.norm();
  }
  T distTo(const PT& pt,const sizeType d=DIM) const {
    return sqrt(distToSqr(pt,d));
  }
  T distToSqr(const PT& pt,const sizeType d=DIM) const {
    PT dist=PT::Zero();
    for(sizeType i=0; i<d; i++) {
      if (pt[i] < _minC[i])
        dist[i] = pt[i] - _minC[i];
      else if (pt[i] > _maxC[i])
        dist[i] = pt[i] - _maxC[i];
    }
    return dist.squaredNorm();
  }
  PT closestTo(const PT& pt,const sizeType d=DIM) const {
    PT dist(pt);
    for(sizeType i=0; i<d; i++) {
      if (pt[i] < _minC[i])
        dist[i] = _minC[i];
      else if (pt[i] > _maxC[i])
        dist[i] = _maxC[i];
    }
    return dist;
  }
  bool intersect(const PT& p,const PT& q,const sizeType d=DIM) const {
    const T lo=1-5*ScalarUtil<T>::scalar_eps;
    const T hi=1+5*ScalarUtil<T>::scalar_eps;

    T s=0, t=1;
    for(sizeType i=0; i<d; ++i) {
      if(p[i]<q[i]) {
        T d=q[i]-p[i];
        T s0=(_minC[i]-p[i])/d, t0=(_maxC[i]-p[i])/d;
        if(s0>s) s=s0;
        if(t0<t) t=t0;
      } else if(p[i]>q[i]) {
        T d=q[i]-p[i];
        T s0=lo*(_maxC[i]-p[i])/d, t0=hi*(_minC[i]-p[i])/d;
        if(s0>s) s=s0;
        if(t0<t) t=t0;
      } else {
        if(p[i]<_minC[i] || p[i]>_maxC[i])
          return false;
      }

      if(s>t)
        return false;
    }
    return true;
  }
  bool intersect(const PT& p,const PT& q,T& s,T& t,const sizeType d=DIM) const {
    const T lo=1-5*ScalarUtil<T>::scalar_eps;
    const T hi=1+5*ScalarUtil<T>::scalar_eps;

    s=0;
    t=1;
    for(sizeType i=0; i<d; ++i) {
      if(p[i]<q[i]) {
        T d=q[i]-p[i];
        T s0=lo*(_minC[i]-p[i])/d, t0=hi*(_maxC[i]-p[i])/d;
        if(s0>s) s=s0;
        if(t0<t) t=t0;
      } else if(p[i]>q[i]) {
        T d=q[i]-p[i];
        T s0=lo*(_maxC[i]-p[i])/d, t0=hi*(_minC[i]-p[i])/d;
        if(s0>s) s=s0;
        if(t0<t) t=t0;
      } else {
        if(p[i]<_minC[i] || p[i]>_maxC[i])
          return false;
      }

      if(s>t)
        return false;
    }
    return true;
  }
  bool intersect(const BBox& other,const sizeType& d=DIM) const {
    for(sizeType i=0; i<d; i++) {
      if(_maxC[i] < other._minC[i] || other._maxC[i] < _minC[i])
        return false;
    }
    return true;
    //return compLE(_minC,other._maxC) && compLE(other._minC,_maxC);
  }
  PT2 project(const PT& a,const sizeType d=DIM) const {
    PT ctr=(_minC+_maxC)*0.5f;
    T ctrD=a.dot(ctr);
    T delta=0.0f;
    ctr=_maxC-ctr;
    for(sizeType i=0; i<d; i++)
      delta+=abs(ctr[i]*a[i]);
    return PT2(ctrD-delta,ctrD+delta);
  }
  template<typename T2>
  BBox& copy(const BBox<T2,DIM>& other) {
    for(sizeType i=0; i<DIM; i++) {
      _minC[i]=convert<T>()(other._minC[i]);
      _maxC[i]=convert<T>()(other._maxC[i]);
    }
    return *this;
  }
  T perimeter(const sizeType d=DIM) const {
    PT ext=getExtent();
    if(d <= 2)
      return ext.sum()*2.0f;
    else {
      ASSERT(d == 3);
      return (ext[0]*ext[1]+ext[1]*ext[2]+ext[0]*ext[2])*2.0f;
    }
  }
  PT _minC;
  PT _maxC;
};
typedef BBox<scalarF> BBoxf;
typedef BBox<scalarD> BBoxd;
//erf function
struct ErfCalc {
  static const sizeType ncof=28;
  static const scalarD cof[28];
  scalarD erf(scalarD x) const;
  scalarD erfc(scalarD x) const;
  scalarD erfccheb(scalarD z) const;
  scalarD inverfc(scalarD p) const;
  scalarD inverf(scalarD p) const;
};
scalarD erf(const scalarD& val);
//random
class RandEngine
{
public:
  //real
  static scalar randR01();
  static scalar randR(scalar l,scalar u);
  static scalar randSR(sizeType seed,scalar l,scalar u);
  //int
  static sizeType randSI(sizeType seed);
  static sizeType randI(sizeType l,sizeType u);
  static sizeType randSI(sizeType seed,sizeType l,sizeType u);
  //normal
  static scalar normal();
  static scalar normal(scalar mean,scalar cov);
  //settings
  static void seedTime();
  static void seed(sizeType i);
  static void useDeterministic();
  static void useNonDeterministic();
  //common random number
  static const vector<int>& getRecordedRandom(sizeType id);
  static void resetAllRecordRandom();
  static void resetRecordRandom(sizeType id);
  static void beginRecordRandom(sizeType id);
  static void endRecordRandom();
  static void clearAllRecord();
  static void clearRecord(sizeType id);
  static vector<sizeType> getRecordId();
  static void printRecordId();
};
//matlab funcs
template <typename T>
typename ScalarUtil<T>::ScalarVec3 randBary()
{
  typename ScalarUtil<T>::ScalarVec3 ret;

  ret.x()=(T)RandEngine::randR(0,1.0f);
  ret.y()=(T)RandEngine::randR(0,1.0f);
  ret.y()*=(1.0f-ret.x());
  ret.z()=1.0f-ret.x()-ret.y();

  return ret;
}
template <typename VECA,typename VECB>
Eigen::Matrix<typename VECA::Scalar,-1,1> concat(const VECA& A,const VECB& B)
{
  Eigen::Matrix<typename VECA::Scalar,-1,1> ret;
  ret.resize(A.size()+B.size());
  ret.segment(0,A.size())=A;
  ret.segment(A.size(),B.size())=B;
  return ret;
}
template <typename MATA,typename MATB>
Eigen::Matrix<typename MATA::Scalar,-1,-1> concatRow(const MATA& A,const MATB& B)
{
  ASSERT(A.cols() == B.cols())
  Eigen::Matrix<typename MATA::Scalar,-1,-1> ret;
  ret.resize(A.rows()+B.rows(),A.cols());
  ret.block(0,0,A.rows(),A.cols())=A;
  ret.block(A.rows(),0,B.rows(),B.cols())=B;
  return ret;
}
template <typename MATA,typename MATB>
Eigen::Matrix<typename MATA::Scalar,-1,-1> concatCol(const MATA& A,const MATB& B)
{
  ASSERT(A.rows() == B.rows())
  Eigen::Matrix<typename MATA::Scalar,-1,-1> ret;
  ret.resize(A.rows(),A.cols()+B.cols());
  ret.block(0,0,A.rows(),A.cols())=A;
  ret.block(0,A.cols(),B.rows(),B.cols())=B;
  return ret;
}
//bit treaking
template<typename RET>
RET countBits(const unsigned char& v)
{
  static const unsigned char BitsSetTable256[256] = {
#   define B2(n) n,     n+1,     n+1,     n+2
#   define B4(n) B2(n), B2(n+1), B2(n+1), B2(n+2)
#   define B6(n) B4(n), B4(n+1), B4(n+1), B4(n+2)
    B6(0), B6(1), B6(1), B6(2)
  };
  return BitsSetTable256[v];
}
template<typename RET>
RET countBits(const sizeType& v)
{
  RET ret=0;
  unsigned char* vc=(unsigned char*)&v;
  for(sizeType i=0; i<sizeof(sizeType); i++)
    ret+=countBits<RET>(vc[i]);
  return ret;
}
//rotation computation
template<typename T>
T getAngle3D(const typename ScalarUtil<T>::ScalarVec3& a,const typename ScalarUtil<T>::ScalarVec3& b)
{
  T denom=std::max<T>(a.norm()*b.norm(),ScalarUtil<T>::scalar_eps);
  return acos(max<T>(min<T>(a.dot(b)/denom,(T)(1.0f-ScalarUtil<T>::scalar_eps)),(T)(-1.0f+ScalarUtil<T>::scalar_eps)));
}
template<typename T>
T getAngle2D(const typename ScalarUtil<T>::ScalarVec2& a,const typename ScalarUtil<T>::ScalarVec2& b)
{
  T denom=std::max<T>(a.norm()*b.norm(),ScalarUtil<T>::scalar_eps);
  return acos(max<T>(min<T>(a.dot(b)/denom,(T)(1.0f-ScalarUtil<T>::scalar_eps)),(T)(-1.0f+ScalarUtil<T>::scalar_eps)));
}
template<typename T>
typename ScalarUtil<T>::ScalarMat3 makeRotation(const typename ScalarUtil<T>::ScalarVec3& rotation)
{
  typename ScalarUtil<T>::ScalarMat3 ret;//=(typename ScalarUtil<T>::ScalarMat3)::Identity();

  typename ScalarUtil<T>::ScalarVec3 negRot=rotation*-1.0f;
  const T cr = cos( negRot.x() );
  const T sr = sin( negRot.x() );
  const T cp = cos( negRot.y() );
  const T sp = sin( negRot.y() );
  const T cy = cos( negRot.z() );
  const T sy = sin( negRot.z() );

  ret(0,0) = ( cp*cy );
  ret(0,1) = ( cp*sy );
  ret(0,2) = ( -sp );

  const T srsp = sr*sp;
  const T crsp = cr*sp;

  ret(1,0) = ( srsp*cy-cr*sy );
  ret(1,1) = ( srsp*sy+cr*cy );
  ret(1,2) = ( sr*cp );

  ret(2,0) = ( crsp*cy+sr*sy );
  ret(2,1) = ( crsp*sy-sr*cy );
  ret(2,2) = ( cr*cp );

  return ret;
}
template<typename T>
typename ScalarUtil<T>::ScalarQuat toAngleAxis(const typename ScalarUtil<T>::ScalarVec3& q)
{
  T angle=q.norm();
  if(angle < ScalarUtil<T>::scalar_eps)
    return ScalarUtil<T>::ScalarQuat::Identity();
  else return (typename ScalarUtil<T>::ScalarQuat)Eigen::AngleAxis<T>(angle,q/angle);
}
template<typename T>
typename ScalarUtil<T>::ScalarVec3 getRotation(const typename ScalarUtil<T>::ScalarMat3& m,bool wrap=true)
{
  const typename ScalarUtil<T>::ScalarVec3 scale(m.block(0,0,1,3).norm(),
      m.block(1,0,1,3).norm(),
      m.block(2,0,1,3).norm());
  const typename ScalarUtil<T>::ScalarVec3 invScale(1.0f/scale.x(),
      1.0f/scale.y(),
      1.0f/scale.z());

  T Y = -asin(m(0,2)*invScale.x());
  const T C = cos(Y);

  T rotx, roty, X, Z;

  if (abs(C) >= ScalarUtil<T>::scalar_eps) {
    const T invC = 1.0f/C;
    rotx = m(2,2) * invC * invScale.z();
    roty = m(1,2) * invC * invScale.y();
    X = atan2( roty, rotx ) ;
    rotx = m(0,0) * invC * invScale.x();
    roty = m(0,1) * invC * invScale.y();
    Z = atan2( roty, rotx ) ;
  } else {
    X = 0.0f;
    rotx = m(1,1) * invScale.y();
    roty = -m(1,0) * invScale.y();
    Z = atan2( roty, rotx ) ;
  }

  X*=-1.0f;
  Y*=-1.0f;
  Z*=-1.0f;

  if(wrap) {
    // fix values that get below zero
    // before it would set (!) values to 360
    // that were above 360:
    if (X < 0.0f) X += ((T)M_PI)*2.0f;
    if (Y < 0.0f) Y += ((T)M_PI)*2.0f;
    if (Z < 0.0f) Z += ((T)M_PI)*2.0f;
  }

  return typename ScalarUtil<T>::ScalarVec3(X,Y,Z);
}
//camera model
template<typename T>
bool getProjectionMatrixFrustum(const typename ScalarUtil<T>::ScalarMat4& prj,
                                T& left,T& right,T& bottom,T& top,T& zNear,T& zFar)
{
  if (prj(3,0)!=0.0f || prj(3,1)!=0.0f || prj(3,2)!=-1.0f || prj(3,3)!=0.0f)
    return false;

  // note: near and far must be used inside this method instead of zNear and zFar
  // because zNear and zFar are references and they may point to the same variable.
  T temp_near = prj(2,3) / (prj(2,2)-1.0f);
  T temp_far = prj(2,3) / (1.0f+prj(2,2));

  left = temp_near * (prj(0,2)-1.0f) / prj(0,0);
  right = temp_near * (1.0f+prj(0,2)) / prj(0,0);

  top = temp_near * (1.0f+prj(1,2)) / prj(1,1);
  bottom = temp_near * (prj(1,2)-1.0f) / prj(1,1);

  zNear = temp_near;
  zFar = temp_far;

  return true;
}
template<typename T>
void getViewMatrixFrame(const typename ScalarUtil<T>::ScalarMat4& m,
                        typename ScalarUtil<T>::ScalarVec3& c,
                        typename ScalarUtil<T>::ScalarVec3& x,
                        typename ScalarUtil<T>::ScalarVec3& y,
                        typename ScalarUtil<T>::ScalarVec3& z)
{
  typename ScalarUtil<T>::ScalarMat4 mInv=m.inverse();

  typename ScalarUtil<T>::ScalarVec4 cH=mInv*typename ScalarUtil<T>::ScalarVec4(0.0f,0.0f,0.0f,1.0f);
  c=(typename ScalarUtil<T>::ScalarVec3(cH.x(),cH.y(),cH.z())*(1.0f/cH.w()));

  typename ScalarUtil<T>::ScalarVec4 xH=mInv*typename ScalarUtil<T>::ScalarVec4(1.0f,0.0f,0.0f,1.0f);
  x=(typename ScalarUtil<T>::ScalarVec3(xH.x(),xH.y(),xH.z())*(1.0f/xH.w())-c).normalized();

  typename ScalarUtil<T>::ScalarVec4 yH=mInv*typename ScalarUtil<T>::ScalarVec4(0.0f,1.0f,0.0f,1.0f);
  y=(typename ScalarUtil<T>::ScalarVec3(yH.x(),yH.y(),yH.z())*(1.0f/yH.w())-c).normalized();

  typename ScalarUtil<T>::ScalarVec4 zH=mInv*typename ScalarUtil<T>::ScalarVec4(0.0f,0.0f,-1.0f,1.0f);
  z=(typename ScalarUtil<T>::ScalarVec3(zH.x(),zH.y(),zH.z())*(1.0f/zH.w())-c).normalized();
}
template<typename T>
bool getViewMatrixFrame(const typename ScalarUtil<T>::ScalarMat4& m,
                        const typename ScalarUtil<T>::ScalarMat4& prj,
                        typename ScalarUtil<T>::ScalarVec3& c,
                        typename ScalarUtil<T>::ScalarVec3& x,
                        typename ScalarUtil<T>::ScalarVec3& y,
                        typename ScalarUtil<T>::ScalarVec3& z)
{
  T left,right,bottom,top,zNear,zFar;
  if(!getProjectionMatrixFrustum<T>(prj,left,right,bottom,top,zNear,zFar))
    return false;

  typename ScalarUtil<T>::ScalarMat4 mInv=m.inverse();
  typename ScalarUtil<T>::ScalarMat3 mRotInv=(m.block(0,0,3,3)).inverse();

  typename ScalarUtil<T>::ScalarVec4 cH=mInv*typename ScalarUtil<T>::ScalarVec4(0.0f,0.0f,0.0f,1.0f);
  c=(typename ScalarUtil<T>::ScalarVec3(cH.x(),cH.y(),cH.z())*(1.0f/cH.w()));

  x=mRotInv*(typename ScalarUtil<T>::ScalarVec3(1.0f,0.0f,0.0f));
  x=x.normalized()*right;

  y=mRotInv*(typename ScalarUtil<T>::ScalarVec3(0.0f,1.0f,0.0f));
  y=y.normalized()*top;

  z=mRotInv*(typename ScalarUtil<T>::ScalarVec3(0.0f,0.0f,-1.0f));
  z=z.normalized()*zNear;

  return true;
}
template<typename T>
typename ScalarUtil<T>::ScalarVec3
transformHomo(const typename ScalarUtil<T>::ScalarMat4& m,
              const typename ScalarUtil<T>::ScalarVec3& p)
{
  return m.block(0,0,3,3)*p+m.block(0,3,3,1);
}
template<typename T>
typename ScalarUtil<T>::ScalarVec3
transformHomoInv(const typename ScalarUtil<T>::ScalarMat4& m,
                 const typename ScalarUtil<T>::ScalarVec3& p)
{
  return m.block(0,0,3,3).inverse()*(p-m.block(0,3,3,1));
}
template<typename T>
typename ScalarUtil<T>::ScalarMat4
getOrtho2D(const typename ScalarUtil<T>::ScalarVec3& minC,
           const typename ScalarUtil<T>::ScalarVec3& maxC)
{
  typename ScalarUtil<T>::ScalarMat4 mt=typename ScalarUtil<T>::ScalarMat4::Zero();
  mt(0,0)=2.0f/(maxC.x()-minC.x());
  mt(0,3)=(minC.x()+maxC.x())/(minC.x()-maxC.x());

  mt(1,1)=2.0f/(maxC.y()-minC.y());
  mt(1,3)=(minC.y()+maxC.y())/(minC.y()-maxC.y());

  mt(2,2)=1.0f;
  mt(3,3)=1.0f;
  return mt;
}

//interpolation
#include "Interp.h"

//openmp
#ifdef _MSC_VER
#define STRINGIFY_OMP(X) X
#define PRAGMA __pragma
#else
#define STRINGIFY_OMP(X) #X
#define PRAGMA _Pragma
#endif

#define OMP_PARALLEL_FOR_ PRAGMA(STRINGIFY_OMP(omp parallel for num_threads(OmpSettings::getOmpSettings().nrThreads()) schedule(static)))
#define OMP_PARALLEL_FOR_I(...) PRAGMA(STRINGIFY_OMP(omp parallel for num_threads(OmpSettings::getOmpSettings().nrThreads()) schedule(static) __VA_ARGS__))
#define OMP_PARALLEL_FOR_X(X) PRAGMA(STRINGIFY_OMP(omp parallel for num_threads(X) schedule(static)))
#define OMP_PARALLEL_FOR_XI(X,...) PRAGMA(STRINGIFY_OMP(omp parallel for num_threads(X) schedule(static) __VA_ARGS__))
#define OMP_ADD(...) reduction(+: __VA_ARGS__)
#define OMP_PRI(...) private(__VA_ARGS__)
#define OMP_FPRI(...) firstprivate(__VA_ARGS__)
#define OMP_ATOMIC_	PRAGMA(STRINGIFY_OMP(omp atomic))
#define OMP_ATOMIC_CAPTURE_	PRAGMA(STRINGIFY_OMP(omp atomic capture))
#define OMP_CRITICAL_ PRAGMA(STRINGIFY_OMP(omp critical))
#define OMP_FLUSH_(X) PRAGMA(STRINGIFY_OMP(omp flush(X)))
struct OmpSettings {
public:
  static const OmpSettings& getOmpSettings();
  static OmpSettings& getOmpSettingsNonConst();
  int nrThreads() const;
  int threadId() const;
  void setNrThreads(int nr);
  void useAllThreads();
private:
  OmpSettings();
  int _nrThreads;
  static OmpSettings _ompSettings;
};

PRJ_END

#endif
