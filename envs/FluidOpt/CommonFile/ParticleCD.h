#ifndef PARTICLE_CD_H
#define PARTICLE_CD_H

#include "GridBasic.h"
#include "CollisionFunc.h"

PRJ_BEGIN

//collision detection
template <typename T,typename PS_TYPE,typename EXTRACT_POS>
class CollisionInterfaceTpl : public Serializable
{
public:
  typedef typename ScalarUtil<T>::ScalarVec3 Vec3Type;
  CollisionInterfaceTpl():Serializable(typeid(CollisionInterfaceTpl<T,PS_TYPE,EXTRACT_POS>).name()) {}
  virtual ~CollisionInterfaceTpl() {}
  virtual bool read(istream& is,IOData* dat) override {
    readVector(_pid,is);
    readVector(_pidBK,is);
    readVector(_index,is);
    readVector(_indexBK,is);
    _gridStartEnd.read(is);
    readBinaryData(_maxIndex,is);
    readBinaryData(_gridLen,is);
    return is.good();
  }
  virtual bool write(ostream& os,IOData* dat) const override {
    writeVector(_pid,os);
    writeVector(_pidBK,os);
    writeVector(_index,os);
    writeVector(_indexBK,os);
    _gridStartEnd.write(os);
    writeBinaryData(_maxIndex,os);
    writeBinaryData(_gridLen,os);
    return os.good();
  }
  virtual boost::shared_ptr<Serializable> copy() const override {
    return boost::shared_ptr<Serializable>(new CollisionInterfaceTpl);
  }
  void reset(const BBox<T>& bb,const Vec3i& nrCell) {
    _gridStartEnd.reset(nrCell,bb,pair<sizeType,sizeType>(0,0),false,16);
    _maxIndex=_gridStartEnd.getNrCell()-Vec3i::Ones();
    if(_gridStartEnd.getDim() == 2)
      _gridLen=min<T>(_gridStartEnd.getCellSize().x(),_gridStartEnd.getCellSize().y());
    else
      _gridLen=_gridStartEnd.getCellSize().minCoeff();
    _warpMode=Vec3i::Zero();
  }
  void reset(const BBox<T>& bb,const Vec3Type& cellSz) {
    Vec3i nrCell=ceilV((Vec3Type)(bb.getExtent().array()/cellSz.array()).matrix());
    reset(bb,nrCell);
    _warpMode=Vec3i::Zero();
  }
  void resize(const sizeType& nrParticle) {
    _pid.resize(nrParticle);
    _pidBK.resize(nrParticle);
    _index.resize(nrParticle);
    _indexBK.resize(nrParticle);
  }
  virtual void prepare(const PS_TYPE& pSet) {
    const sizeType n=pSet.size();
    if(n == 0)
      return;
    if((sizeType)_pid.size() != n)
      resize(pSet.size());

    calcHash(n,pSet);
    radixSort(n,&(_pid[0]),&(_pidBK[0]),&(_index[0]),&(_indexBK[0]));
    calcStartEnd(n);
  }
  //fast version
  bool fill(const PS_TYPE& pSet,const Vec3Type& pos,const T& radSqr,CollisionFuncEarlyStop<typename PS_TYPE::value_type>& f) const {
    Vec3Type radVec=Vec3Type::Zero();
    sizeType DIM=_gridStartEnd.getDim();
    radVec.segment(0,DIM).setConstant(sqrt(radSqr));
    Vec3Type pos0=pos-radVec;
    Vec3Type pos1=pos+radVec;
    //warp compensate
    const Vec3i& nrP=_gridStartEnd.getNrPoint();
    const Vec3i& stride=_gridStartEnd.getStride();
    Vec3i warpCountMax=Vec3i::Zero();
    Vec3Type warpRange;
    for(sizeType d=0; d<DIM; d++)
      if(_warpMode[d] > 0) {
        //adjust bound
        T pos0Old=pos0[d];
        warpRange[d]=handleScalarWarp<T>(pos0[d],_gridStartEnd.getBB()._minC[d],_gridStartEnd.getBB()._maxC[d],NULL);
        pos1[d]+=pos0[d]-pos0Old;
        //adjust maxC
        handleScalarWarp<T>(pos1[d],_gridStartEnd.getBB()._minC[d],_gridStartEnd.getBB()._maxC[d],&warpCountMax[d]);
        ASSERT(warpCountMax[d] <= 0)
        warpCountMax[d]*=-1;
      }
    //increase one last cell
    Vec3i id0=floorV(_gridStartEnd.getIndexFracSafe(pos0));
    Vec3i id1=ceilV(_gridStartEnd.getIndexFracSafe(pos1));
    for(sizeType d=0; d<DIM; d++) {
      if(id1[d] > nrP[d]) {
        if(_warpMode[d] > 0) {
          id1[d]-=nrP[d];
          warpCountMax[d]++;
        } else id1[d]=min<sizeType>(id1[d],nrP[d]);
      }
      if(warpCountMax[d] == 0 && id1[d] <= id0[d])
        return false;
    }
    //setup iterator
    //beg
    Vec3i idBeg=id0;
    Vec3i warpCountBeg=Vec3i::Zero();
    //end
    Vec3i idEnd=id0;
    idEnd[0]=id1[0];
    Vec3i warpCountEnd=Vec3i::Zero();
    warpCountEnd[0]=warpCountMax[0];
    sizeType off=idBeg.dot(stride);
    //run iterator loop
    Vec3Type delta,delta2;
    while(!(idBeg == idEnd && warpCountBeg == warpCountEnd)) {
      //add neigh
      const pair<sizeType,sizeType>& range=_gridStartEnd.get(off);
      for(sizeType k=range.first; k < range.second; k++) {
        delta=EXTRACT_POS::extract(pSet[_index[k]])-pos;
        warp(delta2=delta,DIM);
        if(delta2.squaredNorm() < radSqr)
          if(f(EXTRACT_POS::add(pSet[_index[k]],delta2-delta),_index[k]))
            return true;
      }
      //advance iterator
      for(sizeType d=DIM-1; d>=0; d--) {
        idBeg[d]++;
        off+=stride[d];
        if(idBeg[d] == nrP[d] && warpCountBeg[d] < warpCountMax[d]) {
          idBeg[d]-=nrP[d];
          off-=nrP[d]*stride[d];
          warpCountBeg[d]++;
        }
        if(d == 0)
          break;
        if(idBeg[d] == id1[d] && warpCountBeg[d] == warpCountMax[d]) {
          idBeg[d]=id0[d];
          off+=(id0[d]-id1[d])*stride[d];
          warpCountBeg[d]=0;
        } else break;
      }
    }
    return false;
  }
  bool fill3D(const PS_TYPE& pSet,const Vec3Type& pos,const T& radSqr,CollisionFuncEarlyStop<typename PS_TYPE::value_type>& f) const {
    return fill(pSet,pos,radSqr,f);  //deprecated
  }
  bool fill2D(const PS_TYPE& pSet,const Vec3Type& pos,const T& radSqr,CollisionFuncEarlyStop<typename PS_TYPE::value_type>& f) const {
    return fill(pSet,pos,radSqr,f);  //deprecated
  }
  //brute force version
  bool fillBF(const PS_TYPE& pSet,const Vec3Type& pos,const T& radSqr,CollisionFuncEarlyStop<typename PS_TYPE::value_type>& f) const {
    const sizeType n=pSet.size();
    sizeType DIM=_gridStartEnd.getDim();
    Vec3Type delta,delta2;
    for(sizeType k=0; k < n; k++) {
      delta=EXTRACT_POS::extract(pSet[k])-pos;
      warp(delta2=delta,DIM);
      if(delta2.squaredNorm() < radSqr)
        if(f(EXTRACT_POS::assign(pSet[_index[k]],delta2-delta),k))
          return true;
    }
    return false;
  }
  bool fill3DBF(const PS_TYPE& pSet,const Vec3Type& pos,const T& radSqr,CollisionFuncEarlyStop<typename PS_TYPE::value_type>& f) const {
    return fillBF(pSet,pos,radSqr,f);  //deprecated
  }
  bool fill2DBF(const PS_TYPE& pSet,const Vec3Type& pos,const T& radSqr,CollisionFuncEarlyStop<typename PS_TYPE::value_type>& f) const {
    return fillBF(pSet,pos,radSqr,f);  //deprecated
  }
  //neigh check
  bool hasNeighBF(const PS_TYPE& pSet,const Vec3Type& pos,const T& radSqr) const {
    CollisionFuncAlwaysTrue<typename PS_TYPE::value_type> f;
    return fillBF(pSet,pos,radSqr,f);
  }
  bool hasNeigh(const PS_TYPE& pSet,const Vec3Type& pos,const T& radSqr) const {
    CollisionFuncAlwaysTrue<typename PS_TYPE::value_type> f;
    return fill(pSet,pos,radSqr,f);
  }
  bool hasNeigh3D(const PS_TYPE& pSet,const Vec3Type& pos,const T& radSqr) const {
    return hasNeigh(pSet,pos,radSqr);
  }
  bool hasNeigh2D(const PS_TYPE& pSet,const Vec3Type& pos,const T& radSqr) const {
    return hasNeigh(pSet,pos,radSqr);
  }
  //version that does not return
  void fill(const PS_TYPE& pSet,const Vec3Type& pos,const T& radSqr,CollisionFunc<typename PS_TYPE::value_type>& f) const {
    CollisionFuncWrapper<typename PS_TYPE::value_type> wrapper(f);
    fill(pSet,pos,radSqr,wrapper);
  }
  void fill3D(const PS_TYPE& pSet,const Vec3Type& pos,const T& radSqr,CollisionFunc<typename PS_TYPE::value_type>& f) const {
    fill(pSet,pos,radSqr,f);
  }
  void fill2D(const PS_TYPE& pSet,const Vec3Type& pos,const T& radSqr,CollisionFunc<typename PS_TYPE::value_type>& f) const {
    fill(pSet,pos,radSqr,f);
  }
  //brute force version
  void fillBF(const PS_TYPE& pSet,const Vec3Type& pos,const T& radSqr,CollisionFunc<typename PS_TYPE::value_type>& f) const {
    CollisionFuncWrapper<typename PS_TYPE::value_type> wrapper(f);
    fillBF(pSet,pos,radSqr,wrapper);
  }
  void fill3DBF(const PS_TYPE& pSet,const Vec3Type& pos,const T& radSqr,CollisionFunc<typename PS_TYPE::value_type>& f) const {
    fillBF(pSet,pos,radSqr,f);
  }
  void fill2DBF(const PS_TYPE& pSet,const Vec3Type& pos,const T& radSqr,CollisionFunc<typename PS_TYPE::value_type>& f) const {
    fillBF(pSet,pos,radSqr,f);
  }
  //get neigh
  set<sizeType> getNeighBF(const PS_TYPE& pSet,const Vec3Type& pos,const T& radSqr) const {
    CollisionFuncRecord<typename PS_TYPE::value_type> f;
    fillBF(pSet,pos,radSqr,f);
    return f._neigh;
  }
  set<sizeType> getNeigh(const PS_TYPE& pSet,const Vec3Type& pos,const T& radSqr) const {
    CollisionFuncRecord<typename PS_TYPE::value_type> f;
    fill(pSet,pos,radSqr,f);
    return f._neigh;
  }
  //misc
  sizeType getDim() const {
    return _gridStartEnd.getDim();
  }
  BBox<T> getBB() const {
    return _gridStartEnd.getBB();
  }
  Vec3i getNrCell() const {
    return _gridStartEnd.getNrCell();
  }
  const Grid<pair<sizeType,sizeType>,T>& getStartEndGrid() const {
    return _gridStartEnd;
  }
  const std::vector<sizeType,Eigen::aligned_allocator<sizeType> >& getIndex() const {
    return _index;
  }
  //warpMode
  void setWarp(unsigned char D) {
    _warpMode[D]=1;
  }
  void clearWarp(unsigned char D) {
    _warpMode[D]=0;
  }
  const Vec3i& getWarp() const {
    return _warpMode;
  }
public:
  void warp(Vec3Type& delta,sizeType DIM) const {
    for(sizeType d=0; d<DIM; d++)
      if(_warpMode[d] > 0)
        handleScalarWarpDiff(delta[d],_gridStartEnd.getBB()._minC[d],_gridStartEnd.getBB()._maxC[d],NULL);
  }
  static void radixSort(const sizeType& n,sizeType* pid,sizeType* pidBK,sizeType* index,sizeType* indexBK) {
    sizeType bucket[8][256];
    memset(bucket,0,sizeof(sizeType)*256*8);

    //count number of values in a bucket
    for(sizeType i=0; i<n; i++) {
      bucket[0][(pid[i]>>0LL )&255LL]++;
      bucket[1][(pid[i]>>8LL )&255LL]++;
      bucket[2][(pid[i]>>16LL)&255LL]++;
      bucket[3][(pid[i]>>24LL)&255LL]++;
      bucket[4][(pid[i]>>32LL)&255LL]++;
      bucket[5][(pid[i]>>40LL)&255LL]++;
      bucket[6][(pid[i]>>48LL)&255LL]++;
      bucket[7][(pid[i]>>56LL)&255LL]++;
      index[i]=i;
    }

    //accumulate buckets
    for(sizeType i=1; i<256; i++) {
      bucket[0][i]+=bucket[0][i-1];
      bucket[1][i]+=bucket[1][i-1];
      bucket[2][i]+=bucket[2][i-1];
      bucket[3][i]+=bucket[3][i-1];
      bucket[4][i]+=bucket[4][i-1];
      bucket[5][i]+=bucket[5][i-1];
      bucket[6][i]+=bucket[6][i-1];
      bucket[7][i]+=bucket[7][i-1];
    }
    for(sizeType i=255; i>=1; i--) {
      bucket[0][i]=bucket[0][i-1];
      bucket[1][i]=bucket[1][i-1];
      bucket[2][i]=bucket[2][i-1];
      bucket[3][i]=bucket[3][i-1];
      bucket[4][i]=bucket[4][i-1];
      bucket[5][i]=bucket[5][i-1];
      bucket[6][i]=bucket[6][i-1];
      bucket[7][i]=bucket[7][i-1];
    }
    bucket[0][0]=0;
    bucket[1][0]=0;
    bucket[2][0]=0;
    bucket[3][0]=0;
    bucket[4][0]=0;
    bucket[5][0]=0;
    bucket[6][0]=0;
    bucket[7][0]=0;

    //redistribute
    for(sizeType p=0; p<8; p++) {
      sizeType k=p*8;
      for(sizeType i=0; i<n; i++) {
        sizeType& pos=bucket[p][(pid[i]>>(sizeType)k)&255LL];
        indexBK[pos]=index[i];
        pidBK[pos]=pid[i];
        pos++;
      }

      std::swap(pid,pidBK);
      std::swap(index,indexBK);
    }
  }
  void calcHash(const sizeType& n,const PS_TYPE& pSet) {
    const sizeType maxVal=_gridStartEnd.getSzLinear();
    sizeType DIM=_gridStartEnd.getDim();
    OMP_PARALLEL_FOR_
    for(sizeType i=0; i<n; i++) {
      Vec3Type pos=EXTRACT_POS::extract(pSet[i]);
      for(sizeType d=0; d<DIM; d++)
        if(_warpMode[d] > 0)
          handleScalarWarp<T>(pos[d],_gridStartEnd.getBB()._minC[d],_gridStartEnd.getBB()._maxC[d],NULL);
      Vec3i base=floorV(_gridStartEnd.getIndexFracSafe(pos));
      _pid[i]=_gridStartEnd.getIndex(base);
      ASSERT(_pid[i] >= 0 && _pid[i] <= maxVal)
    }
  }
  void calcStartEnd(const sizeType& n) {
    const sizeType maxVal=_gridStartEnd.getSzLinear();
    _gridStartEnd.init(pair<sizeType,sizeType>(0,0));
    OMP_PARALLEL_FOR_
    for(sizeType i=1; i<n; i++) {
      if(_pid[i] != _pid[i-1]) {
        if(_pid[i] != maxVal)
          _gridStartEnd.get(_pid[i]).first=i;
        _gridStartEnd.get(_pid[i-1]).second=i;
      }
    }

    if(_pid[0] != maxVal)
      _gridStartEnd.get(_pid[0]).first=0;
    if(_pid[n-1] != maxVal)
      _gridStartEnd.get(_pid[n-1]).second=n;
  }
  std::vector<sizeType,Eigen::aligned_allocator<sizeType> > _pid;
  std::vector<sizeType,Eigen::aligned_allocator<sizeType> > _pidBK;
  std::vector<sizeType,Eigen::aligned_allocator<sizeType> > _index;
  std::vector<sizeType,Eigen::aligned_allocator<sizeType> > _indexBK;
  Grid<pair<sizeType,sizeType>,T> _gridStartEnd;
  Vec3i _maxIndex;
  T _gridLen;
  //warp
  Vec3i _warpMode;
};
template <int DIM>
void debugParticleCollisionInterface(sizeType N,const Vec3i& warpMode);
template <int DIM>
void debugParticleCollisionInterfaceAllWarp(sizeType N);
void debugParticleCollisionInterfaceAllDIM(sizeType N);

template <typename T,typename PS_TYPE>
class CollisionInterface : public CollisionInterfaceTpl<T,PS_TYPE,ExtractPosParticle<typename PS_TYPE::ParticleType> > {};

typedef CollisionInterfaceTpl<scalarF,std::vector<Vec3f,Eigen::aligned_allocator<Vec3f> >,ExtractPosDirect<Vec3f> > CollisionInterfaceDirectF;
typedef CollisionInterfaceTpl<scalarD,std::vector<Vec3d,Eigen::aligned_allocator<Vec3d> >,ExtractPosDirect<Vec3d> > CollisionInterfaceDirectD;
typedef CollisionInterfaceTpl<scalar,std::vector<Vec3,Eigen::aligned_allocator<Vec3> >,ExtractPosDirect<Vec3> > CollisionInterfaceDirect;

PRJ_END

#endif
