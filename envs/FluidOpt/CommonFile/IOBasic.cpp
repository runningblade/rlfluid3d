#include "IOBasic.h"
#include <boost/unordered_map.hpp>
#include <boost/pool/pool_alloc.hpp>

PRJ_BEGIN

struct IOData {
public:
  typedef boost::shared_ptr<Serializable> SPTR;
  typedef boost::fast_pool_allocator<std::pair<const SPTR,sizeType> > ALLOC_MAP_WR;
  typedef boost::fast_pool_allocator<std::pair<const sizeType,SPTR> > ALLOC_MAP_RD;
  typedef boost::fast_pool_allocator<std::pair<const string,SPTR> > ALLOC_TYPE_SET;
  typedef boost::unordered_map<SPTR,sizeType,boost::hash<SPTR>,std::equal_to<SPTR>,ALLOC_MAP_WR> MAP_WR;
  typedef boost::unordered_map<sizeType,SPTR,boost::hash<sizeType>,std::equal_to<sizeType>,ALLOC_MAP_RD> MAP_RD;
  typedef boost::unordered_map<string,SPTR,boost::hash<string>,std::equal_to<string>,ALLOC_TYPE_SET> TYPE_SET;
public:
  IOData():_index(0) {}
  sizeType getIndex() {
    return _index++;
  }
  void registerType(boost::shared_ptr<Serializable> type) {
    ASSERT_MSG(!type->type().empty(),"Given type doesn't support shared_ptr serialization!");
    if(_tSet.find(type->type()) != _tSet.end())
      ASSERT_MSGV(typeid(*_tSet[type->type()]) == typeid(*type),"Conflicit type id: %s",type->type().c_str())
      _tSet[type->type()]=type;
  }
  template <typename T>void createNew(istream& is,boost::shared_ptr<T>& val) const {
    string type;
    readBinaryData(type,is);
    ASSERT_MSG(type != "","Type not found!")
    for(TYPE_SET::const_iterator beg=_tSet.begin(),end=_tSet.end(); beg!=end; beg++)
      if(beg->first == type) {
        val=boost::dynamic_pointer_cast<T>(beg->second->copy());
        return;
      }
    ASSERT_MSG(false,"Cannot find compatible type!")
  }
  MAP_WR _ptrMapWr;
  MAP_RD _ptrMapRd;
private:
  sizeType _index;
  TYPE_SET _tSet;
};
boost::shared_ptr<IOData> getIOData()
{
  return boost::shared_ptr<IOData>(new IOData);
}
ostream& writeSerializableData(boost::shared_ptr<Serializable> val,ostream& os,IOData* dat)
{
  ASSERT_MSG(dat,"You must provide pool for serialize shared_ptr!")
  if(val.get() == NULL) {
    writeBinaryData((sizeType)-1,os);
    return os;
  }
  boost::shared_ptr<Serializable> ptrS=boost::dynamic_pointer_cast<Serializable>(val);
  ASSERT_MSG(ptrS,"Not serializable type!")
  IOData::MAP_WR::const_iterator it=dat->_ptrMapWr.find(ptrS);
  if(it == dat->_ptrMapWr.end()) {
    sizeType id=dat->getIndex();
    writeBinaryData(id,os,dat);
    writeBinaryData(val->type(),os,dat);
    dat->_ptrMapWr[val]=id;
    writeBinaryData(*val,os,dat);
  } else {
    writeBinaryData(it->second,os,dat);
  }
  return os;
}
istream& readSerializableData(boost::shared_ptr<Serializable>& val,istream& is,IOData* dat)
{
  ASSERT_MSG(dat,"You must provide pool for serialize shared_ptr!")
  sizeType id;
  readBinaryData(id,is,dat);
  if(id == (sizeType)-1) {
    val.reset((Serializable*)NULL);
    return is;
  }
  IOData::MAP_RD::const_iterator it=dat->_ptrMapRd.find(id);
  if(it == dat->_ptrMapRd.end()) {
    dat->createNew(is,val);
    dat->_ptrMapRd[id]=val;
    readBinaryData(*val,is,dat);
  } else {
    val=boost::dynamic_pointer_cast<Serializable>(dat->_ptrMapRd[id]);
  }
  return is;
}
void registerType(IOData* dat,boost::shared_ptr<Serializable> type)
{
  dat->registerType(type);
}

#define IO_BASIC(T)                                                                                             \
ostream& writeBinaryData(const T& val,ostream& os,IOData* dat){os.write((char*)&val,sizeof(T));return os;}	\
istream& readBinaryData(T& val,istream& is,IOData* dat){is.read((char*)&val,sizeof(T));return is;}
IO_BASIC(char)
IO_BASIC(unsigned char)
IO_BASIC(short)
IO_BASIC(unsigned short)
IO_BASIC(int)
IO_BASIC(unsigned int)
//IO_BASIC(scalarD)
IO_BASIC(bool)
IO_BASIC(sizeType)
IO_BASIC(TripF)
IO_BASIC(TripD)
#undef IO_BASIC

istream& readBinaryData(string& str,istream& is,IOData* dat)
{
  sizeType len;
  readBinaryData(len,is,dat);
  str.assign(len,' ');
  is.read(&(str[0]),len);
  return is;
}
ostream& writeBinaryData(const string& str,ostream& os,IOData* dat)
{
  const sizeType len=(sizeType)str.length();
  writeBinaryData(len,os,dat);
  return os.write(str.c_str(),len);
}

//type invariant IO
#if defined(FOUND_QUADMATH) && defined(__GNUC__)
#include "quadmath.h"
typedef __float128 FLOAT_TO_TYPE;
#else
typedef double FLOAT_TO_TYPE;
#endif
template <typename T>
struct IOType {
  typedef T TO_TYPE;
};
template <>
struct IOType<scalarF> {
  typedef FLOAT_TO_TYPE TO_TYPE;
};
template <>
struct IOType<scalarD> {
  typedef FLOAT_TO_TYPE TO_TYPE;
};

ostream& writeBinaryData(scalarF val,ostream& os,IOData* dat)
{
  IOType<scalarF>::TO_TYPE valD=val;
  os.write((char*)&valD,sizeof(IOType<scalarF>::TO_TYPE));
  return os;
}
istream& readBinaryData(scalarF& val,istream& is,IOData* dat)
{
  IOType<scalarF>::TO_TYPE valD;
  is.read((char*)&valD,sizeof(IOType<scalarF>::TO_TYPE));
  val=(scalarF)valD;
  return is;
}
ostream& writeBinaryData(scalarD val,ostream& os,IOData* dat)
{
  IOType<scalarD>::TO_TYPE valD=convert<IOType<scalarD>::TO_TYPE>()(val);
  os.write((char*)&valD,sizeof(IOType<scalarD>::TO_TYPE));
  return os;
}
istream& readBinaryData(scalarD& val,istream& is,IOData* dat)
{
  IOType<scalarD>::TO_TYPE valD;
  is.read((char*)&valD,sizeof(IOType<scalarD>::TO_TYPE));
  val=scalarD(valD);
  return is;
}

//io for fixed matrix
#define NO_CONFLICT
#define FIXED_ONLY
#include "BeginAllEigen.h"
//redefine atomic operation
#undef NAME_EIGEN
#define NAME_EIGEN(type,NAME,size1,size2) \
ostream& writeBinaryData(const Eigen::Matrix<type,size1,size2>& v,ostream& os,IOData* dat) \
{ \
  typedef Eigen::Matrix<type,size1,size2> TYPE; \
  typedef IOType<type>::TO_TYPE TO_TYPE;  \
  sizeType d0=TYPE::RowsAtCompileTime;os.write((char*)&d0,sizeof(sizeType)); \
  sizeType d1=TYPE::ColsAtCompileTime;os.write((char*)&d1,sizeof(sizeType)); \
  for(sizeType r=0;r<TYPE::RowsAtCompileTime;r++) \
  for(sizeType c=0;c<TYPE::ColsAtCompileTime;c++) \
  { \
    TO_TYPE val=convert<TO_TYPE>()(v(r,c)); \
    os.write((char*)&val,sizeof(TO_TYPE)); \
  } \
  return os; \
} \
istream& readBinaryData(Eigen::Matrix<type,size1,size2>& v,istream& is,IOData* dat) \
{ \
  typedef Eigen::Matrix<type,size1,size2> TYPE; \
  typedef IOType<type>::TO_TYPE TO_TYPE;  \
  sizeType d0;is.read((char*)&d0,sizeof(sizeType));ASSERT(d0 == TYPE::RowsAtCompileTime) \
  sizeType d1;is.read((char*)&d1,sizeof(sizeType));ASSERT(d1 == TYPE::ColsAtCompileTime) \
  for(sizeType r=0;r<TYPE::RowsAtCompileTime;r++) \
  for(sizeType c=0;c<TYPE::ColsAtCompileTime;c++) \
  { \
    TO_TYPE val; \
    is.read((char*)&val,sizeof(TO_TYPE)); \
    v(r,c)=TYPE::Scalar(val); \
  } \
  return is; \
}
//realize
NAME_EIGEN_ROWCOL_ALLTYPES_SPECIALSIZE()
NAME_EIGEN_MAT_ALLTYPES_SPECIALSIZE()
#include "EndAllEigen.h"

//io for non-fixed matrix
#define NO_CONFLICT
#define NON_FIXED_ONLY
#include "BeginAllEigen.h"
//redefine atomic operation
#undef NAME_EIGEN
#define NAME_EIGEN(type,NAME,size1,size2) \
ostream& writeBinaryData(const Eigen::Matrix<type,size1,size2>& v,ostream& os,IOData* dat) \
{ \
  typedef IOType<type>::TO_TYPE TO_TYPE;  \
  sizeType d0=v.rows(); \
  sizeType d1=v.cols(); \
  os.write((char*)&d0,sizeof(sizeType)); \
  os.write((char*)&d1,sizeof(sizeType)); \
  if(d0 <= 1 || d1 <= 1) { \
    if(d0*d1 > 0)os.write((char*)(v.cast<TO_TYPE>().eval().data()),sizeof(TO_TYPE)*d0*d1); \
    return os; \
  } \
  Eigen::Matrix<TO_TYPE,-1,1> sub; \
  sub.setZero(d1); \
  for(sizeType r=0; r<d0; r++) { \
    sub=v.row(r).cast<TO_TYPE>(); \
    if(d1 > 0)os.write((char*)(sub.data()),sizeof(TO_TYPE)*d1); \
  } \
  return os; \
} \
istream& readBinaryData(Eigen::Matrix<type,size1,size2>& v,istream& is,IOData* dat) \
{ \
  typedef Eigen::Matrix<type,size1,size2> TYPE; \
  typedef IOType<type>::TO_TYPE TO_TYPE;  \
  typedef Eigen::Matrix<TO_TYPE,TYPE::RowsAtCompileTime,TYPE::ColsAtCompileTime> TONAME; \
  sizeType d0; \
  is.read((char*)&d0,sizeof(sizeType)); \
  sizeType d1; \
  is.read((char*)&d1,sizeof(sizeType)); \
  v.resize(d0,d1); \
  if(d0 <= 1 || d1 <= 1) { \
    TONAME vcast(d0,d1); \
    if(d0*d1 > 0)is.read((char*)(vcast.data()),sizeof(TO_TYPE)*d0*d1); \
    v=vcast.cast<TYPE::Scalar>(); \
    return is; \
  } \
  Eigen::Matrix<TO_TYPE,-1,1> sub; \
  sub.setZero(d1); \
  for(sizeType r=0; r<d0; r++) { \
    if(d1 > 0)is.read((char*)(sub.data()),sizeof(TO_TYPE)*d1); \
    v.row(r)=sub.cast<TYPE::Scalar>(); \
  } \
  return is; \
}
//realize
NAME_EIGEN_ROWCOL_ALLTYPES_SPECIALSIZE()
#include "EndAllEigen.h"

#define IO_FIXED_QUAT(NAMEQ,NAMET,NAMEA,TO_TYPE)			\
ostream& writeBinaryData(const NAMEQ& v,ostream& os,IOData* dat)	\
{									\
	TO_TYPE val=convert<TO_TYPE>()(v.w());				\
	os.write((char*)&val,sizeof(TO_TYPE));				\
	val=convert<TO_TYPE>()(v.x());					\
	os.write((char*)&val,sizeof(TO_TYPE));				\
	val=convert<TO_TYPE>()(v.y());					\
	os.write((char*)&val,sizeof(TO_TYPE));				\
	val=convert<TO_TYPE>()(v.z());					\
	os.write((char*)&val,sizeof(TO_TYPE));				\
	return os;							\
}									\
istream& readBinaryData(NAMEQ& v,istream& is,IOData* dat)		\
{									\
	TO_TYPE val;							\
	is.read((char*)&val,sizeof(TO_TYPE));				\
	v.w()=NAMEQ::Scalar(val);					\
	is.read((char*)&val,sizeof(TO_TYPE));				\
	v.x()=NAMEQ::Scalar(val);					\
	is.read((char*)&val,sizeof(TO_TYPE));				\
	v.y()=NAMEQ::Scalar(val);					\
	is.read((char*)&val,sizeof(TO_TYPE));				\
	v.z()=NAMEQ::Scalar(val);					\
	return is;							\
}									\
ostream& writeBinaryData(const NAMET& v,ostream& os,IOData* dat)	\
{									\
	TO_TYPE val=convert<TO_TYPE>()(v.x());				\
	os.write((char*)&val,sizeof(TO_TYPE));				\
	val=convert<TO_TYPE>()(v.y());					\
	os.write((char*)&val,sizeof(TO_TYPE));				\
	val=convert<TO_TYPE>()(v.z());					\
	os.write((char*)&val,sizeof(TO_TYPE));				\
	return os;							\
}									\
istream& readBinaryData(NAMET& v,istream& is,IOData* dat)		\
{									\
	TO_TYPE val;							\
	is.read((char*)&val,sizeof(TO_TYPE));				\
	v.x()=NAMEQ::Scalar(val);					\
	is.read((char*)&val,sizeof(TO_TYPE));				\
	v.y()=NAMEQ::Scalar(val);					\
	is.read((char*)&val,sizeof(TO_TYPE));				\
	v.z()=NAMEQ::Scalar(val);					\
	return is;							\
}									\
ostream& writeBinaryData(const NAMEA& v,ostream& os,IOData* dat)	\
{                                                                       \
  Eigen::Matrix<NAMEA::Scalar,3,3> l=v.linear();                        \
  Eigen::Matrix<NAMEA::Scalar,3,1> t=v.translation();                   \
  writeBinaryData(l,os,dat);                                            \
  writeBinaryData(t,os,dat);                                            \
  return os;                                                            \
}                                                                       \
istream& readBinaryData(NAMEA& v,istream& is,IOData* dat)		\
{									\
  Eigen::Matrix<NAMEA::Scalar,3,3> l;                                   \
  Eigen::Matrix<NAMEA::Scalar,3,1> t;                                   \
  readBinaryData(l,is,dat);                                             \
  readBinaryData(t,is,dat);                                             \
  v.linear()=l;                                                         \
  v.translation()=t;                                                    \
  return is;                                                            \
}
IO_FIXED_QUAT(Quatd,Transd,Affined,IOType<scalarD>::TO_TYPE)
IO_FIXED_QUAT(Quatf,Transf,Affinef,IOType<scalarF>::TO_TYPE)
#undef IO_FIXED_QUAT

#define IO_BB(NAME,D)							\
ostream& writeBinaryData(const BBox<NAME,D>& b,ostream& os,IOData* dat)	\
{									\
	writeBinaryData(b._minC,os,dat);				\
	writeBinaryData(b._maxC,os,dat);				\
	return os;							\
}									\
istream& readBinaryData(BBox<NAME,D>& b,istream& is,IOData* dat)	\
{									\
	readBinaryData(b._minC,is,dat);					\
	readBinaryData(b._maxC,is,dat);					\
	return is;							\
}
IO_BB(scalarD,3)
IO_BB(scalarD,2)
IO_BB(scalarF,3)
IO_BB(scalarF,2)
IO_BB(unsigned char,3)
IO_BB(unsigned char,2)
IO_BB(sizeType,3)
IO_BB(sizeType,2)
#undef IO_BB

PRJ_END
