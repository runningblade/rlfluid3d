#include "RotationUtil.h"
#include <iostream>

PRJ_BEGIN

//internal
template <typename T>
struct EpsRotation {
  static const T value;
};
template <>
const scalarD EpsRotation<scalarD>::value=1E-6f;
template <>
const scalarF EpsRotation<scalarF>::value=1E-6f;
template <typename T>
FORCE_INLINE bool basicVariables(const typename ScalarUtil<T>::ScalarVec3& w,T& theta,typename ScalarUtil<T>::ScalarVec3& wBar,T& A,T& B,T& C,T& D,T& E,T& F,T& G)
{
  //A=sin(theta)
  //B=cos(theta)
  //C=sin(theta)/theta
  //D=(1-cos(theta))/theta
  //E=(1-cos(theta))/theta/theta
  //F=(theta-sin(theta))/theta/theta
  //G=(sin(theta)-theta*cos(theta))/theta/theta
  theta=w.norm();
  if(theta > EpsRotation<T>::value) {
    T invTheta=1/theta;
    T invTheta2=invTheta*invTheta;
    wBar=w*invTheta;
    A=sin(theta);
    B=cos(theta);
    C=A*invTheta;
    D=(1-cos(theta))*invTheta;
    E=D*invTheta;
    F=(theta-A)*invTheta2;
    G=(A-theta*B)*invTheta2;
    return false;
  } else {
    wBar=typename ScalarUtil<T>::ScalarVec3(0,0,1);
    A=0;
    B=1;
    C=1;
    D=0;
    E=0.5f;
    F=0;
    G=0;
    return true;
  }
}
FORCE_INLINE sizeType findNonZeroIndex(const Vec3i& val)
{
  sizeType id;
  val.cwiseAbs().maxCoeff(&id);
  return id;
}
FORCE_INLINE sizeType crossAxis(sizeType a,sizeType b)
{
  const sizeType data[3][3]= {
    findNonZeroIndex(cross<sizeType>(Vec3i::Unit(0))*Vec3i::Unit(0)),
    findNonZeroIndex(cross<sizeType>(Vec3i::Unit(0))*Vec3i::Unit(1)),
    findNonZeroIndex(cross<sizeType>(Vec3i::Unit(0))*Vec3i::Unit(2)),

    findNonZeroIndex(cross<sizeType>(Vec3i::Unit(1))*Vec3i::Unit(0)),
    findNonZeroIndex(cross<sizeType>(Vec3i::Unit(1))*Vec3i::Unit(1)),
    findNonZeroIndex(cross<sizeType>(Vec3i::Unit(1))*Vec3i::Unit(2)),

    findNonZeroIndex(cross<sizeType>(Vec3i::Unit(2))*Vec3i::Unit(0)),
    findNonZeroIndex(cross<sizeType>(Vec3i::Unit(2))*Vec3i::Unit(1)),
    findNonZeroIndex(cross<sizeType>(Vec3i::Unit(2))*Vec3i::Unit(2)),
  };
  return data[a][b];
}
FORCE_INLINE sizeType kronecker(sizeType a,sizeType b)
{
  return a==b?1:0;
}
FORCE_INLINE sizeType crossAxisSgn(sizeType a,sizeType b)
{
  const sizeType data[3][3]= {
    (cross<sizeType>(Vec3i::Unit(0))*Vec3i::Unit(0)).sum(),
    (cross<sizeType>(Vec3i::Unit(0))*Vec3i::Unit(1)).sum(),
    (cross<sizeType>(Vec3i::Unit(0))*Vec3i::Unit(2)).sum(),

    (cross<sizeType>(Vec3i::Unit(1))*Vec3i::Unit(0)).sum(),
    (cross<sizeType>(Vec3i::Unit(1))*Vec3i::Unit(1)).sum(),
    (cross<sizeType>(Vec3i::Unit(1))*Vec3i::Unit(2)).sum(),

    (cross<sizeType>(Vec3i::Unit(2))*Vec3i::Unit(0)).sum(),
    (cross<sizeType>(Vec3i::Unit(2))*Vec3i::Unit(1)).sum(),
    (cross<sizeType>(Vec3i::Unit(2))*Vec3i::Unit(2)).sum(),
  };
  return data[a][b];
}
//basic
template <typename T>
typename ScalarUtil<T>::ScalarMat3 cross(const typename ScalarUtil<T>::ScalarVec3& v)
{
  typename ScalarUtil<T>::ScalarMat3 ret;
  ret.setZero();
  ret(0,1)=-v[2];
  ret(0,2)=v[1];
  ret(1,2)=-v[0];
  ret(1,0)=v[2];
  ret(2,0)=-v[1];
  ret(2,1)=v[0];
  return ret;
}
template <typename T>
typename ScalarUtil<T>::ScalarMat3 crossD(sizeType d)
{
  typename ScalarUtil<T>::ScalarMat3 crossMat[3]= {
    cross<T>(ScalarUtil<T>::ScalarVec3::Unit(0)),
    cross<T>(ScalarUtil<T>::ScalarVec3::Unit(1)),
    cross<T>(ScalarUtil<T>::ScalarVec3::Unit(2)),
  };
  return crossMat[d];
}
template <typename T>
typename Eigen::Matrix<T,9,9> rotationCoef(const typename ScalarUtil<T>::ScalarMat3& r)
{
  typename Eigen::Matrix<T,9,9> rCoef;
  rCoef.setZero();
#define GI(I,J) J*3+I
  for(sizeType i=0; i<3; i++)
    for(sizeType j=0; j<3; j++)
      for(sizeType k=0; k<3; k++)
        rCoef(GI(i,j),GI(k,j))=r(i,k);
  return rCoef;
#undef GI
}
template <typename T>
typename ScalarUtil<T>::ScalarVec3 invCross(const typename ScalarUtil<T>::ScalarMat3& wCross)
{
  return typename ScalarUtil<T>::ScalarVec3(wCross(2,1),wCross(0,2),wCross(1,0));
}
template <typename T> //trace([w]*m) = w.dot(invCrossMatTrace(m))
typename ScalarUtil<T>::ScalarVec3 invCrossMatTrace(const typename ScalarUtil<T>::ScalarMat3& m)
{
  typename ScalarUtil<T>::ScalarVec3 ret;
  ret[0]=m(1,2)-m(2,1);
  ret[1]=m(2,0)-m(0,2);
  ret[2]=m(0,1)-m(1,0);
  return ret;
}
template <typename T> //trace([wA]*m*[wB]) = wA.dot(invDoubleCrossMatTrace(m)*wB)
typename ScalarUtil<T>::ScalarMat3 invDoubleCrossMatTrace(const typename ScalarUtil<T>::ScalarMat3& m)
{
  typename ScalarUtil<T>::ScalarMat3 ret=m.transpose();
  ret.diagonal().array()-=m.diagonal().sum();
  return ret;
}
//rotation along X,Y
template <typename T>
typename ScalarUtil<T>::ScalarMat3 expWYZ(T y,T z,
    typename ScalarUtil<T>::ScalarVec3* DRDY,
    typename ScalarUtil<T>::ScalarVec3* DRDZ,
    typename ScalarUtil<T>::ScalarVec3* DRDYDZ)
{
  T sy=sin(y);
  T cy=cos(y);
  T sz=sin(z);
  T cz=cos(z);

  typename ScalarUtil<T>::ScalarMat3 ret;
  ret <<
      cy*cz,-sz,sy*cz,
      cy*sz, cz,sy*sz,
      -sy,    0,   cy;
  if(DRDY)
    *DRDY << -sz,cz,0;
  if(DRDZ)
    *DRDZ <<   0, 0,1;
  if(DRDYDZ)
    *DRDYDZ << -cz,-sz,0;
  return ret;
}
//rotation using rodriguez formulation
template <typename T>
typename ScalarUtil<T>::ScalarMat3 expWBF(const typename ScalarUtil<T>::ScalarVec3& w)
{
  typename ScalarUtil<T>::ScalarMat3 R=ScalarUtil<T>::ScalarMat3::Identity();
  typename ScalarUtil<T>::ScalarMat3 C=cross<T>(w),coef=ScalarUtil<T>::ScalarMat3::Identity();
  for(int i=1;; i++) {
    coef*=C/(T)i;
    R+=coef;
    if(coef.cwiseAbs().maxCoeff() < 1E-7f)
      break;
  }
  return R;
}
template <typename T>
typename ScalarUtil<T>::ScalarMat3 expWGradInte(const typename ScalarUtil<T>::ScalarVec3& w)
{
  //used in: Modal Warping: Real-Time Simulation of Large Rotational Deformation and Manipulation
  typename ScalarUtil<T>::ScalarVec3 wBar;
  T theta,A,B,C,D,E,F,G;
  basicVariables<T>(w,theta,wBar,A,B,C,D,E,F,G);
  typename ScalarUtil<T>::ScalarMat3 CROSS=cross<T>(wBar);
  return ScalarUtil<T>::ScalarMat3::Identity()+D*CROSS+(1-C)*CROSS*CROSS;
}
template <typename T>
typename ScalarUtil<T>::ScalarMat3 expWGradInteOpt(const typename ScalarUtil<T>::ScalarVec3& w)
{
  typename ScalarUtil<T>::ScalarMat3 RInt;
  //temp
  T tt1;
  T tt2;
  T tt3;
  T tt4;
  T tt5;
  T tt6;
  T tt7;
  T tt8;
  T tt9;
  T tt10;
  T tt11;
  T tt12;
  T tt13;
  T tt14;

  tt1=pow(w[1],2);
  tt2=pow(w[0],2);
  tt3=pow(w[2],2);
  tt4=std::max<T>(tt3+tt1+tt2,1E-12);
  tt5=1/tt4;
  tt6=-tt1*tt5;
  tt7=-tt3*tt5;
  tt8=sqrt(tt4);
  tt9=1-sin(tt8)/tt8;
  tt10=1-cos(tt8);
  tt11=w[0]*w[1]*tt5*tt9;
  tt12=w[0]*w[2]*tt5*tt9;
  tt13=-tt2*tt5;
  tt14=w[1]*w[2]*tt5*tt9;
  RInt(0,0)=(tt7+tt6)*tt9+1;
  RInt(0,1)=tt11-w[2]*tt5*tt10;
  RInt(0,2)=tt12+w[1]*tt5*tt10;
  RInt(1,0)=tt11+w[2]*tt5*tt10;
  RInt(1,1)=(tt7+tt13)*tt9+1;
  RInt(1,2)=tt14-w[0]*tt5*tt10;
  RInt(2,0)=tt12-w[1]*tt5*tt10;
  RInt(2,1)=tt14+w[0]*tt5*tt10;
  RInt(2,2)=(tt6+tt13)*tt9+1;
  return RInt;
}
template <typename T>
typename ScalarUtil<T>::ScalarVec3 invExpW(const typename ScalarUtil<T>::ScalarMat3& R)
{
  T cosTheta=(R.trace()-1)/2,coef;
  if(cosTheta < EpsRotation<T>::value-1) {
    sizeType a,b,c;
    R.diagonal().maxCoeff(&a);
    b=(a+1)%3;
    c=(a+2)%3;

    T s=sqrt(R(a,a)-R(b,b)-R(c,c)+1);
    typename ScalarUtil<T>::ScalarVec3 ret;
    ret[a]=s/2;
    ret[b]=(R(a,b)+R(b,a))/(2*s);
    ret[c]=(R(a,c)+R(c,a))/(2*s);
    return ret.normalized()*M_PI;
  } else if(cosTheta > 1-EpsRotation<T>::value) {
    T theta=acos(min<T>(cosTheta,1));
    coef=0.5f+theta*theta/12.0f;  //using taylor expansion
  } else {
    T theta=acos(cosTheta),sinTheta=sin(theta);
    coef=theta/(2*sinTheta);
  }
  return typename ScalarUtil<T>::ScalarVec3(R(2,1)-R(1,2),R(0,2)-R(2,0),R(1,0)-R(0,1))*coef;
}
template <typename T>
typename ScalarUtil<T>::ScalarMat3 expWGradV(const typename ScalarUtil<T>::ScalarVec3& w,
    typename ScalarUtil<T>::ScalarMat3 diffW[3],
    typename ScalarUtil<T>::ScalarMat3 ddiffW[3][3],
    const typename ScalarUtil<T>::ScalarVec3* wDotT,
    typename ScalarUtil<T>::ScalarMat3 diffWDotT[3],
    typename ScalarUtil<T>::ScalarVec3 diffVOut[3],
    typename ScalarUtil<T>::ScalarVec3 vOut[3][3])
{
  typename ScalarUtil<T>::ScalarVec3 wBar,diffV[3],v;
  T theta,A,B,C,D,E,F,G;
  bool singular=basicVariables(w,theta,wBar,A,B,C,D,E,F,G);
  typename ScalarUtil<T>::ScalarMat3 CROSS,R;
  if(singular) {
    CROSS=cross<T>(w);
    R=ScalarUtil<T>::ScalarMat3::Identity()+CROSS/2;
    if(diffW || diffVOut)
      for(int d=0; d<3; d++) {
        //gradient
        if(diffW)
          diffW[d]=crossD<T>(d)+(crossD<T>(d)*CROSS+CROSS*crossD<T>(d))/2;
        if(diffVOut)
          diffVOut[d]=ScalarUtil<T>::ScalarVec3::Unit(d);
        //hessian
        if(ddiffW)
          for(int d2=0; d2<3; d2++)
            ddiffW[d][d2]=(crossD<T>(d)*crossD<T>(d2)+crossD<T>(d2)*crossD<T>(d))/2;
        if(vOut)
          for(int d2=0; d2<3; d2++)
            vOut[d][d2].setZero();
      }
    //gradient dotT
    if(wDotT && diffWDotT && (diffW || diffVOut))
      for(int d=0; d<3; d++)
        diffWDotT[d]=(crossD<T>(d)*cross<T>(*wDotT)+cross<T>(*wDotT)*crossD<T>(d))/2;
    return ScalarUtil<T>::ScalarMat3::Identity()+CROSS*R;
  } else {
    CROSS=cross<T>(wBar);
    R=ScalarUtil<T>::ScalarMat3::Identity()*B+CROSS*A+wBar*wBar.transpose()*(1-B);
    //gradient
    if(diffW || diffVOut)
      for(int d=0; d<3; d++) {
        diffV[d]=(1-C)*wBar*wBar[d]+D*CROSS.col(d);
        diffV[d][d]+=C;
        if(diffW)
          diffW[d]=cross<T>(diffV[d])*R;
        if(diffVOut)
          diffVOut[d]=diffV[d];
      }
    //gradient dotT
    if(wDotT && diffWDotT && (diffW || diffVOut)) {
      T wBarDotWDotT=wBar.dot(*wDotT);
      typename ScalarUtil<T>::ScalarMat3 diffWDotWDotT=diffW[0]*(*wDotT)[0]+diffW[1]*(*wDotT)[1]+diffW[2]*(*wDotT)[2];
      for(int d=0; d<3; d++) {
        v=((G-2*F)*wBar[d]*wBarDotWDotT+(*wDotT)[d]*F)*wBar;
        v+=(C-2*E)*CROSS.col(d)*wBarDotWDotT;
        v+=E*cross<T>(*wDotT).col(d);
        v[d]-=wBarDotWDotT*G;
        v+=(wBar[d]*F)**wDotT;
        diffWDotT[d]=cross<T>(v)*R+cross<T>(diffV[d])*diffWDotWDotT;
      }
    }
    //hessian
    if((diffW || diffVOut) && (ddiffW || vOut))
      for(int d=0; d<3; d++) {
        for(int d2=d; d2<3; d2++) {
          v=((G-2*F)*wBar[d]*wBar[d2]+kronecker(d,d2)*F)*wBar;
          v+=(C-2*E)*CROSS.col(d)*wBar[d2];
          //v+=E*cross(Vec3d::Unit(d2))*Vec3d::Unit(d);
          //std::cout << d << " " << d2 << " " << crossAxis(d2,d) << " " << crossAxisSgn(d2,d) << endl;
          v[d]-=wBar[d2]*G;
          v[d2]+=wBar[d]*F;
          v[crossAxis(d2,d)]+=E*crossAxisSgn(d2,d);
          if(ddiffW)
            ddiffW[d][d2]=ddiffW[d2][d]=cross<T>(v)*R+cross<T>(diffV[d])*diffW[d2];
          if(vOut)
            vOut[d][d2]=vOut[d2][d]=v;
        }
      }
    return R;
  }
}
template <typename T>
void debugExpWGrad(bool debugBasic,bool debugDiff,bool debugDiffDotT,bool debugInte,int maxIt)
{
#define DELTA 1E-7f
  for(int it=0; it<maxIt; it++) {
    //debug basic variable
    if(debugBasic) {
      T theta;
      T A1,B1,C1,D1,E1,F1,G1;
      T A2,B2,C2,D2,E2,F2,G2;
      typename ScalarUtil<T>::ScalarVec3 w2=ScalarUtil<T>::ScalarVec3::Constant(1E-7f);
      typename ScalarUtil<T>::ScalarVec3 w1=ScalarUtil<T>::ScalarVec3::Constant(1E-6f),wBar;
      basicVariables(w1,theta,wBar,A1,B1,C1,D1,E1,F1,G1);
      basicVariables(w2,theta,wBar,A2,B2,C2,D2,E2,F2,G2);
      INFOV("Err: A%f B%f C%f D%f E%f F%f G%f",
            A1-A2,B1-B2,C1-C2,D1-D2,E1-E2,F1-F2,G1-G2)
    }

    //debug rodrigues formula and derivative
    if(debugDiff) {
      typename ScalarUtil<T>::ScalarVec3 w=ScalarUtil<T>::ScalarVec3::Random();
      if(RandEngine::randR01() < 0.05f)
        w.setZero();
      typename ScalarUtil<T>::ScalarMat3 A=expWBF<T>(w),diffWZ[3],diffWZ2[3],ddiffWZ[3][3];
      typename ScalarUtil<T>::ScalarMat3 B=expWGradV<T>(w,diffWZ,ddiffWZ,NULL,NULL);
      INFOV("Norm: %f Err: %f",A.norm(),(A-B).norm())
      for(int d=0; d<3; d++) {
        typename ScalarUtil<T>::ScalarMat3 C2=expWGradV<T>(w+ScalarUtil<T>::ScalarVec3::Unit(d)*DELTA,diffWZ2,NULL,NULL,NULL);
        INFOV("Diff%d: %f Err: %f",d,diffWZ[d].norm(),(diffWZ[d]-(C2-B)/DELTA).norm())
        INFOV("DDiff%d: %f %f %f ErrZ: %f %f %f",d,
              ddiffWZ[0][d].norm(),ddiffWZ[1][d].norm(),ddiffWZ[2][d].norm(),
              (ddiffWZ[0][d]-(diffWZ2[0]-diffWZ[0])/DELTA).norm(),
              (ddiffWZ[1][d]-(diffWZ2[1]-diffWZ[1])/DELTA).norm(),
              (ddiffWZ[2][d]-(diffWZ2[2]-diffWZ[2])/DELTA).norm())
      }
    }

    //debug rodrigues formula and derivative
    if(debugDiffDotT) {
      typename ScalarUtil<T>::ScalarVec3 w=ScalarUtil<T>::ScalarVec3::Random();
      typename ScalarUtil<T>::ScalarVec3 wDotT=ScalarUtil<T>::ScalarVec3::Random();
      if(RandEngine::randR01() < 0.05f)
        w.setZero();
      typename ScalarUtil<T>::ScalarMat3 diffW[3],diffW2[3],diffWDotT[3];
      expWGradV<T>(w,diffW,NULL,&wDotT,diffWDotT);
      expWGradV<T>(w+wDotT*DELTA,diffW2,NULL,NULL,NULL);
      INFOV("DiffDotT: %f %f %f Err: %f %f %f",
            diffWDotT[0].norm(),diffWDotT[1].norm(),diffWDotT[2].norm(),
            (diffWDotT[0]-(diffW2[0]-diffW[0])/DELTA).norm(),
            (diffWDotT[1]-(diffW2[1]-diffW[1])/DELTA).norm(),
            (diffWDotT[2]-(diffW2[2]-diffW[2])/DELTA).norm())
    }

    //debug exponential integral
    if(debugInte) {
      typename ScalarUtil<T>::ScalarVec3 test=ScalarUtil<T>::ScalarVec3::Random();
      INFOV("ExpWInte: %f Err: %f",expWGradInte<T>(test).norm(),(expWGradInte<T>(test)-expWGradInteOpt<T>(test)).norm())
    }
  }
#undef DELTA
}
template <typename T>
void debugInvExpW(int maxIt)
{
  for(sizeType i=0; i<maxIt; i++) {
    typename ScalarUtil<T>::ScalarVec3 w=ScalarUtil<T>::ScalarVec3::Random();
    typename ScalarUtil<T>::ScalarMat3 R=expWGradV<T>(w,NULL,NULL,NULL,NULL);
    typename ScalarUtil<T>::ScalarVec3 wRef=invExpW<T>(R);
    typename ScalarUtil<T>::ScalarMat3 RRef=expWGradV<T>(wRef,NULL,NULL,NULL,NULL);
    INFOV("w: %f Err: %f",R.norm(),(R-RRef).norm())
  }
}
//euler-angle
template <typename T>
typename ScalarUtil<T>::ScalarMat3 rotationX(T angle,bool zero)
{
  typename ScalarUtil<T>::ScalarMat3 ret;
  if(zero)ret.setZero();
  else ret.setIdentity();
  ret(1,1)=ret(2,2)=cos(angle);
  ret(2,1)=sin(angle);
  ret(1,2)=-ret(2,1);
  return ret;
}
template <typename T>
typename ScalarUtil<T>::ScalarMat3 rotationY(T angle,bool zero)
{
  typename ScalarUtil<T>::ScalarMat3 ret;
  if(zero)ret.setZero();
  else ret.setIdentity();
  ret(0,0)=ret(2,2)=cos(angle);
  ret(0,2)=sin(angle);
  ret(2,0)=-ret(0,2);
  return ret;
}
template <typename T>
typename ScalarUtil<T>::ScalarMat3 rotationZ(T angle,bool zero)
{
  typename ScalarUtil<T>::ScalarMat3 ret;
  if(zero)ret.setZero();
  else ret.setIdentity();
  ret(0,0)=ret(1,1)=cos(angle);
  ret(1,0)=sin(angle);
  ret(0,1)=-ret(1,0);
  return ret;
}
template <typename T>
typename ScalarUtil<T>::ScalarMat3 rotationEulerDeriv(typename ScalarUtil<T>::ScalarVec3 xyz,typename ScalarUtil<T>::ScalarMat3 dR[3],typename ScalarUtil<T>::ScalarMat3 ddR[3][3])
{
  typename ScalarUtil<T>::ScalarMat3 RZ=rotationZ<T>(xyz[2],false);
  typename ScalarUtil<T>::ScalarMat3 RY=rotationY<T>(xyz[1],false);
  typename ScalarUtil<T>::ScalarMat3 RX=rotationX<T>(xyz[0],false);
  typename ScalarUtil<T>::ScalarMat3 R=RZ*RY*RX;

  typename ScalarUtil<T>::ScalarMat3 dRZ=rotationZ<T>(xyz[2]+M_PI/2,true);
  typename ScalarUtil<T>::ScalarMat3 dRY=rotationY<T>(xyz[1]+M_PI/2,true);
  typename ScalarUtil<T>::ScalarMat3 dRX=rotationX<T>(xyz[0]+M_PI/2,true);

  if(dR || ddR) {
    dR[2]=dRZ*RY*RX;
    dR[1]=RZ*dRY*RX;
    dR[0]=RZ*RY*dRX;
  }
  if(ddR) {
    ddR[2][0]=dRZ*RY*dRX;
    ddR[2][1]=dRZ*dRY*RX;
    ddR[2][2]=-rotationZ<T>(xyz[2],true)*RY*RX;

    ddR[1][0]=RZ*dRY*dRX;
    ddR[1][1]=-RZ*rotationY<T>(xyz[1],true)*RX;
    ddR[1][2]=ddR[2][1];

    ddR[0][0]=-RZ*RY*rotationX<T>(xyz[0],true);
    ddR[0][1]=ddR[1][0];
    ddR[0][2]=ddR[2][0];
  }
  return R;
}
template <typename T>
void debugRotationEulerDeriv()
{
  typename ScalarUtil<T>::ScalarMat3 dR[3],ddR[3][3];
  typename ScalarUtil<T>::ScalarMat3 dRTmp[3],ddRTmp[3][3];
  typename ScalarUtil<T>::ScalarVec3 xyz=ScalarUtil<T>::ScalarVec3::Random();
  typename ScalarUtil<T>::ScalarMat3 R=rotationEulerDeriv<T>(xyz,dR,ddR);

#define DELTA 1E-8f
  for(sizeType d=0; d<3; d++) {
    typename ScalarUtil<T>::ScalarVec3 tmp=xyz+ScalarUtil<T>::ScalarVec3::Unit(d)*DELTA;
    typename ScalarUtil<T>::ScalarMat3 RTmp=rotationEulerDeriv<T>(tmp,dRTmp,ddRTmp);

    INFOV("dR Val: %f, Err: %f!",dR[d].norm(),(dR[d]-(RTmp-R)/DELTA).norm())
    INFOV("ddR[0] Val: %f, Err: %f!",ddR[d][0].norm(),(ddR[d][0]-(dRTmp[0]-dR[0])/DELTA).norm())
    INFOV("ddR[1] Val: %f, Err: %f!",ddR[d][1].norm(),(ddR[d][1]-(dRTmp[1]-dR[1])/DELTA).norm())
    INFOV("ddR[2] Val: %f, Err: %f!",ddR[d][2].norm(),(ddR[d][2]-(dRTmp[2]-dR[2])/DELTA).norm())
  }
#undef DELTA
}
//se/SE(3) operations
template <typename T>
typename ScalarUtil<T>::ScalarMat4 exponentialSE3(const typename ScalarUtil<T>::ScalarVec6& se3)
{
  typename ScalarUtil<T>::ScalarMat4 SE3=ScalarUtil<T>::ScalarMat4::Identity();
  //rot
  SE3.template block<3,3>(0,0)=expWGradV<T>(se3.template segment<3>(0),NULL,NULL,NULL,NULL,NULL,NULL);
  //trans
  T t=se3.template segment<3>(0).norm(),s,c;
  s=sin(t);
  c=cos(t);
  typename ScalarUtil<T>::ScalarMat3 cw=cross<T>(se3.template segment<3>(0)),V;
  if(abs(t) < EpsRotation<T>::value)
    V=ScalarUtil<T>::ScalarMat3::Identity()+0.5f*cw+1/6.0f*cw*cw;
  else V=ScalarUtil<T>::ScalarMat3::Identity()+(1-c)/(t*t)*cw+(t-s)/(t*t*t)*cw*cw;
  SE3.template block<3,1>(0,3)=V*se3.template segment<3>(3);
  return SE3;
}
template <typename T>
typename ScalarUtil<T>::ScalarVec6 logarithmSE3(const typename ScalarUtil<T>::ScalarMat4& SE3)
{
  typename ScalarUtil<T>::ScalarVec6 se3;
  //rot
  se3.template segment<3>(0)=invExpW<T>(SE3.template block<3,3>(0,0));
  //trans
  T t=se3.template segment<3>(0).norm(),s,c;
  s=sin(t);
  c=cos(t);
  typename ScalarUtil<T>::ScalarMat3 cw=cross<T>(se3.template segment<3>(0)),V;
  if(abs(t) < EpsRotation<T>::value)
    V=ScalarUtil<T>::ScalarMat3::Identity()+0.5f*cw+1/6.0f*cw*cw;
  else V=ScalarUtil<T>::ScalarMat3::Identity()+(1-c)/(t*t)*cw+(t-s)/(t*t*t)*cw*cw;
  se3.template segment<3>(3)=V.inverse()*SE3.template block<3,1>(0,3);
  return se3;
}
template <typename T>
void debugExpLogSE3(int maxIt)
{
#define DELTA 1E-5f
  for(int it=0; it<maxIt; it++) {
    typename ScalarUtil<T>::ScalarVec6 se3=ScalarUtil<T>::ScalarVec6::Random();
    typename ScalarUtil<T>::ScalarMat4 SE3=exponentialSE3<T>(se3);
    typename ScalarUtil<T>::ScalarVec6 se3Test=logarithmSE3<T>(SE3);
    typename ScalarUtil<T>::ScalarMat4 SE3Test=exponentialSE3<T>(se3Test);
    INFOV("Log: %f Err: %f",se3Test.norm(),(se3Test-se3).norm())
    INFOV("Exp: %f Err: %f",SE3Test.norm(),(SE3Test-SE3).norm())
  }
#undef DELTA
  //exit(-1);
}
//rotation-strain transformation
template <typename T>
typename ScalarUtil<T>::ScalarMat3 unitMat3(int r,int c)
{
  typename ScalarUtil<T>::ScalarMat3 ret=ScalarUtil<T>::ScalarMat3::Zero();
  ret(r,c)=1;
  return ret;
}
template <typename T>
typename ScalarUtil<T>::ScalarMat3 calcFGrad(const typename ScalarUtil<T>::ScalarMat3& RS,
    typename ScalarUtil<T>::ScalarMat3 diffRS[9],
    typename ScalarUtil<T>::ScalarMat3 ddiffRS[9][9],
    const typename ScalarUtil<T>::ScalarMat3* RSDotT,
    typename ScalarUtil<T>::ScalarMat3 diffRSDotT[9])
{
#define FETCH(R,C) (R)+(C)*3
  const typename ScalarUtil<T>::ScalarMat3 mask[6]= {
    unitMat3<T>(1,2)+unitMat3<T>(2,1),
    unitMat3<T>(0,2)+unitMat3<T>(2,0),
    unitMat3<T>(0,1)+unitMat3<T>(1,0),
    unitMat3<T>(0,0),
    unitMat3<T>(1,1),
    unitMat3<T>(2,2),
  };

  typename ScalarUtil<T>::ScalarVec3 w=invCross<T>((RS-RS.transpose())/2),wDotT;
  if(RSDotT)
    wDotT=invCross<T>((*RSDotT-RSDotT->transpose())/2);

  typename ScalarUtil<T>::ScalarMat3 diffR[3],ddiffR[3][3],diffRDotT[3],tmp,tmp2;
  typename ScalarUtil<T>::ScalarMat3 R=expWGradV<T>(w,(diffRS||ddiffRS||diffRSDotT)?diffR:NULL,
                                       (ddiffRS||diffRSDotT)?ddiffR:NULL,
                                       diffRSDotT?&wDotT:NULL,
                                       diffRSDotT?diffRDotT:NULL);
  typename ScalarUtil<T>::ScalarMat3 S=(RS+RS.transpose())/2+ScalarUtil<T>::ScalarMat3::Identity();
  if(diffRS) {
    //offdiag
    tmp=R*mask[0];
    tmp2=diffR[0]*S;
    diffRS[FETCH(1,2)]=(tmp-tmp2)/2;
    diffRS[FETCH(2,1)]=(tmp+tmp2)/2;
    tmp=R*mask[1];
    tmp2=diffR[1]*S;
    diffRS[FETCH(0,2)]=(tmp+tmp2)/2;
    diffRS[FETCH(2,0)]=(tmp-tmp2)/2;
    tmp=R*mask[2];
    tmp2=diffR[2]*S;
    diffRS[FETCH(0,1)]=(tmp-tmp2)/2;
    diffRS[FETCH(1,0)]=(tmp+tmp2)/2;
    //diag
    diffRS[FETCH(0,0)]=R*mask[3];
    diffRS[FETCH(1,1)]=R*mask[4];
    diffRS[FETCH(2,2)]=R*mask[5];
  }
  if(ddiffRS) {
    //for(int i=0;i<9;i++)
    //  for(int j=0;j<9;j++)
    //    ddiffRS[i][j].setZero();

    //offdiag
#define ADD_OFFDIAG(R,MASKID,SGN)  \
ddiffRS[R][FETCH(2,1)]=( diffR[0]*mask[MASKID]+SGN (diffR[MASKID]*mask[0]+ddiffR[MASKID][0]*S))/4;  \
ddiffRS[R][FETCH(1,2)]=(-diffR[0]*mask[MASKID]+SGN (diffR[MASKID]*mask[0]-ddiffR[MASKID][0]*S))/4;  \
ddiffRS[R][FETCH(2,0)]=(-diffR[1]*mask[MASKID]+SGN (diffR[MASKID]*mask[1]-ddiffR[MASKID][1]*S))/4;  \
ddiffRS[R][FETCH(0,2)]=( diffR[1]*mask[MASKID]+SGN (diffR[MASKID]*mask[1]+ddiffR[MASKID][1]*S))/4;  \
ddiffRS[R][FETCH(1,0)]=( diffR[2]*mask[MASKID]+SGN (diffR[MASKID]*mask[2]+ddiffR[MASKID][2]*S))/4;  \
ddiffRS[R][FETCH(0,1)]=(-diffR[2]*mask[MASKID]+SGN (diffR[MASKID]*mask[2]-ddiffR[MASKID][2]*S))/4;  \
ddiffRS[R][FETCH(0,0)]=SGN diffR[MASKID]*mask[3]/2;  \
ddiffRS[R][FETCH(1,1)]=SGN diffR[MASKID]*mask[4]/2;  \
ddiffRS[R][FETCH(2,2)]=SGN diffR[MASKID]*mask[5]/2;
    ADD_OFFDIAG(FETCH(1,2),0,-)
    ADD_OFFDIAG(FETCH(2,1),0, )
    ADD_OFFDIAG(FETCH(0,2),1, )
    ADD_OFFDIAG(FETCH(2,0),1,-)
    ADD_OFFDIAG(FETCH(0,1),2,-)
    ADD_OFFDIAG(FETCH(1,0),2, )
#undef ADD_OFFDIAG

    //diag
#define ADD_DIAG(R,MASKID)  \
ddiffRS[R][FETCH(2,1)]= diffR[0]*mask[MASKID]/2;  \
ddiffRS[R][FETCH(1,2)]=-diffR[0]*mask[MASKID]/2;  \
ddiffRS[R][FETCH(2,0)]=-diffR[1]*mask[MASKID]/2;  \
ddiffRS[R][FETCH(0,2)]= diffR[1]*mask[MASKID]/2;  \
ddiffRS[R][FETCH(1,0)]= diffR[2]*mask[MASKID]/2;  \
ddiffRS[R][FETCH(0,1)]=-diffR[2]*mask[MASKID]/2;  \
ddiffRS[R][FETCH(0,0)].setZero();  \
ddiffRS[R][FETCH(1,1)].setZero();  \
ddiffRS[R][FETCH(2,2)].setZero();
    ADD_DIAG(FETCH(0,0),3)
    ADD_DIAG(FETCH(1,1),4)
    ADD_DIAG(FETCH(2,2),5)
#undef ADD_DIAG
  }
  if(diffRSDotT) {
    typename ScalarUtil<T>::ScalarMat3 RdotT=diffR[0]*wDotT[0]+diffR[1]*wDotT[1]+diffR[2]*wDotT[2];
    typename ScalarUtil<T>::ScalarMat3 SdotT=(*RSDotT+RSDotT->transpose())/2;
    //offdiag
    tmp=RdotT*mask[0];
    tmp2=diffRDotT[0]*S+diffR[0]*SdotT;
    diffRSDotT[FETCH(1,2)]=(tmp-tmp2)/2;
    diffRSDotT[FETCH(2,1)]=(tmp+tmp2)/2;
    tmp=RdotT*mask[1];
    tmp2=diffRDotT[1]*S+diffR[1]*SdotT;
    diffRSDotT[FETCH(0,2)]=(tmp+tmp2)/2;
    diffRSDotT[FETCH(2,0)]=(tmp-tmp2)/2;
    tmp=RdotT*mask[2];
    tmp2=diffRDotT[2]*S+diffR[2]*SdotT;
    diffRSDotT[FETCH(0,1)]=(tmp-tmp2)/2;
    diffRSDotT[FETCH(1,0)]=(tmp+tmp2)/2;
    //diag
    diffRSDotT[FETCH(0,0)]=RdotT*mask[3];
    diffRSDotT[FETCH(1,1)]=RdotT*mask[4];
    diffRSDotT[FETCH(2,2)]=RdotT*mask[5];
  }
  return R*S-ScalarUtil<T>::ScalarMat3::Identity();
#undef FETCH
}
template <typename T>
typename ScalarUtil<T>::ScalarMat3 calcFGradBF(const typename ScalarUtil<T>::ScalarMat3& RS)
{
  typename ScalarUtil<T>::ScalarMat3 R=expWBF<T>(invCross<T>((RS-RS.transpose())/2));
  typename ScalarUtil<T>::ScalarMat3 S=(RS+RS.transpose())/2+ScalarUtil<T>::ScalarMat3::Identity();
  return R*S-ScalarUtil<T>::ScalarMat3::Identity();
}
template <typename T>
typename ScalarUtil<T>::ScalarMat3 calcFGradDU(const typename ScalarUtil<T>::ScalarMat3& U)
{
  typename ScalarUtil<T>::ScalarMat3 diffRS[9],ret=ScalarUtil<T>::ScalarMat3::Zero();
  calcFGrad<T>(ScalarUtil<T>::ScalarMat3::Zero(),diffRS,NULL,NULL,NULL);
  for(int r=0; r<9; r++)
    ret+=diffRS[r]*U.data()[r];
  return ret;
}
template <typename T>
typename ScalarUtil<T>::ScalarMat3 calcFGradDDU(const typename ScalarUtil<T>::ScalarMat3& U)
{
  return calcFGradDUDV<T>(U,U);
}
template <typename T>
typename ScalarUtil<T>::ScalarMat3 calcFGradDUDV(const typename ScalarUtil<T>::ScalarMat3& U,const typename ScalarUtil<T>::ScalarMat3& V)
{
  typename ScalarUtil<T>::ScalarMat3 diffRSDotT[9],ret=ScalarUtil<T>::ScalarMat3::Zero();
  calcFGrad<T>(ScalarUtil<T>::ScalarMat3::Zero(),NULL,NULL,&U,diffRSDotT);
  for(int r=0; r<9; r++)
    ret+=diffRSDotT[r]*V.data()[r];
  return ret;
}
template <typename T>
typename ScalarUtil<T>::ScalarMat3 calcFGrad(const typename ScalarUtil<T>::ScalarMat3& F,typename ScalarUtil<T>::ScalarMat9* diffRS)
{
  typename ScalarUtil<T>::ScalarMat3 RS,ret;
  typename ScalarUtil<T>::ScalarMat3 diffRSCols[9];
  for(int i=0; i<9; i++)
    RS.array()(i)=F.array()(i);
  if(diffRS == NULL) {
    return calcFGrad<T>(RS,NULL,NULL,NULL,NULL);
  } else {
    ret=calcFGrad<T>(RS,diffRSCols,NULL,NULL,NULL);
    for(sizeType D=0; D<9; D++)
      diffRS->col(D)=Eigen::Map<const typename ScalarUtil<T>::ScalarVec9>(diffRSCols[D].data());
    return ret;
  }
}
template <typename T>
void debugCalcFGrad(bool debugDiff,bool debugDDiff,bool debugDiffDotT,bool debugDUDV,int maxIt)
{
#define DELTA 1E-5f
  for(int it=0; it<maxIt; it++) {
    //debug RS derivative
    if(debugDiff) {
      typename ScalarUtil<T>::ScalarMat3 RS=ScalarUtil<T>::ScalarMat3::Random(),deltaRS=ScalarUtil<T>::ScalarMat3::Random();
      typename ScalarUtil<T>::ScalarMat3 diffRS[9];
      typename ScalarUtil<T>::ScalarMat3 A=calcFGradBF<T>(RS);
      typename ScalarUtil<T>::ScalarMat3 B=calcFGrad<T>(RS,diffRS,NULL,NULL,NULL);
      typename ScalarUtil<T>::ScalarMat3 B2=calcFGrad<T>(RS+deltaRS*DELTA,NULL,NULL,NULL,NULL);
      typename ScalarUtil<T>::ScalarMat3 diffBRef=ScalarUtil<T>::ScalarMat3::Zero();
      for(int i=0; i<9; i++)
        diffBRef+=diffRS[i]*deltaRS(i%3,i/3);
      INFOV("Euclidean: %f Err: %f",A.norm(),(A-B).norm())
      INFOV("DiffEuclidean: %f Err: %f",diffBRef.norm(),(diffBRef-(B2-B)/DELTA).norm())
    }

    //debug RS double derivative
    if(debugDDiff) {
      typename ScalarUtil<T>::ScalarMat3 RS=ScalarUtil<T>::ScalarMat3::Random(),DRS=ScalarUtil<T>::ScalarMat3::Random();
      typename ScalarUtil<T>::ScalarMat3 diffRS[9],diffRS2[9],ddiffRS[9][9];
      calcFGrad<T>(RS,diffRS,ddiffRS,NULL,NULL);
      calcFGrad<T>(RS+DRS*DELTA,diffRS2,NULL,NULL,NULL);
      for(int i=0; i<9; i++) {
        typename ScalarUtil<T>::ScalarMat3 diffRSDir=ScalarUtil<T>::ScalarMat3::Zero();
        for(int j=0; j<9; j++)
          diffRSDir+=ddiffRS[i][j]*DRS(j%3,j/3);
        INFOV("DDiffEuclidean%d: %f Err: %f",i,diffRSDir.norm(),(diffRSDir-(diffRS2[i]-diffRS[i])/DELTA).norm())
      }
    }

    //debug RS derivative dotT
    if(debugDiffDotT) {
      typename ScalarUtil<T>::ScalarMat3 RS=ScalarUtil<T>::ScalarMat3::Random(),RSDotT=ScalarUtil<T>::ScalarMat3::Random();
      typename ScalarUtil<T>::ScalarMat3 diffRS[9],diffRS2[9],diffRSDotT[9];
      calcFGrad<T>(RS,diffRS,NULL,&RSDotT,diffRSDotT);
      calcFGrad<T>(RS+RSDotT*DELTA,diffRS2,NULL,NULL,NULL);
      for(int i=0; i<9; i++) {
        INFOV("DiffEuclideanDotT%d: %f Err: %f",i,diffRSDotT[i].norm(),(diffRSDotT[i]-(diffRS2[i]-diffRS[i])/DELTA).norm())
      }
    }

    //debug RS double derivative at origin
    if(debugDUDV) {
      //WARNING: delta must be larger than delta used in basicVariable(*) to ensure correct derivative calculation
      ASSERT_MSG(DELTA > EpsRotation<T>::value,"Too small delta, incorrect derivative behavior!")
      typename ScalarUtil<T>::ScalarMat9 gradRSF,gradRSF1,gradRSF2;
      calcFGrad<T>(ScalarUtil<T>::ScalarMat3::Zero(),&gradRSF);
      typename ScalarUtil<T>::ScalarMat3 U=ScalarUtil<T>::ScalarMat3::Random();
      typename ScalarUtil<T>::ScalarMat3 V=ScalarUtil<T>::ScalarMat3::Random();
      Eigen::Map<typename ScalarUtil<T>::ScalarCol> mapU(U.data(),9);
      Eigen::Map<typename ScalarUtil<T>::ScalarCol> mapV(V.data(),9);
      //debug dexpU
      typename ScalarUtil<T>::ScalarMat3 DU=calcFGradDU<T>(U);
      INFOV("DFGradDU: %f Err: %f",(gradRSF*mapU).norm(),(Eigen::Map<typename ScalarUtil<T>::ScalarCol>(DU.data(),9)-gradRSF*mapU).norm());
      //debug ddexpU
      calcFGrad<T>(U*DELTA,&gradRSF1);
      typename ScalarUtil<T>::ScalarMat3 DDU=calcFGradDDU<T>(U);
      INFOV("DDFGradDDU: %f Err: %f",DDU.norm(),(Eigen::Map<typename ScalarUtil<T>::ScalarCol>(DDU.data(),9)-(gradRSF1-gradRSF)*mapU/DELTA).norm());
      //debug ddexpUV
      calcFGrad<T>(V*DELTA,&gradRSF2);
      typename ScalarUtil<T>::ScalarMat3 DUDV=calcFGradDUDV<T>(U,V);
      INFOV("DDFGradDUDV: %f Err: %f %f",DUDV.norm(),
            (Eigen::Map<typename ScalarUtil<T>::ScalarCol>(DUDV.data(),9)-(gradRSF1-gradRSF)*mapV/DELTA).norm(),
            (Eigen::Map<typename ScalarUtil<T>::ScalarCol>(DUDV.data(),9)-(gradRSF2-gradRSF)*mapU/DELTA).norm());
    }
  }
#undef DELTA
}
//derivative of SVD decomposition
template <typename T>
void adjustF3D(typename ScalarUtil<T>::ScalarMat3& F,
               typename ScalarUtil<T>::ScalarMat3& U,
               typename ScalarUtil<T>::ScalarMat3& V,
               typename ScalarUtil<T>::ScalarVec3& S)
{
  Eigen::SelfAdjointEigenSolver<typename ScalarUtil<T>::ScalarMat3> eig(F.transpose()*F);
  //F Hat
  S=eig.eigenvalues().cwiseAbs().cwiseSqrt();
  //adjust V
  V=eig.eigenvectors();
  if(V.determinant() < 0.0f)
    V.col(0)*=-1.0f;
  //adjust U
  sizeType good[3];
  sizeType nrGood=0;
  for(sizeType v=0; v<3; v++)
    if(S[v] > EpsRotation<T>::value) {
      U.col(v)=F*V.col(v)/S[v];
      good[nrGood++]=v;
    }
  //the tet is a point
  if(nrGood == 0)
    U.setIdentity();
  //set columns of U to be orthogonal
  else for(sizeType v=0; v<3; v++)
      if(S[v] <= EpsRotation<T>::value) {
        if(nrGood == 1) {
          typename ScalarUtil<T>::ScalarVec3 d;
          T nd;
          while(true) {
            d=U.col(good[0]).cross(ScalarUtil<T>::ScalarVec3::Random());
            nd=d.norm();
            if(nd > 1E-3f) {
              U.col(v)=d/nd;
              break;
            }
          }
        } else {
          ASSERT(nrGood == 2)
          U.col(v)=U.col(good[0]).cross(U.col(good[1]));
        }
        good[nrGood++]=v;
      }
  //negate negative U columns
  if(U.determinant() < 0.0f) {
    sizeType id;
    S.minCoeff(&id);
    S[id]*=-1.0f;
    U.col(id)*=-1.0f;
  }
  //rebuild F
  typename ScalarUtil<T>::ScalarMat3 Sigma=ScalarUtil<T>::ScalarMat3::Zero();
  Sigma.diagonal()=S;
  F=U*Sigma*V.transpose();
}
template <typename T>
void adjustF2D(typename ScalarUtil<T>::ScalarMat3& F,
               typename ScalarUtil<T>::ScalarMat3& U,
               typename ScalarUtil<T>::ScalarMat3& V,
               typename ScalarUtil<T>::ScalarVec3& S)
{
  Eigen::SelfAdjointEigenSolver<typename ScalarUtil<T>::ScalarMat2> eig((F.transpose()*F).template block<2,2>(0,0));
  //F Hat
  S.setZero();
  S.template block<2,1>(0,0)=eig.eigenvalues().cwiseAbs().cwiseSqrt();
  //adjust V
  V.setZero();
  V.template block<2,2>(0,0)=eig.eigenvectors();
  if(V.template block<2,2>(0,0).determinant() < 0.0f)
    V.col(0)*=-1.0f;
  //adjust U
  U.setZero();
  sizeType good[2];
  sizeType nrGood=0;
  for(sizeType v=0; v<2; v++)
    if(S[v] > EpsRotation<T>::value) {
      U.col(v)=F*V.col(v)/S[v];
      good[nrGood++]=v;
    }
  //the tet is a point
  if(nrGood == 0)
    U.template block<2,2>(0,0).setIdentity();
  //set columns of U to be orthogonal
  else for(sizeType v=0; v<2; v++)
      if(S[v] <= EpsRotation<T>::value) {
        U.col(v)=typename ScalarUtil<T>::ScalarVec3(-U(1,good[0]),U(0,good[0]),0.0f);
        good[nrGood++]=v;
      }
  //negate negative U columns
  if(U.template block<2,2>(0,0).determinant() < 0.0f) {
    sizeType id;
    S.template block<2,1>(0,0).minCoeff(&id);
    S[id]*=-1.0f;
    U.col(id)*=-1.0f;
  }
  //rebuild F
  typename ScalarUtil<T>::ScalarMat3 Sigma=ScalarUtil<T>::ScalarMat3::Zero();
  Sigma.diagonal()=S;
  F=U*Sigma*V.transpose();
}
template <typename T>
void derivSVD(typename ScalarUtil<T>::ScalarMat3& derivU,
              typename ScalarUtil<T>::ScalarVec3& derivD,
              typename ScalarUtil<T>::ScalarMat3& derivV,
              const typename ScalarUtil<T>::ScalarMat3& LHS,
              const typename ScalarUtil<T>::ScalarMat3& U,
              const typename ScalarUtil<T>::ScalarVec3& D,
              const typename ScalarUtil<T>::ScalarMat3& V)
{
#define SOLVE_SAFE(a,b,c) \
tmp << D(c),D(b),D(b),D(c); \
if(std::abs(tmp(0,0)-tmp(0,1)) < 1E-6f)	\
  tmpInv=(tmp*tmp+reg).inverse()*tmp; \
else  \
  tmpInv=tmp.inverse(); \
a=tmpInv*typename ScalarUtil<T>::ScalarVec2(LHS(b,c),-LHS(c,b));

  derivD=typename ScalarUtil<T>::ScalarVec3(LHS(0,0),LHS(1,1),LHS(2,2));

  //21,31,32
  typename ScalarUtil<T>::ScalarMat2 reg=ScalarUtil<T>::ScalarMat2::Identity()*1E-6f,tmp,tmpInv;
  typename ScalarUtil<T>::ScalarVec2 omg10,omg20,omg21;
  SOLVE_SAFE(omg10,1,0)
  SOLVE_SAFE(omg20,2,0)
  SOLVE_SAFE(omg21,2,1)
#undef SOLVE_SAFE

  typename ScalarUtil<T>::ScalarMat3 omgU;
  omgU<<      0,-omg10(0),-omg20(0),
       omg10(0),        0,-omg21(0),
       omg20(0), omg21(0),        0;
  derivU=U*omgU;

  typename ScalarUtil<T>::ScalarMat3 omgV;
  omgV<<      0,-omg10(1),-omg20(1),
       omg10(1),        0,-omg21(1),
       omg20(1), omg21(1),        0;
  derivV=-V*omgV;
}
template <typename T>
void derivSVDDir(typename ScalarUtil<T>::ScalarMat3& derivU,
                 typename ScalarUtil<T>::ScalarVec3& derivD,
                 typename ScalarUtil<T>::ScalarMat3& derivV,
                 const typename ScalarUtil<T>::ScalarMat3& dir,
                 const typename ScalarUtil<T>::ScalarMat3& U,
                 const typename ScalarUtil<T>::ScalarVec3& D,
                 const typename ScalarUtil<T>::ScalarMat3& V)
{
  typename ScalarUtil<T>::ScalarMat3 LHS=ScalarUtil<T>::ScalarMat3::Zero();
  for(sizeType r=0; r<3; r++)
    for(sizeType c=0; c<3; c++)
      LHS+=U.row(r).transpose()*V.row(c)*dir(r,c);
  derivSVD<T>(derivU,derivD,derivV,LHS,U,D,V);
}
template <typename T>
void derivRDF(typename ScalarUtil<T>::ScalarMat9& DRDF,
              const typename ScalarUtil<T>::ScalarMat3& F,
              const typename ScalarUtil<T>::ScalarMat3& U,
              const typename ScalarUtil<T>::ScalarVec3& D,
              const typename ScalarUtil<T>::ScalarMat3& V)
{
  typename ScalarUtil<T>::ScalarVec3 derivD;
  typename ScalarUtil<T>::ScalarMat3 derivU,derivV;
  if(D.minCoeff() < 1E-3f)
    DRDF.setZero();
  for(sizeType r=0; r<3; r++)
    for(sizeType c=0; c<3; c++) {
      derivSVD<T>(derivU,derivD,derivV,U.row(r).transpose()*V.row(c),U,D,V);
      Eigen::Map<typename ScalarUtil<T>::ScalarMat3>(DRDF.col(r+c*3).data())=(derivU*V.transpose()+U*derivV.transpose()).template cast<T>();
    }
}
template <typename T>
void debugDerivSVD(bool debugSVD,bool debugRDF,int maxIt)
{
#define DELTA 1E-7f
  for(int it=0; it<maxIt; it++) {
    //debug derivative of SVD decomposition
    if(debugSVD) {
      typename ScalarUtil<T>::ScalarVec3 dD;
      typename ScalarUtil<T>::ScalarMat3 F=ScalarUtil<T>::ScalarMat3::Random(),dF=ScalarUtil<T>::ScalarMat3::Random(),dU,dV;
      Eigen::JacobiSVD<typename ScalarUtil<T>::ScalarMat3> svd(F,Eigen::ComputeFullU|Eigen::ComputeFullV);
      derivSVDDir<T>(dU,dD,dV,dF,svd.matrixU(),svd.singularValues(),svd.matrixV());
      Eigen::JacobiSVD<typename ScalarUtil<T>::ScalarMat3> svd2(F+dF*DELTA,Eigen::ComputeFullU|Eigen::ComputeFullV);
      INFOV("DerivSVD: %f %f %f Err: %f %f %f",dU.norm(),dD.norm(),dV.norm(),
            (dU-(svd2.matrixU()-svd.matrixU())/DELTA).norm(),
            (dD-(svd2.singularValues()-svd.singularValues())/DELTA).norm(),
            (dV-(svd2.matrixV()-svd.matrixV())/DELTA).norm())
    }

    //debug derivative of RS (polar) decomposition
    if(debugRDF) {
      typename ScalarUtil<T>::ScalarMat9 DRDF;
      typename ScalarUtil<T>::ScalarMat3 F=ScalarUtil<T>::ScalarMat3::Random(),dF=ScalarUtil<T>::ScalarMat3::Random();
      Eigen::JacobiSVD<typename ScalarUtil<T>::ScalarMat3> svd(F,Eigen::ComputeFullU|Eigen::ComputeFullV);
      derivRDF<T>(DRDF,F,svd.matrixU(),svd.singularValues(),svd.matrixV());
      Eigen::JacobiSVD<typename ScalarUtil<T>::ScalarMat3> svd2(F+dF*DELTA,Eigen::ComputeFullU|Eigen::ComputeFullV);
      typename ScalarUtil<T>::ScalarMat3 R=svd.matrixU()*svd.matrixV().transpose();
      typename ScalarUtil<T>::ScalarMat3 R2=svd2.matrixU()*svd2.matrixV().transpose(),DRDFN=(R2-R)/DELTA;
      typename ScalarUtil<T>::ScalarCol DRDFA=DRDF.template cast<T>()*Eigen::Map<typename ScalarUtil<T>::ScalarCol>(dF.data(),9);
      INFOV("DerivRS: %f Err: %f",DRDFA.norm(),(DRDFA-Eigen::Map<typename ScalarUtil<T>::ScalarCol>(DRDFN.data(),9)).norm())
    }
  }
#undef DELTA
}

PRJ_END

//instance double
//basic
template COMMON::ScalarUtil<scalarD>::ScalarMat3 COMMON::cross<scalarD>(const ScalarUtil<scalarD>::ScalarVec3& v);
template COMMON::ScalarUtil<scalarD>::ScalarMat3 COMMON::crossD<scalarD>(sizeType d);
template Eigen::Matrix<scalarD,9,9> COMMON::rotationCoef(const ScalarUtil<scalarD>::ScalarMat3& r);
template COMMON::ScalarUtil<scalarD>::ScalarVec3 COMMON::invCross<scalarD>(const ScalarUtil<scalarD>::ScalarMat3& wCross);
template COMMON::ScalarUtil<scalarD>::ScalarVec3 COMMON::invCrossMatTrace<scalarD>(const ScalarUtil<scalarD>::ScalarMat3& m);
template COMMON::ScalarUtil<scalarD>::ScalarMat3 COMMON::invDoubleCrossMatTrace<scalarD>(const ScalarUtil<scalarD>::ScalarMat3& m);
//rotation along X,Y
template COMMON::ScalarUtil<scalarD>::ScalarMat3 COMMON::expWYZ<scalarD>(scalarD y,scalarD z,
    ScalarUtil<scalarD>::ScalarVec3* DRDY,
    ScalarUtil<scalarD>::ScalarVec3* DRDZ,
    ScalarUtil<scalarD>::ScalarVec3* DRDYDZ);
//rotation using rodriguez formulation
template COMMON::ScalarUtil<scalarD>::ScalarMat3 COMMON::expWGradInte<scalarD>(const ScalarUtil<scalarD>::ScalarVec3& w);
template COMMON::ScalarUtil<scalarD>::ScalarVec3 COMMON::invExpW<scalarD>(const ScalarUtil<scalarD>::ScalarMat3& R);
template COMMON::ScalarUtil<scalarD>::ScalarMat3 COMMON::expWGradV<scalarD>(const ScalarUtil<scalarD>::ScalarVec3& w,
    ScalarUtil<scalarD>::ScalarMat3 diffW[3],
    ScalarUtil<scalarD>::ScalarMat3 ddiffW[3][3],
    const ScalarUtil<scalarD>::ScalarVec3* wDotT,
    ScalarUtil<scalarD>::ScalarMat3 diffWDotT[3],
    ScalarUtil<scalarD>::ScalarVec3 diffVOut[3],
    ScalarUtil<scalarD>::ScalarVec3 vOut[3][3]);
template void COMMON::debugExpWGrad<scalarD>(bool debugBasic,bool debugDiff,bool debugDiffDotT,bool debugInte,int maxIt);
template void COMMON::debugInvExpW<scalarD>(int maxIt);
//euler-angle
template COMMON::ScalarUtil<scalarD>::ScalarMat3 COMMON::rotationX<scalarD>(scalarD angle,bool zero);
template COMMON::ScalarUtil<scalarD>::ScalarMat3 COMMON::rotationY<scalarD>(scalarD angle,bool zero);
template COMMON::ScalarUtil<scalarD>::ScalarMat3 COMMON::rotationZ<scalarD>(scalarD angle,bool zero);
template COMMON::ScalarUtil<scalarD>::ScalarMat3 COMMON::rotationEulerDeriv<scalarD>(typename ScalarUtil<scalarD>::ScalarVec3 xyz,typename ScalarUtil<scalarD>::ScalarMat3 dR[3],typename ScalarUtil<scalarD>::ScalarMat3 ddR[3][3]);
template void COMMON::debugRotationEulerDeriv<scalarD>();
//se/SE(3) operations
template COMMON::ScalarUtil<scalarD>::ScalarMat4 COMMON::exponentialSE3<scalarD>(const ScalarUtil<scalarD>::ScalarVec6& se3);
template COMMON::ScalarUtil<scalarD>::ScalarVec6 COMMON::logarithmSE3<scalarD>(const ScalarUtil<scalarD>::ScalarMat4& SE3);
template void COMMON::debugExpLogSE3<scalarD>(int maxIt);
//rotation-strain transformation
template COMMON::ScalarUtil<scalarD>::ScalarMat3 COMMON::calcFGrad<scalarD>(const ScalarUtil<scalarD>::ScalarMat3& RS,
    ScalarUtil<scalarD>::ScalarMat3 diffRS[9],
    ScalarUtil<scalarD>::ScalarMat3 ddiffRS[9][9],
    const ScalarUtil<scalarD>::ScalarMat3* RSDotT,
    ScalarUtil<scalarD>::ScalarMat3 diffRSDotT[9]);
template COMMON::ScalarUtil<scalarD>::ScalarMat3 COMMON::calcFGradDU<scalarD>(const ScalarUtil<scalarD>::ScalarMat3& U);
template COMMON::ScalarUtil<scalarD>::ScalarMat3 COMMON::calcFGradDDU<scalarD>(const ScalarUtil<scalarD>::ScalarMat3& U);
template COMMON::ScalarUtil<scalarD>::ScalarMat3 COMMON::calcFGradDUDV<scalarD>(const ScalarUtil<scalarD>::ScalarMat3& U,const ScalarUtil<scalarD>::ScalarMat3& V);
template COMMON::ScalarUtil<scalarD>::ScalarMat3 COMMON::calcFGrad<scalarD>(const ScalarUtil<scalarD>::ScalarMat3& F,ScalarUtil<scalarD>::ScalarMat9* diffRS=NULL);
template void COMMON::debugCalcFGrad<scalarD>(bool debugDiff,bool debugDDiff,bool debugDiffDotT,bool debugDUDV,int maxIt);
//svd gradient
template void COMMON::adjustF3D<scalarD>(ScalarUtil<scalarD>::ScalarMat3& F,
    ScalarUtil<scalarD>::ScalarMat3& U,
    ScalarUtil<scalarD>::ScalarMat3& V,
    ScalarUtil<scalarD>::ScalarVec3& S);
template void COMMON::adjustF2D<scalarD>(ScalarUtil<scalarD>::ScalarMat3& F,
    ScalarUtil<scalarD>::ScalarMat3& U,
    ScalarUtil<scalarD>::ScalarMat3& V,
    ScalarUtil<scalarD>::ScalarVec3& S);
template void COMMON::derivSVD<scalarD>(ScalarUtil<scalarD>::ScalarMat3& derivU,
                                        ScalarUtil<scalarD>::ScalarVec3& derivD,
                                        ScalarUtil<scalarD>::ScalarMat3& derivV,
                                        const ScalarUtil<scalarD>::ScalarMat3& LHS,
                                        const ScalarUtil<scalarD>::ScalarMat3& U,
                                        const ScalarUtil<scalarD>::ScalarVec3& D,
                                        const ScalarUtil<scalarD>::ScalarMat3& V);
template void COMMON::derivSVDDir<scalarD>(ScalarUtil<scalarD>::ScalarMat3& derivU,
    ScalarUtil<scalarD>::ScalarVec3& derivD,
    ScalarUtil<scalarD>::ScalarMat3& derivV,
    const ScalarUtil<scalarD>::ScalarMat3& dir,
    const ScalarUtil<scalarD>::ScalarMat3& U,
    const ScalarUtil<scalarD>::ScalarVec3& D,
    const ScalarUtil<scalarD>::ScalarMat3& V);
template void COMMON::derivRDF<scalarD>(ScalarUtil<scalarD>::ScalarMat9& DRDF,
                                        const ScalarUtil<scalarD>::ScalarMat3& F,
                                        const ScalarUtil<scalarD>::ScalarMat3& U,
                                        const ScalarUtil<scalarD>::ScalarVec3& D,
                                        const ScalarUtil<scalarD>::ScalarMat3& V);
template void COMMON::debugDerivSVD<scalarD>(bool debugSVD,bool debugRDF,int maxIt);
//instance float
//basic
template COMMON::ScalarUtil<scalarF>::ScalarMat3 COMMON::cross<scalarF>(const ScalarUtil<scalarF>::ScalarVec3& v);
template COMMON::ScalarUtil<scalarF>::ScalarMat3 COMMON::crossD<scalarF>(sizeType d);
template Eigen::Matrix<scalarF,9,9> COMMON::rotationCoef(const ScalarUtil<scalarF>::ScalarMat3& r);
template COMMON::ScalarUtil<scalarF>::ScalarVec3 COMMON::invCross<scalarF>(const ScalarUtil<scalarF>::ScalarMat3& wCross);
template COMMON::ScalarUtil<scalarF>::ScalarVec3 COMMON::invCrossMatTrace<scalarF>(const ScalarUtil<scalarF>::ScalarMat3& m);
template COMMON::ScalarUtil<scalarF>::ScalarMat3 COMMON::invDoubleCrossMatTrace<scalarF>(const ScalarUtil<scalarF>::ScalarMat3& m);
//rotation along X,Y
template COMMON::ScalarUtil<scalarF>::ScalarMat3 COMMON::expWYZ<scalarF>(scalarF y,scalarF z,
    ScalarUtil<scalarF>::ScalarVec3* DRDY,
    ScalarUtil<scalarF>::ScalarVec3* DRDZ,
    ScalarUtil<scalarF>::ScalarVec3* DRDYDZ);
//rotation using rodriguez formulation
template COMMON::ScalarUtil<scalarF>::ScalarMat3 COMMON::expWGradInte<scalarF>(const ScalarUtil<scalarF>::ScalarVec3& w);
template COMMON::ScalarUtil<scalarF>::ScalarVec3 COMMON::invExpW<scalarF>(const ScalarUtil<scalarF>::ScalarMat3& R);
template COMMON::ScalarUtil<scalarF>::ScalarMat3 COMMON::expWGradV<scalarF>(const ScalarUtil<scalarF>::ScalarVec3& w,
    ScalarUtil<scalarF>::ScalarMat3 diffW[3],
    ScalarUtil<scalarF>::ScalarMat3 ddiffW[3][3],
    const ScalarUtil<scalarF>::ScalarVec3* wDotT,
    ScalarUtil<scalarF>::ScalarMat3 diffWDotT[3],
    ScalarUtil<scalarF>::ScalarVec3 diffVOut[3],
    ScalarUtil<scalarF>::ScalarVec3 vOut[3][3]);
template void COMMON::debugExpWGrad<scalarF>(bool debugBasic,bool debugDiff,bool debugDiffDotT,bool debugInte,int maxIt);
template void COMMON::debugInvExpW<scalarF>(int maxIt);
//euler-angle
template COMMON::ScalarUtil<scalarF>::ScalarMat3 COMMON::rotationX<scalarF>(scalarF angle,bool zero);
template COMMON::ScalarUtil<scalarF>::ScalarMat3 COMMON::rotationY<scalarF>(scalarF angle,bool zero);
template COMMON::ScalarUtil<scalarF>::ScalarMat3 COMMON::rotationZ<scalarF>(scalarF angle,bool zero);
template COMMON::ScalarUtil<scalarF>::ScalarMat3 COMMON::rotationEulerDeriv<scalarF>(typename ScalarUtil<scalarF>::ScalarVec3 xyz,typename ScalarUtil<scalarF>::ScalarMat3 dR[3],typename ScalarUtil<scalarF>::ScalarMat3 ddR[3][3]);
template void COMMON::debugRotationEulerDeriv<scalarF>();
//se/SE(3) operations
template COMMON::ScalarUtil<scalarF>::ScalarMat4 COMMON::exponentialSE3<scalarF>(const ScalarUtil<scalarF>::ScalarVec6& se3);
template COMMON::ScalarUtil<scalarF>::ScalarVec6 COMMON::logarithmSE3<scalarF>(const ScalarUtil<scalarF>::ScalarMat4& SE3);
template void COMMON::debugExpLogSE3<scalarF>(int maxIt);
//rotation-strain transformation
template COMMON::ScalarUtil<scalarF>::ScalarMat3 COMMON::calcFGrad<scalarF>(const ScalarUtil<scalarF>::ScalarMat3& RS,
    ScalarUtil<scalarF>::ScalarMat3 diffRS[9],
    ScalarUtil<scalarF>::ScalarMat3 ddiffRS[9][9],
    const ScalarUtil<scalarF>::ScalarMat3* RSDotT,
    ScalarUtil<scalarF>::ScalarMat3 diffRSDotT[9]);
template COMMON::ScalarUtil<scalarF>::ScalarMat3 COMMON::calcFGradDU<scalarF>(const ScalarUtil<scalarF>::ScalarMat3& U);
template COMMON::ScalarUtil<scalarF>::ScalarMat3 COMMON::calcFGradDDU<scalarF>(const ScalarUtil<scalarF>::ScalarMat3& U);
template COMMON::ScalarUtil<scalarF>::ScalarMat3 COMMON::calcFGradDUDV<scalarF>(const ScalarUtil<scalarF>::ScalarMat3& U,const ScalarUtil<scalarF>::ScalarMat3& V);
template COMMON::ScalarUtil<scalarF>::ScalarMat3 COMMON::calcFGrad<scalarF>(const ScalarUtil<scalarF>::ScalarMat3& F,ScalarUtil<scalarF>::ScalarMat9* diffRS=NULL);
template void COMMON::debugCalcFGrad<scalarF>(bool debugDiff,bool debugDDiff,bool debugDiffDotT,bool debugDUDV,int maxIt);
//svd gradient
template void COMMON::adjustF3D<scalarF>(ScalarUtil<scalarF>::ScalarMat3& F,
    ScalarUtil<scalarF>::ScalarMat3& U,
    ScalarUtil<scalarF>::ScalarMat3& V,
    ScalarUtil<scalarF>::ScalarVec3& S);
template void COMMON::adjustF2D<scalarF>(ScalarUtil<scalarF>::ScalarMat3& F,
    ScalarUtil<scalarF>::ScalarMat3& U,
    ScalarUtil<scalarF>::ScalarMat3& V,
    ScalarUtil<scalarF>::ScalarVec3& S);
template void COMMON::derivSVD<scalarF>(ScalarUtil<scalarF>::ScalarMat3& derivU,
                                        ScalarUtil<scalarF>::ScalarVec3& derivD,
                                        ScalarUtil<scalarF>::ScalarMat3& derivV,
                                        const ScalarUtil<scalarF>::ScalarMat3& LHS,
                                        const ScalarUtil<scalarF>::ScalarMat3& U,
                                        const ScalarUtil<scalarF>::ScalarVec3& D,
                                        const ScalarUtil<scalarF>::ScalarMat3& V);
template void COMMON::derivSVDDir<scalarF>(ScalarUtil<scalarF>::ScalarMat3& derivU,
    ScalarUtil<scalarF>::ScalarVec3& derivD,
    ScalarUtil<scalarF>::ScalarMat3& derivV,
    const ScalarUtil<scalarF>::ScalarMat3& dir,
    const ScalarUtil<scalarF>::ScalarMat3& U,
    const ScalarUtil<scalarF>::ScalarVec3& D,
    const ScalarUtil<scalarF>::ScalarMat3& V);
template void COMMON::derivRDF<scalarF>(ScalarUtil<scalarF>::ScalarMat9& DRDF,
                                        const ScalarUtil<scalarF>::ScalarMat3& F,
                                        const ScalarUtil<scalarF>::ScalarMat3& U,
                                        const ScalarUtil<scalarF>::ScalarVec3& D,
                                        const ScalarUtil<scalarF>::ScalarMat3& V);
template void COMMON::debugDerivSVD<scalarF>(bool debugSVD,bool debugRDF,int maxIt);
