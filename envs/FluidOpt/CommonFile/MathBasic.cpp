#include "MathBasic.h"
#include <boost/random.hpp>
#include <boost/random/random_device.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>

PRJ_BEGIN

//float
float ScalarUtil<float>::scalar_max=numeric_limits<float>::max();
float ScalarUtil<float>::scalar_eps=1E-10f;
float ScalarUtil<float>::scalar_inf=numeric_limits<float>::infinity();
float ScalarUtil<float>::scalar_nanq=numeric_limits<float>::quiet_NaN();
//double
double ScalarUtil<double>::scalar_max=numeric_limits<double>::max();
double ScalarUtil<double>::scalar_eps=1E-20f;
double ScalarUtil<double>::scalar_inf=numeric_limits<double>::infinity();
double ScalarUtil<double>::scalar_nanq=numeric_limits<double>::quiet_NaN();
//__float128
#if defined(QUADMATH_SUPPORT) && defined(__GNUC__)
__float128 ScalarUtil<__float128>::scalar_max=numeric_limits<double>::max();
__float128 ScalarUtil<__float128>::scalar_eps=1E-20f;
__float128 ScalarUtil<__float128>::scalar_inf=strtoflt128("INF",NULL);
__float128 ScalarUtil<__float128>::scalar_nanq=strtoflt128("NAN",NULL);
#endif
//int
int ScalarUtil<int>::scalar_max=numeric_limits<int>::max();
int ScalarUtil<int>::scalar_eps=1;
//uint
unsigned int ScalarUtil<unsigned int>::scalar_max=numeric_limits<unsigned int>::max();
unsigned int ScalarUtil<unsigned int>::scalar_eps=1;
//int64_t
int64_t ScalarUtil<int64_t>::scalar_max=numeric_limits<int64_t>::max();
int64_t ScalarUtil<int64_t>::scalar_eps=1;

scalarD compMax(scalarD a,scalarD b)
{
  return std::max(a,b);
}
scalarF compMax(scalarF a,scalarF b)
{
  return std::max(a,b);
}
sizeType compMax(sizeType a,sizeType b)
{
  return std::max(a,b);
}
scalarD compMin(scalarD a,scalarD b)
{
  return std::min(a,b);
}
scalarF compMin(scalarF a,scalarF b)
{
  return std::min(a,b);
}
sizeType compMin(sizeType a,sizeType b)
{
  return std::min(a,b);
}

const scalarD ErfCalc::cof[28] = {
  -1.3026537197817094, 6.4196979235649026e-1,
  1.9476473204185836e-2,-9.561514786808631e-3,-9.46595344482036e-4,
  3.66839497852761e-4,4.2523324806907e-5,-2.0278578112534e-5,
  -1.624290004647e-6,1.303655835580e-6,1.5626441722e-8,-8.5238095915e-8,
  6.529054439e-9,5.059343495e-9,-9.91364156e-10,-2.27365122e-10,
  9.6467911e-11, 2.394038e-12,-6.886027e-12,8.94487e-13, 3.13092e-13,
  -1.12708e-13,3.81e-16,7.106e-15,-1.523e-15,-9.4e-17,1.21e-16,-2.8e-17
};
scalarD ErfCalc::erf(scalarD x) const
{
  if (x >=0.0) return 1.0 - erfccheb(x);
  else return erfccheb(-x) - 1.0;
}
scalarD ErfCalc::erfc(scalarD x) const
{
  if (x >= 0.0) return erfccheb(x);
  else return 2.0 - erfccheb(-x);
}
scalarD ErfCalc::erfccheb(scalarD z) const
{
  sizeType j;
  scalarD t,ty,tmp,d=0.0,dd=0.0;
  if (z < 0.0)
    throw("erfccheb requires nonnegative argument");
  t = 2.0/(2.0+z);
  ty = 4.0*t - 2.0;
  for (j=ncof-1; j>0; j--) {
    tmp = d;
    d = ty*d - dd + cof[j];
    dd = tmp;
  }
  return t*exp(-z*z + 0.5*(cof[0] + ty*d) - dd);
}
scalarD ErfCalc::inverfc(scalarD p) const
{
  scalarD x,err,t,pp;
  if (p >= 2.0) return -100.0;
  if (p <= 0.0) return 100.0;
  pp = (p < 1.0)? p : 2.0 - p;
  t = sqrt(-scalarD(2.0)*log(pp/scalarD(2.0)));
  x = -0.70711*((2.30753+t*0.27061)/(1.+t*(0.99229+t*0.04481)) - t);
  for (sizeType j=0; j<2; j++) {
    err = erfc(x) - pp;
    x += err/(1.12837916709551257*exp(-(x*x))-x*err);
  }
  return (p < 1.0? x : -x);
}
scalarD ErfCalc::inverf(scalarD p) const
{
  return inverfc(1.-p);
}
scalarD erf(const scalarD& val);

//#define USE_BOOST_RANDOM
bool USEDeterministic=false;
boost::random::random_device vgenDevice;
struct STDRandom {
  typedef int result_type;
  typedef map<sizeType,pair<sizeType,vector<int> > > record;
  STDRandom():_id(-1) {}
  int min() {
    return 0;
  }
  int max() {
    return RAND_MAX;
  }
  int operator()() {
    int ret=0;
    if(_id >= 0) {
      pair<sizeType,vector<int> >& pair=_record[_id];
      while(pair.first >= (sizeType)pair.second.size())
#ifdef USE_BOOST_RANDOM
        pair.second.push_back(vgen());
#else
        pair.second.push_back(rand());
#endif
      ret=pair.second[pair.first++];
    } else {
#ifdef USE_BOOST_RANDOM
      ret=vgen();
#else
      ret=rand();
#endif
    }
    return ret;
  }
  void seed(unsigned int s) {
#ifdef USE_BOOST_RANDOM
    vgen.seed(s);
#else
    srand(s);
#endif
  }
  boost::random::mt19937 vgen;
  record _record;
  sizeType _id;
} vgenPesudo;
//real
scalar RandEngine::randR01()
{
  scalar ret=0;
  OMP_CRITICAL_
  if(USEDeterministic)
    ret=boost::uniform_real<scalar>(0,1)(vgenPesudo);
  else ret=boost::uniform_real<scalar>(0,1)(vgenDevice);
  return ret;
}
scalar RandEngine::randR(scalar l,scalar u)
{
  if(l == u)
    return l;
  else if(l > u)
    swap(l,u);
  scalar ret=0;
  OMP_CRITICAL_
  if(USEDeterministic)
    ret=boost::uniform_real<scalar>(l,u)(vgenPesudo);
  else ret=boost::uniform_real<scalar>(l,u)(vgenDevice);
  return ret;
}
scalar RandEngine::randSR(sizeType seed,scalar l,scalar u)
{
  if(l == u)
    return l;
  else if(l > u)
    swap(l,u);
  scalar ret=0;
  OMP_CRITICAL_ {
    vgenPesudo.seed((const uint32_t)seed);
    ret=boost::uniform_real<scalar>(l,u)(vgenPesudo);
  }
  return ret;
}
//int
sizeType RandEngine::randSI(sizeType seed)
{
  return randSI(seed,0,numeric_limits<int>::max());
}
sizeType RandEngine::randI(sizeType l,sizeType u)
{
  if(l == u)
    return l;
  else if(l > u)
    swap(l,u);
  sizeType ret=0;
  OMP_CRITICAL_
  if(USEDeterministic)
    ret=boost::uniform_int<sizeType>(l,u)(vgenPesudo);
  else ret=boost::uniform_int<sizeType>(l,u)(vgenDevice);
  return ret;
}
sizeType RandEngine::randSI(sizeType seed,sizeType l,sizeType u)
{
  if(l == u)
    return l;
  else if(l > u)
    swap(l,u);
  sizeType ret=0;
  OMP_CRITICAL_ {
    vgenPesudo.seed((const uint32_t)seed);
    ret=boost::uniform_int<sizeType>(l,u)(vgenPesudo);
  }
  return ret;
}
//normal
scalar RandEngine::normal(scalar mean,scalar cov)
{
  double ret=0;
  OMP_CRITICAL_
  if(USEDeterministic)
    ret=boost::normal_distribution<double>(mean,cov)(vgenPesudo);
  else ret=boost::normal_distribution<double>(mean,cov)(vgenDevice);
  return ret;
}
scalar RandEngine::normal()
{
  double ret=0;
  OMP_CRITICAL_
  if(USEDeterministic)
    ret=boost::normal_distribution<double>()(vgenPesudo);
  else ret=boost::normal_distribution<double>()(vgenDevice);
  return ret;
}
//settings
void RandEngine::seedTime()
{
  boost::posix_time::ptime time=boost::posix_time::microsec_clock::local_time();
  boost::posix_time::time_duration duration(time.time_of_day());
  seed(duration.total_milliseconds());
}
void RandEngine::seed(sizeType i)
{
  vgenPesudo.seed((const uint32_t)i);
}
void RandEngine::useDeterministic()
{
  USEDeterministic=true;
}
void RandEngine::useNonDeterministic()
{
  USEDeterministic=false;
}
//common random number
const vector<int>& RandEngine::getRecordedRandom(sizeType id)
{
  ASSERT_MSGV(false,"Invalid random id: %ld!",id)
  STDRandom::record& rcd=vgenPesudo._record;
  return rcd[id].second;
}
void RandEngine::resetAllRecordRandom()
{
  STDRandom::record& rcd=vgenPesudo._record;
  for(STDRandom::record::iterator
      beg=rcd.begin(),end=rcd.end(); beg!=end; beg++)
    beg->second.first=0;
}
void RandEngine::resetRecordRandom(sizeType id)
{
  ASSERT_MSGV(id >= 0,"Invalid random id: %ld!",id)
  STDRandom::record& rcd=vgenPesudo._record;
  rcd[id].first=0;
}
void RandEngine::beginRecordRandom(sizeType id)
{
  ASSERT_MSGV(id >= 0,"Invalid random id: %ld!",id)
  STDRandom::record& rcd=vgenPesudo._record;
  if(rcd.find(id) == rcd.end())
    rcd[id]=make_pair((sizeType)0,vector<int>());
  vgenPesudo._id=id;
}
void RandEngine::endRecordRandom()
{
  vgenPesudo._id=-1;
}
void RandEngine::clearAllRecord()
{
  ASSERT_MSG(vgenPesudo._id == -1,"We must have vgenPesudo._id == -1 to modify cache!")
  STDRandom::record& rcd=vgenPesudo._record;
  rcd.clear();
}
void RandEngine::clearRecord(sizeType id)
{
  ASSERT_MSG(vgenPesudo._id == -1,"We must have vgenPesudo._id == -1 to modify cache!")
  STDRandom::record& rcd=vgenPesudo._record;
  if(rcd.find(id) != rcd.end())
    rcd.erase(id);
}
vector<sizeType> RandEngine::getRecordId()
{
  vector<sizeType> ret;
  STDRandom::record& rcd=vgenPesudo._record;
  for(STDRandom::record::const_iterator
      beg=rcd.begin(),end=rcd.end(); beg!=end; beg++)
    ret.push_back(beg->first);
  return ret;
}
void RandEngine::printRecordId()
{
  cout << "Random ids: ";
  STDRandom::record& rcd=vgenPesudo._record;
  for(STDRandom::record::const_iterator
      beg=rcd.begin(),end=rcd.end(); beg!=end; beg++)
    cout << beg->first << " ";
  cout << endl;
}

//OpenMP Settings
const OmpSettings& OmpSettings::getOmpSettings()
{
  return _ompSettings;
}
OmpSettings& OmpSettings::getOmpSettingsNonConst()
{
  return _ompSettings;
}
int OmpSettings::nrThreads() const
{
  return _nrThreads;
}
#ifdef NO_OPENMP
int OmpSettings::threadId() const
{
  return 0;
}
void OmpSettings::setNrThreads(int nr) {}
void OmpSettings::useAllThreads() {}
OmpSettings::OmpSettings():_nrThreads(1) {}
#else
int OmpSettings::threadId() const
{
  return omp_get_thread_num();
}
void OmpSettings::setNrThreads(int nr)
{
  nr=max<int>(nr,1);
  omp_set_num_threads(nr);
  _nrThreads=nr;
}
void OmpSettings::useAllThreads()
{
  _nrThreads=omp_get_num_procs();
}
OmpSettings::OmpSettings():_nrThreads(std::max<int>(omp_get_num_procs(),2)*3/4) {}
#endif
OmpSettings OmpSettings::_ompSettings;

PRJ_END
