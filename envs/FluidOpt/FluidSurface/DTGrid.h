#ifndef DTGRID_H
#define DTGRID_H

#include "../CommonFile/MathBasic.h"
#include <boost/unordered_map.hpp>
#include <boost/pool/pool_alloc.hpp>

PRJ_BEGIN

//grid
struct IDPool : public vector<sizeType> {
  FORCE_INLINE void pushId(Vec3i id) {
    resize(size()+1);
    setId(size()-1,id);
  }
  FORCE_INLINE void setId(sizeType off,Vec3i id) {
    id.array()+=OFF;
    operator[](off)=(id[2]<<(DIMBIT*2))+(id[1]<<DIMBIT)+id[0];
  }
  FORCE_INLINE sizeType getDim(sizeType off,int d) const {
    sizeType val=operator[](off)>>(DIMBIT*d);
    return (val&MASK)-OFF;
  }
private:
  static const int DIMBIT=21;
  static const int MASK=(1<<DIMBIT)-1;
  static const int OFF=(1<<(DIMBIT-1))-1;
};
template <typename INNER>
struct DTSegment {
  bool operator<=(sizeType other) const {
    return _beg<=other;
  }
  bool operator>(sizeType other) const {
    return _end>other;
  }
  sizeType length() const {
    return _end-_beg;
  }
  sizeType _beg,_end;
  INNER _inner;
};
template <int d>
struct DTGrid {
  typedef DTGrid<d-1> INNER;
  typedef vector<DTSegment<vector<INNER> > > SEGMENTS;
  template <typename VECTOR>
  void buildGrid(const VECTOR& pool,sizeType f,sizeType t) {
    ASSERT((t-f)%2 == 0)
    if(f>=t)
      return;

    sizeType last=f,dCurr=pool.getDim(f,d);
    _seg.push_back(DTSegment<vector<INNER> >());
    _seg.back()._beg=_seg.back()._end=dCurr;
    for(f+=2; f<=t; f+=2) {
      if(f < t)
        dCurr=pool.getDim(f,d);
      if(f == t || dCurr > _seg.back()._end) {
        _seg.back()._end++;
        _seg.back()._inner.push_back(INNER());
        _seg.back()._inner.back().buildGrid(pool,last,f);
        last=f;
      }
      if(dCurr > _seg.back()._end) {
        _seg.push_back(DTSegment<vector<INNER> >());
        _seg.back()._beg=_seg.back()._end=dCurr;
      }
    }
  }
  void accumulate(sizeType& nrCell) {
    sizeType nrS=(sizeType)_seg.size();
    for(sizeType i=0; i<nrS; i++)
      for(sizeType off=_seg[i]._beg; off<_seg[i]._end; off++)
        _seg[i]._inner[off-_seg[i]._beg].accumulate(nrCell);
  }
  void size(sizeType& nrCell) const {
    sizeType nrS=(sizeType)_seg.size();
    for(sizeType i=0; i<nrS; i++)
      for(sizeType off=_seg[i]._beg; off<_seg[i]._end; off++)
        _seg[i]._inner[off-_seg[i]._beg].size(nrCell);
  }
  template <typename VECTOR,bool PARALLEL=true>
  void dilateBy(VECTOR& pool,sizeType nrL,Vec3i crd,int subD) const {
#define FOR_SEQ	\
for(sizeType k=0;k<(sizeType)_seg[i]._inner.size();k++)	\
{Vec3i crdOff=crd;crdOff[d]=_seg[i]._beg+k+j;	\
_seg[i]._inner[k].dilateBy<VECTOR,false>(pool,nrL,crdOff,subD);}

    sizeType subNrL=subD < d ? 0 : nrL;
    sizeType nrS=(sizeType)_seg.size();
    for(sizeType i=0; i<nrS; i++)
      for(sizeType j=-subNrL; j<=subNrL; j++)
        if(PARALLEL)
          OMP_PARALLEL_FOR_
          FOR_SEQ
          else FOR_SEQ
#undef FOR_SEQ
          }
  template <typename INDEX>
  sizeType find(const INDEX& id) const {
    sizeType a=0,b=(sizeType)_seg.size(),mid;
    while(b-a>1) {
      mid=(a+b)/2;
      if(_seg[mid]<=id[d])
        a=mid;
      else b=mid;
    }
    if(b-a>=1 && _seg[a]<=id[d] && _seg[a]>id[d])
      return _seg[a]._inner[id[d]-_seg[a]._beg].find(id);
    else return -1;
  }
  SEGMENTS _seg;
};
template <>
struct DTGrid<0> {
  static const char d=0;
  typedef sizeType INNER;
  typedef vector<DTSegment<INNER> > SEGMENTS;
  template <typename VECTOR>
  void buildGrid(const VECTOR& pool,sizeType f,sizeType t) {
    ASSERT((t-f)%2 == 0)
    if(f>=t)
      return;

    sizeType dBeg=pool.getDim(f,d),dEnd=pool.getDim(f+1,d);//last=f,
    _seg.push_back(DTSegment<INNER>());
    _seg.back()._beg=dBeg;
    _seg.back()._end=dEnd;
    for(f+=2; f<=t; f+=2) {
      if(f < t) {
        dBeg=pool.getDim(f,d);
        dEnd=pool.getDim(f+1,d);
      }
      if(f == t || dBeg <= _seg.back()._end)
        _seg.back()._end=std::max(_seg.back()._end,dEnd);
      else {
        _seg.push_back(DTSegment<INNER>());
        _seg.back()._beg=dBeg;
        _seg.back()._end=dEnd;
      }
    }
  }
  void accumulate(sizeType& nrCell) {
    sizeType nrS=(sizeType)_seg.size();
    for(sizeType i=0; i<nrS; i++) {
      _seg[i]._inner=nrCell;
      nrCell+=_seg[i].length();
    }
  }
  void size(sizeType& nrCell) const {
    sizeType nrS=(sizeType)_seg.size();
    for(sizeType i=0; i<nrS; i++)
      nrCell+=_seg[i].length();
  }
  template <typename VECTOR,bool PARALLEL=true>
  void dilateBy(VECTOR& pool,sizeType nrL,Vec3i crd,int subD) const {
    sizeType subNrL=subD < d ? 0 : nrL;
    Vec3i crdOff=crd;
    for(sizeType i=0; i<(sizeType)_seg.size(); i++) {
      crdOff[d]=_seg[i]._beg-subNrL;
      pool[OmpSettings::getOmpSettings().threadId()].pushId(crdOff);
      crdOff[d]=_seg[i]._end+subNrL;
      pool[OmpSettings::getOmpSettings().threadId()].pushId(crdOff);
    }
  }
  template <typename INDEX>
  sizeType find(const INDEX& id) const {
    sizeType a=0,b=(sizeType)_seg.size(),mid=(a+b)/2;
    while(b-a>1) {
      mid=(a+b)/2;
      if(_seg[mid]<=id[d])
        a=mid;
      else b=mid;
    }
    if(b-a>=1 && _seg[a]<=id[d] && _seg[a]>id[d])
      return _seg[a]._inner+id[d]-_seg[a]._beg;
    else return -1;
  }
  SEGMENTS _seg;
};
//iterator
template <int d>
struct DTIter {
  typedef typename DTGrid<d>::SEGMENTS SEGMENTS;
  DTIter() {}
  DTIter(const DTGrid<d>& grd) {
    reset(grd);
  }
  FORCE_INLINE void reset(const DTGrid<d>& grd) {
    _iter=grd._seg.begin();
    _end=grd._seg.end();
    _off=0;
    if(!isEnd())
      _innerIter.findFirst(_iter->_inner[_off]);
  }
  FORCE_INLINE sizeType cellId() const {
    return _innerIter.cellId();
  }
  FORCE_INLINE const Vec3i& getId(Vec3i& id) const {
    id[d]=_iter->_beg+_off;
    _innerIter.getId(id);
    return id;
  }
  FORCE_INLINE bool isEnd() const {
    return _off < 0 || _iter == _end;
  }
  FORCE_INLINE DTIter& incrTo(const Vec3i& id) {
    if(isEnd())
      return *this;
    sizeType idD=_iter->_beg+_off;
    if(idD > id[d])
      return *this;
    if(idD == id[d]) {
      if(_innerIter.incrTo(id).isEnd())
        restart();
      return *this;
    }
    for(; !isEnd(); _iter++) {
      _off=std::max<sizeType>(0,id[d]-_iter->_beg);
      if(_off < _iter->length()) {
        _innerIter.findFirst(_iter->_inner[_off]);
        if(_innerIter.incrTo(id).isEnd())
          restart();
        return *this;
      }
    }
    return *this;
  }
  FORCE_INLINE DTIter& operator++() {
    if((++_innerIter).isEnd())
      restart();
    return *this;
  }
  FORCE_INLINE DTIter& operator++(int) {
    return operator++();
  }
  FORCE_INLINE void restart() {
    if((++_off) == _iter->length()) {
      if((++_iter) != _end)
        _innerIter.findFirst(_iter->_inner[_off=0]);
    } else _innerIter.findFirst(_iter->_inner[_off]);
  }
  FORCE_INLINE void findFirst(const DTGrid<d>& grd) {
    _iter=grd._seg.begin();
    _end=grd._seg.end();
    _off=0;
    _innerIter.findFirst(_iter->_inner[_off]);
  }
  DTIter<d-1> _innerIter;
  typename SEGMENTS::const_iterator _iter,_end;
  sizeType _off;
};
template <int d>
struct PDTIter : public DTIter<d> {
  using DTIter<d>::_innerIter;
  using DTIter<d>::_iter;
  using DTIter<d>::_end;
  using DTIter<d>::_off;
  PDTIter() {}
  PDTIter(const DTGrid<d>& grd,sizeType valD) {
    reset(grd,valD);
  }
  FORCE_INLINE void reset(const DTGrid<d>& grd,sizeType valD) {
    _innerIter._off=-1;
    for(_iter=grd._seg.begin(); _iter != grd._seg.end(); _iter++) {
      _off=valD-_iter->_beg;
      if(_off >= 0 && _off < _iter->length()) {
        _innerIter.findFirst(_iter->_inner[_off]);
        break;
      }
    }
    _end=grd._seg.end();
  }
  FORCE_INLINE bool isEnd() const {
    return _innerIter.isEnd();
  }
  FORCE_INLINE DTIter<d>& incrTo(const Vec3i& id) {
    _innerIter.incrTo(id);
    return *this;
  }
  FORCE_INLINE PDTIter& operator++() {
    _innerIter++;
    return *this;
  }
  FORCE_INLINE PDTIter& operator++(int) {
    return operator++();
  }
};
template <>
struct DTIter<0> {
  typedef typename DTGrid<0>::SEGMENTS SEGMENTS;
  DTIter() {}
  DTIter(const DTGrid<0>& grd) {
    reset(grd);
  }
  FORCE_INLINE void reset(const DTGrid<0>& grd) {
    _iter=grd._seg.begin();
    _end=grd._seg.end();
    _off=0;
  }
  FORCE_INLINE sizeType cellId() const {
    return _iter->_inner+_off;
  }
  FORCE_INLINE const Vec3i& getId(Vec3i& id) const {
    id[0]=_iter->_beg+_off;
    return id;
  }
  FORCE_INLINE bool isEnd() const {
    return _iter == _end || _off < 0 || _off >= _iter->length();
  }
  FORCE_INLINE DTIter& incrTo(const Vec3i& id) {
    for(; _iter != _end; _iter++) {
      _off=std::max<sizeType>(0,id[0]-_iter->_beg);
      if(_off < _iter->length())
        return *this;
    }
    return *this;
  }
  FORCE_INLINE DTIter& operator++() {
    if((++_off) == _iter->length()) {
      _iter++;
      _off=0;
    }
    return *this;
  }
  FORCE_INLINE DTIter& operator++(int) {
    return operator++();
  }
  FORCE_INLINE void findFirst(const DTGrid<0>& grd) {
    _iter=grd._seg.begin();
    _end=grd._seg.end();
    _off=0;
  }
  typename SEGMENTS::const_iterator _iter,_end;
  sizeType _off;
};

template <int dim=2>
class DTHash
{
public:
  static const int DIM=dim;
  typedef DTIter<dim> CITER;
  typedef PDTIter<dim> PCITER;
  DTHash() {
    clearCache();
  }
  void clearCache() {
    if((int)_pool.size() < OmpSettings::getOmpSettings().nrThreads())
      _pool.resize(OmpSettings::getOmpSettings().nrThreads());
    for(sizeType i=0; i<(sizeType)_pool.size(); i++)
      _pool[i].clear();
    _cellIdPool.clear();
  }
  void insertCache(Vec3i id) {
    _pool[OmpSettings::getOmpSettings().threadId()].pushId(id);
    id[0]++;
    _pool[OmpSettings::getOmpSettings().threadId()].pushId(id);
  }
  void insertCache(Vec3i minC,Vec3i maxC) {
    Vec3i id;
    insertCacheInner(id,minC,maxC,dim);
  }
  sizeType size() const {
    return _nrCell;
  }
  sizeType size(sizeType d) const {
    sizeType off=0,offSeg,sz;
    for(typename DTGrid<dim>::SEGMENTS::const_iterator beg=_gridTopo._seg.begin(),end=_gridTopo._seg.end(); beg!=end; beg++) {
      offSeg=d-off;
      if(offSeg >= 0 && offSeg < beg->length()) {
        beg->_inner[offSeg].size(sz=0);
        return sz;
      }
      off+=beg->length();
    }
    return -1;
  }
  void constructFromCache() {
    sizeType nr=0;
    for(sizeType i=0; i<(sizeType)_pool.size(); i++)
      nr+=(sizeType)_pool[i].size();
    _pool[0].reserve(nr);
    for(sizeType i=1; i<(sizeType)_pool.size(); i++)
      _pool[0].insert(_pool[0].end(),_pool[i].begin(),_pool[i].end());
    IDPool backup;
    radixSort(_pool[0],backup);

    _gridTopo._seg.clear();
    _gridTopo.buildGrid(_pool[0],0,(sizeType)_pool[0].size());

    _nrCell=0;
    _gridTopo.accumulate(_nrCell);
    clearCache();
  }
  void dilateBy(sizeType nrL,bool recon=true,int subDim=dim) {
    clearCache();
    _gridTopo.dilateBy(_pool,nrL,Vec3i(),subDim);
    if(recon)constructFromCache();
  }
  DTHash& operator+=(const DTHash& other) {
    for(sizeType i=0; i<(sizeType)other._pool.size(); i++)
      _pool[0].insert(_pool[0].end(),other._pool[i].begin(),other._pool[i].end());
    return *this;
  }
  //if random access is desired
  sizeType find(const Vec3i& id) const {
    return _gridTopo.find(id);
  }
  sizeType searchD(sizeType d) const {
    sizeType off=0,offSeg;
    for(typename DTGrid<dim>::SEGMENTS::const_iterator beg=_gridTopo._seg.begin(),end=_gridTopo._seg.end(); beg!=end; beg++) {
      offSeg=d-off;
      if(offSeg >= 0 && offSeg < beg->length())
        return offSeg+beg->_beg;
      off+=beg->length();
    }
    return numeric_limits<sizeType>::max();
  }
  const DTGrid<dim>& getGrid() const {
    return _gridTopo;
  }
  //if iterator is used
  CITER getIter() const {
    return CITER(_gridTopo);
  }
  PCITER getPIter(sizeType d) const {
    return PCITER(_gridTopo,searchD(d));
  }
  sizeType nrPIter() const {
    sizeType nr=0,nrSeg=(sizeType)_gridTopo._seg.size();
    for(sizeType i=0; i<nrSeg; i++)
      nr+=_gridTopo._seg[i].length();
    return nr;
  }
  //interpolation stencil
  bool findStencilMAC(const Vec3& pos,Eigen::Matrix<sizeType,24,1>& cid,Eigen::Matrix<scalar,24,1>& coef,int Dim) const {
    ASSERT(Dim == 2 || Dim == 3)
    int nrS=(Dim-1)*4;
    Vec3i id0=floorV(Vec3(pos[0],pos[1]-0.5f,pos[2]-0.5f));
    Vec3i id1=floorV(Vec3(pos[0]-0.5f,pos[1],pos[2]-0.5f));
    Vec3i id2=floorV(Vec3(pos[0]-0.5f,pos[1]-0.5f,pos[2]));
    if(Dim == 2)
      id0[2]=id1[2]=id2[2]=0;

    if(findCube(cid.data()+0, id0) < nrS ||
       findCube(cid.data()+8, id1) < nrS ||
       (Dim == 3 && findCube(cid.data()+16,id2) < nrS))
      return false;
    else if(Dim == 2) {
      stencil2D(coef.data()+0, frac(pos[0]),frac(pos[1]-0.5f));
      stencil2D(coef.data()+8, frac(pos[0]-0.5f),frac(pos[1]));
      return true;
    } else {
      stencil3D(coef.data()+0, frac(pos[0]),frac(pos[1]-0.5f),frac(pos[2]-0.5f));
      stencil3D(coef.data()+8, frac(pos[0]-0.5f),frac(pos[1]),frac(pos[2]-0.5f));
      stencil3D(coef.data()+16,frac(pos[0]-0.5f),frac(pos[1]-0.5f),frac(pos[2]));
      return true;
    }
  }
  template <typename VEC>
  Vec3 applyStencilMAC(const Eigen::Matrix<sizeType,24,1>& cid,const Eigen::Matrix<scalar,24,1>& coef,const VEC& data,int Dim) const {
    ASSERT(Dim == 2 || Dim == 3)
    int nrS=(Dim-1)*4;
    Vec3 ret=Vec3::Zero();
    for(int d=0,off=0; d<Dim; d++,off+=8)
      for(int c=0; c<nrS; c++)
        ret[d]+=coef[off+c]*data[cid[off+c]][d];
    return ret;
  }
  template <typename VEC>
  bool isValidStencil(const Eigen::Matrix<sizeType,24,1>& cid,const Eigen::Matrix<scalar,24,1>& coef,const VEC& valid,int Dim) const {
    ASSERT(Dim == 2 || Dim == 3)
    int nrS=(Dim-1)*4;
    for(int d=0,off=0; d<Dim; d++,off+=8)
      for(int c=0; c<nrS; c++)
        if(valid[cid[off+c]][d] == 0)
          return false;
    return true;
  }
  //interpolation debugger
  void debugStencilMAC(const Vec3& pos0,const Eigen::Matrix<sizeType,24,1> cid,const Eigen::Matrix<scalar,24,1> coef,scalar S,int Dim) const {
    ASSERT(Dim == 2 || Dim == 3)
    int nrS=(Dim-1)*4;
    for(int d=0,off=0; d<Dim; d++,off+=8) {
      Vec3 pos=Vec3::Zero();
      for(int c=0; c<nrS; c++) {
        Vec3 fCtr=_cellIdPool[cid[off+c]].cast<scalar>()*S;
        fCtr+=faceCtr(d,S,Dim);
        pos+=coef[off+c]*fCtr;
      }
      INFOV("%f Err: %f!",convert<double>()(pos.norm()),convert<double>()((pos0-pos).norm()))
    }
  }
  void genCellIdPool() {
    if(!_cellIdPool.empty())
      return;
    _cellIdPool.resize(size());

    Vec3i id=Vec3i::Zero();
    const sizeType nrPI=nrPIter();
    OMP_PARALLEL_FOR_I(OMP_FPRI(id))
    for(sizeType i=0; i<nrPI; i++)
      for(PCITER it=getPIter(i); !it.isEnd(); it++) {
        it.getId(id);
        _cellIdPool[it.cellId()]=id;
      }
  }
  //cell point offsets
  Vec3 cellCtr(scalar S,int Dim) const {
    Vec3 ret=Vec3::Zero();
    ret.block(0,0,Dim,1).setConstant(S/2);
    return ret;
  }
  Vec3 faceCtr(int d,scalar S,int Dim) const {
    Vec3 ret=Vec3::Zero();
    for(int D=0; D<Dim; D++)
      if(D != d)
        ret[D]=S/2;
    return ret;
  }
  Vec3 faceCor(int d,scalar S,int Dim) const {
    return Vec3::Unit(d)*(-S/2);
  }
private:
  //helper
  sizeType findCube(sizeType cid[8],const Vec3i& id) const {
    static const Vec3i stencil[8]= {
      Vec3i(0,0,0),
      Vec3i(1,0,0),
      Vec3i(0,1,0),
      Vec3i(1,1,0),
      Vec3i(0,0,1),
      Vec3i(1,0,1),
      Vec3i(0,1,1),
      Vec3i(1,1,1),
    };

    for(int d=0; d<8; d++)
      if((cid[d]=find(id+stencil[d])) < 0)
        return d;
    return 8;
  }
  void insertCacheInner(Vec3i& id,const Vec3i& minC,const Vec3i& maxC,int dimI) {
    if(dimI == 0) {
      IDPool& pool=_pool[OmpSettings::getOmpSettings().threadId()];
      id[0]=minC[0];
      pool.pushId(id);
      id[0]=maxC[0];
      pool.pushId(id);
    } else {
      for(id[dimI]=minC[dimI]; id[dimI]<maxC[dimI]; id[dimI]++)
        insertCacheInner(id,minC,maxC,dimI-1);
    }
  }
  static void radixSort(IDPool& pool,IDPool& poolBK) {
    sizeType bucket[8][256];
    memset(bucket,0,sizeof(sizeType)*256*8);

    //count number of values in a bucket
    poolBK.resize(pool.size());
    sizeType n=(sizeType)pool.size();
    for(sizeType i=0; i<n; i+=2) {
      bucket[0][(pool[i]>>0LL )&255LL]++;
      bucket[1][(pool[i]>>8LL )&255LL]++;
      bucket[2][(pool[i]>>16LL)&255LL]++;
      bucket[3][(pool[i]>>24LL)&255LL]++;
      bucket[4][(pool[i]>>32LL)&255LL]++;
      bucket[5][(pool[i]>>40LL)&255LL]++;
      bucket[6][(pool[i]>>48LL)&255LL]++;
      bucket[7][(pool[i]>>56LL)&255LL]++;
    }

    //accumulate buckets
    for(sizeType i=1; i<256; i++) {
      bucket[0][i]+=bucket[0][i-1];
      bucket[1][i]+=bucket[1][i-1];
      bucket[2][i]+=bucket[2][i-1];
      bucket[3][i]+=bucket[3][i-1];
      bucket[4][i]+=bucket[4][i-1];
      bucket[5][i]+=bucket[5][i-1];
      bucket[6][i]+=bucket[6][i-1];
      bucket[7][i]+=bucket[7][i-1];
    }
    for(sizeType i=255; i>=1; i--) {
      bucket[0][i]=bucket[0][i-1];
      bucket[1][i]=bucket[1][i-1];
      bucket[2][i]=bucket[2][i-1];
      bucket[3][i]=bucket[3][i-1];
      bucket[4][i]=bucket[4][i-1];
      bucket[5][i]=bucket[5][i-1];
      bucket[6][i]=bucket[6][i-1];
      bucket[7][i]=bucket[7][i-1];
    }
    bucket[0][0]=0;
    bucket[1][0]=0;
    bucket[2][0]=0;
    bucket[3][0]=0;
    bucket[4][0]=0;
    bucket[5][0]=0;
    bucket[6][0]=0;
    bucket[7][0]=0;

    //redistribute
    for(sizeType p=0; p<8; p++) {
      sizeType k=p*8;
      for(sizeType i=0; i<n; i+=2) {
        sizeType& pos=bucket[p][(pool[i]>>(sizeType)k)&255LL];
        poolBK[pos*2]=pool[i];
        poolBK[pos*2+1]=pool[i+1];
        pos++;
      }
      pool.swap(poolBK);
    }

    //parity check
    for(sizeType i=0; i<n; i+=2)
      ASSERT(pool.getDim(i+1,0) > pool.getDim(i,0));
  }
  static scalar frac(scalar val) {
    return val-floor(val);
  }
  //data
  DTGrid<dim> _gridTopo;
  vector<Vec3i> _cellIdPool;
  vector<IDPool> _pool;
  sizeType _nrCell;
};
template <int nr,int dim>
struct SPDTIter {
  SPDTIter() {}
  SPDTIter(const Vec3i off[nr]) {
    for(int i=0; i<nr; i++)
      _off[i]=off[i];
    ASSERT(_off[0].isZero());
  }
  SPDTIter(const DTHash<dim>& hash,const Vec3i off[nr],sizeType d) {
    for(int i=0; i<nr; i++)
      _off[i]=off[i];
    ASSERT(_off[0].isZero());
    reset(hash.getGrid(),hash.searchD(d));
  }
  FORCE_INLINE void reset(const DTGrid<dim>& grd,sizeType valD) {
    Vec3i idT;
    _iter[0].reset(grd,valD);
    for(int i=1; i<nr; i++) {
      _iter[i].reset(grd,valD+_off[i][dim]);
      _iter[i].incrTo(getId(idT,i));
    }
  }
  FORCE_INLINE const Vec3i& getId(Vec3i& id,sizeType i) const {
    _iter[0].getId(id);
    return id+=_off[i];
  }
  FORCE_INLINE bool isEnd() const {
    return _iter[0].isEnd();
  }
  FORCE_INLINE SPDTIter& operator++() {
    Vec3i idT;
    _iter[0]++;
    if(_iter[0].isEnd())
      return *this;
    for(int i=1; i<nr; i++)
      _iter[i].incrTo(getId(idT,i));
    return *this;
  }
  FORCE_INLINE SPDTIter& operator++(int) {
    return operator++();
  }
  FORCE_INLINE bool cellId(Eigen::Matrix<sizeType,nr,1>& ret) const {
    Vec3i id=Vec3i::Zero(),idT=Vec3i::Zero();
    ret[0]=_iter[0].cellId();
    bool all=true;
    for(int i=1; i<nr; i++)
      if(!_iter[i].isEnd() && _iter[i].getId(id) == getId(idT,i))
        ret[i]=_iter[i].cellId();
      else {
        ret[i]=-1;
        all=false;
      }
    return all;
  }
  Vec3i _off[nr];
  PDTIter<dim> _iter[nr];
};
template <typename HASH>
class DTHashIO
{
public:
  template <typename VEC>
  static void writeScalarHash(const std::string& path,const HASH& grid,scalar cellSz,const VEC& vals) {
    vector<scalar> css;
    Vec3 corner[2];
    Vec3i id=Vec3i::Zero();
    VTKWriter<scalar> os("DTHash",path,true);
    for(typename HASH::CITER it=grid.getIter(); !it.isEnd(); it++) {
      it.getId(id);
      corner[0]=id.cast<scalar>()*cellSz;
      corner[1]=corner[0];
      corner[1].block(0,0,HASH::DIM+1,1).array()+=cellSz;

      css.push_back(vals[it.cellId()]);
      os.appendVoxels(corner,corner+2,false);
    }
    os.appendCustomData("NodalDTGrid",css.begin(),css.end());
  }
  template <typename T,typename ALLOC>
  static void writeVelocityHash(const std::string& path,const HASH& grid,scalar cellSz,const vector<T,ALLOC>& vals,scalar len=1.0f) {
    Vec3 offVel[3]= {Vec3::Zero(),Vec3::Zero(),Vec3::Zero(),};
    for(int d=0; d<=HASH::DIM; d++)
      for(int k=0; k<=HASH::DIM; k++)
        if(k != d)offVel[d][k]+=cellSz/2;

    vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
    Vec3 pos;
    Vec3i id=Vec3i::Zero();
    VTKWriter<scalar> os("DTHash",path,true);
    for(typename HASH::CITER it=grid.getIter(); !it.isEnd(); it++) {
      it.getId(id);
      pos=id.cast<scalar>()*cellSz;
      for(int d=0; d<=HASH::DIM; d++) {
        vss.push_back(pos+offVel[d]);
        vss.push_back(pos+offVel[d]+Vec3::Unit(d)*vals[it.cellId()][d]*len);
      }
    }
    os.appendPoints(vss.begin(),vss.end());
    os.appendCells(VTKWriter<scalar>::IteratorIndex<Vec3i>(0,2,0),
                   VTKWriter<scalar>::IteratorIndex<Vec3i>((sizeType)vss.size()/2,2,0),
                   VTKWriter<scalar>::LINE);
  }
  template <typename T,typename ALLOC>
  static void writeVectorHash(const std::string& path,const HASH& grid,scalar cellSz,const vector<T,ALLOC>& vals,int d) {
    Vec3 pos;
    Vec3i id=Vec3i::Zero();
    vector<scalar> css;
    vector<Vec3,Eigen::aligned_allocator<Vec3> > vss;
    for(typename HASH::CITER it=grid.getIter(); !it.isEnd(); it++) {
      it.getId(id);
      pos=(id.cast<scalar>()-Vec3::Unit(d)*0.5f)*cellSz;
      vss.push_back(pos);
      vss.push_back(pos+Vec3::Constant(cellSz));
      css.push_back(vals[it.cellId()][d]);
    }

    VTKWriter<scalar> os("Vector",path,true);
    os.appendVoxels(vss.begin(),vss.end(),true);
    os.appendCustomData("VectorComponent",css.begin(),css.end());
  }
  template <typename T,typename ALLOC>
  static void writeVectorHash(const std::string& path,const HASH& grid,scalar cellSz,const vector<T,ALLOC>& vals) {
    writeVectorHash(path+"U.vtk",grid,cellSz,vals,0);
    writeVectorHash(path+"V.vtk",grid,cellSz,vals,1);
    writeVectorHash(path+"W.vtk",grid,cellSz,vals,2);
  }
};

PRJ_END

#endif
