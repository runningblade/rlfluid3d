#include "ParticleSetHash.h"
#include "DTGrid.h"
#include <CommonFile/ParticleCD.h>
#include <CommonFile/GridBasic.h>
#include <CommonFile/ImplicitFuncInterface.h>

PRJ_BEGIN

//implementation
template <typename T,typename PS_TYPE,typename EXTRACT_POS>
class ParticleSetHash
{
  typedef typename PS_TYPE::value_type P_TYPE;
  typedef typename ScalarUtil<T>::ScalarVec3 Vec3Type;
public:
  class CountFunc : public CollisionFunc<P_TYPE>
  {
  public:
    CountFunc():_nr(0) {}
    virtual void operator()(const P_TYPE& p,const sizeType& id) {
      _nr++;
    }
    sizeType _nr;
  };
  ParticleSetHash(const PS_TYPE& pset):_pset(pset) {}
  void update(T cell) {
    _cell=cell;
    OMP_PARALLEL_FOR_
    for(sizeType i=0; i<_pset.size(); i++)
      _head.insertCache(getId(_pset[i]));
    _head.constructFromCache();
    _headVal.assign(_head.size(),-1);

    //insert
    _next.assign(_pset.size(),-1);
    for(sizeType i=0; i<_pset.size(); i++) {
      sizeType& head=_headVal[_head.find(getId(_pset[i]))];
      if(head != -1)
        _next[i]=head;
      head=i;
    }
  }
  Vec3i getId(const P_TYPE& p) const {
    Vec3Type pFrac=EXTRACT_POS::extract(p)/_cell;
    return floorV(pFrac);
  }
  void find(const Vec3Type& ctr,T sqrRad,CollisionFunc<P_TYPE>& f) const {
    sizeType off=convert<sizeType>()(ceil(sqrt(sqrRad)/_cell));
    Vec3i id=floorV(Vec3Type(ctr/_cell)),curr;
    for(curr[0]=id[0]-off; curr[0]<=id[0]+off; curr[0]++)
      for(curr[1]=id[1]-off; curr[1]<=id[1]+off; curr[1]++)
        for(curr[2]=id[2]-off; curr[2]<=id[2]+off; curr[2]++) {
          const sizeType cid=_head.find(curr);
          if(cid >= 0) {
            sizeType curr=_headVal[cid];
            while(curr != -1) {
              if((EXTRACT_POS::extract(_pset[curr])-ctr).squaredNorm() < sqrRad)
                f(_pset[curr],curr);
              curr=_next[curr];
            }
          }
        }
  }
  sizeType count(const Vec3Type& ctr,T sqrRad) const {
    CountFunc f;
    find(ctr,sqrRad,f);
    return f._nr;
  }
protected:
  DTHash<2> _head;
  vector<sizeType> _headVal;
  vector<sizeType> _next;
  const PS_TYPE& _pset;
  T _cell;
};
template <typename T,typename PS_TYPE,typename EXTRACT_POS>
class ParticleSetList
{
  typedef typename PS_TYPE::value_type P_TYPE;
  typedef typename ScalarUtil<T>::ScalarVec3 Vec3Type;
public:
  ParticleSetList(const PS_TYPE& pset):_pset(pset) {}
  void update(T cell) {
    //initialize
    vector<BBox<T> > bbs(OmpSettings::getOmpSettings().nrThreads());
    OMP_PARALLEL_FOR_
    for(sizeType i=0; i<_pset.size(); i++)
      bbs[OmpSettings::getOmpSettings().threadId()].setUnion(_pset[i]._pos);
    for(sizeType i=1; i<(sizeType)bbs.size(); i++)
      bbs[0].setUnion(bbs[i]);
    bbs[0].enlargedEps(0.01f);

    //build lists
    Vec3i id;
    _next.assign(_pset.size(),-1);
    _head.reset(ceilV(Vec3(bbs[0].getExtent()/cell)),bbs[0],-1,true,1);
    for(sizeType i=0; i<_pset.size(); i++) {
      id=floorV(_head.getIndexFracSafe(_pset[i]._pos));
      sizeType& head=_head.get(id);
      if(head != -1)
        _next[i]=head;
      head=i;
    }
  }
  void find(const Vec3Type& ctr,T sqrRad,CollisionFunc<P_TYPE>& f) const {
    if(_head.getNrPoint()[0] <= 0 || _head.getNrPoint()[1] <= 0 || _head.getNrPoint()[2] <= 0)
      return;

    sizeType head;
    Vec3 delta=Vec3::Constant(sqrt(sqrRad));
    Vec3i minC=floorV(_head.getIndexFracSafe(ctr-delta));
    Vec3i maxC=floorV(_head.getIndexFracSafe(ctr+delta)),curr;
    for(curr[0]=minC[0]; curr[0]<=maxC[0]; curr[0]++)
      for(curr[1]=minC[1]; curr[1]<=maxC[1]; curr[1]++)
        for(curr[2]=minC[2]; curr[2]<=maxC[2]; curr[2]++) {
          head=_head.get(curr);
          while(head != -1) {
            if((EXTRACT_POS::extract(_pset[head])-ctr).squaredNorm() < sqrRad)
              f(_pset[head],head);
            head=_next[head];
          }
        }
  }
  sizeType count(const Vec3Type& ctr,T sqrRad) const {
    typename ParticleSetHash<T,PS_TYPE,EXTRACT_POS>::CountFunc f;
    find(ctr,sqrRad,f);
    return f._nr;
  }
protected:
  Grid<sizeType,T> _head;
  vector<sizeType> _next;
  const PS_TYPE& _pset;
};
template <typename T,typename PS_TYPE,typename EXTRACT_POS>
class PSetImplicitFunc : public ImplicitFunc<T>
{
  typedef typename PS_TYPE::value_type P_TYPE;
  typedef typename ScalarUtil<T>::ScalarVec3 Vec3Type;
  typedef typename ScalarUtil<T>::ScalarMat3 Mat3Type;
  class ImplicitCollisionFunc : public CollisionFunc<P_TYPE>
  {
  public:
    ImplicitCollisionFunc(const Vec3Type& ctr,T rad,T radQSqr):_weight(0.0f),_dXBardX(Mat3Type::Zero()),_xBar(Vec3Type::Zero()),_dWeightdX(Vec3Type::Zero()),_ctr(ctr),_rad(rad),_radQSqr(radQSqr) {}
    virtual void operator()(const P_TYPE& p,const sizeType& id) {
      Vec3Type pos=EXTRACT_POS::extract(p);
      T distSqr=1-(pos-_ctr).squaredNorm()/_radQSqr;
      if(distSqr <= 0.0f)
        return;

      T w=distSqr*distSqr;
      Vec3Type dWdX=(6*w/_radQSqr)*(pos-_ctr);
      _dXBardX+=pos*dWdX.transpose();
      _dWeightdX+=dWdX;

      w*=distSqr;
      _xBar+=w*pos;
      _weight+=w;
    }
    Mat3Type getDXDx() const {
      return (_dXBardX*_weight-_xBar*_dWeightdX.transpose())/(_weight*_weight);
    }
    Vec3Type getXBar() const {
      return _xBar/_weight;
    }
    T get() const {
      const T Thres=0.5f,TLow=0.4f,THigh=2.0f;
      if(_weight == 0.0f)
        return sqrt(_radQSqr);
      else {
        const T dist=(_ctr-getXBar()).norm();
        if(dist < _rad*Thres) {
          const T ev=EVMax();
          if(ev >= TLow) {
            const T gamma=(THigh-ev)/(THigh-TLow);
            return dist-_rad*gamma*((gamma-3)*gamma+3);
          }
        }
        return dist-_rad;
      }
    }
  protected:
    T EVMax() const {
      Mat3Type dXdx=getDXDx();
      dXdx=(dXdx*dXdx.transpose()).eval();
      double A[3][3],w[3];
      for(int r=0; r<3; r++)
        for(int c=0; c<3; c++)
          A[r][c]=convert<double>()(dXdx(r,c));
      dsyevc3(A,w);
      return convert<T>()(sqrt(max(w[0],max(w[1],w[2]))));
    }
    static void dsyevc3(double A[3][3], double w[3]) {
#define M_SQRT3    1.73205080756887729352744634151   // sqrt(3)
      // Macros
#define SQR(x)      ((x)*(x))                        // x^2
      double m, c1, c0;

      // Determine coefficients of characteristic poynomial. We write
      //       | a   d   f  |
      //  A =  | d*  b   e  |
      //       | f*  e*  c  |
      double de = A[0][1] * A[1][2];                                    // d * e
      double dd = SQR(A[0][1]);                                         // d^2
      double ee = SQR(A[1][2]);                                         // e^2
      double ff = SQR(A[0][2]);                                         // f^2
      m  = A[0][0] + A[1][1] + A[2][2];
      c1 = (A[0][0]*A[1][1] + A[0][0]*A[2][2] + A[1][1]*A[2][2])        // a*b + a*c + b*c - d^2 - e^2 - f^2
           - (dd + ee + ff);
      c0 = A[2][2]*dd + A[0][0]*ee + A[1][1]*ff - A[0][0]*A[1][1]*A[2][2]
           - 2.0 * A[0][2]*de;                                     // c*d^2 + a*e^2 + b*f^2 - a*b*c - 2*f*d*e)

      double p, sqrt_p, q, c, s, phi;
      p = SQR(m) - 3.0*c1;
      q = m*(p - (3.0/2.0)*c1) - (27.0/2.0)*c0;
      sqrt_p = sqrt(fabs(p));

      phi = 27.0 * ( 0.25*SQR(c1)*(p - c1) + c0*(q + 27.0/4.0*c0));
      phi = (1.0/3.0) * atan2(sqrt(fabs(phi)), q);

      c = sqrt_p*cos(phi);
      s = (1.0/M_SQRT3)*sqrt_p*sin(phi);

      w[1]  = (1.0/3.0)*(m - c);
      w[2]  = w[1] + s;
      w[0]  = w[1] + c;
      w[1] -= s;
    }
    T _weight;
    Mat3Type _dXBardX;
    Vec3Type _xBar,_dWeightdX;
    const Vec3Type& _ctr;
    const T _rad,_radQSqr;
  };
public:
  PSetImplicitFunc(const PS_TYPE& pset,T rad,T qRad)
    :_pset(pset),_hash(pset),_rad(rad),_qRad(qRad) {
    update();
    //debugDXdx();
  }
  void update() {
    _hash.update(_qRad);
  }
  T operator()(const Vec3Type& pos) const {
    T qRadSqr=_qRad*_qRad;
    ImplicitCollisionFunc f(pos,_rad,qRadSqr);
    _hash.find(pos,qRadSqr,f);
    return f.get();
  }
  const PS_TYPE& getPSet() const {
    return _pset;
  }
  T getRad() const {
    return _rad;
  }
  T& getQRad() {
    return _qRad;
  }
  const T& getQRad() const {
    return _qRad;
  }
  BBox<T> getBB() const {
    BBox<T> bb;
    for(sizeType i=0; i<(sizeType)_pset.size(); i++)
      bb.setUnion(EXTRACT_POS::extract(_pset[i]));
    return bb;
  }
  //debug derivative function
  void debugDXdx() const {
    T qRadSqr=_qRad*_qRad;
    for(sizeType i=0; i<_pset.size(); i++) {
      Vec3Type p=_pset[i]._pos;
      ImplicitCollisionFunc f(p,_rad,qRadSqr);
      _hash.find(p,qRadSqr,f);

#define DELTA 1E-6f
      Mat3Type DXDx=f.getDXDx();
      for(int d=0; d<3; d++) {
        Vec3Type p2=_pset[i]._pos+Vec3Type::Unit(d)*DELTA;
        ImplicitCollisionFunc f2(p2,_rad,qRadSqr);
        _hash.find(p,qRadSqr,f2);
        INFOV("%f, Err: %f!",convert<double>()(DXDx.col(d).norm()),
              convert<double>()((DXDx.col(d)-(f2.getXBar()-f.getXBar())/DELTA).norm()))
      }
#undef DELTA
    }
  }
private:
  const PS_TYPE& _pset;
  ParticleSetHash<T,PS_TYPE,EXTRACT_POS> _hash;
  T _rad,_qRad;
};

//adaptor
NeighQueryN::NeighQueryN(const ParticleSetN& pset)
{
  _impl.reset(new IMPL(pset));
}
void NeighQueryN::update(scalar cell)
{
  _impl->update(cell);
}
void NeighQueryN::find(const Vec3& ctr,scalar sqrRad,CollisionFunc<ParticleN<scalar> >& f) const
{
  _impl->find(ctr,sqrRad,f);
}
sizeType NeighQueryN::count(const Vec3& ctr,scalar sqrRad) const
{
  return _impl->count(ctr,sqrRad);
}

PSetImplicitFuncN::PSetImplicitFuncN(const ParticleSetN& pset, scalar rad, scalar qRad)
{
  _impl.reset(new IMPL(pset,rad,qRad));
}
void PSetImplicitFuncN::update()
{
  _impl->update();
}
scalar PSetImplicitFuncN::operator()(const Vec3& pos) const
{
  return _impl->operator()(pos);
}
const ParticleSetN& PSetImplicitFuncN::getPSet() const
{
  return _impl->getPSet();
}
scalar PSetImplicitFuncN::getRad() const
{
  return _impl->getRad();
}
scalar& PSetImplicitFuncN::getQRad()
{
  return _impl->getQRad();
}
const scalar& PSetImplicitFuncN::getQRad() const
{
  return _impl->getQRad();
}
BBox<scalar> PSetImplicitFuncN::getBB() const
{
  return _impl->getBB();
}

PRJ_END
