#include <Fluid/Utils.h>
#include <Fluid/RigidSolver.h>
#include <CommonFile/geom/StaticGeomCell.h>

USE_PRJ_NAMESPACE

#define ALL_BOX
#define NR_RIGID 1000
int main()
{
  //add rigid
  Vec3i warpMode(1,0,0);
  BBox<scalar> bb(Vec3(0,0,0),Vec3(2,2,1));
  RigidSolver sol(3,1/32.0f);
  sol.setGravity(Vec3::UnitY()*-9.81f);
  sol.setBB(bb);
  RandEngine::seed(100);
  RandEngine::useDeterministic();
  //add solid
  Mat4 T=Mat4::Identity();
  T.block<3,1>(0,3)=Vec3(1,0.5f,0.5f);
  T.block<3,3>(0,0)=makeRotation<scalar>(Vec3(0,0,M_PI/4));
  boost::shared_ptr<StaticGeomCell> cell(new BoxGeomCell(T,3,Vec3(0.5f,0.1f,0.5f)));
  sol.addSolid(cell);
  //add body
  for(sizeType i=0; i<NR_RIGID; i++) {
    scalar rad=RandEngine::randR(0.1f,0.3f);
    Vec3 pos=Vec3(RandEngine::randR(bb._minC[0]+rad,bb._maxC[0]-rad),
                  RandEngine::randR(bb._minC[1]+rad,bb._maxC[1]-rad),
                  RandEngine::randR(bb._minC[2]+rad,bb._maxC[2]-rad));
    if(!sol.intersect(rad,pos,false)) {
#ifdef ALL_BOX
        sol.addBox(Vec3(rad/2,rad/4,rad/2),pos,Vec3(RandEngine::randR01(),RandEngine::randR01(),RandEngine::randR01()));
#else
      if(i < NR_RIGID/2)
        sol.addCross(Vec3(rad/2,rad/4,rad/2),pos,Vec3(RandEngine::randR01(),RandEngine::randR01(),RandEngine::randR01()));
      else sol.addBall(rad,pos);
#endif
    }
  }
  sol.setWarpMode(warpMode);
  //save load
  {
    boost::filesystem::ofstream os("rigid.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    sol.write(os,dat.get());
  }
  {
    sol=RigidSolver();
    boost::filesystem::ifstream is("rigid.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    sol.read(is,dat.get());
  }
  //simulate
  recreate("rigid");
  scalar gTime=0,dt=1E-3f;
  sol.writeMeshVTK("rigid/mesh.vtk");
  sol.writePSetVTK("rigid/pset.vtk");
  for(sizeType i=0,I=10; i<5000; i++) {
    INFOV("Simulating frame: %ld!",i)
    sol.advance(dt,gTime);
    if(i%I == 0) {
      std::ostringstream oss;
      oss << "rigid/mesh" << i/I << ".vtk";
      sol.writeMeshVTK(oss.str());
    }
    gTime+=dt;
  }
}
