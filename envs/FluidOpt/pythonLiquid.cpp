#include <Fluid/ScriptedSource.h>
#include <Fluid/LiquidSolver.h>
#include <CommonFile/RotationUtil.h>

USE_PRJ_NAMESPACE

#define SPARSITY 1
class FuncLiquid : public ImplicitFunc<scalar>
{
public:
  scalar operator()(const Vec3& pos) const {
    return 0;
  }
};
extern "C" {
  LiquidSolverFLIP<2,EncoderConstant<SPARSITY> >* solLiquid=NULL;
  boost::shared_ptr<RandomSource> sLiquid[10];
  VectorField featureLiquid;
  void destroyLiquid()
  {
    if(solLiquid) {
      delete solLiquid;
      solLiquid=NULL;
    }
  }
  void createLiquid(scalar x,scalar y,scalar sz,scalar szShooter,scalar sgn[4], bool warpModeOn=false)
  {
    ASSERT(x > 1 && y > 1 && sz > 5)
    scalar coef=1<<SPARSITY;
    destroyLiquid();
    //create liquid
    solLiquid=new LiquidSolverFLIP<2,EncoderConstant<SPARSITY> >();
    Vec3i *warpMode=NULL;
    if(warpModeOn)
      warpMode=new Vec3i(1,0,0);
    solLiquid->initDomain(BBox<scalar>(Vec3(0.0f,0.0f,0.0f),Vec3(x,y,0.0f)),Vec3(1.0f/sz,1.0f/sz,0.0f),warpMode);
    {
      boost::shared_ptr<RigidSolver> rigid(new RigidSolver(2,1/sz));
      rigid->setBB(BBox<scalar>(Vec3::Constant(1/sz),Vec3(x-1/sz,y-1/sz,0)));
      solLiquid->initSolid(rigid);
    }
    solLiquid->initLiquid(FuncLiquid());
    solLiquid->setGlobalTime(0.0f);
    solLiquid->setGlobalId(0);
    solLiquid->setUseVariational(true);
    solLiquid->setUseUnilateral(false);
    //add source
    for(int i=0; i<2; ++i) {
      sLiquid[i].reset(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitSphere(Vec3(x/2,szShooter,0.0f),szShooter,2,1.0f/sz,Vec3(0,0,0)));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(sLiquid[i])->_sources.push_back(sf);
      solLiquid->addSource(sLiquid[i]);
    }
    //add sink
    {
      boost::shared_ptr<SourceRegion> s(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf;
      if(warpModeOn)
        sf=boost::shared_ptr<SourceImplicitFunc>(new ImplicitRectNeg(Vec3(-x,2*sgn[1]*coef/sz,0),Vec3(2*x,y-2*sgn[3]*coef/sz,0),2,1/sz));
      else
        sf=boost::shared_ptr<SourceImplicitFunc>(new ImplicitRectNeg(Vec3(2*sgn[0]*coef/sz,2*sgn[1]*coef/sz,0),Vec3(x-2*sgn[2]*coef/sz,y-2*sgn[3]*coef/sz,0),2,1/sz));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(s)->_sources.push_back(sf);
      s->setSource(false);
      sf->_source=false;
      solLiquid->addSource(s);
    }
  }
  void createLiquidSingle(scalar x,scalar y,scalar sz,scalar szShooter,scalar sgn[4],bool warpModeOn=false)
  {
    ASSERT(x > 1 && y > 1 && sz > 5)
    scalar coef=1<<SPARSITY;
    destroyLiquid();
    //create liquid
    solLiquid=new LiquidSolverFLIP<2,EncoderConstant<SPARSITY> >();
    Vec3i *warpMode=NULL;
    if(warpModeOn)
      warpMode=new Vec3i(1,0,0);
    solLiquid->initDomain(BBox<scalar>(Vec3(0.0f,0.0f,0.0f),Vec3(x,y,0.0f)),Vec3(1.0f/sz,1.0f/sz,0.0f),warpMode);
    {
      boost::shared_ptr<RigidSolver> rigid(new RigidSolver(2,1/sz));
      rigid->setBB(BBox<scalar>(Vec3::Constant(1/sz),Vec3(x-1/sz,y-1/sz,0)));
      solLiquid->initSolid(rigid);
    }
    solLiquid->initLiquid(FuncLiquid());
    solLiquid->setGlobalTime(0.0f);
    solLiquid->setGlobalId(0);
    solLiquid->setUseVariational(true);
    solLiquid->setUseUnilateral(true);
    //add source
    {
      sLiquid[0].reset(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitSphere(Vec3(x/2,szShooter,0.0f),szShooter,2,1.0f/sz,Vec3(0,0,0)));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(sLiquid[0])->_sources.push_back(sf);
      solLiquid->addSource(sLiquid[0]);
    }
    //add sink
    {
      boost::shared_ptr<SourceRegion> s(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf;
      if(warpModeOn)
        sf=boost::shared_ptr<SourceImplicitFunc>(new ImplicitRectNeg(Vec3(-x,2*sgn[1]*coef/sz,0),Vec3(2*x,y-2*sgn[3]*coef/sz,0),2,1/sz));
      else
        sf=boost::shared_ptr<SourceImplicitFunc>(new ImplicitRectNeg(Vec3(2*sgn[0]*coef/sz,2*sgn[1]*coef/sz,0),Vec3(x-2*sgn[2]*coef/sz,y-2*sgn[3]*coef/sz,0),2,1/sz));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(s)->_sources.push_back(sf);
      s->setSource(false);
      sf->_source=false;
      solLiquid->addSource(s);
    }
  }
  void createLiquidTriple(scalar x,scalar y,scalar sz,scalar szShooter, scalar spd, scalar sgn[4])
  {
    ASSERT(x > 1 && y > 1 && sz > 5)
    scalar coef=1<<SPARSITY;
    destroyLiquid();
    //create liquid
    solLiquid=new LiquidSolverFLIP<2,EncoderConstant<SPARSITY> >();
    solLiquid->initDomain(BBox<scalar>(Vec3(0.0f,0.0f,0.0f),Vec3(x,y,0.0f)),Vec3(1.0f/sz,1.0f/sz,0.0f));
    {
      boost::shared_ptr<RigidSolver> rigid(new RigidSolver(2,1/sz));
      rigid->setBB(BBox<scalar>(Vec3::Constant(1/sz),Vec3(x-1/sz,y-1/sz,0)));
      solLiquid->initSolid(rigid);
    }
    solLiquid->initLiquid(FuncLiquid());
    solLiquid->setGlobalTime(0.0f);
    solLiquid->setGlobalId(0);
    solLiquid->setUseVariational(true);
    solLiquid->setUseUnilateral(true);
    //add source: L, R
    for(int i=0; i<2; ++i) {
      sLiquid[i].reset(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitSphere(Vec3(szShooter,y/2,0.0f),szShooter,2,1.0f/sz,Vec3(0,0,0)));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(sLiquid[i])->_sources.push_back(sf);
      solLiquid->addSource(sLiquid[i]);
    }
    //add source: 1
    sLiquid[2].reset(new RandomSource(1.0f));
    boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitSphere(Vec3(x/2,szShooter,0.0f),szShooter,2,1.0f/sz,Vec3(0,0,0)));
    boost::dynamic_pointer_cast<ImplicitFuncSource>(sLiquid[2])->_sources.push_back(sf);
    solLiquid->addSource(sLiquid[2]);
    //add sink
    {
      boost::shared_ptr<SourceRegion> s(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitRectNeg(Vec3(2*sgn[0]*coef/sz,2*sgn[1]*coef/sz,0),Vec3(x-2*sgn[2]*coef/sz,y-2*sgn[3]*coef/sz,0),2,1/sz));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(s)->_sources.push_back(sf);
      s->setSource(false);
      sf->_source=false;
      solLiquid->addSource(s);
    }
    boost::shared_ptr<ImplicitSphere> ss[3];
    for(int i=0; i<3; ++i) {
      ss[i]=boost::dynamic_pointer_cast<ImplicitSphere>(sLiquid[i]->_sources[0]);
    }
    scalar width=solLiquid->getGrid0().getBBNodal().getExtent()[0];
    ss[0]->_ctr[0]=szShooter;
    ss[1]->_ctr[0]=width - szShooter;
    ss[2]->_ctr[1]=szShooter;
  }
  void createLiquidQuadruple(scalar x,scalar y,scalar sz,scalar szShooter, scalar spd, scalar sgn[4])
  {
    ASSERT(x > 1 && y > 1 && sz > 5)
    scalar coef=1<<SPARSITY;
    destroyLiquid();
    //create liquid
    solLiquid=new LiquidSolverFLIP<2,EncoderConstant<SPARSITY> >();
    solLiquid->initDomain(BBox<scalar>(Vec3(0.0f,0.0f,0.0f),Vec3(x,y,0.0f)),Vec3(1.0f/sz,1.0f/sz,0.0f));
    {
      boost::shared_ptr<RigidSolver> rigid(new RigidSolver(2,1/sz));
      rigid->setBB(BBox<scalar>(Vec3::Constant(1/sz),Vec3(x-1/sz,y-1/sz,0)));
      solLiquid->initSolid(rigid);
    }
    solLiquid->initLiquid(FuncLiquid());
    solLiquid->setGlobalTime(0.0f);
    solLiquid->setGlobalId(0);
    solLiquid->setUseVariational(true);
    solLiquid->setUseUnilateral(true);
    //add source: L, R
    for(int i=0; i<2; ++i) {
      sLiquid[i].reset(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitSphere(Vec3(szShooter,y/2,0.0f),szShooter,2,1.0f/sz,Vec3(0,0,0)));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(sLiquid[i])->_sources.push_back(sf);
      solLiquid->addSource(sLiquid[i]);
    }
    //add source: 1-2
    for(int i=2; i<4; ++i) {
      sLiquid[i].reset(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitSphere(Vec3(x/2,szShooter,0.0f),szShooter,2,1.0f/sz,Vec3(0,0,0)));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(sLiquid[i])->_sources.push_back(sf);
      solLiquid->addSource(sLiquid[i]);
    }
    //add sink
    {
      boost::shared_ptr<SourceRegion> s(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitRectNeg(Vec3(2*sgn[0]*coef/sz,2*sgn[1]*coef/sz,0),Vec3(x-2*sgn[2]*coef/sz,y-2*sgn[3]*coef/sz,0),2,1/sz));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(s)->_sources.push_back(sf);
      s->setSource(false);
      sf->_source=false;
      solLiquid->addSource(s);
    }
    boost::shared_ptr<ImplicitSphere> ss[4];
    for(int i=0; i<4; ++i) {
      ss[i]=boost::dynamic_pointer_cast<ImplicitSphere>(sLiquid[i]->_sources[0]);
    }
    scalar width=solLiquid->getGrid0().getBBNodal().getExtent()[0];
    ss[0]->_ctr[0]=szShooter;
    ss[0]->_vel=Vec3(spd, 0, 0);
    ss[0]->reset(ss[0]->_ctr, ss[0]->_rad, 2, ss[0]->_res, ss[0]->_vel);
    ss[1]->_ctr[0]=width - szShooter;
    ss[1]->_vel=Vec3(-spd, 0, 0);
    ss[1]->reset(ss[1]->_ctr, ss[1]->_rad, 2, ss[1]->_res, ss[1]->_vel);
    ss[2]->_ctr[1]=szShooter;
    ss[3]->_ctr[1]=szShooter;
  }
  void createLiquidMulti(scalar x,scalar y,scalar sz,scalar szShooter, scalar spd, scalar sgn[4])
  {
    ASSERT(x > 1 && y > 1 && sz > 5)
    scalar coef=1<<SPARSITY;
    destroyLiquid();
    //create liquid
    solLiquid=new LiquidSolverFLIP<2,EncoderConstant<SPARSITY> >();
    solLiquid->initDomain(BBox<scalar>(Vec3(0.0f,0.0f,0.0f),Vec3(x,y,0.0f)),Vec3(1.0f/sz,1.0f/sz,0.0f));
    {
      boost::shared_ptr<RigidSolver> rigid(new RigidSolver(2,1/sz));
      rigid->setBB(BBox<scalar>(Vec3::Constant(1/sz),Vec3(x-1/sz,y-1/sz,0)));
      solLiquid->initSolid(rigid);
    }
    solLiquid->initLiquid(FuncLiquid());
    solLiquid->setGlobalTime(0.0f);
    solLiquid->setGlobalId(0);
    solLiquid->setUseVariational(true);
    solLiquid->setUseUnilateral(true);
    //add source: L, R
    for(int i=0; i<2; ++i) {
      sLiquid[i].reset(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitSphere(Vec3(szShooter,y/2,0.0f),szShooter,2,1.0f/sz,Vec3(0,0,0)));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(sLiquid[i])->_sources.push_back(sf);
      solLiquid->addSource(sLiquid[i]);
    }
    //add source: 1-8
    for(int i=2; i<10; ++i) {
      sLiquid[i].reset(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitSphere(Vec3(x/2,szShooter,0.0f),szShooter,2,1.0f/sz,Vec3(0,0,0)));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(sLiquid[i])->_sources.push_back(sf);
      solLiquid->addSource(sLiquid[i]);
    }
    //add sink
    {
      boost::shared_ptr<SourceRegion> s(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitRectNeg(Vec3(2*sgn[0]*coef/sz,2*sgn[1]*coef/sz,0),Vec3(x-2*sgn[2]*coef/sz,y-2*sgn[3]*coef/sz,0),2,1/sz));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(s)->_sources.push_back(sf);
      s->setSource(false);
      sf->_source=false;
      solLiquid->addSource(s);
    }
    boost::shared_ptr<ImplicitSphere> ss[10];
    for(int i=0; i<10; ++i) {
      ss[i]=boost::dynamic_pointer_cast<ImplicitSphere>(sLiquid[i]->_sources[0]);
    }
    scalar width=solLiquid->getGrid0().getBBNodal().getExtent()[0];
    ss[0]->_ctr[0]=szShooter;
    ss[0]->_vel=Vec3(spd, 0, 0);
    ss[0]->reset(ss[0]->_ctr, ss[0]->_rad, 2, ss[0]->_res, ss[0]->_vel);
    ss[1]->_ctr[0]=width - szShooter;
    ss[1]->_vel=Vec3(-spd, 0, 0);
    ss[1]->reset(ss[1]->_ctr, ss[1]->_rad, 2, ss[1]->_res, ss[1]->_vel);
    for(int i=2; i<10; ++i) {
      ss[i]->_ctr[0]=(i - 1.5) * width / 8;
      ss[i]->_ctr[1]=szShooter;
      ss[i]->_vel=Vec3(0, spd, 0);
      ss[i]->reset(ss[i]->_ctr, ss[i]->_rad, 2, ss[i]->_res, ss[i]->_vel);
    }
  }
  void createLiquidBattle(scalar x,scalar y,scalar sz,scalar szShooter,scalar sgn[4])
  {
    // printf("come inside\n");
    printf("x:%f, y:%f, az:%f\n", x, y, sz);
    ASSERT(x > 1 && y > 1 && sz > 5)
    // int count =0;
    // printf("debug count %d\n", count++);
    scalar coef=1<<SPARSITY;
    // printf("debug count %d\n", count++);
    destroyLiquid();
    //create liquid
    // printf("debug count %d\n", count++);
    solLiquid=new LiquidSolverFLIP<2,EncoderConstant<SPARSITY> >();
    solLiquid->initDomain(BBox<scalar>(Vec3(0.0f,0.0f,0.0f),Vec3(x,y,0.0f)),Vec3(1.0f/sz,1.0f/sz,0.0f));
    {
      boost::shared_ptr<RigidSolver> rigid(new RigidSolver(2,1/sz));
      rigid->setBB(BBox<scalar>(Vec3::Constant(1/sz),Vec3(x-1/sz,y-1/sz,0)));
      solLiquid->initSolid(rigid);
    }
    solLiquid->initLiquid(FuncLiquid());
    solLiquid->setGlobalTime(0.0f);
    solLiquid->setGlobalId(0);
    solLiquid->setUseVariational(true);
    solLiquid->setUseUnilateral(true);
    //add source: L, R
    for(int i=0; i<2; ++i) {
      sLiquid[i].reset(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitSphere(Vec3(szShooter,y/2,0.0f),szShooter,2,1.0f/sz,Vec3(0,0,0)));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(sLiquid[i])->_sources.push_back(sf);
      solLiquid->addSource(sLiquid[i]);
    }
    //add sink
    {
      boost::shared_ptr<SourceRegion> s(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitRectNeg(Vec3(2*sgn[0]*coef/sz,2*sgn[1]*coef/sz,0),Vec3(x-2*sgn[2]*coef/sz,y-2*sgn[3]*coef/sz,0),2,1/sz));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(s)->_sources.push_back(sf);
      s->setSource(false);
      sf->_source=false;
      solLiquid->addSource(s);
    }
  }
  void createLiquidBottomBattle(scalar x,scalar y,scalar sz,scalar szShooter,scalar sgn[4])
  {
    ASSERT(x > 1 && y > 1 && sz > 5)
    scalar coef=1<<SPARSITY;
    destroyLiquid();
    //create liquid
    solLiquid=new LiquidSolverFLIP<2,EncoderConstant<SPARSITY> >();
    solLiquid->initDomain(BBox<scalar>(Vec3(0.0f,0.0f,0.0f),Vec3(x,y,0.0f)),Vec3(1.0f/sz,1.0f/sz,0.0f));
    {
      boost::shared_ptr<RigidSolver> rigid(new RigidSolver(2,1/sz));
      rigid->setBB(BBox<scalar>(Vec3::Constant(1/sz),Vec3(x-1/sz,y-1/sz,0)));
      solLiquid->initSolid(rigid);
    }
    solLiquid->initLiquid(FuncLiquid());
    solLiquid->setGlobalTime(0.0f);
    solLiquid->setGlobalId(0);
    solLiquid->setUseVariational(true);
    solLiquid->setUseUnilateral(true);
    //add source: 1
    {
      sLiquid[0].reset(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitSphere(Vec3(x/4,szShooter,0.0f),szShooter,2,1.0f/sz,Vec3(0,0,0)));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(sLiquid[0])->_sources.push_back(sf);
      solLiquid->addSource(sLiquid[0]);
    }
    //add source: 2
    {
      sLiquid[1].reset(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitSphere(Vec3(x*3/4,szShooter,0.0f),szShooter,2,1.0f/sz,Vec3(0,0,0)));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(sLiquid[1])->_sources.push_back(sf);
      solLiquid->addSource(sLiquid[1]);
    }
    //add sink
    {
      boost::shared_ptr<SourceRegion> s(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitRectNeg(Vec3(2*sgn[0]*coef/sz,2*sgn[1]*coef/sz,0),Vec3(x-2*sgn[2]*coef/sz,y-2*sgn[3]*coef/sz,0),2,1/sz));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(s)->_sources.push_back(sf);
      s->setSource(false);
      sf->_source=false;
      solLiquid->addSource(s);
    }
  }
  void addLiquidSolid(scalar cx,scalar cy,scalar x,scalar y,bool influenceLiquid)
  {
    if(solLiquid) {
      RigidSolver& rigid=solLiquid->getRigid();
      Mat4 T=Mat4::Identity();
      T.block<3,1>(0,3)=Vec3(cx,cy,0);
      boost::shared_ptr<StaticGeomCell> cell(new BoxGeomCell(T,2,Vec3(x,y,0),0.0f));
      rigid.addSolid(cell);
      if(influenceLiquid)
        solLiquid->initSolid(boost::shared_ptr<RigidSolver>());
    }
  }
  void setRigidLiquid(scalar x,scalar y,scalar rad,scalar rho, scalar gravity=-9.81f)
  {
    if(solLiquid) {
      RigidSolver& rigid=solLiquid->getRigid();
      rigid.setGravity(Vec3(0, gravity, 0));
      if(rigid.nrBody() == 0)
        rigid.addBall(rad,Vec3(x,y,0));
      else rigid.getBody(0).setPos(Vec3(x,y,0));
      rigid.getBody(0).rho()=rho;
    }
  }
  void setRigidBoxLiquid(scalar x,scalar y,scalar rx,scalar ry,scalar rho,bool cross)
  {
    if(solLiquid) {
      RigidSolver& rigid=solLiquid->getRigid();
      if(rigid.nrBody() == 0) {
        if(cross)
          rigid.addCross(Vec3(rx,ry,0),Vec3(x,y,0),Vec3::Zero());
        else rigid.addBox(Vec3(rx,ry,0),Vec3(x,y,0),Vec3::Zero());
      } else rigid.getBody(0).setPos(Vec3(x,y,0));
      rigid.getBody(0).rho()=rho;
    }
  }
  void setRigidVelLiquid(scalar x,scalar y)
  {
    if(solLiquid) {
      RigidSolver& rigid=solLiquid->getRigid();
      if(rigid.nrBody() == 1)
        rigid.getBody(0).setP(Vec3(x,y,0)*rigid.getBody(0).getM());
    }
  }
  void getRigidPosLiquid(scalar* x,scalar* y)
  {
    if(solLiquid) {
      RigidSolver& rigid=solLiquid->getRigid();
      if(rigid.nrBody() > 0) {
        Vec3 pos=rigid.getBody(0).pos();
        *x=pos[0];
        *y=pos[1];
      }
    }
  }
  void getRigidRotLiquid(scalar r[4])
  {
    if(solLiquid) {
      RigidSolver& rigid=solLiquid->getRigid();
      if(rigid.nrBody() > 0) {
        Mat3 rot=rigid.getBody(0).rot();
        r[0]=rot(0,0);
        r[1]=rot(1,0);
        r[2]=rot(0,1);
        r[3]=rot(1,1);
      }
    }
  }
  void getRigidRotAngleLiquid(scalar* theta)
  {
    if(solLiquid) {
      RigidSolver& rigid=solLiquid->getRigid();
      if(rigid.nrBody() > 0)
        *theta=invExpW<scalar>(rigid.getBody(0).rot())[2];
    }
  }
  void getRigidVelLiquid(scalar* x,scalar* y)
  {
    if(solLiquid) {
      RigidSolver& rigid=solLiquid->getRigid();
      if(rigid.nrBody() > 0) {
        Vec3 vel=rigid.getBody(0).linSpd();
        *x=vel[0];
        *y=vel[1];
      }
    }
  }
  void getRigidRotVelLiquid(scalar* dtheta)
  {
    if(solLiquid) {
      RigidSolver& rigid=solLiquid->getRigid();
      if(rigid.nrBody() > 0)
        *dtheta=rigid.getBody(0).rotSpd()[2];
    }
  }
  void getRigidForceLiquid(scalar* force)
  {
    if(solLiquid) {
      RigidSolver& rigid=solLiquid->getRigid();
      if(rigid.nrBody() > 0) {
        force[0]=solLiquid->getForceOnBody(0)[0];
        force[1]=solLiquid->getForceOnBody(0)[1];
      }
    }
  }
  void getRigidTorqueLiquid(scalar* torque)
  {
    if(solLiquid) {
      RigidSolver& rigid=solLiquid->getRigid();
      if(rigid.nrBody() > 0) {
        torque[0]=solLiquid->getTorqueOnBody(0)[2];
      }
    }
  }
  void getRigidBBLiquid(scalar* minC,scalar* maxC)
  {
    minC[0]=minC[1]=maxC[0]=maxC[1]=0;
    if(solLiquid) {
      RigidSolver& rigid=solLiquid->getRigid();
      if(rigid.nrBody() > 0) {
        BBox<scalar> bb;
        rigid.getBody(0).getWorldBB(bb);
        minC[0]=bb._minC[0];
        minC[1]=bb._minC[1];
        maxC[0]=bb._maxC[0];
        maxC[1]=bb._maxC[1];
      }
    }
  }
  void transferLiquid(scalar dt,bool left,bool right,scalar* x,scalar dist,scalar spd, bool* contact=NULL)
  {
    Vec3 pos;
    if(solLiquid) {
      boost::shared_ptr<ImplicitSphere> ss[2];
      for(int i=0; i<2; ++i) {
        ss[i]=boost::dynamic_pointer_cast<ImplicitSphere>(sLiquid[i]->_sources[0]);
      }
      scalar dx=solLiquid->getGrid0()._cellSz0.maxCoeff();
      scalar width=solLiquid->getGrid0().getBBNodal().getExtent()[0];
      *x=max<scalar>(*x,(dist/2+dx));
      *x=min<scalar>(*x,width-(dist/2+dx));
      ss[0]->_ctr[0]=*x-dist/2;
      ss[1]->_ctr[0]=*x+dist/2;
      //turn the shooter
      {
        scalar rx,ry;
        getRigidPosLiquid(&rx,&ry);
        pos=Vec3(rx,ry,0);
      }
      for(int i=0; i<2; ++i) {
        ss[i]->_vel=(pos-ss[i]->_ctr);
        ss[i]->_vel *= spd/max<scalar>(ss[i]->_vel.norm(),1E-6f);
        ss[i]->reset(ss[i]->_ctr,ss[i]->_rad,2,ss[i]->_res,ss[i]->_vel);
      }
      //add source
      solLiquid->LiquidSolverPIC<2,EncoderConstant<SPARSITY> >::focusOn(0);
      if(left)
        solLiquid->addSource(sLiquid[0]);
      else solLiquid->removeSource(sLiquid[0]);
      if(right)
        solLiquid->addSource(sLiquid[1]);
      else solLiquid->removeSource(sLiquid[1]);
      solLiquid->advance(dt);
      if(contact) {
        *contact=false;
        RigidSolver& rigid=solLiquid->getRigid();
        if(rigid.nrBody() > 0)
          *contact=rigid.getBody(0).hasContactFlag();
      }
    }
  }
  void transferLiquidSingle(scalar dt, bool trigger, scalar *degree, scalar *x, scalar spd, int * vsize, int * isize, bool warpModeOn, bool *contact=NULL)
  {
    Vec3 pos;
    if(solLiquid) {
      boost::shared_ptr<ImplicitSphere> ss=boost::dynamic_pointer_cast<ImplicitSphere>(sLiquid[0]->_sources[0]);
      scalar dx=solLiquid->getGrid0()._cellSz0.maxCoeff();
      scalar width=solLiquid->getGrid0().getBBNodal().getExtent()[0];
      // Pingchuan: in case of moving traspassing
      if(warpModeOn) {
        while(*x<0) *x+=width;
        while(*x>width) *x-=width;
      }else {
        *x=max<scalar>(*x, dx);
        *x=min<scalar>(*x, width - dx);
      }
      // Pingchuan: in case of ratating traspassing
      *degree=min<scalar>(*degree, 150.0);
      *degree=max<scalar>(*degree, 30.0);
      // Pingchuan: set the horizontal position
      // Vec3[0] == Vecf3.x
      ss->_ctr[0]=*x;
      //turn the shooter
      scalar degree_=M_PI **degree / 180.0;
      ss->_vel=Vec3(cos(degree_), sin(degree_), 0);
      ss->_vel *= spd / max<scalar>(ss->_vel.norm(), 1E-6f);
      ss->reset(ss->_ctr, ss->_rad, 2, ss->_res, ss->_vel);
      //add source
      solLiquid->LiquidSolverPIC<2,EncoderConstant<SPARSITY> >::focusOn(0);
      if(trigger)
        solLiquid->addSource(sLiquid[0]);
      else
        solLiquid->removeSource(sLiquid[0]);
      solLiquid->advance(dt);
      if(contact) {
        *contact=false;
        RigidSolver& rigid=solLiquid->getRigid();
        if(rigid.nrBody() > 0)
          *contact=rigid.getBody(0).hasContactFlag();
      }
      const ObjMesh& mesh=solLiquid->getSurfaceMesh(true);
      *vsize=(int)mesh.getV().size();
      *isize=(int)mesh.getI().size();
    }
  }
  void transferLiquidDouble(scalar dt, bool left, bool right, scalar *degree1, scalar *degree2, scalar *x, scalar dist, scalar spd, scalar radsh, int * vsize, int * isize, bool warpModeOn, bool *contact=NULL)
  {
    Vec3 pos;
    if(solLiquid) {
      boost::shared_ptr<ImplicitSphere> ss[2];
      for(int i=0; i<2; ++i) {
        ss[i]=boost::dynamic_pointer_cast<ImplicitSphere>(sLiquid[i]->_sources[0]);
      }
      scalar dx=solLiquid->getGrid0()._cellSz0.maxCoeff();
      scalar width=solLiquid->getGrid0().getBBNodal().getExtent()[0];
      if(warpModeOn) {
        while(*x<0) *x+=width;
        while(*x>width) *x-=width;
      }else {
        *x=max<scalar>(*x,(dist/2+radsh+dx));
        *x=min<scalar>(*x,width-(dist/2+radsh+dx));
      }
      ss[0]->_ctr[0]=*x-dist/2;
      ss[1]->_ctr[0]=*x+dist/2;
      if(warpModeOn) {
        for(int i=0; i<2; ++i) {
          while(ss[i]->_ctr[0]<0) ss[i]->_ctr[0]+=width;
          while(ss[i]->_ctr[0]>width) ss[i]->_ctr[0]-=width;
        }
      }
      // Pingchuan: in case of ratating traspassing
      *degree1=min<scalar>(*degree1, 150.0);
      *degree1=max<scalar>(*degree1, 30.0);
      *degree2=min<scalar>(*degree2, 150.0);
      *degree2=max<scalar>(*degree2, 30.0);
      //turn the shooter
      scalar degree1_=M_PI **degree1 / 180.0;
      ss[0]->_vel=Vec3(cos(degree1_), sin(degree1_), 0);
      ss[0]->_vel *= spd / max<scalar>(ss[0]->_vel.norm(), 1E-6f);
      ss[0]->reset(ss[0]->_ctr, ss[0]->_rad, 2, ss[0]->_res, ss[0]->_vel);
      scalar degree2_=M_PI **degree2 / 180.0;
      ss[1]->_vel=Vec3(cos(degree2_), sin(degree2_), 0);
      ss[1]->_vel *= spd / max<scalar>(ss[1]->_vel.norm(), 1E-6f);
      ss[1]->reset(ss[1]->_ctr, ss[1]->_rad, 2, ss[1]->_res, ss[1]->_vel);
      //add source
      solLiquid->LiquidSolverPIC<2,EncoderConstant<SPARSITY> >::focusOn(0);
      if(left)
        solLiquid->addSource(sLiquid[0]);
      else solLiquid->removeSource(sLiquid[0]);
      if(right)
        solLiquid->addSource(sLiquid[1]);
      else solLiquid->removeSource(sLiquid[1]);
      solLiquid->advance(dt);
      if(contact) {
        *contact=false;
        RigidSolver& rigid=solLiquid->getRigid();
        if(rigid.nrBody() > 0)
          *contact=rigid.getBody(0).hasContactFlag();
      }
      const ObjMesh& mesh=solLiquid->getSurfaceMesh(true);
      *vsize=(int)mesh.getV().size();
      *isize=(int)mesh.getI().size();
    }
  }
  void transferLiquidTriple(scalar dt, bool shoot[3], scalar possh[3], scalar theta[3], scalar hkey, scalar radsh, scalar spd, int * vsize, int * isize, bool *contact=NULL)
  {
    Vec3 pos;
    if(solLiquid) {
      boost::shared_ptr<ImplicitSphere> ss[3];
      for(int i=0; i<3; ++i) {
        ss[i]=boost::dynamic_pointer_cast<ImplicitSphere>(sLiquid[i]->_sources[0]);
      }
      scalar dx=solLiquid->getGrid0()._cellSz0.maxCoeff();
      scalar width=solLiquid->getGrid0().getBBNodal().getExtent()[0];
      scalar height=solLiquid->getGrid0().getBBNodal().getExtent()[1];
      // Pingchuan: in case of moving traspassing
      for(int i=0; i<2; ++i) {
        possh[i]=max<scalar>(possh[i], 2*radsh+dx);
        possh[i]=min<scalar>(possh[i], height-hkey-radsh-dx);
      }
      possh[2]=max<scalar>(possh[2], radsh+dx);
      possh[2]=min<scalar>(possh[2], width-radsh-dx);
      ss[0]->_ctr[1]=possh[0];
      ss[1]->_ctr[1]=possh[1];
      ss[2]->_ctr[0]=possh[2];
      // Pingchuan: in case of ratating traspassing
      theta[0]=max<scalar>(theta[0], -60.0);
      theta[0]=min<scalar>(theta[0], 60.0);
      theta[1]=max<scalar>(theta[1], 120.0);
      theta[1]=min<scalar>(theta[1], 240.0);
      theta[2]=max<scalar>(theta[2], 30.0);
      theta[2]=min<scalar>(theta[2], 150.0);
      //turn the shooter
      for(int i=0; i<3; ++i) {
        scalar degree=M_PI*theta[i]/180.0;
        ss[i]->_vel=Vec3(cos(degree),sin(degree),0);
        ss[i]->_vel*=spd/max<scalar>(ss[i]->_vel.norm(), 1E-6f);
        ss[i]->reset(ss[i]->_ctr, ss[i]->_rad, 2, ss[i]->_res, ss[i]->_vel);
      }
      //add source
      solLiquid->LiquidSolverPIC<2,EncoderConstant<SPARSITY> >::focusOn(0);
      for(int i=0; i<3; ++i) {
        shoot[i] ? solLiquid->addSource(sLiquid[i]) : solLiquid->removeSource(sLiquid[i]);
      }
      solLiquid->advance(dt);
      if(contact) {
        *contact=false;
        RigidSolver& rigid=solLiquid->getRigid();
        if(rigid.nrBody() > 0)
          *contact=rigid.getBody(0).hasContactFlag();
      }
      const ObjMesh& mesh=solLiquid->getSurfaceMesh(true);
      *vsize=(int)mesh.getV().size();
      *isize=(int)mesh.getI().size();
    }
  }
  void transferLiquidQuadruple(scalar dt, bool shoot[4], scalar *degree1, scalar *degree2, scalar *x, scalar *y1, scalar *y2, scalar hkey, scalar radsh, scalar dist, scalar spd, bool *contact=NULL)
  {
    Vec3 pos;
    if(solLiquid) {
      boost::shared_ptr<ImplicitSphere> ss[4];
      for(int i=0; i<4; ++i) {
        ss[i]=boost::dynamic_pointer_cast<ImplicitSphere>(sLiquid[i]->_sources[0]);
      }
      scalar dx=solLiquid->getGrid0()._cellSz0.maxCoeff();
      scalar width=solLiquid->getGrid0().getBBNodal().getExtent()[0];
      scalar height=solLiquid->getGrid0().getBBNodal().getExtent()[1];
      // Pingchuan: in case of moving traspassing
      *y1=max<scalar>(*y1, radsh + dx);
      *y1=min<scalar>(*y1, height - hkey - radsh - dx);
      *y2=max<scalar>(*y2, radsh + dx);
      *y2=min<scalar>(*y2, height - hkey - radsh - dx);
      *x=max<scalar>(*x,(dist/2+radsh+dx));
      *x=min<scalar>(*x,width-(dist/2+radsh+dx));
      ss[0]->_ctr[1]= *y1;
      ss[1]->_ctr[1]= *y2;
      ss[0]->reset(ss[0]->_ctr, ss[0]->_rad, 2, ss[0]->_res, ss[0]->_vel);
      ss[1]->reset(ss[1]->_ctr, ss[1]->_rad, 2, ss[1]->_res, ss[1]->_vel);
      ss[2]->_ctr[0]= *x-dist/2;
      ss[3]->_ctr[0]= *x+dist/2;
      // Pingchuan: in case of ratating traspassing
      *degree1=min<scalar>(*degree1, 150.0);
      *degree1=max<scalar>(*degree1, 30.0);
      *degree2=min<scalar>(*degree2, 150.0);
      *degree2=max<scalar>(*degree2, 30.0);
      //turn the shooter
      scalar degree1_=M_PI **degree1 / 180.0;
      ss[2]->_vel=Vec3(cos(degree1_), sin(degree1_), 0);
      ss[2]->_vel *= spd / max<scalar>(ss[2]->_vel.norm(), 1E-6f);
      ss[2]->reset(ss[2]->_ctr, ss[2]->_rad, 2, ss[2]->_res, ss[2]->_vel);
      scalar degree2_=M_PI **degree2 / 180.0;
      ss[3]->_vel=Vec3(cos(degree2_), sin(degree2_), 0);
      ss[3]->_vel *= spd / max<scalar>(ss[3]->_vel.norm(), 1E-6f);
      ss[3]->reset(ss[3]->_ctr, ss[3]->_rad, 2, ss[3]->_res, ss[3]->_vel);
      //add source
      solLiquid->LiquidSolverPIC<2,EncoderConstant<SPARSITY> >::focusOn(0);
      for(int i=0; i<4; ++i) {
        shoot[i] ? solLiquid->addSource(sLiquid[i]) : solLiquid->removeSource(sLiquid[i]);
      }
      solLiquid->advance(dt);
      if(contact) {
        *contact=false;
        RigidSolver& rigid=solLiquid->getRigid();
        if(rigid.nrBody() > 0)
          *contact=rigid.getBody(0).hasContactFlag();
      }
    }
  }
  void transferLiquidMulti(scalar dt, bool shoot[10], scalar *y1, scalar *y2, scalar hkey, scalar radsh, bool *contact=NULL)
  {
    Vec3 pos;
    if(solLiquid) {
      boost::shared_ptr<ImplicitSphere> ss[2];
      for(int i=0; i<2; ++i) {
        ss[i]=boost::dynamic_pointer_cast<ImplicitSphere>(sLiquid[i]->_sources[0]);
      }
      scalar dx=solLiquid->getGrid0()._cellSz0.maxCoeff();
      scalar height=solLiquid->getGrid0().getBBNodal().getExtent()[1];
      // Pingchuan: in case of moving traspassing
      *y1=max<scalar>(*y1, radsh + dx);
      *y1=min<scalar>(*y1, height - hkey - radsh - dx);
      *y2=max<scalar>(*y2, radsh + dx);
      *y2=min<scalar>(*y2, height - hkey - radsh - dx);
      ss[0]->_ctr[1]= *y1;
      ss[1]->_ctr[1]= *y2;
      ss[0]->reset(ss[0]->_ctr, ss[0]->_rad, 2, ss[0]->_res, ss[0]->_vel);
      ss[1]->reset(ss[1]->_ctr, ss[1]->_rad, 2, ss[1]->_res, ss[1]->_vel);
      //add source
      solLiquid->LiquidSolverPIC<2,EncoderConstant<SPARSITY> >::focusOn(0);
      for(int i=0; i<10; ++i) {
        shoot[i] ? solLiquid->addSource(sLiquid[i]) : solLiquid->removeSource(sLiquid[i]);
      }
      solLiquid->advance(dt);
      if(contact) {
        *contact=false;
        RigidSolver& rigid=solLiquid->getRigid();
        if(rigid.nrBody() > 0)
          *contact=rigid.getBody(0).hasContactFlag();
      }
    }
  }
  void transferLiquidMultiSingle(scalar dt, bool isBottom, bool trigger, scalar *degree, scalar *x, scalar spd, int *vsize, int *isize)
  {
    Vec3 pos;
    if(solLiquid) {
      boost::shared_ptr<ImplicitSphere> ss=boost::dynamic_pointer_cast<ImplicitSphere>(sLiquid[0]->_sources[0]);
      scalar dx=solLiquid->getGrid0()._cellSz0.maxCoeff();
      scalar width=solLiquid->getGrid0().getBBNodal().getExtent()[0];
      scalar height=solLiquid->getGrid0().getBBNodal().getExtent()[1];
      // Pingchuan: in case of moving traspassing
      *x=max<scalar>(*x, dx);
      *x=min<scalar>(*x, width - dx);
      // Pingchuan: in case of ratating traspassing
      if(isBottom) {
        *degree=min<scalar>(*degree, 150.0);
        *degree=max<scalar>(*degree, 30.0);
        ss->_ctr[1]=dx;
      } else {
        ss->_ctr[1]=height - dx;
        *degree=min<scalar>(*degree, 330.0);
        *degree=max<scalar>(*degree, 210.0);
      }
      // Pingchuan: set the horizontal position
      // Vec3[0] == Vecf3.x
      ss->_ctr[0]=*x;
      //turn the shooter
      scalar degree_=M_PI **degree / 180.0;
      ss->_vel=Vec3(cos(degree_), sin(degree_), 0);
      ss->_vel *= spd / max<scalar>(ss->_vel.norm(), 1E-6f);
      ss->reset(ss->_ctr, ss->_rad, 2, ss->_res, ss->_vel);
      //add source
      solLiquid->LiquidSolverPIC<2,EncoderConstant<SPARSITY> >::focusOn(0);
      if(trigger)
        solLiquid->addSource(sLiquid[0]);
      else
        solLiquid->removeSource(sLiquid[0]);
      solLiquid->advance(dt);
      const ObjMesh& mesh=solLiquid->getSurfaceMesh(true);
      *vsize=(int)mesh.getV().size();
      *isize=(int)mesh.getI().size();
    }
  }
  void transferLiquidBottomBattle(scalar dt, bool left, bool right, scalar *theta1, scalar *theta2,
                                  scalar *x1, scalar *x2, scalar spd, int *vsize, int *isize, scalar radsh, bool *contact)
  {
    Vec3 pos;
    if(solLiquid) {
      boost::shared_ptr<ImplicitSphere> ss[2];
      for(int i=0; i<2; ++i) {
        ss[i]=boost::dynamic_pointer_cast<ImplicitSphere>(sLiquid[i]->_sources[0]);
      }
      scalar dx=solLiquid->getGrid0()._cellSz0.maxCoeff();
      scalar width=solLiquid->getGrid0().getBBNodal().getExtent()[0];
      // Pingchuan: in case of moving traspassing
      *x1=max<scalar>(*x1, dx);
      *x1=min<scalar>(*x1, width/2 - radsh - dx);
      *x2=max<scalar>(*x2, width/2 + radsh + dx);
      *x2=min<scalar>(*x2, width - dx);
      // Pingchuan: in case of ratating traspassing
      *theta1=min<scalar>(*theta1, 150.0);
      *theta1=max<scalar>(*theta1, 30.0);
      *theta2=min<scalar>(*theta2, 150.0);
      *theta2=max<scalar>(*theta2, 30.0);
      // Pingchuan: set the horizontal position
      // Vec3[0] == Vecf3.x
      ss[0]->_ctr[0]=*x1;
      ss[1]->_ctr[0]=*x2;
      //turn the shooter
      scalar theta1_=M_PI **theta1 / 180.0;
      ss[0]->_vel=Vec3(cos(theta1_), sin(theta1_), 0);
      ss[0]->_vel *= spd / max<scalar>(ss[0]->_vel.norm(), 1E-6f);
      ss[0]->reset(ss[0]->_ctr, ss[0]->_rad, 2, ss[0]->_res, ss[0]->_vel);
      scalar theta2_=M_PI **theta2 / 180.0;
      ss[1]->_vel=Vec3(cos(theta2_), sin(theta2_), 0);
      ss[1]->_vel *= spd / max<scalar>(ss[1]->_vel.norm(), 1E-6f);
      ss[1]->reset(ss[1]->_ctr, ss[1]->_rad, 2, ss[1]->_res, ss[1]->_vel);
      //add source
      solLiquid->LiquidSolverPIC<2,EncoderConstant<SPARSITY> >::focusOn(0);
      if(left)
        solLiquid->addSource(sLiquid[0]);
      else
        solLiquid->removeSource(sLiquid[0]);
      if(right)
        solLiquid->addSource(sLiquid[1]);
      else
        solLiquid->removeSource(sLiquid[1]);
      solLiquid->advance(dt);
      if(contact) {
        *contact=false;
        RigidSolver& rigid=solLiquid->getRigid();
        if(rigid.nrBody() > 0)
          *contact=rigid.getBody(0).hasContactFlag();
      }
      const ObjMesh& mesh=solLiquid->getSurfaceMesh(true);
      *vsize=(int)mesh.getV().size();
      *isize=(int)mesh.getI().size();
    }
  }
  void transferLiquidBattle(scalar dt, bool left, bool right, scalar *degree1, scalar *degree2, 
    scalar *y1, scalar *y2, scalar spd, int *vsize, int *isize, scalar radsh, bool * contact)
  {
    Vec3 pos;
    if(solLiquid) {
      boost::shared_ptr<ImplicitSphere> ss[2];
      for(int i=0; i<2; ++i) {
        ss[i]=boost::dynamic_pointer_cast<ImplicitSphere>(sLiquid[i]->_sources[0]);
      }
      scalar dx=solLiquid->getGrid0()._cellSz0.maxCoeff();
      scalar width=solLiquid->getGrid0().getBBNodal().getExtent()[0];
      scalar height=solLiquid->getGrid0().getBBNodal().getExtent()[1];
      // Pingchuan: in case of moving traspassing
      *y1=max<scalar>(*y1, radsh + dx);
      *y1=min<scalar>(*y1, height - radsh - dx);
      *y2=max<scalar>(*y2, radsh + dx);
      *y2=min<scalar>(*y2, height - radsh - dx);
      // Pingchuan: in case of ratating traspassing
      *degree1=min<scalar>(*degree1, 90.0);
      *degree1=max<scalar>(*degree1, -90.0);
      ss[0]->_ctr[0]=dx;
      ss[1]->_ctr[0]=width - dx;
      *degree2=min<scalar>(*degree2, 270.0);
      *degree2=max<scalar>(*degree2, 90.0);
      // Pingchuan: set the vertical position
      ss[0]->_ctr[1]=*y1;
      ss[1]->_ctr[1]=*y2;
      //turn the shooter
      scalar degree1_=M_PI **degree1 / 180.0;
      ss[0]->_vel=Vec3(cos(degree1_), sin(degree1_), 0);
      ss[0]->_vel *= spd / max<scalar>(ss[0]->_vel.norm(), 1E-6f);
      ss[0]->reset(ss[0]->_ctr, ss[0]->_rad, 2, ss[0]->_res, ss[0]->_vel);
      scalar degree2_=M_PI **degree2 / 180.0;
      ss[1]->_vel=Vec3(cos(degree2_), sin(degree2_), 0);
      ss[1]->_vel *= spd / max<scalar>(ss[1]->_vel.norm(), 1E-6f);
      ss[1]->reset(ss[1]->_ctr, ss[1]->_rad, 2, ss[1]->_res, ss[1]->_vel);
      //add source
      solLiquid->LiquidSolverPIC<2,EncoderConstant<SPARSITY> >::focusOn(0);
      if(left)
        solLiquid->addSource(sLiquid[0]);
      else
        solLiquid->removeSource(sLiquid[0]);
      if(right)
        solLiquid->addSource(sLiquid[1]);
      else
        solLiquid->removeSource(sLiquid[1]);
      solLiquid->advance(dt);
      if(contact) {
        *contact=false;
        RigidSolver& rigid=solLiquid->getRigid();
        if(rigid.nrBody() > 0)
          *contact=rigid.getBody(0).hasContactFlag();
      }
      const ObjMesh& mesh=solLiquid->getSurfaceMesh(true);
      *vsize=(int)mesh.getV().size();
      *isize=(int)mesh.getI().size();
    }
  }
  void getLiquidMesh(float *mesh_v, int *mesh_i)
  {
    if(solLiquid) {
      const ObjMesh& mesh=solLiquid->getSurfaceMesh(true);
      size_t v_size=mesh.getV().size();
      size_t i_size=mesh.getI().size();
      for(size_t i=0; i<v_size; ++i) {
        mesh_v[i * 3]=mesh.getV(i).x();
        mesh_v[i * 3 + 1]=mesh.getV(i).y();
        mesh_v[i * 3 + 2]=0;
      }
      for(size_t i=0; i<i_size; ++i) {
        mesh_i[i * 3]=mesh.getI(i).x();
        mesh_i[i * 3 + 1]=mesh.getI(i).y();
        mesh_i[i * 3 + 2]=mesh.getI(i).z();
      }
    }
  }
  //set sz > 0 to get a feature region around rigid body, sz indicates size of feature
  //set sz<0 to get a feature of whole region
  void getFeatureLiquid(scalar sz,scalar* rho,scalar* minC,scalar* cellSz,int* nrPoint)
  {
    if(solLiquid) {
      if(!rho) {
        solLiquid->getFeature(featureLiquid,sz);
      } else {
        sizeType k=0;
        for(sizeType i=0; i<featureLiquid.getNrPoint().prod(); i++) {
          rho[k++]=featureLiquid.get(i)[0];
          rho[k++]=featureLiquid.get(i)[1];
        }
      }
      Vec3 pt=featureLiquid.getPt(Vec3i::Zero());
      minC[0]=pt[0];
      minC[1]=pt[1];
      cellSz[0]=featureLiquid.getCellSize()[0];
      cellSz[1]=featureLiquid.getCellSize()[1];
      nrPoint[0]=featureLiquid.getNrPoint()[0];
      nrPoint[1]=featureLiquid.getNrPoint()[1];
    } else {
      minC[0]=minC[1]=0;
      cellSz[0]=cellSz[1]=0;
      nrPoint[0]=nrPoint[1]=0;
    }
  }
  //set sz > 0 to get a feature region around rigid body, sz indicates size of feature
  //set sz<0 to get a feature of whole region
  void getFeatureLiquidSimple(scalar sz, scalar* rho, int* nrPoint)
  {
    if(solLiquid) {
      if(!rho) {
        solLiquid->getFeature(featureLiquid,sz);
        nrPoint[0]=featureLiquid.getNrPoint()[0];
        nrPoint[1]=featureLiquid.getNrPoint()[1];
      } else {
        sizeType k=0;
        for(sizeType i=0; i<featureLiquid.getNrPoint().prod(); i++) {
          rho[k++]=featureLiquid.get(i)[0];
          rho[k++]=featureLiquid.get(i)[1];
        }
      }
    } else {
      nrPoint[0]=nrPoint[1]=0;
    }
  }
  void getParticlesLiquid(scalar* xys,int* nrParticle)
  {
    if(solLiquid) {
      scalar width=solLiquid->getGrid0().getBBNodal().getExtent()[0];
      *nrParticle=solLiquid->getPSet().size();
      if(xys != NULL)
        for(sizeType i=0; i<*nrParticle; i++) {
          xys[i*2+0]=solLiquid->getPSet()[i]._pos[0];
          while(xys[i*2+0]>width) xys[i*2+0]-=width;
          while(xys[i*2+0]<0) xys[i*2+0]+=width;
          xys[i*2+1]=solLiquid->getPSet()[i]._pos[1];
        }
    } else {
      *nrParticle=0;
    }
  }
}
