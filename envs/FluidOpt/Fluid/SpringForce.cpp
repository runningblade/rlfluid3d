#include "SpringForce.h"

USE_PRJ_NAMESPACE

static inline scalar WSmooth(scalar r,scalar radius)
{
  const scalar q=r/radius;
  if(q < 1.0f)
    return 1.0f-q*q;
  return 0.0f;
}
static inline scalar WSmoothGrad(scalar r,scalar radius)
{
  if(r < radius)
    return -2.0f*r/(radius*radius);
  return 0.0f;
}
static inline scalar WSharp(scalar r,scalar radius)
{
  const scalar q=radius/std::max<scalar>(r,radius*1E-3f);
  if(q > 1.0f)
    return q*q-1.0f;
  return 0.0f;
}
class Spring : public CollisionFunc<ParticleN<scalar> >
{
public:
  Spring(const ParticleN<scalar>& p,Vec3& force,
         const scalar& radius,
         const scalar& spring,
         const sizeType& id,
         const sizeType& dim)
    :_p(p),_force(force),_radius(radius),_spring(spring),_id(id),_dim(dim) {}
  void operator()(const ParticleN<scalar>& p,const sizeType& id) {
    if(id != _id) {
      //calculate a safe distance
      Vec3 to=p._pos-_p._pos;
      scalar dist=to.norm();
      if(dist < _radius*1E-4f) {
        dist=_radius*1E-4f;
        scalar norm=0.0f;
        do {
          to=Vec3::Random();
          if(_dim == 2)to[2]=0.0f;
          norm=to.squaredNorm();
        } while(norm < EPS);
        to=to*(dist/sqrt(norm));
      }
      //apply spring force, from potential WSmooth(dist,_radius)*_spring
      scalar W=WSmooth(dist,_radius);
      _force-=_spring*(to/dist)*W;
    }
  }
protected:
  const Particle<scalar>& _p;
  Vec3& _force;
  const scalar _radius;
  const scalar _spring;
  const sizeType _id;
  const sizeType _dim;
};
class SpringBD : public CollisionFunc<ParticleN<scalar> >
{
public:
  SpringBD(const ParticleN<scalar>& p,Vec3& force,const scalar& radius,const scalar& spring)
    :_p(p),_force(force),_radius(radius),_spring(spring) {}
  void operator()(const ParticleN<scalar>& p,const sizeType& id) {
    Vec3 to=p._pos-_p._pos;
    scalar dist=to.norm();
    scalar W=WSmooth(dist,_radius);
    _force+=p._normal*(_spring*_radius*W);
  }
protected:
  const Particle<scalar>& _p;
  Vec3& _force;
  const scalar _radius;
  const scalar _spring;
};
class Resample : public CollisionFunc<ParticleN<scalar> >
{
public:
  Resample(Vec3& newVel,scalar& weight,const Vec3& newPos,const scalar& radius)
    :_newVel(newVel),_weight(weight),_newPos(newPos),_radius(radius) {}
  void operator()(const ParticleN<scalar>& p,const sizeType& id) {
    scalar w=WSharp((p._pos-_newPos).norm(),_radius);
    _newVel+=p._vel*w;
    _weight+=w;
  }
protected:
  Vec3& _newVel;
  scalar& _weight;
  const Vec3& _newPos;
  const scalar _radius;
};

SpringForce::SpringForce():Serializable(typeid(SpringForce).name()) {}
SpringForce::~SpringForce() {}
bool SpringForce::read(istream& is,IOData* dat)
{
  readBinaryData(_dim,is);
  readBinaryData(_radius,is);
  readBinaryData(_spring,is);
  readBinaryData(_maxIter,is);
  readBinaryData(_prepareInterval,is);
  readBinaryData(_nrParticlePerGrid,is);
  readBinaryData(_thresInvalid,is);
  _pSetCD.read(is,dat);
  _solidPSetCD.read(is,dat);
  _newPSet.read(is,dat);
  _solidPSet.read(is,dat);
  return is.good();
}
bool SpringForce::write(ostream& os,IOData* dat) const
{
  writeBinaryData(_dim,os);
  writeBinaryData(_radius,os);
  writeBinaryData(_spring,os);
  writeBinaryData(_maxIter,os);
  writeBinaryData(_prepareInterval,os);
  writeBinaryData(_nrParticlePerGrid,os);
  writeBinaryData(_thresInvalid,os);
  _pSetCD.write(os,dat);
  _solidPSetCD.write(os,dat);
  _newPSet.write(os,dat);
  _solidPSet.write(os,dat);
  return os.good();
}
boost::shared_ptr<Serializable> SpringForce::copy() const
{
  return boost::shared_ptr<Serializable>(new SpringForce);
}
void SpringForce::resetSolid(const ScalarField& solid,scalar radius,sizeType nrParticlePerGrid)
{
  _dim=solid.getDim();
  _radius=radius;
  _spring=50.0f;

  _maxIter=1000;
  _prepareInterval=3;
  _nrParticlePerGrid=nrParticlePerGrid;
  _thresInvalid=0.0f;

  _pSetCD.reset(solid.getBB(),solid.getCellSize());
  _solidPSetCD.reset(solid.getBB(),solid.getCellSize());
  sampleSolidPSet(solid);
}
void SpringForce::sampleSolidPSet(const ScalarField& solid)
{
  Vec3 cellSz=Vec3::Constant(_radius);
  if(solid.getDim() == 2)
    cellSz.z()=1.0f;
  Vec3i nrCell=ceilV((Vec3)(solid.getBB().getExtent().array()/cellSz.array()).matrix());
  if(solid.getDim() == 2)
    nrCell.z()=1;

  ParticleSetN::ParticleType p;
  _solidPSet.clear();
  sizeType seed=0;
  for(sizeType x=0; x<nrCell.x(); x++) {
    for(sizeType y=0; y<nrCell.y(); y++)
      for(sizeType z=0; z<nrCell.z(); z++) {
        Vec3 pt=solid.getBB()._minC+Vec3((scalar)(x+0.5f)*cellSz.x(),(scalar)(y+0.5f)*cellSz.y(),(scalar)(z+0.5f)*cellSz.z());
        for(sizeType nr=0; nr<_nrParticlePerGrid; nr++) {
          p._pos=pt-cellSz*0.5f;
          p._pos.x()+=cellSz.x()*RandEngine::randSR(seed++,0,1);
          p._pos.y()+=cellSz.y()*RandEngine::randSR(seed++,0,1);
          p._pos.z()+=cellSz.z()*RandEngine::randSR(seed++,0,1);
          if(solid.getDim() == 2)
            p._pos.z()=0.0f;
          p._vel=Vec3::Zero();
          if(solid.sampleSafe(p._pos) <= 0) {
            p._normal=solid.sampleSafeGrad(p._pos);
            p._normal/=max<scalar>(p._normal.norm(),1E-6f);
            _solidPSet.addParticle(p);
          }
        }
      }
  }
}
bool SpringForce::correct(ParticleSetN& pSet,scalar dt,const ParticleSetN* additionalSolidPSet,const Vec3i* warpMode)
{
  //account for warpping
  if(warpMode)
    for(sizeType d=0; d<_dim; d++)
      if((*warpMode)[d] > 0) {
        _solidPSetCD.setWarp(d);
        _pSetCD.setWarp(d);
      } else {
        _solidPSetCD.clearWarp(d);
        _pSetCD.clearWarp(d);
      }

  //separate particle
  sizeType nrSolid=_solidPSet.size();
  if(additionalSolidPSet)
    _solidPSet.append(*additionalSolidPSet);
  bool succ=pushParticle(pSet,dt);
  _solidPSet.resize(nrSolid);

  //resample velocity
  _pSetCD.prepare(pSet);
  const scalar radVel=_radius*2.0f;
  for(sizeType i=0; i<_newPSet.size(); i++) {
    ParticleN<scalar>& p=_newPSet.get(i);
    const Vec3& newPos=p._pos;
    Vec3& newVel=p._vel;
    newVel.setZero();
    scalar weight=0.0f;
    if(_dim) {
      Resample rs(newVel,weight,newPos,radVel);
      _pSetCD.fill2D(pSet,newPos,radVel*radVel,rs);
    } else {
      Resample rs(newVel,weight,newPos,radVel);
      _pSetCD.fill3D(pSet,newPos,radVel*radVel,rs);
    }

    if(weight == 0.0f)
      newVel=pSet[i]._vel;
    else
      newVel/=std::max<scalar>(weight,EPS);
  }

  pSet=_newPSet;
  return succ;
}
bool SpringForce::pushParticle(ParticleSetN& pSet,scalar dt)
{
  _solidPSetCD.prepare(_solidPSet);
  _newPSet=pSet;

  //adjust position
  sizeType nr=0;
  bool again=true;
  while(again && nr < _maxIter) {
    if(nr%_prepareInterval == 0)
      _pSetCD.prepare(_newPSet);
    again=adjustPosition(dt);
    nr++;
  }
  //INFOV("Did %d Corrects!",nr)
  return nr < _maxIter;
}
bool SpringForce::adjustPosition(scalar dt)
{
  if(_newPSet.size() == 0)
    return false;

  scalar radSqr=_radius*_radius;
  if(_dim == 2) {
    for(sizeType i=0; i<_newPSet.size(); i++) {
      ParticleN<scalar>& p=_newPSet.get(i);
      Vec3& force=p._vel;
      force.setZero();
      Spring ss(p,force,_radius,_spring,i,_dim);
      _pSetCD.fill2D(_newPSet,p._pos,radSqr,ss);
      SpringBD sss(p,force,_radius,_spring);
      _solidPSetCD.fill2D(_solidPSet,p._pos,radSqr,sss);
    }
  } else {
    for(sizeType i=0; i<_newPSet.size(); i++) {
      ParticleN<scalar>& p=_newPSet.get(i);
      Vec3& force=p._vel;
      force.setZero();
      Spring ss(p,force,_radius,_spring,i,_dim);
      _pSetCD.fill3D(_newPSet,p._pos,radSqr,ss);
      SpringBD sss(p,force,_radius,_spring);
      _solidPSetCD.fill3D(_solidPSet,p._pos,radSqr,sss);
    }
  }

  sizeType nrInvalid=0;
  scalar thresMin=_radius*1E-1f/dt;
  scalar thresMax=_radius*0.75f/dt;
  for(sizeType i=0; i<_newPSet.size(); i++) {
    ParticleN<scalar>& p=_newPSet.get(i);
    scalar norm=p._vel.norm();
    if(norm > thresMax)
      p._vel*=thresMax/norm;
    p._pos+=p._vel*dt;
    if(norm > thresMin)
      nrInvalid++;
  }
  //INFOV("nrInvalid: %ld!",nrInvalid)
  return (scalar)nrInvalid/(scalar)_newPSet.size() > _thresInvalid;
}
const ParticleSetN& SpringForce::getSolidPSet() const
{
  return _solidPSet;
}
ParticleSetN& SpringForce::getSolidPSet()
{
  return _solidPSet;
}
const CollisionInterface<scalar,ParticleSetN>& SpringForce::getPSetCD() const
{
  return _pSetCD;
}
CollisionInterface<scalar,ParticleSetN>& SpringForce::getPSetCD()
{
  return _pSetCD;
}
