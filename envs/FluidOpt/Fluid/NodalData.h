#ifndef NODAL_DATA_H
#define NODAL_DATA_H

#include "VariableSizedGrid.h"

PRJ_BEGIN

//Iterator
template <typename TDATA>
struct NodalData : public Serializable {
  NodalData():Serializable(typeid(NodalData<TDATA>).name()) {}
  template <typename T,int DIM,typename Encoder>
  NodalData(const VariableSizedGrid<T,DIM,Encoder>& grid)
    :Serializable(typeid(NodalData<TDATA>).name()) {
    reset(grid);
  }
  template <typename T,int DIM,typename Encoder>
  void reset(const VariableSizedGrid<T,DIM,Encoder>& grid) {
    _data.setZero(grid._nrPointNodal.prod());
  }
  virtual bool read(istream& is,IOData* dat) override {
    readBinaryData(_data,is);
    return is.good();
  }
  virtual bool write(ostream& os,IOData* dat) const override {
    writeBinaryData(_data,os);
    return os.good();
  }
  virtual boost::shared_ptr<Serializable> copy() const override {
    return boost::shared_ptr<Serializable>(new NodalData<TDATA>);
  }
  Eigen::Matrix<TDATA,-1,1> _data;
};
template <typename T,int DIM,typename Encoder>
struct NodalIteratorPoint : public TypeTraits<T,DIM> {
  using typename TypeTraits<T,DIM>::VecDIMd;
  using typename TypeTraits<T,DIM>::VecDIMi;
  typedef VariableSizedGrid<T,DIM,Encoder> Grid;
  typedef VecDIMd value_type;
  NodalIteratorPoint(const Grid& grd,int verbose,const VecDIMi& id0=VecDIMi::Zero(),const VecDIMi& id1=VecDIMi::Zero(),
                     const VecDIMi& extendMin=VecDIMi::Zero(),const VecDIMi& extendMax=VecDIMi::Zero());
  NodalIteratorPoint(const Grid& grd,int verbose,const BBox<T,DIM>& bb,
                     const VecDIMi& extendMin=VecDIMi::Zero(),const VecDIMi& extendMax=VecDIMi::Zero());
  virtual ~NodalIteratorPoint() {}
  bool operator!=(const NodalIteratorPoint<T,DIM,Encoder>& other) const;
  value_type operator*() const;
  value_type pointWithWarp() const;
  const value_type& warpCompensate() const;
  void operator++();
  sizeType off() const;
  const VecDIMi& id() const;
protected:
  const Grid& _grd;
  VecDIMi _id0,_id1,_id,_warpCount,_warpCountMax;
  VecDIMd _warpCompensate,_warpCompensate0,_warpRange;
  sizeType _off;
};
template <typename T,int DIM,typename Encoder>
struct NodalIteratorPointIndex : public NodalIteratorPoint<T,DIM,Encoder> {
  using typename NodalIteratorPoint<T,DIM,Encoder>::VecDIMd;
  using typename NodalIteratorPoint<T,DIM,Encoder>::VecDIMi;
  using typename NodalIteratorPoint<T,DIM,Encoder>::Grid;
  typedef VecDIMi value_type;
  using NodalIteratorPoint<T,DIM,Encoder>::_off;
  NodalIteratorPointIndex(const Grid& grd,int verbose,const VecDIMi& id0=VecDIMi::Zero(),const VecDIMi& id1=VecDIMi::Zero());
  value_type operator*() const;
};
template <typename T,int DIM,typename Encoder>
struct NodalIteratorCell : public NodalIteratorPoint<T,DIM,Encoder>  {
  using typename NodalIteratorPoint<T,DIM,Encoder>::VecDIMd;
  using typename NodalIteratorPoint<T,DIM,Encoder>::VecDIMi;
  using typename NodalIteratorPoint<T,DIM,Encoder>::Grid;
  using typename TypeTraits<T,DIM>::VecSTENCILi;
  typedef VecSTENCILi value_type;
  using NodalIteratorPoint<T,DIM,Encoder>::_grd;
  using NodalIteratorPoint<T,DIM,Encoder>::_id;
  NodalIteratorCell(const Grid& grd,int verbose,const VecDIMi& id0=VecDIMi::Zero(),const VecDIMi& id1=VecDIMi::Zero());
  value_type operator*() const;
};

PRJ_END

#endif
