#include "Utils.h"
#include <boost/version.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

PRJ_BEGIN

//ImplicitFuncCircleSmooth
ImplicitFuncCircleSmooth::ImplicitFuncCircleSmooth(scalar rad,const Vec3& ctr,scalar alpha):_rad(rad),_alpha(alpha),_ctr(ctr) {}
scalar ImplicitFuncCircleSmooth::operator()(const Vec3& pos) const
{
  return 1/(1+exp(((pos-_ctr).norm()-_rad)*_alpha));
}
BBox<scalar> ImplicitFuncCircleSmooth::getBB() const
{
  BBox<scalar> bb;
  bb._minC=_ctr-Vec3::Constant(_rad);
  bb._maxC=_ctr+Vec3::Constant(_rad);
  return bb;
}
//ImplicitFuncCircle
ImplicitFuncCircle::ImplicitFuncCircle(scalar rad,const Vec3& ctr):_rad(rad),_ctr(ctr) {}
scalar ImplicitFuncCircle::operator()(const Vec3& pos) const
{
  return (pos-_ctr).norm() < _rad ? 1 : 0;
}
BBox<scalar> ImplicitFuncCircle::getBB() const
{
  BBox<scalar> bb;
  bb._minC=_ctr-Vec3::Constant(_rad);
  bb._maxC=_ctr+Vec3::Constant(_rad);
  return bb;
}
//ImplicitFuncBox
scalar ImplicitFuncBox::operator()(const Vec3& pos) const
{
  return _bb.contain(pos,_DIM) ? 1 : 0;
}
BBox<scalar> ImplicitFuncBox::getBB() const
{
  return _bb;
}
//VelFuncRotate
VelFuncRotate::VelFuncRotate():_ctr(Vec3::Zero()),_dir(Vec3::Zero()) {}
VelFuncRotate::VelFuncRotate(const Vec3& ctr,const Vec3& dir):_ctr(Vec3::Zero()),_dir(Vec3::Zero()) {}
Vec3 VelFuncRotate::operator()(const Vec3& pos) const
{
  return (pos-_ctr).cross(_dir);
}
//VelFuncConstant
VelFuncConstant::VelFuncConstant():_dir(Vec3::Zero()) {}
VelFuncConstant::VelFuncConstant(const Vec3& dir):_dir(Vec3::Zero()) {}
Vec3 VelFuncConstant::operator()(const Vec3& pos) const
{
  return _dir;
}
//filesystem
bool notDigit(char c)
{
  return !std::isdigit(c);
}
bool lessDirByNumber(boost::filesystem::path A,boost::filesystem::path B)
{
  string strA=A.string();
  string::iterator itA=std::remove_if(strA.begin(),strA.end(),notDigit);
  strA.erase(itA,strA.end());

  string strB=B.string();
  string::iterator itB=std::remove_if(strB.begin(),strB.end(),notDigit);
  strB.erase(itB,strB.end());

  return boost::lexical_cast<sizeType>(strA) < boost::lexical_cast<sizeType>(strB);
}
bool exists(const boost::filesystem::path& path)
{
  return boost::filesystem::exists(path);
}
void removeDir(const boost::filesystem::path& path)
{
  if(boost::filesystem::exists(path))
    try {
      boost::filesystem::remove_all(path);
    } catch(...) {}
}
void create(const boost::filesystem::path& path)
{
  if(!boost::filesystem::exists(path))
    try {
      boost::filesystem::create_directory(path);
    } catch(...) {}
}
void recreate(const boost::filesystem::path& path)
{
  removeDir(path);
  try {
    boost::filesystem::create_directory(path);
  } catch(...) {}
}
vector<boost::filesystem::path> files(const boost::filesystem::path& path)
{
  vector<boost::filesystem::path> ret;
  for(boost::filesystem::directory_iterator beg(path),end; beg!=end; beg++)
    if(boost::filesystem::is_regular_file(*beg))
      ret.push_back(*beg);
  return ret;
}
vector<boost::filesystem::path> directories(const boost::filesystem::path& path)
{
  vector<boost::filesystem::path> ret;
  for(boost::filesystem::directory_iterator beg(path),end; beg!=end; beg++)
    if(boost::filesystem::is_directory(*beg))
      ret.push_back(*beg);
  return ret;
}
void sortFilesByNumber(vector<boost::filesystem::path>& files)
{
  std::sort(files.begin(),files.end(),lessDirByNumber);
}
bool isDir(const boost::filesystem::path& path)
{
  return boost::filesystem::is_directory(path);
}
size_t fileSize(const boost::filesystem::path& path)
{
  return (size_t)boost::filesystem::file_size(path);
}
string parseProps(sizeType argc,char** argv,boost::property_tree::ptree& pt)
{
  string addParam;
  for(sizeType i=0; i<argc; i++) {
    string str(argv[i]);
    size_t pos=str.find("=");
    if(pos != std::string::npos) {
      pt.put<string>(str.substr(0,pos),str.substr(pos+1));
      addParam+="_"+str;
    }
  }
  return addParam+"_";
}
//property tree
void readPtreeBinary(boost::property_tree::ptree& pt,istream& is)
{
  string str;
  readBinaryData(str,is);
  istringstream iss(str);
  boost::property_tree::read_xml(iss,pt);
}
void writePtreeBinary(const boost::property_tree::ptree& pt,ostream& os)
{
  ostringstream oss;
  boost::property_tree::write_xml(oss,pt);
  writeBinaryData(oss.str(),os);
}
void readPtreeAscii(boost::property_tree::ptree& pt,const boost::filesystem::path& path)
{
  boost::property_tree::read_xml(path.string(),pt);
}
void writePtreeAscii(const boost::property_tree::ptree& pt,const boost::filesystem::path& path)
{
#if BOOST_VERSION > 105600
  boost::property_tree::xml_writer_settings<string> settings('\t',1);
  boost::property_tree::write_xml(path.string(),pt,std::locale(),settings);
#else
  boost::property_tree::xml_writer_settings<boost::property_tree::ptree::key_type::value_type> settings('\t',1);
  boost::property_tree::write_xml(path.string(),pt,std::locale(),settings);
#endif
}

PRJ_END
