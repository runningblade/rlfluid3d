#include "VariableSizedParticleOp.h"
#include "VariableSizedGridOp.h"
#include "Utils.h"

USE_PRJ_NAMESPACE

template <typename T,int DIM,typename Encoder>
void VariableSizedParticleOp<T,DIM,Encoder>::compactParticle(const GridType& grd,const ScalarField& nodalSolidPhi,ParticleSetN& pset,T radius)
{
  DEFINE_GRID_WARP(nodalSolidPhi)
  BBox<scalar> bb=nodalSolidPhi.getBB();
  sizeType n=pset.size(),j=0;
  for(sizeType i=0; i<n; i++) {
    const Vec3& p=pset[i]._pos;
    bool valid=nodalSolidPhi.sampleSafe(p,&warpMode) > -radius;
    for(sizeType D=0;D<DIM && valid;D++)
      if(grd._warpMode[D] == 0 && (p[D] < bb._minC[D] || p[D] > bb._maxC[D]))
         valid=false;
    if(valid)
      pset[j++]=pset[i];
  }
  pset.resize(j);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedParticleOp<T,DIM,Encoder>::projectToGrid(const ParticleSetN& pset,const GridType& grd,MACVelData<DIM,T>& vel,MACVelData<DIM,T>& weight,MACVelData<DIM,unsigned char>& valid)
{
  VecDIMb ob;
  VecDIMi id;
  VecDIMd pos;
  VecSTENCILd weights;
  VecSTENCILi indices;
  vel.setZero();
  weight.setZero();
  valid.setZero();
  sizeType n=pset.size();
  for(sizeType D=0; D<DIM; D++) {
    for(sizeType i=0; i<n; i++) {
      pos=pset[i]._pos.segment<DIM>(0);
      grd.sampleStencilMACVel(D,pos,id,weights,indices,ob);
      for(unsigned char w=0; w<weights.size(); w++)
        if(weights[w] > 1E-8f) {
          vel._data[D][indices[w]]+=pset[i]._vel[D]*weights[w];
          weight._data[D][indices[w]]+=weights[w];
          valid._data[D][indices[w]]=1;
        }
    }
  }
  for(sizeType D=0; D<DIM; D++)
    for(sizeType i=0; i<vel._data[D].size(); i++)
      if(valid._data[D][i])
        vel._data[D][i]/=weight._data[D][i];
}
template <typename T,int DIM,typename Encoder>
void VariableSizedParticleOp<T,DIM,Encoder>::projectToParticle(ParticleSetN& pset,const GridType& grd,MACVelData<DIM,T>& velOld,MACVelData<DIM,T>& velNew,T PICWeight)
{
  VecDIMb ob;
  VecDIMi id;
  VecDIMd pos;
  VecSTENCILd weights;
  VecSTENCILi indices;
  T newVel,oldVel;
  const sizeType n=pset.size();
  for(sizeType D=0; D<DIM; D++)
    for(sizeType i=0; i<n; i++) {
      pos=pset[i]._pos.segment<DIM>(0);
      grd.sampleStencilMACVel(D,pos,id,weights,indices,ob);
      newVel=sample<T>(velNew._data[D],weights,indices);
      oldVel=sample<T>(velOld._data[D],weights,indices);
      pset[i]._vel[D]=(pset[i]._vel[D]+(newVel-oldVel))*(1.0f-PICWeight)+newVel*PICWeight;
    }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedParticleOp<T,DIM,Encoder>::clamp(ParticleSetN& pset,T maxVel)
{
  const sizeType n=pset.size();
  //OMP_PARALLEL_FOR_
  for(sizeType i=0; i<n; i++) {
    const scalar vLen=pset[i]._vel.norm();
    if(vLen > maxVel)
      pset[i]._vel*=(maxVel/vLen);
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedParticleOp<T,DIM,Encoder>::addExternalForce(ParticleSetN& pset,T g,T dt)
{
  const sizeType n=pset.size();
  //OMP_PARALLEL_FOR_
  for(sizeType i=0; i<n; i++)
    pset[i]._vel[1]-=g*dt;
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
TDATA VariableSizedParticleOp<T,DIM,Encoder>::sample(const Eigen::Matrix<TDATA,-1,1>& data,const VecSTENCILd& weights,const VecSTENCILi& indices)
{
  VecSTENCILd vals;
  for(unsigned char w=0; w<vals.size(); w++)
    vals[w]=data[indices[w]];
  return vals.dot(weights);
}
//instance
#define INSTANCE(DIM,TYPE,SPARSITY) \
template class VariableSizedParticleOp<scalar,DIM,Encoder##TYPE<SPARSITY> >;
INSTANCE_ALL
