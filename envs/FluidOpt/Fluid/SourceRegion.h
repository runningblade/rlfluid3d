#ifndef SOURCE_REGION_H
#define SOURCE_REGION_H

#include "CellCenterData.h"
#include "NodalData.h"
#include "MACVelData.h"
#include "Utils.h"
#include <CommonFile/GridBasic.h>
#include <CommonFile/ParticleCD.h>

PRJ_BEGIN

class SourceRegion : public Serializable
{
public:
  class SourceRegionFilter : public Serializable
  {
  public:
    SourceRegionFilter();
    virtual void resetFilter(const scalar& globalTime);
    virtual void filter(ParticleSetN::ParticleType& p,const scalar& globalTime);
    virtual bool read(istream& is,IOData* dat) override;
    virtual bool write(ostream& os,IOData* dat) const override;
    virtual boost::shared_ptr<Serializable> copy() const override;
  };
  SourceRegion();
  SourceRegion(const scalar& rho,const scalar& T);
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  ScalarField& getRegion();
  const ScalarField& getRegion() const;
  MACVelocityField& getVel();
  const MACVelocityField& getVel() const;
  void setSource(bool isSource);
  virtual void sampleRegion(const ScalarField& solid,const CollisionInterface<scalar,ParticleSetN>& cd,const scalar& rad,ParticleSetN& pSet,const scalar& globalTime);
  virtual void sampleSource(const ScalarField& solid,const CollisionInterface<scalar,ParticleSetN>& cd,const scalar& rad,ParticleSetN& pSet,const scalar& globalTime);
  template <int DIM,typename Encoder>
  void sampleRegion(VariableSizedGrid<scalar,DIM,Encoder>& grd,CellCenterData<scalar>* rho,CellCenterData<scalar>* T,MACVelData<DIM,scalar>& vel,const scalar& globalTime) {
    typedef typename TypeTraits<scalar,DIM>::VecDIMd VecDIMd;
    if(!filterSource(globalTime))
      return;
    if(_filter)
      _filter->resetFilter(globalTime);
    const BBox<scalar> bb=_region.getBB();
    const BBox<scalar,DIM> bbDIM=assignVariableTo<scalar,DIM,scalar,3>(bb);
    if(_alterProp && rho && T) {
      CellCenterIteratorPoint<scalar,DIM,Encoder> beg(grd,0,bbDIM);
      CellCenterIteratorPoint<scalar,DIM,Encoder> end(grd,-1,bbDIM);
      while(beg!=end) {
        rho->_data[beg.off()]=_rho;
        T->_data[beg.off()]=_T;
        ++beg;
      }
    }
    if(_alterVel)
      for(sizeType D=0; D<DIM; D++) {
        MACVelIteratorPoint<scalar,DIM,Encoder> beg(D,grd,0,bbDIM);
        MACVelIteratorPoint<scalar,DIM,Encoder> end(D,grd,-1,bbDIM);
        while(beg!=end) {
          vel._data[D][beg.off()]=_vel.getComp(D).sampleSafe(assignVariableTo<Vec3,VecDIMd>(*beg));
          ++beg;
        }
      }
  }
  template <int DIM,typename Encoder>
  void sampleRegionSWE(VariableSizedGrid<scalar,DIM,Encoder>& grd,CellCenterData<scalar> U[DIM+1],const NodalData<scalar>& B,const scalar& globalTime) {
    typedef typename TypeTraits<scalar,DIM>::VecDIMd VecDIMd;
    typedef typename TypeTraits<scalar,DIM>::VecDIMi VecDIMi;
    if(!filterSource(globalTime))
      return;
    if(_filter)
      _filter->resetFilter(globalTime);
    const BBox<scalar> bb=_region.getBB();
    const BBox<scalar,DIM> bbDIM=assignVariableTo<scalar,DIM,scalar,3>(bb);
    CellCenterIteratorPoint<scalar,DIM,Encoder> beg(grd,0,bbDIM);
    CellCenterIteratorPoint<scalar,DIM,Encoder> end(grd,-1,bbDIM);
    VecDIMi id;
    VecDIMd x;
    while(beg!=end) {
      if(_alterProp) {
        scalar& w=U[0]._data[beg.off()];
        if(_isSource)
          w=max<scalar>(w,bb._maxC[DIM]);
        else w=min<scalar>(w,bb._minC[DIM]);
      }
      if(_alterVel)
        for(sizeType d=0; d<DIM; d++) {
          scalar& huv=U[d+1]._data[beg.off()];
          const scalar& w=U[0]._data[beg.off()];
          huv=max<scalar>(w-grd.sampleNodal(B,x=*beg,id),0)*_vel.getComp(d).sampleSafe(assignVariableTo<Vec3,VecDIMd>(*beg));
        }
      ++beg;
    }
  }
  virtual void sampleSink(ParticleSetN& pSet,const CollisionInterface<scalar,ParticleSetN>& cd);
  void compact(ParticleSetN& pSet);
  void setFilter(boost::shared_ptr<SourceRegionFilter> filter);
  void setNrParticlePerGrid(const sizeType& nrParticlePerGrid);
  void setCellSz(const Vec3& cellSz);
  //smoke
  void setRho(const scalar& rho);
  scalar getRho() const;
  void setT(const scalar& T);
  scalar getT() const;
  void setAlterVel(bool alterVel);
  void setAlterProp(bool alterProp);
protected:
  virtual bool filterSource(const scalar& globalTime);
  boost::shared_ptr<SourceRegionFilter> _filter;
  ScalarField _region;
  MACVelocityField _vel;
  vector<unsigned char> _valid;
  bool _isSource;
  bool _alterProp;
  bool _alterVel;
  sizeType _nrParticlePerGrid;
  Vec3 _cellSz;
  scalar _rho;
  scalar _T;
};

PRJ_END

#endif
