﻿#ifndef VARIABLE_SIZED_GRID_OP_PROJECT_H
#define VARIABLE_SIZED_GRID_OP_PROJECT_H

#include "Utils.h"
#include "NodalData.h"
#include "MACVelData.h"
#include "CellCenterData.h"
#include "VariableSizedGrid.h"
#include <CommonFile/GridBasic.h>
#include <CommonFile/ParticleSet.h>
#include <CommonFile/ImplicitFuncInterface.h>

PRJ_BEGIN

//VariableSizedGridOpProject
#define PRECOMPUTE_RIGID_MATRIX
//#define DEBUG_CELLSZ
//#define DEBUG_MATRIX
DECL_FORWARD_LINEAR_SOLVER
template <typename T,int DIM,typename Encoder>
struct VariableSizedGridOpProject : public TypeTraits<T,DIM> {
  enum CELL_TYPE {
    FLUID=8,
    AIR=1,
    SOLID=0,
  };
  DECL_TYPES
  static const T SOLVER_EPS;
  //project
  static void sampleWeightMACVel(const GridType& grd,MACVelData<DIM,T>& weight,const ScalarField& nodalSolidPhi);
  static void assembleMatrix(const GridType& grd,SMat& LHS,const SparseSTENCILd& LHSd,const SparseSTENCILi& LHSi);
  static void computeTag(const GridType& grd,CellCenterData<unsigned char>& tag,const CellCenterData<T>& phi,const ScalarField& nodalSolidPhi);
  static Cold buildMatrix(const GridType& grd,const CellCenterData<unsigned char>& tag,const CellCenterData<T>& phi,MACVelData<DIM,T>& vel,SMat& LHS,Cold& RHS,T dt,T density,Cold* lowerBound=NULL);
  static void projectOut(const GridType& grd,const CellCenterData<scalarD>& pressure,const CellCenterData<unsigned char>& tag,const CellCenterData<T>& phi,const ScalarField& nodalSolidPhi,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density);
  static void project(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const ScalarField& nodalSolidPhi,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,CellCenterData<unsigned char>& tag,T dt,T density);
  static void projectUnilateral(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const ScalarField& nodalSolidPhi,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,CellCenterData<unsigned char>& tag,T dt,T density);
  //project variational
  static bool checkFaceValid(const MACVelPointIt& beg,T weight,const CellCenterData<T>& phi,sizeType& LID,sizeType& RID,T& LF,T& RF,T& theta);
  static Cold buildMatrixVariational(const GridType& grd,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,MACVelData<DIM,T>& vel,SMat& LHS,Cold& RHS,T dt,T density,Cold* lowerBound=NULL);
  static void projectOutVariational(const GridType& grd,const CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density);
  static void projectVariational(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density);
  static void projectVariationalUnilateral(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density);
  //project with rigid body
  static void computeRigidBodyWeightMACVel(const GridType& grd,MACVelData<DIM,T>* weightRigid,MACVelData<DIM,T>* weightWithRigid,MACVelData<DIM,sizeType>* rigidId,const RigidBody& body,sizeType RID);
  static void buildForceMatrix(const Cold& diag,const GridType& grd,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weightRigid,const RigidBody& body,T dt,sizeType i,MJ& mj,bool changeRHS,Cold* lowerBound=NULL);
  static void buildRigidBodyMatrix(const Cold& diag,vector<MJ>& MJs,vector<sizeType>& MJIDs,Cold& RHS,const GridType& grd,const CellCenterData<T>& phi,const vector<MACVelData<DIM,T> >& weightRigids,const RigidSolver& rigid,T dt,bool changeRHS,Cold* lowerBound=NULL);
  static void rigidProjectVariational(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const vector<MACVelData<DIM,T> >& weightRigids,RigidSolver& rigid,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density);
  static void rigidProjectVariationalMINRES(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const vector<MACVelData<DIM,T> >& weightRigids,RigidSolver& rigid,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density);
  static void rigidProjectVariationalUnilateral(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const vector<MACVelData<DIM,T> >& weightRigids,RigidSolver& rigid,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density);
  //projectc using back-of-the-envelop computation
  static void buildForceMatrixBOTE(const Cold& diag,const GridType& grd,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const MACVelData<DIM,sizeType>& rigidId,const RigidBody& body,const MACVelData<DIM,T>& vel,T dt,sizeType i,GeneralizedMJ& mj,T density,Cold* lowerBound=NULL,bool volWeight=false);
  static void buildRigidBodyMatrixBOTE(const Cold& diag,vector<GeneralizedMJ>& MJs,vector<sizeType>& MJIDs,Cold& RHS,const GridType& grd,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const MACVelData<DIM,sizeType>& rigidId,const RigidSolver& rigid,const MACVelData<DIM,T>& vel,T dt,T density,bool changeRHS,Cold* lowerBound=NULL,bool volWeight=false);
  static Cold buildMatrixBOTE(const GridType& grd,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const MACVelData<DIM,sizeType>& rigidId,MACVelData<DIM,T>& vel,SMat& LHS,Cold& RHS,T dt,T density,Cold* lowerBound=NULL);
  static void projectOutBOTE(const GridType& grd,const CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const MACVelData<DIM,sizeType>& rigidId,RigidSolver& rigid,const vector<GeneralizedMJ>& MJs,const vector<sizeType>& MJIDs,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density,bool changeRHS);
  static void rigidProjectBOTE(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const MACVelData<DIM,sizeType>& rigidId,RigidSolver& rigid,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density);
  static void rigidProjectBOTEMINRES(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const MACVelData<DIM,sizeType>& rigidId,RigidSolver& rigid,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density);
  static void rigidProjectBOTEUnilateral(const GridType& grd,CellCenterData<scalarD>& pressure,const CellCenterData<T>& phi,const MACVelData<DIM,T>& weight,const MACVelData<DIM,sizeType>& rigidId,RigidSolver& rigid,MACVelData<DIM,T>& vel,MACVelData<DIM,unsigned char>& valid,T dt,T density);
  static void debugRigidProjectBOTELump(const string& path,const Vec3i& warpMode);
  static void debugRigidProjectBOTELumpAllWarp();
  //debug
  static void debugRigidProject(const string& path,const Vec3i& warpMode);
  static void debugRigidProjectAllWarp();
  static void debugProject(const string& path,const Vec3i& warpMode);
  static void debugProjectAllWarp();
  static void debugWeight(const string& path,const Vec3i& warpMode);
  static void debugWeightAllWarp();
};

PRJ_END

#endif
