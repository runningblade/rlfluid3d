#ifndef RIGID_MATRIX_H
#define RIGID_MATRIX_H

#include "RigidSolver.h"
#include "CellCenterData.h"
#include <CommonFile/solvers/LinearSolver.h>

PRJ_BEGIN

struct MJAbstract
{
  //method
  typedef Kernel<scalarD>::Vec Vec;
  virtual ~MJAbstract() {}
  virtual void multiplyInvMDt(const Vec6d& b,Vec6d& out) const=0;
  virtual void getInvMDt(Mat6d& InvMDt) const=0;
  //for construction
  void clear();
  void insert(sizeType col,const Vec6d& data);
  //matrix operation
  void toMatrix(FixedSparseMatrix<scalarD,Kernel<scalarD> >& m) const;
  void multiplyJ(const Vec& b,Vec6d& out) const;
  void multiplyJSubtract(const Vec& b,Vec6d& out) const;
  void multiplyJAdd(const Vec& b,Vec6d& out) const;
  void multiplyJT(const Vec6d& b,Vec& out) const;
  void multiplyJTSubtract(const Vec6d& b,Vec& out) const;
  void multiplyJTAdd(const Vec6d& b,Vec& out) const;
  vector<Vec6d,Eigen::aligned_allocator<Vec6d> > _colData;
  vector<sizeType> _col;
private:
  map<sizeType,sizeType> _index;
};
struct MJ : public MJAbstract
{
  virtual void multiplyInvMDt(const Vec6d& b,Vec6d& out) const override;
  virtual void getInvMDt(Mat6d& InvMDt) const override;
  scalarD _invMDt;
  Mat3d _invIDt;
};
struct GeneralizedMJ : public MJAbstract
{
  virtual void multiplyInvMDt(const Vec6d& b,Vec6d& out) const override;
  virtual void getInvMDt(Mat6d& InvMDt) const override;
  Mat6d _invMDt;
  Vec6d _RHS;
};
template <typename MJ_TYPE>
struct RigidBodyKrylovMatrix : public DefaultKrylovMatrix<scalarD,Kernel<scalarD> >
{
  RigidBodyKrylovMatrix(const FixedSparseMatrix<scalarD,Kernel<scalarD> > &matrix,const vector<MJ_TYPE>& MJs,const vector<sizeType>& MJIDs);
  virtual void multiply(const Vec& b,Vec& out) const override;
  virtual void applyForce(const CellCenterData<scalarD>& pressure,RigidSolver& solver,scalar dt);
  virtual void applyForceMINRES(const CellCenterData<scalarD>& pressure,RigidSolver& solver,scalar dt);
  void toFixed(FixedSparseMatrix<scalarD,Kernel<scalarD> > &matrix);
  const vector<MJ_TYPE>& _MJs;
  const vector<sizeType>& _MJIDs;
  vector<pair<Vec3,Vec3> > _forces;
};
template <typename MJ_TYPE>
struct RigidBodyKrylovMatrixSparse : public RigidBodyKrylovMatrix<MJ_TYPE>
{
  typedef typename RigidBodyKrylovMatrix<MJ_TYPE>::Vec Vec;
  using RigidBodyKrylovMatrix<MJ_TYPE>::_MJs;
  using RigidBodyKrylovMatrix<MJ_TYPE>::_MJIDs;
  using RigidBodyKrylovMatrix<MJ_TYPE>::_forces;
  using RigidBodyKrylovMatrix<MJ_TYPE>::_fixedMatrix;
  RigidBodyKrylovMatrixSparse(const FixedSparseMatrix<scalarD,Kernel<scalarD> > &matrix,const vector<MJ_TYPE>& MJs,const vector<sizeType>& MJIDs);
  virtual void multiply(const Vec& b,Vec& out) const override;
  virtual sizeType n() const override;
};
class RigidFluidFilter : public MIC0Solver<scalarD>::MatrixFilter
{
public:
  RigidFluidFilter(sizeType sSize);
  bool operator()(sizeType r,sizeType c) const;
  sizeType _sSize;
};
class RigidFluidPrecondition : public MIC0Solver<scalarD>
{
public:
  void reset(const vector<MJ>& MJs,sizeType sSize);
  void reset(const vector<GeneralizedMJ>& MJs,sizeType sSize);
  Solver<scalarD,Kernel<scalarD> >::SOLVER_RESULT solve(const Vec& rhs,Vec& result);
  vector<Mat6d,Eigen::aligned_allocator<Mat6d> > _bodies;
  sizeType _sSize;
};

PRJ_END

#endif
