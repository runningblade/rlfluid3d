#include "CellCenterData.h"
#include "Utils.h"
#include <functional>

USE_PRJ_NAMESPACE

//Iterator: Point
template <typename T,int DIM,typename Encoder>
CellCenterIteratorPoint<T,DIM,Encoder>::CellCenterIteratorPoint(const Grid& grd,int verbose,const VecDIMi& id0,const VecDIMi& id1,const VecDIMi& extendMin,const VecDIMi& extendMax)
  :_grd(grd),_id0(id0),_id1(grd._nrPointCellCenter-id1),_warpCount(VecDIMi::Zero())
{
  _warpRange=_grd.getBBNodal().getExtent();
  for(int D=0; D<DIM; D++) {
    IteratorUtil<T,DIM>::makeIteratorRangeSafe(_id0[D],_id1[D],_grd._nrPointCellCenter[D],_grd._warpMode[D],_warpCountMax[D],_warpRange[D],_warpCompensate0[D]);
    IteratorUtil<T,DIM>::extendIterator(_id0[D],_id1[D],_grd._nrPointCellCenter[D],_grd._warpMode[D],_warpCountMax[D],extendMin[D],extendMax[D],_warpRange[D],_warpCompensate0[D]);
  }
  IteratorUtil<T,DIM>::initIterator(_id,_id0,_id1,_warpCount,_warpCountMax,verbose);
  _warpCompensate=_warpCompensate0;
  _off=_id.dot(_grd._strideCellCenter);
}
template <typename T,int DIM,typename Encoder>
CellCenterIteratorPoint<T,DIM,Encoder>::CellCenterIteratorPoint(const Grid& grd,int verbose,const BBox<T,DIM>& bb,const VecDIMi& extendMin,const VecDIMi& extendMax)
  :_grd(grd),_warpCount(VecDIMi::Zero())
{
  _warpRange=_grd.getBBNodal().getExtent();
  using namespace std::placeholders;
  IteratorUtil<T,DIM>::computeIdBB(_grd,_id0,_id1,bb,_warpCountMax,_warpRange,_warpCompensate0,std::bind(&Grid::indexSafeCellCenter,_grd,_1,_2,_3));
  for(sizeType D=0; D<DIM; D++)
    IteratorUtil<T,DIM>::extendIterator(_id0[D],_id1[D],_grd._nrPointCellCenter[D],_grd._warpMode[D],_warpCountMax[D],extendMin[D],extendMax[D],_warpRange[D],_warpCompensate0[D]);
  IteratorUtil<T,DIM>::initIterator(_id,_id0,_id1,_warpCount,_warpCountMax,verbose);
  _warpCompensate=_warpCompensate0;
  _off=_id.dot(_grd._strideCellCenter);
}
template <typename T,int DIM,typename Encoder>
bool CellCenterIteratorPoint<T,DIM,Encoder>::operator!=(const CellCenterIteratorPoint<T,DIM,Encoder>& other) const
{
  return _id != other._id || _warpCount != other._warpCount;
}
template <typename T,int DIM,typename Encoder>
typename CellCenterIteratorPoint<T,DIM,Encoder>::value_type CellCenterIteratorPoint<T,DIM,Encoder>::operator*() const
{
  VecDIMd pt;
  _grd.getPtCellCenter(_id,pt);
  return pt;
}
template <typename T,int DIM,typename Encoder>
typename CellCenterIteratorPoint<T,DIM,Encoder>::value_type CellCenterIteratorPoint<T,DIM,Encoder>::pointWithWarp() const
{
  return operator*()+_warpCompensate;
}
template <typename T,int DIM,typename Encoder>
const typename CellCenterIteratorPoint<T,DIM,Encoder>::value_type& CellCenterIteratorPoint<T,DIM,Encoder>::warpCompensate() const
{
  return _warpCompensate;
}
template <typename T,int DIM,typename Encoder>
void CellCenterIteratorPoint<T,DIM,Encoder>::operator++()
{
  OperatorTraits<T,DIM,Encoder,DIM-1>::advanceIterator
  (_grd._strideCellCenter,_grd._nrPointCellCenter,_warpCountMax,
   _warpRange,_warpCompensate0,_warpCompensate,
   _id0,_id1,_id,_warpCount,_off);
}
template <typename T,int DIM,typename Encoder>
sizeType CellCenterIteratorPoint<T,DIM,Encoder>::off() const
{
  return _off;
}
template <typename T,int DIM,typename Encoder>
const typename CellCenterIteratorPoint<T,DIM,Encoder>::VecDIMi& CellCenterIteratorPoint<T,DIM,Encoder>::id() const
{
  return _id;
}
template <typename T,int DIM,typename Encoder>
sizeType CellCenterIteratorPoint<T,DIM,Encoder>::offFaceLeft(sizeType D) const
{
  return _id.dot(_grd._strideMACVel[D]);
}
template <typename T,int DIM,typename Encoder>
sizeType CellCenterIteratorPoint<T,DIM,Encoder>::offFaceRight(sizeType D) const
{
  if(_id[D] < _grd._nrPointMACVel[D][D]-1)
    return _id.dot(_grd._strideMACVel[D])+_grd._strideMACVel[D][D];
  else {
    ASSERT(_grd._warpMode[D] > 0)
    VecDIMi id=_id;
    id[D]=0;
    return id.dot(_grd._strideMACVel[D]);
  }
}
template <typename T,int DIM,typename Encoder>
bool CellCenterIteratorPoint<T,DIM,Encoder>::hasCellCenter(sizeType D) const
{
  return _id[D] < _grd._nrPointCellCenter[D]-1 || _grd._warpMode[D] > 0;
}
template <typename T,int DIM,typename Encoder>
bool CellCenterIteratorPoint<T,DIM,Encoder>::hasCellCenterLeft(sizeType D) const
{
  return _id[D] > 0 || _grd._warpMode[D] > 0;
}
template <typename T,int DIM,typename Encoder>
sizeType CellCenterIteratorPoint<T,DIM,Encoder>::offCellCenter(sizeType D,VecDIMi* id) const
{
  if(_id[D] < _grd._nrPointCellCenter[D]-1) {
    if(id)
      *id=_id+VecDIMi::Unit(D);
    return _off+_grd._strideCellCenter[D];
  } else if(_grd._warpMode[D] > 0) {
    if(id) {
      *id=_id;
      (*id)[D]=0;
    }
    return _off-(_grd._nrPointCellCenter[D]-1)*_grd._strideCellCenter[D];
  } else {
    ASSERT(false)
    return -1;
  }
}
template <typename T,int DIM,typename Encoder>
sizeType CellCenterIteratorPoint<T,DIM,Encoder>::offCellCenterLeft(sizeType D,VecDIMi* id) const
{
  if(_id[D] > 0) {
    if(id)
      *id=_id-VecDIMi::Unit(D);
    return _off-_grd._strideCellCenter[D];
  } else if(_grd._warpMode[D] > 0) {
    if(id) {
      *id=_id;
      (*id)[D]=_grd._nrPointCellCenter[D]-1;
    }
    return _off+(_grd._nrPointCellCenter[D]-1)*_grd._strideCellCenter[D];
  } else {
    ASSERT(false)
    return -1;
  }
}
//Iterator: PointIndex
template <typename T,int DIM,typename Encoder>
CellCenterIteratorPointIndex<T,DIM,Encoder>::CellCenterIteratorPointIndex(const Grid& grd,int verbose,const VecDIMi& id0,const VecDIMi& id1)
  :CellCenterIteratorPoint<T,DIM,Encoder>(grd,verbose,id0,id1) {}
template <typename T,int DIM,typename Encoder>
typename CellCenterIteratorPointIndex<T,DIM,Encoder>::value_type CellCenterIteratorPointIndex<T,DIM,Encoder>::operator*() const
{
  return VecDIMi::Constant(_off);
}
//Iterator: Cell
template <typename T,int DIM,typename Encoder>
CellCenterIteratorCell<T,DIM,Encoder>::CellCenterIteratorCell(const Grid& grd,int verbose,const VecDIMi& id0,const VecDIMi& id1)
  :CellCenterIteratorPoint<T,DIM,Encoder>(grd,verbose,id0,id1+VecDIMi::Ones()) {}
template <typename T,int DIM,typename Encoder>
typename CellCenterIteratorCell<T,DIM,Encoder>::value_type CellCenterIteratorCell<T,DIM,Encoder>::operator*() const
{
  VecSTENCILi stencil;
  OperatorTraits<T,DIM,Encoder,DIM-1>::getStencil(_grd._strideCellCenter,_id.dot(_grd._strideCellCenter),stencil,0);
  return stencil;
}
//instance
#define INSTANCE(DIM,TYPE,SPARSITY) \
template class CellCenterIteratorPoint<scalar,DIM,Encoder##TYPE<SPARSITY> >;  \
template class CellCenterIteratorPointIndex<scalar,DIM,Encoder##TYPE<SPARSITY> >;  \
template class CellCenterIteratorCell<scalar,DIM,Encoder##TYPE<SPARSITY> >;
INSTANCE_ALL
