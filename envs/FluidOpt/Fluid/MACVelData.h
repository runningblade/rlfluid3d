#ifndef MACVEL_DATA_H
#define MACVEL_DATA_H

#include "VariableSizedGrid.h"

PRJ_BEGIN

//Iterator
template <int DIM,typename TDATA>
struct MACVelData : public Serializable {
  MACVelData():Serializable(typeid(MACVelData<DIM,TDATA>).name()) {}
  template <typename T,typename Encoder>
  MACVelData(const VariableSizedGrid<T,DIM,Encoder>& grid)
    :Serializable(typeid(MACVelData<DIM,TDATA>).name()) {
    reset(grid);
  }
  virtual bool read(istream& is,IOData* dat) override {
    for(sizeType D=0; D<DIM; D++)
      readBinaryData(_data[D],is);
    return is.good();
  }
  virtual bool write(ostream& os,IOData* dat) const override {
    for(sizeType D=0; D<DIM; D++)
      writeBinaryData(_data[D],os);
    return os.good();
  }
  virtual boost::shared_ptr<Serializable> copy() const override {
    return boost::shared_ptr<Serializable>(new CellCenterData<TDATA>);
  }
  template <typename T,typename Encoder>
  void reset(const VariableSizedGrid<T,DIM,Encoder>& grid) {
    for(sizeType D=0; D<DIM; D++)
      _data[D].setZero(grid._nrPointMACVel[D].prod());
  }
  void setZero() {
    for(sizeType D=0; D<DIM; D++)
      _data[D].setZero();
  }
  void setRandom() {
    for(sizeType D=0; D<DIM; D++)
      _data[D].setRandom();
  }
  void mul(TDATA coef) {
    for(sizeType D=0; D<DIM; D++)
      _data[D]*=coef;
  }
  void clamp(TDATA coef) {
    for(sizeType D=0; D<DIM; D++) {
      _data[D]=_data[D].cwiseMin(coef);
      _data[D]=_data[D].cwiseMax(-coef);
    }
  }
  void clamp(TDATA coefMin,TDATA coefMax) {
    for(sizeType D=0; D<DIM; D++) {
      _data[D]=_data[D].cwiseMin(coefMax);
      _data[D]=_data[D].cwiseMax(coefMin);
    }
  }
  void add(const MACVelData& other,TDATA coef) {
    for(sizeType D=0; D<DIM; D++)
      _data[D]+=other._data[D]*coef;
  }
  TDATA maxAbs() const {
    TDATA ret=0;
    for(sizeType D=0; D<DIM; D++)
      ret=max<TDATA>(ret,_data[D].cwiseAbs().maxCoeff());
    return ret;
  }
  void swap(const MACVelData<DIM,TDATA>& other) {
    for(sizeType D=0; D<DIM; D++)
      _data[D].swap(other._data[D]);
  }
  Eigen::Matrix<TDATA,-1,1> _data[DIM];
};
template <typename T,int DIM,typename Encoder>
struct MACVelIteratorPoint : public TypeTraits<T,DIM>  {
  using typename TypeTraits<T,DIM>::VecDIMd;
  using typename TypeTraits<T,DIM>::VecDIMi;
  typedef VariableSizedGrid<T,DIM,Encoder> Grid;
  typedef VecDIMd value_type;
  MACVelIteratorPoint(int D,const Grid& grd,int verbose,const VecDIMi& id0=VecDIMi::Zero(),const VecDIMi& id1=VecDIMi::Zero(),
                      const VecDIMi& extendMin=VecDIMi::Zero(),const VecDIMi& extendMax=VecDIMi::Zero());
  MACVelIteratorPoint(int D,const Grid& grd,int verbose,const BBox<T,DIM>& bb,
                      const VecDIMi& extendMin=VecDIMi::Zero(),const VecDIMi& extendMax=VecDIMi::Zero());
  virtual ~MACVelIteratorPoint() {}
  bool operator!=(const MACVelIteratorPoint<T,DIM,Encoder>& other) const;
  value_type operator*() const;
  value_type pointWithWarp() const;
  const value_type& warpCompensate() const;
  void operator++();
  sizeType D() const;
  sizeType off() const;
  bool hasCellCenter() const;
  bool hasCellCenterLeft() const;
  sizeType offCellCenter(VecDIMi* id=NULL) const;
  sizeType offCellCenterLeft(VecDIMi* id=NULL) const;
  const VecDIMi& id() const;
  //convenient function to iterator all interior faces
  static MACVelIteratorPoint<T,DIM,Encoder> iterAllInternal(int D,const Grid& grd,int verbose);
protected:
  const Grid& _grd;
  VecDIMi _id0,_id1,_id,_warpCount,_warpCountMax;
  VecDIMd _warpCompensate,_warpCompensate0,_warpRange;
  sizeType _off,_offCellCenter,_D;
};
template <typename T,int DIM,typename Encoder>
struct MACVelIteratorPointIndex : public MACVelIteratorPoint<T,DIM,Encoder>  {
  using typename MACVelIteratorPoint<T,DIM,Encoder>::VecDIMd;
  using typename MACVelIteratorPoint<T,DIM,Encoder>::VecDIMi;
  using typename MACVelIteratorPoint<T,DIM,Encoder>::Grid;
  typedef VecDIMi value_type;
  using MACVelIteratorPoint<T,DIM,Encoder>::_off;
  MACVelIteratorPointIndex(int D,const Grid& grd,int verbose,const VecDIMi& id0=VecDIMi::Zero(),const VecDIMi& id1=VecDIMi::Zero());
  virtual ~MACVelIteratorPointIndex() {}
  value_type operator*() const;
};
template <typename T,int DIM,typename Encoder>
struct MACVelIteratorCell : public MACVelIteratorPoint<T,DIM,Encoder>  {
  using typename MACVelIteratorPoint<T,DIM,Encoder>::VecDIMd;
  using typename MACVelIteratorPoint<T,DIM,Encoder>::VecDIMi;
  using typename MACVelIteratorPoint<T,DIM,Encoder>::Grid;
  using typename TypeTraits<T,DIM>::VecSTENCILi;
  typedef VecSTENCILi value_type;
  using MACVelIteratorPoint<T,DIM,Encoder>::_grd;
  using MACVelIteratorPoint<T,DIM,Encoder>::_id;
  using MACVelIteratorPoint<T,DIM,Encoder>::_D;
  MACVelIteratorCell(int D,const Grid& grd,int verbose,const VecDIMi& id0=VecDIMi::Zero(),const VecDIMi& id1=VecDIMi::Zero());
  value_type operator*() const;
};

PRJ_END

#endif
