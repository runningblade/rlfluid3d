#include "MACVelData.h"
#include "Utils.h"
#include <functional>

USE_PRJ_NAMESPACE

//Iterator: FacePoint
template <typename T,int DIM,typename Encoder>
MACVelIteratorPoint<T,DIM,Encoder>::MACVelIteratorPoint(int D,const Grid& grd,int verbose,const VecDIMi& id0,const VecDIMi& id1,const VecDIMi& extendMin,const VecDIMi& extendMax)
  :_grd(grd),_id0(id0),_id1(grd._nrPointMACVel[D]-id1),_warpCount(VecDIMi::Zero())
{
  _D=D;
  _warpRange=_grd.getBBNodal().getExtent();
  for(int D2=0; D2<DIM; D2++) {
    IteratorUtil<T,DIM>::makeIteratorRangeSafe(_id0[D2],_id1[D2],_grd._nrPointMACVel[D][D2],_grd._warpMode[D2],_warpCountMax[D2],_warpRange[D2],_warpCompensate0[D2]);
    IteratorUtil<T,DIM>::extendIterator(_id0[D2],_id1[D2],_grd._nrPointMACVel[D][D2],_grd._warpMode[D2],_warpCountMax[D2],extendMin[D2],extendMax[D2],_warpRange[D2],_warpCompensate0[D2]);
  }
  IteratorUtil<T,DIM>::initIterator(_id,_id0,_id1,_warpCount,_warpCountMax,verbose);
  _warpCompensate=_warpCompensate0;
  _off=_id.dot(_grd._strideMACVel[D]);
  _offCellCenter=_id.dot(_grd._strideCellCenter);
}
template <typename T,int DIM,typename Encoder>
MACVelIteratorPoint<T,DIM,Encoder>::MACVelIteratorPoint(int D,const Grid& grd,int verbose,const BBox<T,DIM>& bb,const VecDIMi& extendMin,const VecDIMi& extendMax)
  :_grd(grd),_warpCount(VecDIMi::Zero())
{
  _D=D;
  _warpRange=_grd.getBBNodal().getExtent();
  using namespace std::placeholders;
  IteratorUtil<T,DIM>::computeIdBB(_grd,_id0,_id1,bb,_warpCountMax,_warpRange,_warpCompensate0,std::bind(&Grid::indexSafeMACVel,_grd,D,_1,_2,_3));
  for(int D2=0; D2<DIM; D2++)
    IteratorUtil<T,DIM>::extendIterator(_id0[D2],_id1[D2],_grd._nrPointMACVel[D][D2],_grd._warpMode[D2],_warpCountMax[D2],extendMin[D2],extendMax[D2],_warpRange[D2],_warpCompensate0[D2]);
  IteratorUtil<T,DIM>::initIterator(_id,_id0,_id1,_warpCount,_warpCountMax,verbose);
  _warpCompensate=_warpCompensate0;
  _off=_id.dot(_grd._strideMACVel[D]);
  _offCellCenter=_id.dot(_grd._strideCellCenter);
}
template <typename T,int DIM,typename Encoder>
bool MACVelIteratorPoint<T,DIM,Encoder>::operator!=(const MACVelIteratorPoint<T,DIM,Encoder>& other) const
{
  return _id != other._id || _warpCount != other._warpCount;
}
template <typename T,int DIM,typename Encoder>
typename MACVelIteratorPoint<T,DIM,Encoder>::value_type MACVelIteratorPoint<T,DIM,Encoder>::operator*() const
{
  VecDIMd pt;
  _grd.getPtMACVel(_D,_id,pt);
  return pt;
}
template <typename T,int DIM,typename Encoder>
typename MACVelIteratorPoint<T,DIM,Encoder>::value_type MACVelIteratorPoint<T,DIM,Encoder>::pointWithWarp() const
{
  return operator*()+_warpCompensate;
}
template <typename T,int DIM,typename Encoder>
const typename MACVelIteratorPoint<T,DIM,Encoder>::value_type& MACVelIteratorPoint<T,DIM,Encoder>::warpCompensate() const
{
  return _warpCompensate;
}
template <typename T,int DIM,typename Encoder>
void MACVelIteratorPoint<T,DIM,Encoder>::operator++()
{
  OperatorTraits<T,DIM,Encoder,DIM-1>::advanceIteratorFaceCell
  (_grd._strideMACVel[_D],_grd._nrPointMACVel[_D],
   _grd._strideCellCenter,_grd._nrPointCellCenter,_warpCountMax,
   _warpRange,_warpCompensate0,_warpCompensate,
   _id0,_id1,_id,_warpCount,_off,_offCellCenter);
}
template <typename T,int DIM,typename Encoder>
sizeType MACVelIteratorPoint<T,DIM,Encoder>::D() const
{
  return _D;
}
template <typename T,int DIM,typename Encoder>
sizeType MACVelIteratorPoint<T,DIM,Encoder>::off() const
{
  return _off;
}
template <typename T,int DIM,typename Encoder>
bool MACVelIteratorPoint<T,DIM,Encoder>::hasCellCenter() const
{
  return _id[_D] < _grd._nrPointCellCenter[_D] || _grd._warpMode[_D] > 0;
}
template <typename T,int DIM,typename Encoder>
bool MACVelIteratorPoint<T,DIM,Encoder>::hasCellCenterLeft() const
{
  return _id[_D] > 0 || _grd._warpMode[_D] > 0;
}
template <typename T,int DIM,typename Encoder>
sizeType MACVelIteratorPoint<T,DIM,Encoder>::offCellCenter(VecDIMi* id) const
{
  if(_id[_D] < _grd._nrPointCellCenter[_D]) {
    if(id)
      *id=_id;
    return _offCellCenter;
  } else if(_grd._warpMode[_D] > 0) {
    if(id) {
      *id=_id;
      (*id)[_D]=0;
    }
    return _offCellCenter-_grd._nrPointCellCenter[_D]*_grd._strideCellCenter[_D];
  } else {
    ASSERT(false)
  }
}
template <typename T,int DIM,typename Encoder>
sizeType MACVelIteratorPoint<T,DIM,Encoder>::offCellCenterLeft(VecDIMi* id) const
{
  if(_id[_D] > 0) {
    if(id)
      *id=_id-VecDIMi::Unit(_D);
    return _offCellCenter-_grd._strideCellCenter[_D];
  } else if(_grd._warpMode[_D] > 0) {
    if(id) {
      *id=_id;
      (*id)[_D]=_grd._nrPointCellCenter[_D]-1;
    }
    return _offCellCenter+(_grd._nrPointCellCenter[_D]-1)*_grd._strideCellCenter[_D];
  } else {
    ASSERT(false)
  }
}
template <typename T,int DIM,typename Encoder>
const typename MACVelIteratorPoint<T,DIM,Encoder>::VecDIMi& MACVelIteratorPoint<T,DIM,Encoder>::id() const
{
  return _id;
}
//convenient function to iterator all interior faces
template <typename T,int DIM,typename Encoder>
MACVelIteratorPoint<T,DIM,Encoder> MACVelIteratorPoint<T,DIM,Encoder>::iterAllInternal(int D,const Grid& grd,int verbose)
{
  if(grd._warpMode[D] == 0)
    return MACVelIteratorPoint(D,grd,verbose,VecDIMi::Unit(D),VecDIMi::Unit(D));
  else return MACVelIteratorPoint(D,grd,verbose,VecDIMi::Zero(),VecDIMi::Zero());
}
//Iterator: FaceIndex
template <typename T,int DIM,typename Encoder>
MACVelIteratorPointIndex<T,DIM,Encoder>::MACVelIteratorPointIndex(int D,const Grid& grd,int verbose,const VecDIMi& id0,const VecDIMi& id1)
  :MACVelIteratorPoint<T,DIM,Encoder>(D,grd,verbose,id0,id1) {}
template <typename T,int DIM,typename Encoder>
typename MACVelIteratorPointIndex<T,DIM,Encoder>::value_type MACVelIteratorPointIndex<T,DIM,Encoder>::operator*() const
{
  return VecDIMi::Constant(_off);
}
//Iterator: FaceCell
template <typename T,int DIM,typename Encoder>
MACVelIteratorCell<T,DIM,Encoder>::MACVelIteratorCell(int D,const Grid& grd,int verbose,const VecDIMi& id0,const VecDIMi& id1)
  :MACVelIteratorPoint<T,DIM,Encoder>(D,grd,verbose,id0,id1+VecDIMi::Ones()) {}
template <typename T,int DIM,typename Encoder>
typename MACVelIteratorCell<T,DIM,Encoder>::value_type MACVelIteratorCell<T,DIM,Encoder>::operator*() const
{
  VecSTENCILi stencil;
  OperatorTraits<T,DIM,Encoder,DIM-1>::getStencil(_grd._strideMACVel[_D],_id.dot(_grd._strideMACVel[_D]),stencil,0);
  return stencil;
}
//instance
#define INSTANCE(DIM,TYPE,SPARSITY) \
template class MACVelIteratorPoint<scalar,DIM,Encoder##TYPE<SPARSITY> >;  \
template class MACVelIteratorPointIndex<scalar,DIM,Encoder##TYPE<SPARSITY> >;  \
template class MACVelIteratorCell<scalar,DIM,Encoder##TYPE<SPARSITY> >;
INSTANCE_ALL
