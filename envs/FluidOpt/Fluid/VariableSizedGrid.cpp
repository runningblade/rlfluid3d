#include "VariableSizedGrid.h"
#include "CellCenterData.h"
#include "NodalData.h"
#include "MACVelData.h"
#include "Utils.h"
#include <boost/lexical_cast.hpp>

USE_PRJ_NAMESPACE

//EncoderOctree
template <int sparsity>
int EncoderOctree<sparsity>::indexDeltaDIM(int input) const
{
  static const int iFromClz=31-sparsity;
  int i=iFromClz-__builtin_clz(input+blockSz);
  int j=((input+blockSz)>>i)-blockSz;
  return j+blockSz*i;
}
template <int sparsity>
int EncoderOctree<sparsity>::decodeDeltaDIM(int input) const
{
  int i=input>>sparsity;
  int j=input%blockSz;
  return ((j+blockSz)<<i)-blockSz;
}
//EncoderConstant
template <int sparsity>
int EncoderConstant<sparsity>::indexDeltaDIM(int input) const
{
  return input>>sparsity;
}
template <int sparsity>
int EncoderConstant<sparsity>::decodeDeltaDIM(int input) const
{
  return input<<sparsity;
}
//OperatorTraits<T,DIM,Encoder,DIM2>
template <typename T,int DIM,typename Encoder,int DIM2>
void OperatorTraits<T,DIM,Encoder,DIM2>::sampleStencil(const VecDIMd& x,const VecDIMd& x0,const VecDIMi& stride,const VecDIMd& invCellSz,sizeType off,VecSTENCILd& weights,VecSTENCILi& indices,T w0,char offStencil)
{
  T alpha=(x[DIM2]-x0[DIM2])*invCellSz[DIM2];
  OperatorTraits<T,DIM,Encoder,DIM2-1>::sampleStencil(x,x0,stride,invCellSz,off,weights,indices,(1-alpha)*w0,offStencil);
  OperatorTraits<T,DIM,Encoder,DIM2-1>::sampleStencil(x,x0,stride,invCellSz,off+stride[DIM2],weights,indices,alpha*w0,offStencil+(1<<DIM2));
}
template <typename T,int DIM,typename Encoder,int DIM2>
void OperatorTraits<T,DIM,Encoder,DIM2>::computeStrideWithWarp(const VecDIMi& id,const VecDIMi& nrPoint,const VecDIMi& stride,const VecDIMi& warpMode,VecDIMi& strideWithWarp)
{
  strideWithWarp[DIM2]=(warpMode[DIM2] > 0 && id[DIM2] == nrPoint[DIM2]-1) ? -(nrPoint[DIM2]-1)*stride[DIM2] : stride[DIM2];
  OperatorTraits<T,DIM,Encoder,DIM2-1>::computeStrideWithWarp(id,nrPoint,stride,warpMode,strideWithWarp);
}
template <typename T,int DIM,typename Encoder,int DIM2>
template <typename DATA>
typename DATA::Scalar OperatorTraits<T,DIM,Encoder,DIM2>::sample(const DATA& data,const VecDIMd& x,const VecDIMd& x0,const VecDIMi& stride,const VecDIMd& invCellSz,sizeType off,T& minV,T& maxV)
{
  T alpha=(x[DIM2]-x0[DIM2])*invCellSz[DIM2];
  return OperatorTraits<T,DIM,Encoder,DIM2-1>::sample(data,x,x0,stride,invCellSz,off,minV,maxV)*(1-alpha)+
         OperatorTraits<T,DIM,Encoder,DIM2-1>::sample(data,x,x0,stride,invCellSz,off+stride[DIM2],minV,maxV)*alpha;
}
template <typename T,int DIM,typename Encoder,int DIM2>
template <typename DATA>
typename DATA::Scalar OperatorTraits<T,DIM,Encoder,DIM2>::sample(const DATA& data,const VecDIMd& x,const VecDIMd& x0,const VecDIMi& stride,const VecDIMd& invCellSz,sizeType off)
{
  T alpha=(x[DIM2]-x0[DIM2])*invCellSz[DIM2];
  return OperatorTraits<T,DIM,Encoder,DIM2-1>::sample(data,x,x0,stride,invCellSz,off)*(1-alpha)+
         OperatorTraits<T,DIM,Encoder,DIM2-1>::sample(data,x,x0,stride,invCellSz,off+stride[DIM2])*alpha;
}
template <typename T,int DIM,typename Encoder,int DIM2>
template <typename DATA>
void OperatorTraits<T,DIM,Encoder,DIM2>::sampleVel(const Grid& grd,const DATA& data,VecDIMd& x,VecDIMi& id,VecDIMd& vel)
{
  VecDIMd xTmp=x;
  vel[DIM2]=grd.sampleMACVel(DIM2,data,xTmp,id);
  OperatorTraits<T,DIM,Encoder,DIM2-1>::sampleVel(grd,data,x,id,vel);
}
template <typename T,int DIM,typename Encoder,int DIM2>
void OperatorTraits<T,DIM,Encoder,DIM2>::getNeigh(const VecDIMi& nrPoint,const VecDIMi& stride,const VecDIMi& warpMode,const VecDIMi& id,sizeType off,VecSTENCILi& neigh,int& nr)
{
  //left
  if(id[DIM2] > 0)
    neigh[nr++]=off-stride[DIM2];
  else if(warpMode[DIM2] > 0)
    neigh[nr++]=off+(nrPoint[DIM2]-1)*stride[DIM2];
  //right
  if(id[DIM2] < nrPoint[DIM2]-1)
    neigh[nr++]=off+stride[DIM2];
  else if(warpMode[DIM2] > 0)
    neigh[nr++]=off-(nrPoint[DIM2]-1)*stride[DIM2];
  //descend dimension
  OperatorTraits<T,DIM,Encoder,DIM2-1>::getNeigh(nrPoint,stride,warpMode,id,off,neigh,nr);
}
template <typename T,int DIM,typename Encoder,int DIM2>
void OperatorTraits<T,DIM,Encoder,DIM2>::getVal(const vector<T> vals[DIM],const VecDIMi& id,VecDIMd& val)
{
  val[DIM2]=vals[DIM2][id[DIM2]];
  OperatorTraits<T,DIM,Encoder,DIM2-1>::getVal(vals,id,val);
}
template <typename T,int DIM,typename Encoder,int DIM2>
void OperatorTraits<T,DIM,Encoder,DIM2>::getValBut(unsigned char D,const vector<T> valButs[DIM],const vector<T> vals[DIM],const VecDIMi& id,VecDIMd& val)
{
  val[DIM2]=D == DIM2 ? valButs[DIM2][id[DIM2]] : vals[DIM2][id[DIM2]];
  OperatorTraits<T,DIM,Encoder,DIM2-1>::getValBut(D,valButs,vals,id,val);
}
template <typename T,int DIM,typename Encoder,int DIM2>
void OperatorTraits<T,DIM,Encoder,DIM2>::stride(VecDIMi& stride,const VecDIMi& nrPoint)
{
  if(DIM2 == nrPoint.size()-1)
    stride[DIM2]=1;
  else stride[DIM2]=nrPoint[DIM2+1]*stride[DIM2+1];
  OperatorTraits<T,DIM,Encoder,DIM2-1>::stride(stride,nrPoint);
}
template <typename T,int DIM,typename Encoder,int DIM2>
void OperatorTraits<T,DIM,Encoder,DIM2>::decode(sizeType id,const VecDIMi& stride,VecDIMi& decoded)
{
  decoded[CO_DIM2]=id/stride[CO_DIM2];
  OperatorTraits<T,DIM,Encoder,DIM2-1>::decode(id%stride[CO_DIM2],stride,decoded);
}
template <typename T,int DIM,typename Encoder,int DIM2>
void OperatorTraits<T,DIM,Encoder,DIM2>::getStencil(const VecDIMi& stride,sizeType off,VecSTENCILi& indices,unsigned char offStencil)
{
  OperatorTraits<T,DIM,Encoder,DIM2-1>::getStencil(stride,off,indices,offStencil);
  OperatorTraits<T,DIM,Encoder,DIM2-1>::getStencil(stride,off+stride[DIM2],indices,offStencil+(1<<DIM2));
}
template <typename T,int DIM,typename Encoder,int DIM2>
void OperatorTraits<T,DIM,Encoder,DIM2>::advanceIterator
(const VecDIMi& stride,const VecDIMi& nrPoint,const VecDIMi& warpCountMax,
 const VecDIMd& warpRange,const VecDIMd& warpCompensate0,VecDIMd& warpCompensate,
 const VecDIMi& id0,const VecDIMi& id1,VecDIMi& id,VecDIMi& warpCount,sizeType& off)
{
  id[DIM2]++;
  off+=stride[DIM2];
  if(id[DIM2] == nrPoint[DIM2] && warpCount[DIM2] < warpCountMax[DIM2]) {
    id[DIM2]-=nrPoint[DIM2];
    off-=nrPoint[DIM2]*stride[DIM2];
    warpCount[DIM2]++;
    warpCompensate[DIM2]+=warpRange[DIM2];
  }
  if(id[DIM2] == id1[DIM2] && warpCount[DIM2] == warpCountMax[DIM2]) {
    off+=(id0[DIM2]-id[DIM2])*stride[DIM2];
    id[DIM2]=id0[DIM2];
    warpCount[DIM2]=0;
    warpCompensate[DIM2]=warpCompensate0[DIM2];
    OperatorTraits<T,DIM,Encoder,DIM2-1>::advanceIterator(stride,nrPoint,warpCountMax,
        warpRange,warpCompensate0,warpCompensate,
        id0,id1,id,warpCount,off);
  }
}
template <typename T,int DIM,typename Encoder,int DIM2>
void OperatorTraits<T,DIM,Encoder,DIM2>::advanceIteratorFaceCell
(const VecDIMi& strideF,const VecDIMi& nrPointF,
 const VecDIMi& strideC,const VecDIMi& nrPointC,const VecDIMi& warpCountMax,
 const VecDIMd& warpRange,const VecDIMd& warpCompensate0,VecDIMd& warpCompensate,
 const VecDIMi& id0,const VecDIMi& id1,VecDIMi& id,VecDIMi& warpCount,sizeType& off,sizeType& offC)
{
  id[DIM2]++;
  off+=strideF[DIM2];
  offC+=strideC[DIM2];
  if(id[DIM2] == nrPointF[DIM2] && warpCount[DIM2] < warpCountMax[DIM2]) {
    id[DIM2]-=nrPointF[DIM2];
    off-=nrPointF[DIM2]*strideF[DIM2];
    offC-=nrPointC[DIM2]*strideC[DIM2];
    warpCount[DIM2]++;
    warpCompensate[DIM2]+=warpRange[DIM2];
  }
  if(id[DIM2] == id1[DIM2] && warpCount[DIM2] == warpCountMax[DIM2]) {
    off+=(id0[DIM2]-id[DIM2])*strideF[DIM2];
    offC+=(id0[DIM2]-id[DIM2])*strideC[DIM2];
    id[DIM2]=id0[DIM2];
    warpCount[DIM2]=0;
    warpCompensate[DIM2]=warpCompensate0[DIM2];
    OperatorTraits<T,DIM,Encoder,DIM2-1>::advanceIteratorFaceCell(strideF,nrPointF,strideC,nrPointC,warpCountMax,
        warpRange,warpCompensate0,warpCompensate,
        id0,id1,id,warpCount,off,offC);
  }
}
//OperatorTraits<T,DIM,Encoder,0>
template <typename T,int DIM,typename Encoder>
void OperatorTraits<T,DIM,Encoder,0>::sampleStencil(const VecDIMd& x,const VecDIMd& x0,const VecDIMi& stride,const VecDIMd& invCellSz,sizeType off,VecSTENCILd& weights,VecSTENCILi& indices,T w0,char offStencil)
{
  T alpha=(x[0]-x0[0])*invCellSz[0];
  weights[offStencil+0]=(1-alpha)*w0;
  weights[offStencil+1]=alpha*w0;
  indices[offStencil+0]=off;
  indices[offStencil+1]=off+stride[0];
}
template <typename T,int DIM,typename Encoder>
void OperatorTraits<T,DIM,Encoder,0>::computeStrideWithWarp(const VecDIMi& id,const VecDIMi& nrPoint,const VecDIMi& stride,const VecDIMi& warpMode,VecDIMi& strideWithWarp)
{
  strideWithWarp[0]=(warpMode[0] > 0 && id[0] == nrPoint[0]-1) ? -(nrPoint[0]-1)*stride[0] : stride[0];
}
template <typename T,int DIM,typename Encoder>
template <typename DATA>
typename DATA::Scalar OperatorTraits<T,DIM,Encoder,0>::sample(const DATA& data,const VecDIMd& x,const VecDIMd& x0,const VecDIMi& stride,const VecDIMd& invCellSz,sizeType off,T& minV,T& maxV)
{
  T alpha=(x[0]-x0[0])*invCellSz[0];
  T a=data[off],b=data[off+stride[0]];
  minV=min(min(a,b),minV);
  maxV=max(max(a,b),maxV);
  return a*(1-alpha)+b*alpha;
}
template <typename T,int DIM,typename Encoder>
template <typename DATA>
typename DATA::Scalar OperatorTraits<T,DIM,Encoder,0>::sample(const DATA& data,const VecDIMd& x,const VecDIMd& x0,const VecDIMi& stride,const VecDIMd& invCellSz,sizeType off)
{
  T alpha=(x[0]-x0[0])*invCellSz[0];
  T a=data[off],b=data[off+stride[0]];
  return a*(1-alpha)+b*alpha;
}
template <typename T,int DIM,typename Encoder>
template <typename DATA>
void OperatorTraits<T,DIM,Encoder,0>::sampleVel(const Grid& grd,const DATA& data,VecDIMd& x,VecDIMi& id,VecDIMd& vel)
{
  VecDIMd xTmp=x;
  vel[0]=grd.sampleMACVel(0,data,xTmp,id);
}
template <typename T,int DIM,typename Encoder>
void OperatorTraits<T,DIM,Encoder,0>::getNeigh(const VecDIMi& nrPoint,const VecDIMi& stride,const VecDIMi& warpMode,const VecDIMi& id,sizeType off,VecSTENCILi& neigh,int& nr)
{
  //left
  if(id[0] > 0)
    neigh[nr++]=off-stride[0];
  else if(warpMode[0] > 0)
    neigh[nr++]=off+(nrPoint[0]-1)*stride[0];
  //right
  if(id[0] < nrPoint[0]-1)
    neigh[nr++]=off+stride[0];
  else if(warpMode[0] > 0)
    neigh[nr++]=off-(nrPoint[0]-1)*stride[0];
}
template <typename T,int DIM,typename Encoder>
void OperatorTraits<T,DIM,Encoder,0>::getVal(const vector<T> vals[DIM],const VecDIMi& id,VecDIMd& val)
{
  val[0]=vals[0][id[0]];
}
template <typename T,int DIM,typename Encoder>
void OperatorTraits<T,DIM,Encoder,0>::getValBut(unsigned char D,const vector<T> valButs[DIM],const vector<T> vals[DIM],const VecDIMi& id,VecDIMd& val)
{
  val[0]=D == 0 ? valButs[0][id[0]] : vals[0][id[0]];
}
template <typename T,int DIM,typename Encoder>
void OperatorTraits<T,DIM,Encoder,0>::stride(VecDIMi& stride,const VecDIMi& nrPoint)
{
  stride[0]=stride[1]*nrPoint[1];
}
template <typename T,int DIM,typename Encoder>
void OperatorTraits<T,DIM,Encoder,0>::decode(sizeType id,const VecDIMi& stride,VecDIMi& decoded)
{
  decoded[CO_DIM2]=id;
}
template <typename T,int DIM,typename Encoder>
void OperatorTraits<T,DIM,Encoder,0>::getStencil(const VecDIMi& stride,sizeType off,VecSTENCILi& indices,unsigned char offStencil)
{
  indices[offStencil+0]=off;
  indices[offStencil+1]=off+stride[0];
}
template <typename T,int DIM,typename Encoder>
void OperatorTraits<T,DIM,Encoder,0>::advanceIterator
(const VecDIMi& stride,const VecDIMi& nrPoint,const VecDIMi& warpCountMax,
 const VecDIMd& warpRange,const VecDIMd& warpCompensate0,VecDIMd& warpCompensate,
 const VecDIMi& id0,const VecDIMi& id1,VecDIMi& id,VecDIMi& warpCount,sizeType& off)
{
  id[0]++;
  off+=stride[0];
  if(id[0] == nrPoint[0] && warpCount[0] < warpCountMax[0]) {
    id[0]-=nrPoint[0];
    off-=nrPoint[0]*stride[0];
    warpCount[0]++;
    warpCompensate[0]+=warpRange[0];
  }
}
template <typename T,int DIM,typename Encoder>
void OperatorTraits<T,DIM,Encoder,0>::advanceIteratorFaceCell
(const VecDIMi& strideF,const VecDIMi& nrPointF,
 const VecDIMi& strideC,const VecDIMi& nrPointC,const VecDIMi& warpCountMax,
 const VecDIMd& warpRange,const VecDIMd& warpCompensate0,VecDIMd& warpCompensate,
 const VecDIMi& id0,const VecDIMi& id1,VecDIMi& id,VecDIMi& warpCount,sizeType& off,sizeType& offC)
{
  id[0]++;
  off+=strideF[0];
  offC+=strideC[0];
  if(id[0] == nrPointF[0] && warpCount[0] < warpCountMax[0]) {
    id[0]-=nrPointF[0];
    off-=nrPointF[0]*strideF[0];
    offC-=nrPointC[0]*strideC[0];
    warpCount[0]++;
    warpCompensate[0]+=warpRange[0];
  }
}
//VariableSizedGrid<T,DIM,Encoder>
template <typename T,int DIM,typename Encoder>
VariableSizedGrid<T,DIM,Encoder>::VariableSizedGrid():Serializable(typeid(VariableSizedGrid<T,DIM,Encoder>).name()) {}
template <typename T,int DIM,typename Encoder>
VariableSizedGrid<T,DIM,Encoder>::VariableSizedGrid(const BBox<T>& bbInterior,const Vec3i& nrCellInterior,const BBox<sizeType>& extend,int verbose,const Vec3i* warpMode)
  :Serializable(typeid(VariableSizedGrid<T,DIM,Encoder>).name())
{
  reset(bbInterior,nrCellInterior,extend,verbose,warpMode);
}
template <typename T,int DIM,typename Encoder>
VariableSizedGrid<T,DIM,Encoder>::VariableSizedGrid(const BBox<T,DIM>& bbInterior,const VecDIMi& nrCellInterior,const BBox<sizeType,DIM>& extend,const Vec3i* warpMode)
  :Serializable(typeid(VariableSizedGrid<T,DIM,Encoder>).name())
{
  reset(bbInterior,nrCellInterior,extend,warpMode);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::reset(const BBox<T>& bbInterior,const Vec3i& nrCellInterior,const BBox<sizeType>& extend,int verbose,const Vec3i* warpMode)
{
  BBox<T,DIM> bbInteriorDIM(assignVariableTo<VecDIMd,Vec3>(bbInterior._minC),assignVariableTo<VecDIMd,Vec3>(bbInterior._maxC));
  BBox<sizeType,DIM> extendDIM(assignVariableTo<VecDIMi,Vec3i>(extend._minC),assignVariableTo<VecDIMi,Vec3i>(extend._maxC));
  reset(bbInteriorDIM,nrCellInterior.segment<DIM>(0),extendDIM,warpMode);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::reset(const BBox<T,DIM>& bbInterior,const VecDIMi& nrCellInterior,const BBox<sizeType,DIM>& extend,const Vec3i* warpMode)
{
  if(warpMode)
    _warpMode=warpMode->segment<DIM>(0).cwiseMax(VecDIMi::Zero()).cwiseMin(VecDIMi::Ones());
  else _warpMode.setConstant(0);
  sizeType largeCellSz=Encoder().decodeDeltaDIM(1);
  if(!_warpMode.isZero()) {
    ASSERT_MSG(Encoder::isConstantEncoder,"Only ConstantEncoder allows warping!")
    for(sizeType D=0; D<DIM; D++)
      if(_warpMode[D] > 0) {
        ASSERT_MSGV(nrCellInterior[D]%largeCellSz == 0,"nrCellInterior[%ld] must be power of 2!",D)
      }
  }
  _offset._minC=extend._minC;
  _offset._maxC=extend._minC+nrCellInterior;
  _cellSz0=(bbInterior.getExtent().array()/nrCellInterior.array().template cast<T>()).matrix();
  _invCellSz0.array()=1/_cellSz0.array();
  //precompute nrPoint
  _nrPointNodalWithWarp=nrCellInterior+extend._minC+extend._maxC+VecDIMi::Ones();
  _nrPointNodal=_nrPointNodalWithWarp-_warpMode;
  _nrPointCellCenter=nrCellInterior+extend._minC+extend._maxC;
  _nrPointCellCenterWithWarp=_nrPointCellCenter+_warpMode;
  for(unsigned char D=0; D<DIM; D++)
    for(unsigned char D2=0; D2<DIM; D2++)
      if(D == D2) {
        _nrPointMACVelWithWarp[D][D2]=_nrPointNodalWithWarp[D2];
        _nrPointMACVel[D][D2]=_nrPointNodal[D2];
      } else {
        _nrPointMACVelWithWarp[D][D2]=_nrPointCellCenterWithWarp[D2];
        _nrPointMACVel[D][D2]=_nrPointCellCenter[D2];
      }
  //Precompute stride
  OperatorTraits<T,DIM,Encoder,DIM-1>::stride(_strideNodal,_nrPointNodal);
  OperatorTraits<T,DIM,Encoder,DIM-1>::stride(_strideCellCenter,_nrPointCellCenter);
  for(unsigned char D=0; D<DIM; D++)
    OperatorTraits<T,DIM,Encoder,DIM-1>::stride(_strideMACVel[D],_nrPointMACVel[D]);
  //precompute cellSize: Nodal
  for(unsigned char D=0; D<DIM; D++) {
    _x0Nodals[D].resize(_nrPointNodalWithWarp[D]);
    _cellSzNodals[D].resize(_nrPointNodalWithWarp[D]);
    _invCellSzNodals[D].resize(_nrPointNodalWithWarp[D]);
    for(sizeType i=0; i<_nrPointNodalWithWarp[D]; i++) {
      if(i<_offset._minC[D]) {
        int x0=Encoder::decodeDeltaDIM(_offset._minC[D]-i);
        int x1=Encoder::decodeDeltaDIM(_offset._minC[D]-i-1);
        _x0Nodals[D][i]=bbInterior._minC[D]-x0*_cellSz0[D];
        _cellSzNodals[D][i]=(x0-x1)*_cellSz0[D];
      } else if(i>=_offset._maxC[D]) {
        int x0=Encoder::decodeDeltaDIM(i-_offset._maxC[D]);
        int x1=Encoder::decodeDeltaDIM(i+1-_offset._maxC[D]);
        _x0Nodals[D][i]=bbInterior._maxC[D]+x0*_cellSz0[D];
        _cellSzNodals[D][i]=(x1-x0)*_cellSz0[D];
      } else {
        _x0Nodals[D][i]=bbInterior._minC[D]+(i-_offset._minC[D])*_cellSz0[D];
        _cellSzNodals[D][i]=_cellSz0[D];
      }
      _invCellSzNodals[D][i]=1/_cellSzNodals[D][i];
    }
  }
  //precompute cellSize: CellCenter
  for(unsigned char D=0; D<DIM; D++) {
    _x0CellCenters[D].resize(_nrPointCellCenterWithWarp[D]);
    _cellSzCellCenters[D].resize(_nrPointCellCenter[D]);
    _invCellSzCellCenters[D].resize(_nrPointCellCenter[D]);
    for(sizeType i=0; i<_nrPointCellCenter[D]; i++) {
      _x0CellCenters[D][i]=_x0Nodals[D][i]+_cellSzNodals[D][i]/2;
      _cellSzCellCenters[D][i]=(_cellSzNodals[D][i]+_cellSzNodals[D][i+1])/2;
      _invCellSzCellCenters[D][i]=1/_cellSzCellCenters[D][i];
    }
    if(_warpMode[D]) {
      _cellSzCellCenters[D].back()=(_cellSzNodals[D][0]+_cellSzNodals[D][_nrPointCellCenter[D]-1])/2;
      _invCellSzCellCenters[D].back()=1/_cellSzCellCenters[D].back();
      _x0CellCenters[D].back()=_x0CellCenters[D][_nrPointCellCenter[D]-1]+_cellSzCellCenters[D].back();
    }
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::reset(const VariableSizedGrid<T,DIM,Encoder>& refGrid,BBox<T,DIM> focus,int extendBD)
{
  _warpMode=refGrid._warpMode;
  //we actually can move grid around without a loss of information if warpMode > 0.
  //do this so that focus region is centered
  BBox<T,DIM> refBB=refGrid.getBBNodal();
  sizeType largeCellSz=Encoder().decodeDeltaDIM(1);
  VecDIMd move=VecDIMd::Zero();
  {
    VecDIMd ctr=(focus._minC+focus._maxC)/2;
    VecDIMd ctrRef=(refBB._minC+refBB._maxC)/2;
    for(unsigned char D=0; D<DIM; D++)
      if(_warpMode[D] > 0) {
        T unit=refGrid._cellSz0[D]*(T)largeCellSz;
        while(ctr[D] < ctrRef[D]-unit) {
          ctr[D]+=unit;
          move[D]-=unit;
        }
        while(ctr[D] > ctrRef[D]+unit) {
          ctr[D]-=unit;
          move[D]+=unit;
        }
        focus._minC[D]-=move[D];
        focus._maxC[D]-=move[D];
        //let's don't move too much, in order to reserve some precision of floating point data
        T refExtent=refBB.getExtent()[D];
        ASSERT(abs(refExtent-refExtent/unit*unit) < 1E-6f)
        while(move[D] > refExtent)
          move[D]-=refExtent;
        while(move[D] < -refExtent)
          move[D]+=refExtent;
      }
  }
  //compute ref bounding box: prevent rounding error
  for(unsigned char D=0; D<DIM; D++) {
    refBB._minC[D]+=refGrid._cellSz0[D]*0.1f;
    refBB._maxC[D]-=refGrid._cellSz0[D]*0.1f;
  }
  //compute ref's focus region's lower-left corner
  VecDIMd refBBFocusMinC;
  refGrid.getPtNodal(refGrid._offset._minC,refBBFocusMinC);
  //compute focus region
  BBox<sizeType,DIM> offsetRel;
  offsetRel._minC=floorV(VecDIMd(((focus._minC-refBBFocusMinC).array()/refGrid._cellSz0.array()).matrix()));
  offsetRel._maxC=ceilV(VecDIMd(((focus._maxC-refBBFocusMinC).array()/refGrid._cellSz0.array()).matrix()));
  for(unsigned char D=0; D<DIM; D++) { //confine focus region to bounding box
    if(_warpMode[D] > 0) {
      offsetRel._minC[D]=offsetRel._minC[D]/largeCellSz*largeCellSz;
      offsetRel._maxC[D]=(offsetRel._maxC[D]+largeCellSz-1)/largeCellSz*largeCellSz;
    }
    while(offsetRel._minC[D]*refGrid._cellSz0[D]+refBBFocusMinC[D] < refBB._minC[D])
      offsetRel._minC[D]+=(_warpMode[D] > 0) ? largeCellSz : 1;
    while(offsetRel._maxC[D]*refGrid._cellSz0[D]+refBBFocusMinC[D] > refBB._maxC[D])
      offsetRel._maxC[D]-=(_warpMode[D] > 0) ? largeCellSz : 1;
  }
  //now we can nail down the focus bounding box
  BBox<T,DIM> bbInterior;
  bbInterior._minC.array()=offsetRel._minC.array().template cast<T>()*refGrid._cellSz0.array()+refBBFocusMinC.array();
  bbInterior._maxC.array()=offsetRel._maxC.array().template cast<T>()*refGrid._cellSz0.array()+refBBFocusMinC.array();
  //compute how many cells we should extend
  BBox<sizeType,DIM> extend(VecDIMi::Zero(),VecDIMi::Zero());
  for(unsigned char D=0; D<DIM; D++) {
    while(bbInterior._maxC[D]+Encoder::decodeDeltaDIM(extend._maxC[D])*refGrid._cellSz0[D] < refBB._maxC[D])
      extend._maxC[D]++;
    while(bbInterior._minC[D]-Encoder::decodeDeltaDIM(extend._minC[D])*refGrid._cellSz0[D] > refBB._minC[D])
      extend._minC[D]++;
    if(_warpMode[D] == 0) {
      extend._maxC[D]+=extendBD;
      extend._minC[D]+=extendBD;
    }
  }
  //move
  bbInterior._minC+=move;
  bbInterior._maxC+=move;
  Vec3i warpMode=assignVariableTo<Vec3i,VecDIMi>(_warpMode);
  reset(bbInterior,offsetRel.getExtent(),extend,&warpMode);
}
template <typename T,int DIM,typename Encoder>
bool VariableSizedGrid<T,DIM,Encoder>::read(istream& is,IOData* dat)
{
  //data: Nodal
  readBinaryData(_nrPointNodalWithWarp,is);
  readBinaryData(_nrPointNodal,is);
  readBinaryData(_strideNodal,is);
  for(unsigned char D=0; D<DIM; D++) {
    readVector(_x0Nodals[D],is);
    readVector(_cellSzNodals[D],is);
    readVector(_invCellSzNodals[D],is);
  }
  //data: CellCenter
  readBinaryData(_nrPointCellCenterWithWarp,is);
  readBinaryData(_nrPointCellCenter,is);
  readBinaryData(_strideCellCenter,is);
  for(unsigned char D=0; D<DIM; D++) {
    readVector(_x0CellCenters[D],is);
    readVector(_cellSzCellCenters[D],is);
    readVector(_invCellSzCellCenters[D],is);
  }
  //data: MACVel
  for(unsigned char D=0; D<DIM; D++) {
    readVector(_nrPointMACVelWithWarp[D],is);
    readVector(_nrPointMACVel[D],is);
    readVector(_strideMACVel[D],is);
  }
  //data
  readBinaryData(_cellSz0,is);
  readBinaryData(_invCellSz0,is);
  readBinaryData(_offset,is);
  readBinaryData(_warpMode,is);
  return is.good();
}
template <typename T,int DIM,typename Encoder>
bool VariableSizedGrid<T,DIM,Encoder>::write(ostream& os,IOData* dat) const
{
  //data: Nodal
  writeBinaryData(_nrPointNodalWithWarp,os);
  writeBinaryData(_nrPointNodal,os);
  writeBinaryData(_strideNodal,os);
  for(unsigned char D=0; D<DIM; D++) {
    writeVector(_x0Nodals[D],os);
    writeVector(_cellSzNodals[D],os);
    writeVector(_invCellSzNodals[D],os);
  }
  //data: CellCenter
  writeBinaryData(_nrPointCellCenterWithWarp,os);
  writeBinaryData(_nrPointCellCenter,os);
  writeBinaryData(_strideCellCenter,os);
  for(unsigned char D=0; D<DIM; D++) {
    writeVector(_x0CellCenters[D],os);
    writeVector(_cellSzCellCenters[D],os);
    writeVector(_invCellSzCellCenters[D],os);
  }
  //data: MACVel
  for(unsigned char D=0; D<DIM; D++) {
    writeVector(_nrPointMACVelWithWarp[D],os);
    writeVector(_nrPointMACVel[D],os);
    writeVector(_strideMACVel[D],os);
  }
  //data
  writeBinaryData(_cellSz0,os);
  writeBinaryData(_invCellSz0,os);
  writeBinaryData(_offset,os);
  writeBinaryData(_warpMode,os);
  return os.good();
}
template <typename T,int DIM,typename Encoder>
boost::shared_ptr<Serializable> VariableSizedGrid<T,DIM,Encoder>::copy() const
{
  return boost::shared_ptr<Serializable>(new VariableSizedGrid<T,DIM,Encoder>);
}
//handle warp related function
template <typename T,int DIM,typename Encoder>
string VariableSizedGrid<T,DIM,Encoder>::printWarp(const Vec3i& warpMode)
{
  return boost::lexical_cast<string>(warpMode[0])+
         boost::lexical_cast<string>(warpMode[1])+
         boost::lexical_cast<string>(warpMode[2]);
}
template <typename T,int DIM,typename Encoder>
T VariableSizedGrid<T,DIM,Encoder>::handleWarp(T& x,unsigned char D) const
{
  if(_warpMode[D] > 0)
    return handleScalarWarp<T>(x,_x0Nodals[D].front(),_x0Nodals[D].back(),NULL);
  else return 0;
}
template <typename T,int DIM,typename Encoder>
typename VariableSizedGrid<T,DIM,Encoder>::VecDIMd VariableSizedGrid<T,DIM,Encoder>::handleWarp(VecDIMd x) const
{
  for(sizeType D=0; D<DIM; D++)
    handleWarp(x[D],D);
  return x;
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::handleWarpDiff(T& x,unsigned char D) const
{
  handleScalarWarpDiff<T>(x,_x0Nodals[D].front(),_x0Nodals[D].back(),NULL);
}
template <typename T,int DIM,typename Encoder>
typename VariableSizedGrid<T,DIM,Encoder>::VecDIMd VariableSizedGrid<T,DIM,Encoder>::handleWarpDiff(VecDIMd x) const
{
  for(sizeType D=0; D<DIM; D++)
    handleWarpDiff(x[D],D);
  return x;
}
template <typename T,int DIM,typename Encoder>
typename VariableSizedGrid<T,DIM,Encoder>::VecDIMi VariableSizedGrid<T,DIM,Encoder>::handleWarp(const VecDIMi& id,const VecDIMi& nrPoint,VecDIMi* warpCompensate) const
{
  VecDIMi ret=id;
  if(warpCompensate)
    warpCompensate->setZero();
  for(sizeType D=0; D<DIM; D++)
    if(_warpMode[D] > 0) {
      while(ret[D] < 0) {
        ret[D]+=nrPoint[D];
        if(warpCompensate)
          (*warpCompensate)[D]--;
      }
      while(ret[D] >= nrPoint[D]) {
        ret[D]-=nrPoint[D];
        if(warpCompensate)
          (*warpCompensate)[D]++;
      }
    }
  return ret;
}
//Nodal operator
template <typename T,int DIM,typename Encoder>
BBox<T,DIM> VariableSizedGrid<T,DIM,Encoder>::getBBNodal() const
{
  BBox<T,DIM> ret;
  getPtNodal(VecDIMi::Zero(),ret._minC);
  getPtNodal(_nrPointNodalWithWarp-VecDIMi::Ones(),ret._maxC);
  return ret;
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::sampleStencilNodal(VecDIMd& x,VecDIMi& id,VecSTENCILd& weight,VecSTENCILi& indices,VecDIMb& ob) const
{
  VecDIMd x0,invCellSz;
  indexSafeNodal(x,id,ob);
  getPtNodal(id,x0);
  getInvCellSzNodal(id,invCellSz);
  sizeType off=id.dot(_strideNodal);
  VecDIMi strideNodalWithWarp;
  OperatorTraits<T,DIM,Encoder,DIM-1>::computeStrideWithWarp(id,_nrPointNodal,_strideNodal,_warpMode,strideNodalWithWarp);
  OperatorTraits<T,DIM,Encoder,DIM-1>::sampleStencil(x,x0,strideNodalWithWarp,invCellSz,off,weight,indices,1,0);
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
TDATA VariableSizedGrid<T,DIM,Encoder>::sampleNodal(const NodalData<TDATA>& data,VecDIMd& x,VecDIMi& id,T& minV,T& maxV,VecDIMb& ob) const
{
  VecDIMd x0,invCellSz;
  indexSafeNodal(x,id,ob);
  getPtNodal(id,x0);
  getInvCellSzNodal(id,invCellSz);
  sizeType off=id.dot(_strideNodal);
  minV=ScalarUtil<T>::scalar_max;
  maxV=-ScalarUtil<T>::scalar_max;
  VecDIMi strideNodalWithWarp;
  OperatorTraits<T,DIM,Encoder,DIM-1>::computeStrideWithWarp(id,_nrPointNodal,_strideNodal,_warpMode,strideNodalWithWarp);
  return OperatorTraits<T,DIM,Encoder,DIM-1>::sample(data._data,x,x0,strideNodalWithWarp,invCellSz,off,minV,maxV);
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
TDATA VariableSizedGrid<T,DIM,Encoder>::sampleNodal(const NodalData<TDATA>& data,VecDIMd& x,VecDIMi& id) const
{
  VecDIMb ob;
  VecDIMd x0,invCellSz;
  indexSafeNodal(x,id,ob);
  getPtNodal(id,x0);
  getInvCellSzNodal(id,invCellSz);
  sizeType off=id.dot(_strideNodal);
  VecDIMi strideNodalWithWarp;
  OperatorTraits<T,DIM,Encoder,DIM-1>::computeStrideWithWarp(id,_nrPointNodal,_strideNodal,_warpMode,strideNodalWithWarp);
  return OperatorTraits<T,DIM,Encoder,DIM-1>::sample(data._data,x,x0,strideNodalWithWarp,invCellSz,off);
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGrid<T,DIM,Encoder>::sampleNodal(unsigned char D,sizeType off,Eigen::Matrix<TDATA,-1,1>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const Eigen::Matrix<TDATA,-1,1>& refData) const
{
  if(D == DIM)
    data[off]=refData[0];
  else {
    unsigned char ob;
    T val,invCellSz,x0,alpha;
    Eigen::Matrix<TDATA,-1,1> refDataDIM;
    sizeType strideNodalWithWarp,id,offRef;
    sizeType s=_strideNodal[D],sRef=refGrid._strideNodal[D];
    sizeType nrP=_nrPointNodal[D],nrPRef=refGrid._nrPointNodal[D];
    const vector<T>& x0NodalsRef=refGrid._x0Nodals[D];
    const vector<T>& x0Nodals=_x0Nodals[D];
    for(sizeType i=0; i<nrP; i++,off+=s) {
      id=refGrid.indexSafeDIMNodal(val=x0Nodals[i],D,ob);
      invCellSz=refGrid._invCellSzNodals[D][id];
      x0=x0NodalsRef[id];
      offRef=id*sRef;
      strideNodalWithWarp=(_warpMode[D] > 0 && id == nrPRef-1) ? -(nrPRef-1)*sRef : sRef;
      alpha=(val-x0)*invCellSz;
      refDataDIM=interp1D<Eigen::Matrix<TDATA,-1,1>,T>(refData.segment(offRef,sRef),refData.segment(offRef+strideNodalWithWarp,sRef),alpha);
      sampleNodal(D+1,off,data,refGrid,refDataDIM);
    }
  }
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGrid<T,DIM,Encoder>::sampleNodal(NodalData<TDATA>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const NodalData<TDATA>& refData) const
{
  sampleNodal(0,0,data._data,refGrid,refData._data);
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGrid<T,DIM,Encoder>::sampleNodalBF(NodalData<TDATA>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const NodalData<TDATA>& refData) const
{
  VecDIMi id;
  VecDIMd pos;
  NodalIteratorPoint<T,DIM,Encoder> beg(*this,0);
  NodalIteratorPoint<T,DIM,Encoder> end(*this,-1);
  while(beg!=end) {
    data._data[beg.off()]=refGrid.sampleNodal(refData,pos=*beg,id);
    ++beg;
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::indexSafeNodal(VecDIMd& x,VecDIMi& id,VecDIMb& ob) const
{
  for(sizeType D=0; D<DIM; D++)
    id[D]=indexSafeDIMNodal(x[D],D,ob[D]);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::indexNodal(VecDIMd& x,VecDIMi& id) const
{
  for(sizeType D=0; D<DIM; D++)
    id[D]=indexDIMNodal(x[D],D);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::getInvCellSzNodal(const VecDIMi& id,VecDIMd& invCellSz) const
{
  OperatorTraits<T,DIM,Encoder,DIM-1>::getVal(_invCellSzNodals,id,invCellSz);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::getCellSzNodal(const VecDIMi& id,VecDIMd& cellSz) const
{
  OperatorTraits<T,DIM,Encoder,DIM-1>::getVal(_cellSzNodals,id,cellSz);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::getPtNodal(const VecDIMi& id,VecDIMd& pt) const
{
  OperatorTraits<T,DIM,Encoder,DIM-1>::getVal(_x0Nodals,id,pt);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::getPtNodal(sizeType off,VecDIMd& pt) const
{
  VecDIMi decoded;
  decodeNodal(off,decoded);
  OperatorTraits<T,DIM,Encoder,DIM-1>::getVal(_x0Nodals,decoded,pt);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::decodeNodal(sizeType id,VecDIMi& decoded) const
{
  OperatorTraits<T,DIM,Encoder,DIM-1>::decode(id,_strideNodal,decoded);
}
template <typename T,int DIM,typename Encoder>
int VariableSizedGrid<T,DIM,Encoder>::indexSafeDIMNodal(T& x,unsigned char D,unsigned char& ob) const
{
  int ret=indexDIMNodal(x,D);
  if(ret < 0) {
    ob=_warpMode[D] == 0; //if we are in warpMode, then there are no boundary
    x=_x0Nodals[D][0];
    return 0;
  } else if(ret > _nrPointNodalWithWarp[D]-2) {
    ob=_warpMode[D] == 0;
    x=_x0Nodals[D].back();
    return _nrPointNodalWithWarp[D]-2;
  } else {
    ob=false;
    return ret;
  }
}
template <typename T,int DIM,typename Encoder>
int VariableSizedGrid<T,DIM,Encoder>::indexDIMNodal(T& x,unsigned char D) const
{
  T bd;
  handleWarp(x,D);
  if(x < (bd=_x0Nodals[D][_offset._minC[D]]))
    return _offset._minC[D]-Encoder::indexDeltaDIM((bd-x)*_invCellSz0[D])-1;
  else if(x > (bd=_x0Nodals[D][_offset._maxC[D]]))
    return _offset._maxC[D]+Encoder::indexDeltaDIM((x-bd)*_invCellSz0[D]);
  else return _offset._minC[D]+int((x-(bd=_x0Nodals[D][_offset._minC[D]]))*_invCellSz0[D]);
}
//CellCenter
template <typename T,int DIM,typename Encoder>
BBox<T,DIM> VariableSizedGrid<T,DIM,Encoder>::getBBCellCenter() const
{
  BBox<T,DIM> ret;
  getPtCellCenter(VecDIMi::Zero(),ret._minC);
  getPtCellCenter(_nrPointCellCenterWithWarp-VecDIMi::Ones(),ret._maxC);
  return ret;
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::sampleStencilCellCenter(VecDIMd& x,VecDIMi& id,VecSTENCILd& weight,VecSTENCILi& indices,VecDIMb& ob) const
{
  VecDIMd x0,invCellSz;
  indexSafeCellCenter(x,id,ob);
  getPtCellCenter(id,x0);
  getInvCellSzCellCenter(id,invCellSz);
  sizeType off=id.dot(_strideCellCenter);
  VecDIMi strideCellCenterWithWarp;
  OperatorTraits<T,DIM,Encoder,DIM-1>::computeStrideWithWarp(id,_nrPointCellCenter,_strideCellCenter,_warpMode,strideCellCenterWithWarp);
  OperatorTraits<T,DIM,Encoder,DIM-1>::sampleStencil(x,x0,strideCellCenterWithWarp,invCellSz,off,weight,indices,1,0);
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
TDATA VariableSizedGrid<T,DIM,Encoder>::sampleCellCenter(const CellCenterData<TDATA>& data,VecDIMd& x,VecDIMi& id,T& minV,T& maxV,VecDIMb& ob) const
{
  VecDIMd x0,invCellSz;
  indexSafeCellCenter(x,id,ob);
  getPtCellCenter(id,x0);
  getInvCellSzCellCenter(id,invCellSz);
  sizeType off=id.dot(_strideCellCenter);
  minV=ScalarUtil<T>::scalar_max;
  maxV=-ScalarUtil<T>::scalar_max;
  VecDIMi strideCellCenterWithWarp;
  OperatorTraits<T,DIM,Encoder,DIM-1>::computeStrideWithWarp(id,_nrPointCellCenter,_strideCellCenter,_warpMode,strideCellCenterWithWarp);
  return OperatorTraits<T,DIM,Encoder,DIM-1>::sample(data._data,x,x0,strideCellCenterWithWarp,invCellSz,off,minV,maxV);
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
TDATA VariableSizedGrid<T,DIM,Encoder>::sampleCellCenter(const CellCenterData<TDATA>& data,VecDIMd& x,VecDIMi& id) const
{
  VecDIMb ob;
  VecDIMd x0,invCellSz;
  indexSafeCellCenter(x,id,ob);
  getPtCellCenter(id,x0);
  getInvCellSzCellCenter(id,invCellSz);
  sizeType off=id.dot(_strideCellCenter);
  VecDIMi strideCellCenterWithWarp;
  OperatorTraits<T,DIM,Encoder,DIM-1>::computeStrideWithWarp(id,_nrPointCellCenter,_strideCellCenter,_warpMode,strideCellCenterWithWarp);
  return OperatorTraits<T,DIM,Encoder,DIM-1>::sample(data._data,x,x0,strideCellCenterWithWarp,invCellSz,off);
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGrid<T,DIM,Encoder>::sampleCellCenter(unsigned char D,sizeType off,Eigen::Matrix<TDATA,-1,1>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const Eigen::Matrix<TDATA,-1,1>& refData) const
{
  if(D == DIM)
    data[off]=refData[0];
  else {
    unsigned char ob;
    T val,invCellSz,x0,alpha;
    Eigen::Matrix<TDATA,-1,1> refDataDIM;
    sizeType strideCellCenterWithWarp,id,offRef;
    sizeType s=_strideCellCenter[D],sRef=refGrid._strideCellCenter[D];
    sizeType nrP=_nrPointCellCenter[D],nrPRef=refGrid._nrPointCellCenter[D];
    const vector<T>& x0CellCentersRef=refGrid._x0CellCenters[D];
    const vector<T>& x0CellCenters=_x0CellCenters[D];
    for(sizeType i=0; i<nrP; i++,off+=s) {
      id=refGrid.indexSafeDIMCellCenter(val=x0CellCenters[i],D,ob);
      invCellSz=refGrid._invCellSzCellCenters[D][id];
      x0=x0CellCentersRef[id];
      offRef=id*sRef;
      strideCellCenterWithWarp=(_warpMode[D] > 0 && id == nrPRef-1) ? -(nrPRef-1)*sRef : sRef;
      alpha=(val-x0)*invCellSz;
      refDataDIM=interp1D<Eigen::Matrix<TDATA,-1,1>,T>(refData.segment(offRef,sRef),refData.segment(offRef+strideCellCenterWithWarp,sRef),alpha);
      sampleCellCenter(D+1,off,data,refGrid,refDataDIM);
    }
  }
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGrid<T,DIM,Encoder>::sampleCellCenter(CellCenterData<TDATA>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const CellCenterData<TDATA>& refData) const
{
  sampleCellCenter(0,0,data._data,refGrid,refData._data);
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGrid<T,DIM,Encoder>::sampleCellCenterBF(CellCenterData<TDATA>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const CellCenterData<TDATA>& refData) const
{
  VecDIMi id;
  VecDIMd pos;
  CellCenterIteratorPoint<T,DIM,Encoder> beg(*this,0);
  CellCenterIteratorPoint<T,DIM,Encoder> end(*this,-1);
  while(beg!=end) {
    data._data[beg.off()]=refGrid.sampleCellCenter(refData,pos=*beg,id);
    ++beg;
  }
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::indexSafeCellCenter(VecDIMd& x,VecDIMi& id,VecDIMb& ob) const
{
  for(sizeType D=0; D<DIM; D++)
    id[D]=indexSafeDIMCellCenter(x[D],D,ob[D]);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::indexCellCenter(VecDIMd& x,VecDIMi& id) const
{
  for(sizeType D=0; D<DIM; D++)
    id[D]=indexDIMCellCenter(x[D],D);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::getInvCellSzCellCenter(const VecDIMi& id,VecDIMd& invCellSz) const
{
  OperatorTraits<T,DIM,Encoder,DIM-1>::getVal(_invCellSzCellCenters,id,invCellSz);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::getCellSzCellCenter(const VecDIMi& id,VecDIMd& cellSz) const
{
  OperatorTraits<T,DIM,Encoder,DIM-1>::getVal(_cellSzCellCenters,id,cellSz);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::getPtCellCenter(const VecDIMi& id,VecDIMd& pt) const
{
  OperatorTraits<T,DIM,Encoder,DIM-1>::getVal(_x0CellCenters,id,pt);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::getPtCellCenter(sizeType off,VecDIMd& pt) const
{
  VecDIMi decoded;
  decodeCellCenter(off,decoded);
  OperatorTraits<T,DIM,Encoder,DIM-1>::getVal(_x0CellCenters,decoded,pt);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::decodeCellCenter(sizeType id,VecDIMi& decoded) const
{
  OperatorTraits<T,DIM,Encoder,DIM-1>::decode(id,_strideCellCenter,decoded);
}
template <typename T,int DIM,typename Encoder>
int VariableSizedGrid<T,DIM,Encoder>::indexSafeDIMCellCenter(T& x,unsigned char D,unsigned char& ob) const
{
  int ret=indexDIMCellCenter(x,D);
  if(ret < 0) {
    ob=_warpMode[D] == 0; //if we are in warpMode, then there are no boundary
    x=_x0CellCenters[D][0];
    return 0;
  } else if(ret > _nrPointCellCenterWithWarp[D]-2) {
    ob=_warpMode[D] == 0;
    if(_warpMode[D] == 0)
      x=_x0CellCenters[D].back();
    return _nrPointCellCenterWithWarp[D]-2;
  } else {
    ob=false;
    return ret;
  }
}
template <typename T,int DIM,typename Encoder>
int VariableSizedGrid<T,DIM,Encoder>::indexDIMCellCenter(T& x,unsigned char D) const
{
  int encoded,ret;
  T warpRange=handleWarp(x,D),bd,index,frac,ipart;
  //indexing
  if(x < (bd=_x0Nodals[D][_offset._minC[D]])) {
    index=(bd-x)*_invCellSz0[D];
    encoded=Encoder::indexDeltaDIM(index);
    if(index*2 > Encoder::decodeDeltaDIM(encoded)+Encoder::decodeDeltaDIM(encoded+1))
      ret=_offset._minC[D]-encoded-2;
    else ret=_offset._minC[D]-encoded-1;
  } else if(x > (bd=_x0Nodals[D][_offset._maxC[D]])) {
    index=(x-bd)*_invCellSz0[D];
    encoded=Encoder::indexDeltaDIM(index);
    if(index*2 > Encoder::decodeDeltaDIM(encoded)+Encoder::decodeDeltaDIM(encoded+1))
      ret=_offset._maxC[D]+encoded;
    else ret=_offset._maxC[D]+encoded-1;
  } else {
    bd=_x0Nodals[D][_offset._minC[D]];
    index=(x-bd)*_invCellSz0[D];
    frac=modf(index,&ipart);
    if(frac > 0.5f)
      ret=_offset._minC[D]+int(ipart);
    else ret=_offset._minC[D]+int(ipart)-1;
  }
  //handle warp: handle the first half cell, move it to the last
  if(_warpMode[D] > 0 && ret < 0) {
    x=x+warpRange;
    ret+=_nrPointCellCenter[D];
  }
  return ret;
}
//MACVel operator
template <typename T,int DIM,typename Encoder>
BBox<T,DIM> VariableSizedGrid<T,DIM,Encoder>::getBBMACVel(unsigned char D) const
{
  BBox<T,DIM> ret;
  getPtMACVel(D,VecDIMi::Zero(),ret._minC);
  getPtMACVel(D,_nrPointMACVelWithWarp[D]-VecDIMi::Ones(),ret._maxC);
  return ret;
}
template <typename T,int DIM,typename Encoder>
BBox<T,DIM> VariableSizedGrid<T,DIM,Encoder>::getCellMACVel(unsigned char D,const VecDIMi& id) const
{
  BBox<T,DIM> ret;
  for(sizeType d=0; d<DIM; d++)
    if(d == D) {
      if(id[d] == 0) {
        ret._minC[d]=_x0Nodals[d][id[d]];
        ret._maxC[d]=_x0CellCenters[d][id[d]];
        if(_warpMode[d] > 0)
          ret._minC[d]-=_cellSzNodals[d][_nrPointNodal[d]-1]/2;
      } else if(id[d] == _nrPointMACVel[d][d]-1 && _warpMode[d] == 0) {
        ret._minC[d]=_x0CellCenters[d][id[d]-1];
        ret._maxC[d]=_x0Nodals[d][id[d]];
      } else {
        ret._minC[d]=_x0CellCenters[d][id[d]-1];
        ret._maxC[d]=_x0CellCenters[d][id[d]];
      }
    } else {
      ret._minC[d]=_x0Nodals[d][id[d]];
      ret._maxC[d]=_x0Nodals[d][id[d]+1];
    }
  return ret;
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::sampleStencilMACVel(unsigned char D,VecDIMd& x,VecDIMi& id,VecSTENCILd& weight,VecSTENCILi& indices,VecDIMb& ob) const
{
  VecDIMd x0,invCellSz;
  indexSafeMACVel(D,x,id,ob);
  getPtMACVel(D,id,x0);
  getInvCellSzMACVel(D,id,invCellSz);
  sizeType off=id.dot(_strideMACVel[D]);
  VecDIMi strideMACVelWithWarp;
  OperatorTraits<T,DIM,Encoder,DIM-1>::computeStrideWithWarp(id,_nrPointMACVel[D],_strideMACVel[D],_warpMode,strideMACVelWithWarp);
  OperatorTraits<T,DIM,Encoder,DIM-1>::sampleStencil(x,x0,strideMACVelWithWarp,invCellSz,off,weight,indices,1,0);
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
TDATA VariableSizedGrid<T,DIM,Encoder>::sampleMACVel(unsigned char D,const MACVelData<DIM,TDATA>& data,VecDIMd& x,VecDIMi& id,T& minV,T& maxV,VecDIMb& ob) const
{
  VecDIMd x0,invCellSz;
  indexSafeMACVel(D,x,id,ob);
  getPtMACVel(D,id,x0);
  getInvCellSzMACVel(D,id,invCellSz);
  sizeType off=id.dot(_strideMACVel[D]);
  minV=ScalarUtil<T>::scalar_max;
  maxV=-ScalarUtil<T>::scalar_max;
  VecDIMi strideMACVelWithWarp;
  OperatorTraits<T,DIM,Encoder,DIM-1>::computeStrideWithWarp(id,_nrPointMACVel[D],_strideMACVel[D],_warpMode,strideMACVelWithWarp);
  return OperatorTraits<T,DIM,Encoder,DIM-1>::sample(data._data[D],x,x0,strideMACVelWithWarp,invCellSz,off,minV,maxV);
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
TDATA VariableSizedGrid<T,DIM,Encoder>::sampleMACVel(unsigned char D,const MACVelData<DIM,TDATA>& data,VecDIMd& x,VecDIMi& id) const
{
  VecDIMb ob;
  VecDIMd x0,invCellSz;
  indexSafeMACVel(D,x,id,ob);
  getPtMACVel(D,id,x0);
  getInvCellSzMACVel(D,id,invCellSz);
  sizeType off=id.dot(_strideMACVel[D]);
  VecDIMi strideMACVelWithWarp;
  OperatorTraits<T,DIM,Encoder,DIM-1>::computeStrideWithWarp(id,_nrPointMACVel[D],_strideMACVel[D],_warpMode,strideMACVelWithWarp);
  return OperatorTraits<T,DIM,Encoder,DIM-1>::sample(data._data[D],x,x0,strideMACVelWithWarp,invCellSz,off);
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGrid<T,DIM,Encoder>::sampleMACVel(unsigned char D,unsigned char D2,sizeType off,Eigen::Matrix<TDATA,-1,1>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const Eigen::Matrix<TDATA,-1,1>& refData) const
{
  if(D2 == DIM)
    data[off]=refData[0];
  else {
    unsigned char ob;
    T val,invCellSz,x0,alpha;
    Eigen::Matrix<TDATA,-1,1> refDataDIM;
    sizeType strideMACVelWithWarp,id,offRef;
    sizeType s=_strideMACVel[D][D2],sRef=refGrid._strideMACVel[D][D2];
    sizeType nrP=_nrPointMACVel[D][D2],nrPRef=refGrid._nrPointMACVel[D][D2];
    const vector<T>& x0MACVelsRef=D == D2 ? refGrid._x0Nodals[D2] : refGrid._x0CellCenters[D2];
    const vector<T>& x0MACVels=D == D2 ? _x0Nodals[D2] : _x0CellCenters[D2];
    for(sizeType i=0; i<nrP; i++,off+=s) {
      id=refGrid.indexSafeDIMMACVel(D,val=x0MACVels[i],D2,ob);
      invCellSz=D == D2 ? refGrid._invCellSzNodals[D2][id] : refGrid._invCellSzCellCenters[D2][id];
      x0=x0MACVelsRef[id];
      offRef=id*sRef;
      strideMACVelWithWarp=(_warpMode[D2] > 0 && id == nrPRef-1) ? -(nrPRef-1)*sRef : sRef;
      alpha=(val-x0)*invCellSz;
      refDataDIM=interp1D<Eigen::Matrix<TDATA,-1,1>,T>(refData.segment(offRef,sRef),refData.segment(offRef+strideMACVelWithWarp,sRef),alpha);
      sampleMACVel(D,D2+1,off,data,refGrid,refDataDIM);
    }
  }
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGrid<T,DIM,Encoder>::sampleMACVel(unsigned char D,MACVelData<DIM,TDATA>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const MACVelData<DIM,TDATA>& refData) const
{
  sampleMACVel(D,0,0,data._data[D],refGrid,refData._data[D]);
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGrid<T,DIM,Encoder>::sampleMACVel(MACVelData<DIM,TDATA>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const MACVelData<DIM,TDATA>& refData) const
{
  for(unsigned char D=0; D<DIM; D++)
    sampleMACVel(D,data,refGrid,refData);
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGrid<T,DIM,Encoder>::sampleMACVelBF(MACVelData<DIM,TDATA>& data,const VariableSizedGrid<T,DIM,Encoder>& refGrid,const MACVelData<DIM,TDATA>& refData) const
{
  for(unsigned char D=0; D<DIM; D++) {
    VecDIMi id;
    VecDIMd pos;
    MACVelIteratorPoint<T,DIM,Encoder> beg(D,*this,0);
    MACVelIteratorPoint<T,DIM,Encoder> end(D,*this,-1);
    while(beg!=end) {
      data._data[D][beg.off()]=refGrid.sampleMACVel(D,refData,pos=*beg,id);
      ++beg;
    }
  }
}
template <typename T,int DIM,typename Encoder>
template <typename TDATA>
void VariableSizedGrid<T,DIM,Encoder>::sampleMACVel(const MACVelData<DIM,TDATA>& data,VecDIMd& x,VecDIMi& id,VecDIMd& vel) const
{
  OperatorTraits<T,DIM,Encoder,DIM-1>::sampleVel(*this,data,x,id,vel);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::indexSafeMACVel(unsigned char D,VecDIMd& x,VecDIMi& id,VecDIMb& ob) const
{
  for(sizeType D2=0; D2<DIM; D2++)
    id[D2]=indexSafeDIMMACVel(D,x[D2],D2,ob[D2]);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::indexMACVel(unsigned char D,VecDIMd& x,VecDIMi& id) const
{
  for(sizeType D2=0; D2<DIM; D2++)
    id[D2]=indexDIMMACVel(D,x[D2],D2);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::getInvCellSzMACVel(unsigned char D,const VecDIMi& id,VecDIMd& invCellSz) const
{
  OperatorTraits<T,DIM,Encoder,DIM-1>::getValBut(D,_invCellSzNodals,_invCellSzCellCenters,id,invCellSz);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::getCellSzMACVel(unsigned char D,const VecDIMi& id,VecDIMd& cellSz) const
{
  OperatorTraits<T,DIM,Encoder,DIM-1>::getValBut(D,_cellSzNodals,_cellSzCellCenters,id,cellSz);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::getCellSzCtrMACVel(unsigned char D,const VecDIMi& id,VecDIMd& cellSz) const
{
  if(id[D] == 0) {
    getCellSzNodal(id,cellSz);
    if(_warpMode[D] > 0)
      cellSz[D]+=_cellSzNodals[D][_nrPointNodal[D]-1];
    cellSz[D]/=2;
  } else if(id[D] == _nrPointMACVel[D][D]-1 && _warpMode[D] == 0) {
    getCellSzNodal(id-VecDIMi::Unit(D),cellSz);
    cellSz[D]/=2;
  } else OperatorTraits<T,DIM,Encoder,DIM-1>::getValBut(D,_cellSzCellCenters,_cellSzNodals,id-VecDIMi::Unit(D),cellSz);
}
template <typename T,int DIM,typename Encoder>
T VariableSizedGrid<T,DIM,Encoder>::getFaceAreaMACVel(unsigned char D,const VecDIMi& id) const
{
  VecDIMd cellSz;
  getCellSzCtrMACVel(D,id,cellSz);
  cellSz[D]=1;
  return cellSz.prod();
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::getPtMACVel(unsigned char D,const VecDIMi& id,VecDIMd& pt) const
{
  OperatorTraits<T,DIM,Encoder,DIM-1>::getValBut(D,_x0Nodals,_x0CellCenters,id,pt);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::getPtMACVel(unsigned char D,sizeType off,VecDIMd& pt) const
{
  VecDIMi decoded;
  decodeMACVel(D,off,decoded);
  OperatorTraits<T,DIM,Encoder,DIM-1>::getValBut(D,_x0Nodals,_x0CellCenters,decoded,pt);
}
template <typename T,int DIM,typename Encoder>
void VariableSizedGrid<T,DIM,Encoder>::decodeMACVel(unsigned char D,sizeType id,VecDIMi& decoded) const
{
  OperatorTraits<T,DIM,Encoder,DIM-1>::decode(id,_strideMACVel[D],decoded);
}
template <typename T,int DIM,typename Encoder>
int VariableSizedGrid<T,DIM,Encoder>::indexSafeDIMMACVel(unsigned char D,T& x,unsigned char D0,unsigned char& ob) const
{
  if(D == D0)
    return indexSafeDIMNodal(x,D0,ob);
  else return indexSafeDIMCellCenter(x,D0,ob);
}
template <typename T,int DIM,typename Encoder>
int VariableSizedGrid<T,DIM,Encoder>::indexDIMMACVel(unsigned char D,T& x,unsigned char D0) const
{
  if(D == D0)
    return indexDIMNodal(x,D0);
  else return indexDIMCellCenter(x,D0);
}
template <typename T,int DIM,typename Encoder>
int VariableSizedGrid<T,DIM,Encoder>::getNeighMACVel(unsigned char D,const VecDIMi& id,sizeType off,VecSTENCILi& neigh) const
{
  int nr=0;
  OperatorTraits<T,DIM,Encoder,DIM-1>::getNeigh(_nrPointMACVel[D],_strideMACVel[D],_warpMode,id,off,neigh,nr);
  return nr;
}
#include "VariableSizedGridDebug.cpp"
//interp
#define INTERP_OP(DIM,TYPE,SPARSITY,TDATA) \
template TDATA VariableSizedGrid<scalar,DIM,Encoder##TYPE<SPARSITY> >::sampleNodal<TDATA>(const NodalData<TDATA>& data,VecDIMd& x,VecDIMi& id,scalar& minV,scalar& maxV,VecDIMb& ob) const;  \
template TDATA VariableSizedGrid<scalar,DIM,Encoder##TYPE<SPARSITY> >::sampleNodal<TDATA>(const NodalData<TDATA>& data,VecDIMd& x,VecDIMi& id) const;  \
template void VariableSizedGrid<scalar,DIM,Encoder##TYPE<SPARSITY> >::sampleNodal<TDATA>(NodalData<TDATA>& data,const VariableSizedGrid<scalar,DIM,Encoder##TYPE<SPARSITY> >& refGrid,const NodalData<TDATA>& refData) const;  \
template TDATA VariableSizedGrid<scalar,DIM,Encoder##TYPE<SPARSITY> >::sampleCellCenter<TDATA>(const CellCenterData<TDATA>& data,VecDIMd& x,VecDIMi& id,scalar& minV,scalar& maxV,VecDIMb& ob) const;  \
template TDATA VariableSizedGrid<scalar,DIM,Encoder##TYPE<SPARSITY> >::sampleCellCenter<TDATA>(const CellCenterData<TDATA>& data,VecDIMd& x,VecDIMi& id) const;  \
template void VariableSizedGrid<scalar,DIM,Encoder##TYPE<SPARSITY> >::sampleCellCenter<TDATA>(CellCenterData<TDATA>& data,const VariableSizedGrid<scalar,DIM,Encoder##TYPE<SPARSITY> >& refGrid,const CellCenterData<TDATA>& refData) const; \
template TDATA VariableSizedGrid<scalar,DIM,Encoder##TYPE<SPARSITY> >::sampleMACVel<TDATA>(unsigned char D,const MACVelData<DIM,TDATA>& data,VecDIMd& x,VecDIMi& id,scalar& minV,scalar& maxV,VecDIMb& ob) const;  \
template TDATA VariableSizedGrid<scalar,DIM,Encoder##TYPE<SPARSITY> >::sampleMACVel<TDATA>(unsigned char D,const MACVelData<DIM,TDATA>& data,VecDIMd& x,VecDIMi& id) const;  \
template void VariableSizedGrid<scalar,DIM,Encoder##TYPE<SPARSITY> >::sampleMACVel<TDATA>(MACVelData<DIM,TDATA>& data,const VariableSizedGrid<scalar,DIM,Encoder##TYPE<SPARSITY> >& refGrid,const MACVelData<DIM,TDATA>& refData) const; \
template void VariableSizedGrid<scalar,DIM,Encoder##TYPE<SPARSITY> >::sampleMACVel<TDATA>(const MACVelData<DIM,TDATA>& data,VecDIMd& x,VecDIMi& id,VecDIMd& vel) const; \
//instance
#define INSTANCE(DIM,TYPE,SPARSITY) \
template class VariableSizedGrid<scalar,DIM,Encoder##TYPE<SPARSITY> >;  \
INTERP_OP(DIM,TYPE,SPARSITY,scalarF)  \
INTERP_OP(DIM,TYPE,SPARSITY,scalarD)  \
INTERP_OP(DIM,TYPE,SPARSITY,sizeType) \
INTERP_OP(DIM,TYPE,SPARSITY,unsigned char)
INSTANCE_ALL
//instance2
#define INSTANCE2(DIM,TYPE,SPARSITY,DIM2) \
template class OperatorTraits<scalar,DIM,Encoder##TYPE<SPARSITY>,DIM2 >;
INSTANCE2_ALL
