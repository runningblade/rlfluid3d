#ifndef VARIABLE_SIZED_PARTICLE_OP_H
#define VARIABLE_SIZED_PARTICLE_OP_H

#include "NodalData.h"
#include "MACVelData.h"
#include "CellCenterData.h"
#include "VariableSizedGrid.h"
#include <CommonFile/GridBasic.h>
#include <CommonFile/ParticleSet.h>

PRJ_BEGIN

template <typename T,int DIM,typename Encoder>
struct VariableSizedParticleOp : public TypeTraits<T,DIM> {
  using typename TypeTraits<T,DIM>::VecDIMd;
  using typename TypeTraits<T,DIM>::VecDIMb;
  using typename TypeTraits<T,DIM>::VecDIMi;
  using typename TypeTraits<T,DIM>::VecSTENCILd;
  using typename TypeTraits<T,DIM>::VecSTENCILi;
  typedef NodalIteratorPoint<T,DIM,Encoder> NodalPointIt;
  typedef NodalIteratorPointIndex<T,DIM,Encoder> NodalPointIndexIt;
  typedef NodalIteratorCell<T,DIM,Encoder> NodalCellIt;
  typedef CellCenterIteratorPoint<T,DIM,Encoder> CellCenterPointIt;
  typedef CellCenterIteratorPointIndex<T,DIM,Encoder> CellCenterPointIndexIt;
  typedef CellCenterIteratorCell<T,DIM,Encoder> CellCenterCellIt;
  typedef MACVelIteratorPoint<T,DIM,Encoder> MACVelPointIt;
  typedef MACVelIteratorPointIndex<T,DIM,Encoder> MACVelPointIndexIt;
  typedef MACVelIteratorCell<T,DIM,Encoder> MACVelCellIt;
  typedef VariableSizedGrid<T,DIM,Encoder> GridType;
  static void compactParticle(const GridType& grd,const ScalarField& nodalSolidPhi,ParticleSetN& pset,T radius);
  static void projectToGrid(const ParticleSetN& pset,const GridType& grd,MACVelData<DIM,T>& vel,MACVelData<DIM,T>& weight,MACVelData<DIM,unsigned char>& valid);
  static void projectToParticle(ParticleSetN& pset,const GridType& grd,MACVelData<DIM,T>& velOld,MACVelData<DIM,T>& velNew,T PICWeight);
  static void addExternalForce(ParticleSetN& pset,T g,T dt);
  static void clamp(ParticleSetN& pset,T maxVel);
  template <typename TDATA>
  static TDATA sample(const Eigen::Matrix<TDATA,-1,1>& data,const VecSTENCILd& weights,const VecSTENCILi& indices);
};

PRJ_END

#endif
