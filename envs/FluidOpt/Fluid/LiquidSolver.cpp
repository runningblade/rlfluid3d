#include "Utils.h"
#include "LiquidSolver.h"
#include "ScriptedSource.h"
#include "VariableSizedGridOp.h"
#include "VariableSizedGridOpProject.h"
#include "VariableSizedParticleOp.h"
#include <CommonFile/ParticleToGrid.h>
#include <boost/lexical_cast.hpp>
#include <CommonFile/IO.h>

USE_PRJ_NAMESPACE

//#define USE_MINRES
//LiquidSolverPIC
template <int DIM,typename Encoder>
LiquidSolverPIC<DIM,Encoder>::LiquidSolverPIC()
  :Serializable(typeid(LiquidSolverPIC<DIM,Encoder>).name())
{
  _globalId=-1;
  _globalTime=-1;
}
template <int DIM,typename Encoder>
LiquidSolverPIC<DIM,Encoder>::~LiquidSolverPIC() {}
template <int DIM,typename Encoder>
void LiquidSolverPIC<DIM,Encoder>::registerSource(IOData* dat)
{
  registerType<ImplicitRect>(dat);
  registerType<ImplicitRectNeg>(dat);
  registerType<ImplicitCylinder>(dat);
  registerType<ImplicitSphere>(dat);

  registerType<SourceRegion>(dat);
  registerType<RandomSource>(dat);
  registerType<TokenRingSource>(dat);
  registerType<VariableTimeTokenRingSource>(dat);
}
template <int DIM,typename Encoder>
bool LiquidSolverPIC<DIM,Encoder>::read(istream& is,IOData* dat)
{
  registerType<RigidSolver>(dat);
  registerType<SpringForce>(dat);
  registerSource(dat);
  _nodalSolidPhi.read(is,dat);
  _grd.read(is,dat);
  _grd0.read(is,dat);
  _phi.read(is,dat);
  _vel.read(is,dat);
  readBinaryData(_rigid,is,dat);
  _pSet.read(is,dat);
  readVector(_sources,is,dat);
  readBinaryData(_spring,is,dat);
  //param
  readBinaryData(_density,is);
  readBinaryData(_narrowBand,is);
  readBinaryData(_radius,is);
  readBinaryData(_springRad,is);
  readBinaryData(_nrPass,is);
  readBinaryData(_nrParticlePerGrid,is);
  //timing
  readBinaryData(_globalTime,is);
  readBinaryData(_globalId,is);
  readBinaryData(_maxSubstep,is);
  readBinaryData(_maxCFL,is);
  readBinaryData(_useUnilateral,is);
  return is.good();
}
template <int DIM,typename Encoder>
bool LiquidSolverPIC<DIM,Encoder>::write(ostream& os,IOData* dat) const
{
  registerType<RigidSolver>(dat);
  registerType<SpringForce>(dat);
  registerSource(dat);
  _nodalSolidPhi.write(os,dat);
  _grd.write(os,dat);
  _grd0.write(os,dat);
  _phi.write(os,dat);
  _vel.write(os,dat);
  writeBinaryData(_rigid,os,dat);
  _pSet.write(os,dat);
  writeVector(_sources,os,dat);
  writeBinaryData(_spring,os,dat);
  //param
  writeBinaryData(_density,os);
  writeBinaryData(_narrowBand,os);
  writeBinaryData(_radius,os);
  writeBinaryData(_springRad,os);
  writeBinaryData(_nrPass,os);
  writeBinaryData(_nrParticlePerGrid,os);
  //timing
  writeBinaryData(_globalTime,os);
  writeBinaryData(_globalId,os);
  writeBinaryData(_maxSubstep,os);
  writeBinaryData(_maxCFL,os);
  writeBinaryData(_useUnilateral,os);
  return os.good();
}
template <int DIM,typename Encoder>
boost::shared_ptr<Serializable> LiquidSolverPIC<DIM,Encoder>::copy() const
{
  return boost::shared_ptr<Serializable>(new LiquidSolverPIC<DIM,Encoder>);
}
//timing
template <int DIM,typename Encoder>
scalar LiquidSolverPIC<DIM,Encoder>::getGlobalTime()
{
  return _globalTime;
}
template <int DIM,typename Encoder>
sizeType LiquidSolverPIC<DIM,Encoder>::getGlobalId()
{
  return _globalId;
}
template <int DIM,typename Encoder>
void LiquidSolverPIC<DIM,Encoder>::setGlobalTime(const scalar& globalTime)
{
  _globalTime=globalTime;
}
template <int DIM,typename Encoder>
void LiquidSolverPIC<DIM,Encoder>::setGlobalId(const sizeType& globalId)
{
  _globalId=globalId;
}
template <int DIM,typename Encoder>
void LiquidSolverPIC<DIM,Encoder>::advanceTime(const scalar& dt)
{
  if(_globalTime < 0.0f) {
    NOTIFY_MSG("You Forgot to Set Global Time!")
  }
  if(_globalId < 0) {
    NOTIFY_MSG("You Forgot to Set Global Id!")
  }

  _globalTime+=dt;
  _globalId++;
}
template <int DIM,typename Encoder>
void LiquidSolverPIC<DIM,Encoder>::setCFLMode(const scalar& maxCFL,const scalar& maxSubstep)
{
  _maxCFL=maxCFL;
  _maxSubstep=maxSubstep;
  _nrPass=(sizeType)ceil(maxCFL+(scalar)1.0f);
  // INFOV("Your Max Substep: %f, Your Max Velocity: %f, Your Extrapolation Pass: %d",_maxSubstep,getMaxVelocity(),_nrPass)
}
template <int DIM,typename Encoder>
scalar LiquidSolverPIC<DIM,Encoder>::getSubstep(scalar& t,const scalar& dt) const
{
  scalar substep=VariableSizedGridOp<scalar,DIM,Encoder>::checkCFL(_grd,_vel)*_maxCFL;
  if(substep < 1.0f/_maxSubstep)
    substep=1.0f/_maxSubstep;
  if(t+substep > dt)
    substep=dt-t;
  t+=substep;
  return substep;
}
template <int DIM,typename Encoder>
scalar LiquidSolverPIC<DIM,Encoder>::getMaxVelocity() const
{
  const VecDIMd cellSz=_grd._cellSz0;
  scalar minCellSz=cellSz[0];
  for(sizeType i=1; i<DIM; i++)
    minCellSz=min<scalar>(minCellSz,cellSz[i]);
  return minCellSz*_maxCFL*_maxSubstep;
}
//setup
template <int DIM,typename Encoder>
void LiquidSolverPIC<DIM,Encoder>::computeRadius()
{
  scalar v=_grd._cellSz0.prod()/(1<<DIM);
  if(DIM == 3)
    _springRad=pow((scalar)(v/(4.0f*M_PI/3.0f)),(scalar)(1.0f/DIM));
  else _springRad=pow((scalar)(v/M_PI),(scalar)(1.0f/DIM));
  _springRad*=2.0f;
}
template <int DIM,typename Encoder>
void LiquidSolverPIC<DIM,Encoder>::initSolid(boost::shared_ptr<RigidSolver> sol)
{
  typedef VariableSizedGrid<scalar,DIM,Encoder> GridType;
  typedef typename GridType::VecDIMi VecDIMi;
  if(!sol) {
    ASSERT(_rigid)
  } else _rigid=sol;

  //account for warp
  _rigid->setWarpMode(assignVariableTo<Vec3i,VecDIMi>(_grd0._warpMode));
  BBox<scalar,DIM> bbLiquid=_grd0.getBBNodal();
  BBox<scalar> bb=_rigid->getBB();
  for(sizeType D=0; D<DIM; D++)
    if(_grd0._warpMode[D] > 0) {
      bb._minC[D]=bbLiquid._minC[D];
      bb._maxC[D]=bbLiquid._maxC[D];
    }
  _rigid->setBB(bb);

  RigidSolverImplicitFunc f(*_rigid,true);
  GridOp<scalar,scalar>::copyFromImplictFunc(_nodalSolidPhi,f);
  //scalar thres=_nodalSolidPhi.getBB().getExtent().norm()/2;
  //GridOp<scalar,scalar>::solveEikonalPDE(_nodalSolidPhi,thres,true);
  GridOp<scalar,scalar>::reinitialize(_nodalSolidPhi);

  computeRadius();
  _spring->resetSolid(_nodalSolidPhi,_springRad,_nrParticlePerGrid);
  //GridOp<scalar,scalar>::write2DScalarGridVTK("solidPhi.vtk",_nodalSolidPhi,true);
  //_spring->getSolidPSet().writeVTK("solidPSet.vtk");
  //exit(-1);
}
template <int DIM,typename Encoder>
void LiquidSolverPIC<DIM,Encoder>::initLiquid(const ImplicitFunc<scalar>& l,const scalar& sparsity)
{
  Vec3 cellSz=_nodalSolidPhi.getCellSize()*sparsity;
  if(DIM == 2)
    cellSz.z()=1.0f;
  Vec3i nrCell=ceilV((Vec3)(_nodalSolidPhi.getBB().getExtent().array()/cellSz.array()).matrix());
  if(DIM == 2)
    nrCell.z()=1;

  ParticleSetN::ParticleType p;
  _pSet.clear();
  sizeType seed=0;
  //OMP_PARALLEL_FOR_
  for(sizeType x=0; x<nrCell.x(); x++) {
    for(sizeType y=0; y<nrCell.y(); y++)
      for(sizeType z=0; z<nrCell.z(); z++) {
        Vec3 pt=_nodalSolidPhi.getBB()._minC+Vec3((scalar)(x+0.5f)*cellSz.x(),(scalar)(y+0.5f)*cellSz.y(),(scalar)(z+0.5f)*cellSz.z());
        if(_nodalSolidPhi.sampleSafe(pt) >= 0) {
          for(sizeType nr=0; nr<_nrParticlePerGrid; nr++) {
            p._pos=pt-cellSz*0.5f;
            p._pos.x()+=cellSz.x()*RandEngine::randSR(seed++,0,1);
            p._pos.y()+=cellSz.y()*RandEngine::randSR(seed++,0,1);
            p._pos.z()+=cellSz.z()*RandEngine::randSR(seed++,0,1);
            if(DIM == 2)
              p._pos.z()=0.0f;
            p._vel=Vec3::Zero();
            //if(l(p._pos) <= 0 && _nodalSolidPhi.sampleSafe(p._pos) >= 0.0f)
            if(l(p._pos) <= -_radius && _nodalSolidPhi.sampleSafe(p._pos) >= 0.0f)
              _pSet.addParticle(p);
          }
        }
      }
  }
}
template <int DIM,typename Encoder>
void LiquidSolverPIC<DIM,Encoder>::initDomain(BBox<scalar> bb,const Vec3& cellSz,const Vec3i* warpMode)
{
  const Vec3i nrCell=ceilV(Vec3((bb.getExtent().array()/cellSz.array()).matrix()));
  const Vec3 ext=(nrCell.template cast<scalar>().array()*cellSz.array()).matrix();
  bb=BBox<scalar>(bb._minC,bb._minC+ext);
  _nodalSolidPhi.reset(nrCell,bb,0,false);
  _grd.reset(bb,nrCell,BBox<sizeType>(Vec3i::Zero(),Vec3i::Zero()),0,warpMode);
  _grd0=_grd;
  _vel.reset(_grd);
  _phi.reset(_grd);

  _density=1000;
  _useUnilateral=false;
  _narrowBand=_grd._cellSz0.maxCoeff()*3.0f;
  _radius=_grd._cellSz0.norm()/2.0f;
  _nrPass=(sizeType)ceil((scalar)(ext.maxCoeff()*0.1f/cellSz.maxCoeff()));
  _nrPass=min<sizeType>(10,_nrPass);
  // INFOV("Nr Extra Iter: %d",_nrPass)
  _nrParticlePerGrid=1;
  setCFLMode(3.0f,200.0f);
  _spring.reset(new SpringForce);
}
template <int DIM,typename Encoder>
void LiquidSolverPIC<DIM,Encoder>::focusOn(const BBox<scalar>& region)
{
  BBox<scalar,DIM> regionDIM;
  regionDIM._minC=assignVariableTo<VecDIMd,Vec3>(region._minC);
  regionDIM._maxC=assignVariableTo<VecDIMd,Vec3>(region._maxC);
  VariableSizedGrid<scalar,DIM,Encoder> grd;
  grd.reset(_grd0,regionDIM,2);
  if((grd._offset.getExtent().array() > 2).all()) {
    //phi
    CellCenterData<scalar> phiNew(grd);
    grd.sampleCellCenter(phiNew,_grd,_phi);
    _phi=phiNew;
    //vel
    MACVelData<DIM,scalar> velNew(grd);
    grd.sampleMACVel(velNew,_grd,_vel);
    _vel=velNew;
    //grd
    _grd=grd;
  } else {
    WARNING("Focus region too small!")
  }
}
template <int DIM,typename Encoder>
void LiquidSolverPIC<DIM,Encoder>::focusOn(sizeType bodyId,scalar enlargedEps)
{
  BBox<scalar> bb;
  if(bodyId < _rigid->nrBody()) {
    _rigid->getBody(bodyId).getWorldBB(bb);
    bb.enlargedEps(enlargedEps);
    focusOn(bb);
  }
}
template <int DIM,typename Encoder>
void LiquidSolverPIC<DIM,Encoder>::getFeature(VectorField& field,scalar sz)
{
  if(_rigid->nrBody() < 1) {
    INFO("Cannot call getFeature without RigidBodies!")
    exit(EXIT_FAILURE);
  }

  Vec3i id;
  BBox<scalar> bb;
  ScalarField weight;
  sizeType nrStencil=1<<DIM;

  if(sz >= 0) {
    _rigid->getBody(0).getWorldBB(bb);
    bb.enlargedEps(sz);
  } else {
    bb=_nodalSolidPhi.getBB();
  }
  VecDIMi nrCell=ceilV(VecDIMd((bb.getExtent().template segment<DIM>(0).array()/_grd._cellSz0.array()).matrix()));
  field.reset(assignVariableTo<Vec3i,VecDIMi>(nrCell),bb,Vec3::Zero(),true,1);
  weight.makeSameGeometry(field);
  weight.init(0);

  static const Vec3i offs[8]= {
    Vec3i(0,0,0),
    Vec3i(1,0,0),
    Vec3i(0,1,0),
    Vec3i(1,1,0),

    Vec3i(0,0,1),
    Vec3i(1,0,1),
    Vec3i(0,1,1),
    Vec3i(1,1,1),
  };
  for(sizeType i=0; i<_pSet.size(); i++)
    if(bb.contain(_pSet[i]._pos,(sizeType)DIM)) {
      id=floorV(field.getIndexFracSafe(_pSet[i]._pos));
      for(unsigned char off=0; off<nrStencil; off++) {
        weight.get(id+offs[off])+=1;
        field.get(id+offs[off])+=_pSet[i]._vel;
      }
    }
  for(sizeType i=0; i<(sizeType)weight.data().size(); i++)
    if(weight.data()[i] > 0)
      field.dataRef()[i]/=weight.data()[i];
}
template <int DIM,typename Encoder>
void LiquidSolverPIC<DIM,Encoder>::setUseUnilateral(bool unilateral)
{
  _useUnilateral=unilateral;
}
//getter
template <int DIM,typename Encoder>
BBox<scalar> LiquidSolverPIC<DIM,Encoder>::getBB() const
{
  return assignVariableTo<scalar,3,scalar,DIM>(_grd.getBBNodal());
}
template <int DIM,typename Encoder>
scalar LiquidSolverPIC<DIM,Encoder>::getRadius() const
{
  return _radius;
}
template <int DIM,typename Encoder>
Vec3i LiquidSolverPIC<DIM,Encoder>::getNrCell() const
{
  return assignVariableToOnes<Vec3i,VecDIMi>(_grd._nrPointNodal);
}
template <int DIM,typename Encoder>
const VariableSizedGrid<scalar,DIM,Encoder>& LiquidSolverPIC<DIM,Encoder>::getGrid() const
{
  return _grd;
}
template <int DIM,typename Encoder>
const VariableSizedGrid<scalar,DIM,Encoder>& LiquidSolverPIC<DIM,Encoder>::getGrid0() const
{
  return _grd0;
}
template <int DIM,typename Encoder>
const CellCenterData<scalar>& LiquidSolverPIC<DIM,Encoder>::getPhi() const
{
  return _phi;
}
template <int DIM,typename Encoder>
const MACVelData<DIM,scalar>& LiquidSolverPIC<DIM,Encoder>::getVel() const
{
  return _vel;
}
template <int DIM,typename Encoder>
const ScalarField& LiquidSolverPIC<DIM,Encoder>::getSolid() const
{
  return _nodalSolidPhi;
}
template <int DIM,typename Encoder>
const ParticleSetN& LiquidSolverPIC<DIM,Encoder>::getPSet() const
{
  return _pSet;
}
template <int DIM,typename Encoder>
const RigidSolver& LiquidSolverPIC<DIM,Encoder>::getRigid() const
{
  return *_rigid;
}
template <int DIM,typename Encoder>
RigidSolver& LiquidSolverPIC<DIM,Encoder>::getRigid()
{
  return *_rigid;
}
//solver
template <int DIM,typename Encoder>
sizeType LiquidSolverPIC<DIM,Encoder>::advance(const scalar& dt)
{
  _rigid->clearContactFlag();
  ASSERT_MSG(_rigid->nrBody() == 0,"PIC does not support rigid body!")
  CellCenterData<unsigned char> tag(_grd);
  MACVelData<DIM,unsigned char> valid(_grd);
  MACVelData<DIM,scalar> velTmp(_grd);
  sizeType index=0;
  scalar t=0;
  while(t < dt) {
    //compute timestep
    scalar substep=getSubstep(t,dt);
    if(_rigid && _rigid->nrBody() > 0)
      _rigid->advanceVelocity(substep,getGlobalTime()+t);
    computePSet();
    //add external force
    scalar g=-_rigid->getGravity().dot(Vec3::UnitY());
    VariableSizedGridOp<scalar,DIM,Encoder>::addExternalForce(_grd,_vel,NULL,g,0,0,substep);
    //add source region
    sampleSource(_spring->getPSetCD(),getGlobalTime()+t);
    //compute a valid and compatible velocityParticleSetN additionalPSet
    computePhi();
    VariableSizedGridOp<scalar,DIM,Encoder>::constraintMACVel(_grd,_nodalSolidPhi,_vel);
    //project
    makeDivergenceFree(substep,tag,valid);
    _vel.clamp(getMaxVelocity());
    //advect particle
    if(_rigid && _rigid->nrBody() > 0)
      _rigid->advancePosition(substep);
    VariableSizedGridOp<scalar,DIM,Encoder>::advectParticle(_grd,_vel,_pSet,substep);
    VariableSizedGridOp<scalar,DIM,Encoder>::advectSemiLagrangian(_grd,_vel,velTmp,_vel,substep);
    _vel=velTmp;
    index++;
  }
  advanceTime(dt);
  correct(dt);
  return index;
}
template <int DIM,typename Encoder>
bool LiquidSolverPIC<DIM,Encoder>::addSource(boost::shared_ptr<SourceRegion> s)
{
  vector<boost::shared_ptr<SourceRegion> >::iterator iter=find(_sources.begin(),_sources.end(),s);
  if(iter == _sources.end()) {
    _sources.push_back(s);
    return true;
  }
  return false;
}
template <int DIM,typename Encoder>
bool LiquidSolverPIC<DIM,Encoder>::removeSource(boost::shared_ptr<SourceRegion> s)
{
  vector<boost::shared_ptr<SourceRegion> >::iterator iter=find(_sources.begin(),_sources.end(),s);
  if(iter == _sources.end())
    return false;
  _sources.erase(iter);
  return true;
}
template <int DIM,typename Encoder>
void LiquidSolverPIC<DIM,Encoder>::sampleSource(CollisionInterface<scalar,ParticleSetN>& cd,const scalar& globalTime)
{
  cd.prepare(_pSet);
  typedef vector<boost::shared_ptr<SourceRegion> >::iterator iter;
  for(iter beg=_sources.begin(),end=_sources.end(); beg!=end; beg++) {
    (*beg)->setCellSz(assignVariableTo<Vec3,VecDIMd>(_grd._cellSz0));
    (*beg)->setNrParticlePerGrid(_nrParticlePerGrid);
    (*beg)->sampleRegion(_nodalSolidPhi,cd,_radius,_pSet,globalTime);
    (*beg)->sampleRegion(_grd,NULL,NULL,_vel,globalTime);
  }
}
//render
template <int DIM,typename Encoder>
const ObjMesh& LiquidSolverPIC<DIM,Encoder>::getSurfaceMesh(bool vol,scalar smoothRange)
{
  ParticleSetN tmp=_pSet;
  if(!_surface)
    _surface.reset(new ParticleSurface(_pSet));
  VariableSizedGridOp<scalar,DIM,Encoder>::warpParticle(_grd0,_pSet);
  _surface->generate(DIM,_grd._cellSz0.maxCoeff()/2,_grd._cellSz0.maxCoeff()/2*smoothRange,vol);
  _pSet=tmp;
  return _surface->getMesh();
}
//helper
template <int DIM,typename Encoder>
void LiquidSolverPIC<DIM,Encoder>::correct(scalar dt)
{
  ParticleSetN additionalPSet;
  Vec3i warpMode=assignVariableTo<Vec3i,VecDIMi>(_grd0._warpMode);
  VariableSizedGridOp<scalar,DIM,Encoder>::warpParticle(_grd,_pSet);
  VariableSizedParticleOp<scalar,DIM,Encoder>::compactParticle(_grd,_nodalSolidPhi,_pSet,_radius);
  if(_rigid && _rigid->nrBody() > 0) {
    _rigid->getPSet(additionalPSet);
    _spring->correct(_pSet,dt,&additionalPSet,&warpMode);
  } else _spring->correct(_pSet,dt,NULL,&warpMode);
}
template <int DIM,typename Encoder>
void LiquidSolverPIC<DIM,Encoder>::computePhi()
{
  VariableSizedGridOp<scalar,DIM,Encoder>::computePhiAdaptiveCellCenter(_grd,_phi,_pSet,_narrowBand,_radius);
}
template <int DIM,typename Encoder>
void LiquidSolverPIC<DIM,Encoder>::makeDivergenceFree(scalar dt,CellCenterData<unsigned char>& tag,MACVelData<DIM,unsigned char>& valid)
{
  //solve for pressure
  ASSERT(_rigid->nrBody() == 0)
  CellCenterData<scalarD> pressure(_grd);
  if(_useUnilateral)
    VariableSizedGridOpProject<scalar,DIM,Encoder>::projectUnilateral(_grd,pressure,_phi,_nodalSolidPhi,_vel,valid,tag,dt,_density);
  else VariableSizedGridOpProject<scalar,DIM,Encoder>::project(_grd,pressure,_phi,_nodalSolidPhi,_vel,valid,tag,dt,_density);
}
template <int DIM,typename Encoder>
void LiquidSolverPIC<DIM,Encoder>::projectToGrid(MACVelData<DIM,scalar>& velTmp,MACVelData<DIM,unsigned char>& valid)
{
  VariableSizedParticleOp<scalar,DIM,Encoder>::projectToGrid(_pSet,_grd,_vel,velTmp,valid);
  VariableSizedGridOp<scalar,DIM,Encoder>::extrapolateMACVel(_grd,valid,_vel,_nrPass);
}
template <int DIM,typename Encoder>
void LiquidSolverPIC<DIM,Encoder>::computePSet()
{
  _pSetAll=_pSet;
  _pSetRigid.clear();
  _rigid->getPSet(_pSetRigid);
  _pSetAll.append(_pSetRigid);
  _pSetAll.append(_spring->getSolidPSet());
}

//LiquidSolverFLIP
template <int DIM,typename Encoder>
LiquidSolverFLIP<DIM,Encoder>::LiquidSolverFLIP()
{
  Serializable::setType(typeid(LiquidSolverFLIP<DIM,Encoder>).name());
  _PICWeight=0.9f;
}
template <int DIM,typename Encoder>
bool LiquidSolverFLIP<DIM,Encoder>::read(istream& is,IOData* dat)
{
  LiquidSolverPIC<DIM,Encoder>::read(is,dat);
  readBinaryData(_PICWeight,is);
  readBinaryData(_useVariational,is);
  readBinaryData(_useBOTE,is);
  _weight.read(is,dat);
  return is.good();
}
template <int DIM,typename Encoder>
bool LiquidSolverFLIP<DIM,Encoder>::write(ostream& os,IOData* dat) const
{
  LiquidSolverPIC<DIM,Encoder>::write(os,dat);
  writeBinaryData(_PICWeight,os);
  writeBinaryData(_useVariational,os);
  writeBinaryData(_useBOTE,os);
  _weight.write(os,dat);
  return os.good();
}
template <int DIM,typename Encoder>
boost::shared_ptr<Serializable> LiquidSolverFLIP<DIM,Encoder>::copy() const
{
  return boost::shared_ptr<Serializable>(new LiquidSolverFLIP<DIM,Encoder>);
}
//setup
template <int DIM,typename Encoder>
void LiquidSolverFLIP<DIM,Encoder>::initSolid(boost::shared_ptr<RigidSolver> sol)
{
  LiquidSolverPIC<DIM,Encoder>::initSolid(sol);
  VariableSizedGridOpProject<scalar,DIM,Encoder>::sampleWeightMACVel(_grd,_weight,_nodalSolidPhi);
}
template <int DIM,typename Encoder>
void LiquidSolverFLIP<DIM,Encoder>::initDomain(BBox<scalar> bb,const Vec3& cellSz,const Vec3i* warpMode)
{
  LiquidSolverPIC<DIM,Encoder>::initDomain(bb,cellSz,warpMode);
  _weight.reset(_grd);
  _useVariational=true;
  _useBOTE=false;
}
template <int DIM,typename Encoder>
void LiquidSolverFLIP<DIM,Encoder>::focusOn(const BBox<scalar>& region)
{
  BBox<scalar,DIM> regionDIM;
  regionDIM._minC=assignVariableTo<VecDIMd,Vec3>(region._minC);
  regionDIM._maxC=assignVariableTo<VecDIMd,Vec3>(region._maxC);
  VariableSizedGrid<scalar,DIM,Encoder> grd;
  grd.reset(_grd0,regionDIM,2);
  if((grd._offset.getExtent().array() > 2).all()) {
    //phi
    CellCenterData<scalar> phiNew(grd);
    grd.sampleCellCenter(phiNew,_grd,_phi);
    _phi=phiNew;
    //weight
    _weight.reset(grd);
    VariableSizedGridOpProject<scalar,DIM,Encoder>::sampleWeightMACVel(grd,_weight,_nodalSolidPhi);
    //vel
    MACVelData<DIM,scalar> velNew(grd);
    grd.sampleMACVel(velNew,_grd,_vel);
    _vel=velNew;
    //grd
    _grd=grd;
  } else {
    WARNING("Focus region too small!")
  }
}
template <int DIM,typename Encoder>
void LiquidSolverFLIP<DIM,Encoder>::setUseVariational(bool variational)
{
  _useVariational=variational;
}
template <int DIM,typename Encoder>
void LiquidSolverFLIP<DIM,Encoder>::setUseBOTE(bool BOTE)
{
  _useBOTE=BOTE;
}
//getter
template <int DIM,typename Encoder>
Vec3 LiquidSolverFLIP<DIM,Encoder>::getForceOnBody(sizeType bodyId) const
{
  return _forceTorque[bodyId].segment<3>(0);
}
template <int DIM,typename Encoder>
Vec3 LiquidSolverFLIP<DIM,Encoder>::getTorqueOnBody(sizeType bodyId) const
{
  return _forceTorque[bodyId].segment<3>(3);
}
//solver
template <int DIM,typename Encoder>
sizeType LiquidSolverFLIP<DIM,Encoder>::advance(const scalar& dt)
{
  _rigid->clearContactFlag();
  _forceTorque.clear();
  CellCenterData<unsigned char> tag(_grd);
  MACVelData<DIM,unsigned char> valid(_grd);
  MACVelData<DIM,scalar> velTmp(_grd);
  sizeType index=0;
  scalar t=0;
  while(t < dt) {
    //compute timestep
    scalar substep=Parent::getSubstep(t,dt);
    if(_rigid && _rigid->nrBody() > 0)
      _rigid->advanceVelocity(substep,Parent::getGlobalTime()+t);
    Parent::computePSet();
    //add external force
    scalar g=-_rigid->getGravity().dot(Vec3::UnitY());
    VariableSizedParticleOp<scalar,DIM,Encoder>::addExternalForce(_pSet,g,substep);
    //add source region
    Parent::sampleSource(_spring->getPSetCD(),Parent::getGlobalTime()+t);
    //compute a valid and compatible velocity: OLD
    Parent::projectToGrid(velTmp,valid);
    velTmp=_vel;
    //compute a valid and compatible velocity: NEW
    Parent::computePhi();
    makeDivergenceFree(substep,tag,valid);
    //compute a valid and compatible velocity: MIX FLIP PIC
    VariableSizedParticleOp<scalar,DIM,Encoder>::projectToParticle(_pSet,_grd,velTmp,_vel,_PICWeight);
    VariableSizedParticleOp<scalar,DIM,Encoder>::clamp(_pSet,Parent::getMaxVelocity());
    //advect particle
    if(_rigid && _rigid->nrBody() > 0)
      _rigid->advancePosition(substep);
    VariableSizedGridOp<scalar,DIM,Encoder>::advectParticle(_grd,_vel,_pSet,substep);
    index++;
  }
  Parent::advanceTime(dt);
  Parent::correct(dt);
  //compute force applied on each rigid body
  for(sizeType i=0; i<_rigid->nrBody(); i++)
    _forceTorque[i]/=dt;
  return index;
}
template <int DIM,typename Encoder>
void LiquidSolverFLIP<DIM,Encoder>::sampleSource(CollisionInterface<scalar,ParticleSetN>& cd,const scalar& globalTime)
{
  cd.prepare(_pSet);
  typedef vector<boost::shared_ptr<SourceRegion> >::iterator iter;
  for(iter beg=_sources.begin(),end=_sources.end(); beg!=end; beg++) {
    (*beg)->setCellSz(assignVariableTo<Vec3,VecDIMd>(_grd._cellSz0));
    (*beg)->setNrParticlePerGrid(_nrParticlePerGrid);
    (*beg)->sampleRegion(_nodalSolidPhi,cd,_radius,_pSet,globalTime);
  }
}
template <int DIM,typename Encoder>
void LiquidSolverFLIP<DIM,Encoder>::makeDivergenceFreeRigidBOTE(scalar dt,CellCenterData<unsigned char>& tag,MACVelData<DIM,unsigned char>& valid)
{
  //adjust phi
  MACVelData<DIM,sizeType> rigidId(_grd);
  MACVelData<DIM,scalar> weightWithRigid=_weight;
  rigidId.clamp(-1,-1);
  for(sizeType i=0; i<_rigid->nrBody(); i++)
    VariableSizedGridOpProject<scalar,DIM,Encoder>::computeRigidBodyWeightMACVel(_grd,NULL,&weightWithRigid,&rigidId,_rigid->getBody(i),i);
  //project
  CellCenterData<scalarD> pressure(_grd);
  if(_useUnilateral) {
    VariableSizedGridOpProject<scalar,DIM,Encoder>::rigidProjectBOTEUnilateral(_grd,pressure,_phi,weightWithRigid,rigidId,*_rigid,_vel,valid,dt,_density);
  } else {
#ifdef USE_MINRES
    VariableSizedGridOpProject<scalar,DIM,Encoder>::rigidProjectBOTEMINRES(_grd,pressure,_phi,weightWithRigid,rigidId,*_rigid,_vel,valid,dt,_density);
#else
    VariableSizedGridOpProject<scalar,DIM,Encoder>::rigidProjectBOTE(_grd,pressure,_phi,weightWithRigid,rigidId,*_rigid,_vel,valid,dt,_density);
#endif
  }
}
template <int DIM,typename Encoder>
void LiquidSolverFLIP<DIM,Encoder>::makeDivergenceFreeRigid(scalar dt,CellCenterData<unsigned char>& tag,MACVelData<DIM,unsigned char>& valid)
{
  //adjust phi
  MACVelData<DIM,scalar> weightWithRigid=_weight;
  vector<MACVelData<DIM,scalar> > weightRigids(_rigid->nrBody());
  for(sizeType i=0; i<_rigid->nrBody(); i++) {
    weightRigids[i].reset(_grd);
    VariableSizedGridOpProject<scalar,DIM,Encoder>::computeRigidBodyWeightMACVel(_grd,&weightRigids[i],NULL,NULL,_rigid->getBody(i),i);
    weightWithRigid.add(weightRigids[i],-1);
    weightWithRigid.clamp(0,1);
  }
  VariableSizedGridOp<scalar,DIM,Encoder>::extendPhiCellCenter(_grd,_phi,weightWithRigid,2);
  //project
  CellCenterData<scalarD> pressure(_grd);
  if(_useUnilateral) {
    VariableSizedGridOpProject<scalar,DIM,Encoder>::rigidProjectVariationalUnilateral(_grd,pressure,_phi,weightWithRigid,weightRigids,*_rigid,_vel,valid,dt,_density);
  } else {
#ifdef USE_MINRES
    VariableSizedGridOpProject<scalar,DIM,Encoder>::rigidProjectVariationalMINRES(_grd,pressure,_phi,weightWithRigid,weightRigids,*_rigid,_vel,valid,dt,_density);
#else
    VariableSizedGridOpProject<scalar,DIM,Encoder>::rigidProjectVariational(_grd,pressure,_phi,weightWithRigid,weightRigids,*_rigid,_vel,valid,dt,_density);
#endif
  }
}
template <int DIM,typename Encoder>
void LiquidSolverFLIP<DIM,Encoder>::makeDivergenceFreeNoRigid(scalar dt,CellCenterData<unsigned char>& tag,MACVelData<DIM,unsigned char>& valid)
{
  //adjust phi
  VariableSizedGridOp<scalar,DIM,Encoder>::extendPhiCellCenter(_grd,_phi,_weight,2);
  //project
  CellCenterData<scalarD> pressure(_grd);
  if(_useVariational) {
    if(_useUnilateral)
      VariableSizedGridOpProject<scalar,DIM,Encoder>::projectVariationalUnilateral(_grd,pressure,_phi,_weight,_vel,valid,dt,_density);
    else VariableSizedGridOpProject<scalar,DIM,Encoder>::projectVariational(_grd,pressure,_phi,_weight,_vel,valid,dt,_density);
  } else {
    if(_useUnilateral)
      VariableSizedGridOpProject<scalar,DIM,Encoder>::projectUnilateral(_grd,pressure,_phi,_nodalSolidPhi,_vel,valid,tag,dt,_density);
    else VariableSizedGridOpProject<scalar,DIM,Encoder>::project(_grd,pressure,_phi,_nodalSolidPhi,_vel,valid,tag,dt,_density);
  }
}
template <int DIM,typename Encoder>
void LiquidSolverFLIP<DIM,Encoder>::makeDivergenceFree(scalar dt,CellCenterData<unsigned char>& tag,MACVelData<DIM,unsigned char>& valid)
{
  //save force
  if(_forceTorque.empty())
    _forceTorque.assign(_rigid->nrBody(),Vec6::Zero());
  for(sizeType i=0; i<_rigid->nrBody(); i++) {
    _forceTorque[i].segment<3>(0)-=_rigid->getBody(i).p();
    _forceTorque[i].segment<3>(3)-=_rigid->getBody(i).L();
  }
  //project
  //solve for pressure
  if(_useBOTE)
    makeDivergenceFreeRigidBOTE(dt,tag,valid);
  else if(_rigid->nrBody() > 0)
    makeDivergenceFreeRigid(dt,tag,valid);
  else makeDivergenceFreeNoRigid(dt,tag,valid);
  VariableSizedGridOp<scalar,DIM,Encoder>::extrapolateMACVel(_grd,valid,_vel,_nrPass);
  //load force
  for(sizeType i=0; i<_rigid->nrBody(); i++) {
    _forceTorque[i].segment<3>(0)+=_rigid->getBody(i).p();
    _forceTorque[i].segment<3>(3)+=_rigid->getBody(i).L();
  }
}
//instance
#define INSTANCE(DIM,TYPE,SPARSITY) \
template class LiquidSolverPIC<DIM,Encoder##TYPE<SPARSITY> >; \
template class LiquidSolverFLIP<DIM,Encoder##TYPE<SPARSITY> >;
INSTANCE_ALL
