#ifndef LIQUID_SOLVER_H
#define LIQUID_SOLVER_H

#include "RigidSolver.h"
#include "SourceRegion.h"
#include "SpringForce.h"
#include "VariableSizedGrid.h"
#include <CommonFile/GridBasic.h>
#include <CommonFile/ParticleSet.h>
#include <FluidSurface/ParticleSurface.h>

PRJ_BEGIN

template <int DIM,typename Encoder>
class LiquidSolverPIC : public TypeTraits<scalar,DIM>, public Serializable
{
public:
  typedef typename TypeTraits<scalar,DIM>::VecDIMd VecDIMd;
  typedef typename TypeTraits<scalar,DIM>::VecDIMi VecDIMi;
  LiquidSolverPIC();
  virtual ~LiquidSolverPIC();
  static void registerSource(IOData* dat);
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  //timing
  virtual scalar getGlobalTime();
  virtual sizeType getGlobalId();
  virtual void setGlobalTime(const scalar& globalTime);
  virtual void setGlobalId(const sizeType& globalId);
  virtual void advanceTime(const scalar& dt);
  virtual void setCFLMode(const scalar& maxCFL,const scalar& maxSubstep);
  virtual scalar getSubstep(scalar& t,const scalar& dt) const;
  virtual scalar getMaxVelocity() const;
  //setup
  virtual void computeRadius();
  virtual void initSolid(boost::shared_ptr<RigidSolver> sol);
  virtual void initLiquid(const ImplicitFunc<scalar>& l,const scalar& sparsity=1.0f);
  virtual void initDomain(BBox<scalar> bb,const Vec3& cellSz,const Vec3i* warpMode=NULL);
  virtual void focusOn(const BBox<scalar>& region);
  virtual void focusOn(sizeType bodyId,scalar enlargedEps=1);
  virtual void getFeature(VectorField& field,scalar sz);
  virtual void setUseUnilateral(bool unilateral);
  //getter
  virtual BBox<scalar> getBB() const;
  virtual scalar getRadius() const;
  virtual Vec3i getNrCell() const;
  virtual const VariableSizedGrid<scalar,DIM,Encoder>& getGrid() const;
  virtual const VariableSizedGrid<scalar,DIM,Encoder>& getGrid0() const;
  virtual const CellCenterData<scalar>& getPhi() const;
  virtual const MACVelData<DIM,scalar>& getVel() const;
  virtual const ScalarField& getSolid() const;
  virtual const ParticleSetN& getPSet() const;
  virtual const RigidSolver& getRigid() const;
  virtual RigidSolver& getRigid();
  //solver
  virtual sizeType advance(const scalar& dt);
  virtual bool addSource(boost::shared_ptr<SourceRegion> s);
  virtual bool removeSource(boost::shared_ptr<SourceRegion> s);
  virtual void sampleSource(CollisionInterface<scalar,ParticleSetN>& cd,const scalar& globalTime);
  //render
  virtual const ObjMesh& getSurfaceMesh(bool vol,scalar smoothRange=6);
protected:
  virtual void correct(scalar dt);
  virtual void computePhi();
  virtual void makeDivergenceFree(scalar dt,CellCenterData<unsigned char>& tag,MACVelData<DIM,unsigned char>& valid);
  virtual void projectToGrid(MACVelData<DIM,scalar>& velTmp,MACVelData<DIM,unsigned char>& valid);
  virtual void computePSet();
  //data
  ScalarField _nodalSolidPhi;
  VariableSizedGrid<scalar,DIM,Encoder> _grd,_grd0;
  CellCenterData<scalar> _phi;
  MACVelData<DIM,scalar> _vel;
  boost::shared_ptr<RigidSolver> _rigid;
  ParticleSetTpl<ParticleN<scalar> > _pSet;
  vector<boost::shared_ptr<SourceRegion> > _sources;
  boost::shared_ptr<ParticleSurface> _surface;
  boost::shared_ptr<SpringForce> _spring;
  //param
  scalar _density;
  scalar _narrowBand;
  scalar _radius,_springRad;
  sizeType _nrPass,_nrParticlePerGrid;
  //timing
  scalar _globalTime;
  sizeType _globalId;
  scalar _maxSubstep;
  scalar _maxCFL;
  bool _useUnilateral;  //whether pressure should be always positive on boundary
  //temporary data not involved in IO
  ParticleSetTpl<ParticleN<scalar> > _pSetAll,_pSetRigid;
};
template <int DIM,typename Encoder>
class LiquidSolverFLIP : public LiquidSolverPIC<DIM,Encoder>
{
public:
  typedef typename TypeTraits<scalar,DIM>::VecDIMd VecDIMd;
  typedef typename TypeTraits<scalar,DIM>::VecDIMi VecDIMi;
  typedef LiquidSolverPIC<DIM,Encoder> Parent;
  using Parent::_nodalSolidPhi;
  using Parent::_grd;
  using Parent::_grd0;
  using Parent::_phi;
  using Parent::_vel;
  using Parent::_rigid;
  using Parent::_pSet;
  using Parent::_sources;
  using Parent::_spring;
  //param
  using Parent::_density;
  using Parent::_narrowBand;
  using Parent::_radius;
  using Parent::_springRad;
  using Parent::_nrPass;
  using Parent::_nrParticlePerGrid;
  //timing
  using Parent::_globalTime;
  using Parent::_globalId;
  using Parent::_maxSubstep;
  using Parent::_maxCFL;
  using Parent::_useUnilateral;
  //temporary data not involved in IO
  using Parent::_pSetAll;
  using Parent::_pSetRigid;
  LiquidSolverFLIP();
  virtual bool read(istream& is,IOData* dat) override;
  virtual bool write(ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<Serializable> copy() const override;
  //setup
  virtual void initSolid(boost::shared_ptr<RigidSolver> sol) override;
  virtual void initDomain(BBox<scalar> bb,const Vec3& cellSz,const Vec3i* warpMode=NULL) override;
  virtual void focusOn(const BBox<scalar>& region) override;
  virtual void setUseVariational(bool variational);
  virtual void setUseBOTE(bool BOTE);
  //getter
  Vec3 getForceOnBody(sizeType bodyId) const;
  Vec3 getTorqueOnBody(sizeType bodyId) const;
  //solver
  virtual sizeType advance(const scalar& dt) override;
  virtual void sampleSource(CollisionInterface<scalar,ParticleSetN>& cd,const scalar& globalTime);
protected:
  virtual void makeDivergenceFreeRigidBOTE(scalar dt,CellCenterData<unsigned char>& tag,MACVelData<DIM,unsigned char>& valid);
  virtual void makeDivergenceFreeRigid(scalar dt,CellCenterData<unsigned char>& tag,MACVelData<DIM,unsigned char>& valid);
  virtual void makeDivergenceFreeNoRigid(scalar dt,CellCenterData<unsigned char>& tag,MACVelData<DIM,unsigned char>& valid);
  virtual void makeDivergenceFree(scalar dt,CellCenterData<unsigned char>& tag,MACVelData<DIM,unsigned char>& valid) override;
  scalar _PICWeight;
  bool _useVariational; //note that when we detect rigid body, we have to use variational form even if _useVariational=false
  bool _useBOTE;        //use back-of-the-envelop computation for accurate momentum
  MACVelData<DIM,scalar> _weight;
  //temporary data not involved in IO
  vector<Vec6,Eigen::aligned_allocator<Vec6> > _forceTorque;
};

PRJ_END

#endif
