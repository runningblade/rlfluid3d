#ifndef SMOKE_H
#define SMOKE_H

#include <CommonFile/IO.h>
#include <CommonFile/ImplicitFuncInterface.h>
#include <boost/property_tree/ptree_fwd.hpp>
#include "VariableSizedGrid.h"

PRJ_BEGIN

//variables
template <typename TT,typename TF>
void assignVariable(TT& to,const TF& from)
{
  to.setZero();
  sizeType sz=min(to.size(),from.size());
  to.segment(0,sz)=from.segment(0,sz).template cast<typename TT::Scalar>();
}
template <typename TT,typename TF>
TT assignVariableTo(const TF& from)
{
  TT to=TT::Zero();
  sizeType sz=min(to.size(),from.size());
  to.segment(0,sz)=from.segment(0,sz).template cast<typename TT::Scalar>();
  return to;
}
template <typename TT,typename TF>
void assignVariableOnes(TT& to,const TF& from)
{
  to.setOnes();
  sizeType sz=min(to.size(),from.size());
  to.segment(0,sz)=from.segment(0,sz).template cast<typename TT::Scalar>();
}
template <typename TT,typename TF>
TT assignVariableToOnes(const TF& from)
{
  TT to=TT::Ones();
  sizeType sz=min(to.size(),from.size());
  to.segment(0,sz)=from.segment(0,sz).template cast<typename TT::Scalar>();
  return to;
}
template <typename TT1,int DIM1,typename TT2,int DIM2>
void assignVariable(BBox<TT1,DIM1>& to,const BBox<TT2,DIM2>& from)
{
  assignVariable(to._minC,from._minC);
  assignVariable(to._maxC,from._maxC);
}
template <typename TT1,int DIM1,typename TT2,int DIM2>
BBox<TT1,DIM1> assignVariableTo(const BBox<TT2,DIM2>& from)
{
  BBox<TT1,DIM1> to;
  assignVariable(to._minC,from._minC);
  assignVariable(to._maxC,from._maxC);
  return to;
}
//iterator
template <typename T,int DIM>
struct IteratorUtil : public TypeTraits<T,DIM> {
  using typename TypeTraits<T,DIM>::VecDIMd;
  using typename TypeTraits<T,DIM>::VecDIMb;
  using typename TypeTraits<T,DIM>::VecDIMi;
  static void makeIteratorRangeSafe(sizeType& id0,sizeType& id1,sizeType nrP,sizeType warp,sizeType& warpCount,T warpRange,T& warpCompensate) {
    warpCount=0;
    warpCompensate=0;
    if(warp > 0) {
      while(id0 < 0) {
        id0+=nrP;
        id1+=nrP;
        warpCompensate-=warpRange;
      }
      while(id0 >= nrP) {
        id0-=nrP;
        id1-=nrP;
        warpCompensate+=warpRange;
      }
      while(id1 >= nrP) {
        id1-=nrP;
        warpCount++;
      }
    } else {
      id0=max<sizeType>(id0,0);
      id1=min<sizeType>(id1,nrP);
    }
  }
  static void extendIterator(sizeType& id0,sizeType& id1,sizeType nrP,sizeType warpMode,sizeType& warpCount,sizeType extendMin,sizeType extendMax,T warpRange,T& warpCompensate) {
    for(sizeType e=0; e<extendMin; e++)
      if(id0 > 0) {
        id0--;
      } else if(warpMode > 0) {
        id0=nrP-1;
        warpCount++;
        warpCompensate-=warpRange;
      }
    for(sizeType e=0; e<extendMax; e++)
      if(id1 < nrP-1) {
        id1++;
      } else if(warpMode > 0) {
        id1=0;
        warpCount++;
      }
  }
  static void initIterator(VecDIMi& id,const VecDIMi& id0,const VecDIMi& id1,VecDIMi& warpCount,const VecDIMi& warpCountMax,int verbose) {
    warpCount.setZero();
    if(verbose == -1) {
      id=id0;
      id[0]=id1[0];
      warpCount[0]=warpCountMax[0];
    } else {
      id=id0;
    }
  }
  template <typename GRID_TYPE,typename FUNC>
  static void computeIdBB(const GRID_TYPE& grd,VecDIMi& id0,VecDIMi& id1,const BBox<T,DIM>& bb,VecDIMi& warpCountMax,const VecDIMd& warpRange,VecDIMd& warpCompensate,FUNC f) {
    VecDIMb ob;
    warpCompensate.setZero();
    //min
    VecDIMd minCAdj=bb._minC;
    f(minCAdj,id0,ob);
    //adjust
    VecDIMd maxC=bb._maxC;
    for(sizeType D=0; D<DIM; D++)
      if(grd._warpMode[D] > 0) {
        maxC[D]+=(minCAdj-bb._minC)[D];
        warpCompensate[D]-=(minCAdj-bb._minC)[D];
      }
    //max
    VecDIMd maxCAdj=maxC;
    f(maxCAdj,id1,ob);
    maxCAdj-=maxC;
    //warpCount
    warpCountMax.setZero();
    for(sizeType D=0; D<DIM; D++)
      if(grd._warpMode[D] > 0)
        while(maxCAdj[D] < -warpRange[D]/2) {
          maxCAdj[D]+=warpRange[D];
          warpCountMax[D]++;
        }
  }
};
//filesystem
bool notDigit(char c);
bool lessDirByNumber(boost::filesystem::path A,boost::filesystem::path B);
bool exists(const boost::filesystem::path& path);
void removeDir(const boost::filesystem::path& path);
void create(const boost::filesystem::path& path);
void recreate(const boost::filesystem::path& path);
vector<boost::filesystem::path> files(const boost::filesystem::path& path);
vector<boost::filesystem::path> directories(const boost::filesystem::path& path);
void sortFilesByNumber(vector<boost::filesystem::path>& files);
bool isDir(const boost::filesystem::path& path);
size_t fileSize(const boost::filesystem::path& path);
string parseProps(sizeType argc,char** argv,boost::property_tree::ptree& pt);
//property tree
void readPtreeBinary(boost::property_tree::ptree& pt,istream& is);
void writePtreeBinary(const boost::property_tree::ptree& pt,ostream& os);
void readPtreeAscii(boost::property_tree::ptree& pt,const boost::filesystem::path& path);
void writePtreeAscii(const boost::property_tree::ptree& pt,const boost::filesystem::path& path);
//implicit functions
class ImplicitFuncCircleSmooth : public ImplicitFunc<scalar>
{
public:
  ImplicitFuncCircleSmooth(scalar rad,const Vec3& ctr,scalar alpha);
  template <typename VCTR>
  ImplicitFuncCircleSmooth(scalar rad,const VCTR& ctr,scalar alpha) {
    _rad=rad;
    _alpha=alpha;
    assignVariable(_ctr,ctr);
  }
  scalar operator()(const Vec3& pos) const override;
  virtual BBox<scalar> getBB() const override;
private:
  scalar _rad,_alpha;
  Vec3 _ctr;
};
class ImplicitFuncCircle : public ImplicitFunc<scalar>
{
public:
  ImplicitFuncCircle(scalar rad,const Vec3& ctr);
  template <typename VCTR>
  ImplicitFuncCircle(scalar rad,const VCTR& ctr) {
    _rad=rad;
    assignVariable(_ctr,ctr);
  }
  scalar operator()(const Vec3& pos) const override;
  virtual BBox<scalar> getBB() const override;
private:
  scalar _rad;
  Vec3 _ctr;
};
class ImplicitFuncBox : public ImplicitFunc<scalar>
{
public:
  template <typename VEC>
  ImplicitFuncBox(const VEC& minC,const VEC& maxC,sizeType DIM) {
    _DIM=DIM;
    assignVariable(_bb._minC,minC);
    assignVariable(_bb._maxC,maxC);
  }
  scalar operator()(const Vec3& pos) const override;
  virtual BBox<scalar> getBB() const override;
private:
  sizeType _DIM;
  BBox<scalar> _bb;
};
class VelFuncRotate : public VelFunc<scalar>
{
public:
  VelFuncRotate();
  VelFuncRotate(const Vec3& ctr,const Vec3& dir);
  template <typename VCTR,typename VDIR>
  VelFuncRotate(const VCTR& ctr,const VDIR& dir) {
    assignVariable(_ctr,ctr);
    assignVariable(_dir,dir);
  }
  Vec3 operator()(const Vec3& pos) const override;
private:
  Vec3 _ctr,_dir;
};
class VelFuncConstant : public VelFunc<scalar>
{
public:
  VelFuncConstant();
  VelFuncConstant(const Vec3& dir);
  template <typename VDIR>
  VelFuncConstant(const VDIR& dir) {
    assignVariable(_dir,dir);
  }
  Vec3 operator()(const Vec3& pos) const override;
private:
  Vec3 _dir;
};
//instance
#define INSTANCE_ALLDIM(TYPE,SPARSITY)  \
INSTANCE(2,TYPE,SPARSITY) INSTANCE(3,TYPE,SPARSITY)
#define INSTANCE_ALLDIM_ALLTYPE(SPARSITY)  \
INSTANCE_ALLDIM(Octree,SPARSITY) INSTANCE_ALLDIM(Constant,SPARSITY)
#define INSTANCE_ALL  \
INSTANCE_ALLDIM_ALLTYPE(1)  \
INSTANCE_ALLDIM_ALLTYPE(2)  \
INSTANCE_ALLDIM_ALLTYPE(3)  \
INSTANCE_ALLDIM_ALLTYPE(4)  \
//instance2
#define INSTANCE2_ALLDIM(TYPE,SPARSITY) \
INSTANCE2(2,TYPE,SPARSITY,2) INSTANCE2(2,TYPE,SPARSITY,1) INSTANCE2(2,TYPE,SPARSITY,0)  \
INSTANCE2(3,TYPE,SPARSITY,2) INSTANCE2(3,TYPE,SPARSITY,1) INSTANCE2(3,TYPE,SPARSITY,0)
#define INSTANCE2_ALLDIM_ALLTYPE(SPARSITY)  \
INSTANCE2_ALLDIM(Octree,SPARSITY) INSTANCE2_ALLDIM(Constant,SPARSITY)
#define INSTANCE2_ALL  \
INSTANCE2_ALLDIM_ALLTYPE(1)  \
INSTANCE2_ALLDIM_ALLTYPE(2)  \
INSTANCE2_ALLDIM_ALLTYPE(3)  \
INSTANCE2_ALLDIM_ALLTYPE(4)  \
//convenient types
#define DECL_TYPES  \
using typename TypeTraits<T,DIM>::VecDIMd;  \
using typename TypeTraits<T,DIM>::VecDIMPd;  \
using typename TypeTraits<T,DIM>::VecDIMb;  \
using typename TypeTraits<T,DIM>::VecDIMi;  \
using typename TypeTraits<T,DIM>::VecSTENCILd;  \
using typename TypeTraits<T,DIM>::VecSTENCILi;  \
typedef FixedSparseMatrix<scalarD,Kernel<scalarD> > SMat;  \
typedef Eigen::Matrix<scalarD,(DIM<<1)+1,-1> SparseSTENCILd;  \
typedef Eigen::Matrix<sizeType,(DIM<<1)+1,-1> SparseSTENCILi;  \
typedef NodalIteratorPoint<T,DIM,Encoder> NodalPointIt;  \
typedef NodalIteratorPointIndex<T,DIM,Encoder> NodalPointIndexIt;  \
typedef NodalIteratorCell<T,DIM,Encoder> NodalCellIt;  \
typedef CellCenterIteratorPoint<T,DIM,Encoder> CellCenterPointIt;  \
typedef CellCenterIteratorPointIndex<T,DIM,Encoder> CellCenterPointIndexIt;  \
typedef CellCenterIteratorCell<T,DIM,Encoder> CellCenterCellIt;  \
typedef MACVelIteratorPoint<T,DIM,Encoder> MACVelPointIt;  \
typedef MACVelIteratorPointIndex<T,DIM,Encoder> MACVelPointIndexIt;  \
typedef MACVelIteratorCell<T,DIM,Encoder> MACVelCellIt;  \
typedef VariableSizedGrid<T,DIM,Encoder> GridType;  \
//GridBasic's warp parameter
#define DEFINE_GRID_WARP(GRID)  \
Vec3i warpMode=assignVariableTo<Vec3i,VecDIMi>(grd._warpMode);  \
warpMode.array()*=(GRID).getNrPoint().array();  \
//forward declaration for solving linear system
#define DECL_FORWARD_LINEAR_SOLVER \
template <typename T> \
struct Kernel;  \
template <typename T,typename KERNEL_TYPE>  \
struct FixedSparseMatrix; \
struct GeneralizedMJ; \
class RigidSolver;  \
class RigidBody;  \
struct MJ;  \

PRJ_END

#endif
