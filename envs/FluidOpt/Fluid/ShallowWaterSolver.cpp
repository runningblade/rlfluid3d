#include "ShallowWaterSolver.h"
#include "VariableSizedGridOpShallowWater.h"
#include "VariableSizedGridOp.h"
#include "ScriptedSource.h"
#include <CommonFile/IO.h>

USE_PRJ_NAMESPACE

template <int DIM,typename Encoder>
ShallowWaterSolver<DIM,Encoder>::ShallowWaterSolver()
{
  Serializable::setType(typeid(LiquidSolverFLIP<DIM,Encoder>).name());
}
template <int DIM,typename Encoder>
ShallowWaterSolver<DIM,Encoder>::~ShallowWaterSolver() {}
template <int DIM,typename Encoder>
bool ShallowWaterSolver<DIM,Encoder>::read(istream& is,IOData* dat)
{
  registerType<RigidSolver>(dat);
  Parent::registerSource(dat);
  _nodalSolidPhi.read(is,dat);
  _grd.read(is,dat);
  _grd0.read(is,dat);
  readVector(_sources,is,dat);
  readBinaryData(_theta,is);
  readBinaryData(_CDrag,is);
  readBinaryData(_CDragLiquid,is);
  readBinaryData(_CLift,is);
  _B.read(is,dat);
  for(sizeType d=0; d<=DIM; d++)
    _U[d].read(is,dat);
  readBinaryData(_rigid,is,dat);
  //param
  readBinaryData(_density,is);
  //timing
  readBinaryData(_globalTime,is);
  readBinaryData(_globalId,is);
  readBinaryData(_maxSubstep,is);
  readBinaryData(_maxCFL,is);
  return is.good();
}
template <int DIM,typename Encoder>
bool ShallowWaterSolver<DIM,Encoder>::write(ostream& os,IOData* dat) const
{
  registerType<RigidSolver>(dat);
  Parent::registerSource(dat);
  _nodalSolidPhi.write(os,dat);
  _grd.write(os,dat);
  _grd0.write(os,dat);
  writeVector(_sources,os,dat);
  writeBinaryData(_theta,os);
  writeBinaryData(_CDrag,os);
  writeBinaryData(_CDragLiquid,os);
  writeBinaryData(_CLift,os);
  _B.write(os,dat);
  for(sizeType d=0; d<=DIM; d++)
    _U[d].write(os,dat);
  writeBinaryData(_rigid,os,dat);
  //param
  writeBinaryData(_density,os);
  //timing
  writeBinaryData(_globalTime,os);
  writeBinaryData(_globalId,os);
  writeBinaryData(_maxSubstep,os);
  writeBinaryData(_maxCFL,os);
  return os.good();
}
template <int DIM,typename Encoder>
boost::shared_ptr<Serializable> ShallowWaterSolver<DIM,Encoder>::copy() const
{
  return boost::shared_ptr<Serializable>(new ShallowWaterSolver<DIM,Encoder>);
}
template <int DIM,typename Encoder>
scalar ShallowWaterSolver<DIM,Encoder>::getSubstep(scalar& t,const scalar& dt,scalar limit) const
{
  scalar substep=limit*_maxCFL;
  //if(substep < 1.0f/_maxSubstep)
  //  substep=1.0f/_maxSubstep;
  if(t+substep > dt)
    substep=dt-t;
  t+=substep;
  return substep;
}
//setup
class ConstantFunc : public ImplicitFunc<scalar>
{
public:
  ConstantFunc(scalar val):_val(val) {}
  virtual scalar operator()(const Vec3& pos) const {
    return _val;
  }
  scalar _val;
};
class BottomFunc : public ImplicitFunc<scalar>
{
public:
  BottomFunc(const Vec3& pt0,const Vec3& pt):_pt0(pt0),_pt(pt) {}
  scalar operator()(const Vec3& pos) const {
    return (pos-_pt0).dot(_pt);
  }
  const Vec3 _pt0,_pt;
};
template <int DIM,typename Encoder>
void ShallowWaterSolver<DIM,Encoder>::initLiquidLevel(scalar l,const scalar& sparsity)
{
  initLiquid(ConstantFunc(l),sparsity);
}
template <int DIM,typename Encoder>
void ShallowWaterSolver<DIM,Encoder>::initSolid(boost::shared_ptr<RigidSolver> sol)
{
  if(!sol) {
    ASSERT(_rigid)
  } else _rigid=sol;

  _rigid->setWarpMode(assignVariableTo<Vec3i,VecDIMi>(_grd0._warpMode));
  BBox<scalar,DIM> bbLiquid=_grd0.getBBNodal();
  BBox<scalar> bb=_rigid->getBB();
  for(sizeType D=0; D<DIM; D++)
    if(_grd0._warpMode[D] > 0) {
      bb._minC[D]=bbLiquid._minC[D];
      bb._maxC[D]=bbLiquid._maxC[D];
    }
  _rigid->setBB(bb);

  for(sizeType x=0; x<_nodalSolidPhi.getNrPoint()[0]; x++)
    for(sizeType y=0; y<_nodalSolidPhi.getNrPoint()[1]; y++)
      for(sizeType z=0; z<_nodalSolidPhi.getNrPoint()[2]; z++) {
        Vec3 from=_nodalSolidPhi.getPt(Vec3i(x,y,z)),to=from;
        from[DIM]=bb._maxC[DIM];
        to[DIM]=bb._minC[DIM];
        //find bottom
        scalar s=_rigid->intersect(from,to,true);
        scalar& val=_nodalSolidPhi.get(Vec3i(x,y,z));
        val=interp1D(from[DIM],to[DIM],s);
      }
  _B.reset(_grd);
  VariableSizedGridOp<scalar,DIM,Encoder>::copyFromGridNodal(_grd,_B,_nodalSolidPhi);
}
template <int DIM,typename Encoder>
void ShallowWaterSolver<DIM,Encoder>::initLiquid(const ImplicitFunc<scalar>& l,const scalar& sparsity)
{
  _U[0].reset(_grd);
  CellCenterData<scalar> h,u[DIM];
  VariableSizedGridOp<scalar,DIM,Encoder>::copyImplicitFuncCellCenter(_grd,_U[0],l);
  for(sizeType d=0; d<DIM; d++) {
    _U[d+1].reset(_grd);
    _U[d+1]._data.setZero();
  }
  VariableSizedGridOpShallowWater<scalar,DIM,Encoder>::computehu(_grd,h,u,_U,_B);
  VariableSizedGridOpShallowWater<scalar,DIM,Encoder>::computeU(_grd,h,u,_U,_B);
}
template <int DIM,typename Encoder>
void ShallowWaterSolver<DIM,Encoder>::initDomain(BBox<scalar> bb,const Vec3& cellSz,const Vec3i* warpMode)
{
  const Vec3i nrCell=ceilV(Vec3((bb.getExtent().array()/cellSz.array()).matrix()));
  const Vec3 ext=(nrCell.template cast<scalar>().array()*cellSz.array()).matrix();
  bb=BBox<scalar>(bb._minC,bb._minC+ext);
  _nodalSolidPhi.reset(nrCell,bb,0,false);
  _grd.reset(bb,nrCell,BBox<sizeType>(Vec3i::Zero(),Vec3i::Zero()),0,warpMode);
  _grd0=_grd;

  _density=1000;
  _CDrag=200.0f;
  _CDragLiquid=_CDrag;
  _CLift=10.0f;
  _theta=1.5f;
  LiquidSolverPIC<DIM,Encoder>::setCFLMode(0.5f,200.0f);
}
template <int DIM,typename Encoder>
void ShallowWaterSolver<DIM,Encoder>::focusOn(const BBox<scalar>& region)
{
  BBox<scalar,DIM> regionDIM;
  regionDIM._minC=assignVariableTo<VecDIMd,Vec3>(region._minC);
  regionDIM._maxC=assignVariableTo<VecDIMd,Vec3>(region._maxC);
  VariableSizedGrid<scalar,DIM,Encoder> grd;
  grd.reset(_grd0,regionDIM,2);
  if((grd._offset.getExtent().array() > 2).all()) {
    //U
    for(sizeType d=0; d<=DIM; d++) {
      CellCenterData<scalar> UNew(grd);
      grd.sampleCellCenter(UNew,_grd,_U[d]);
      _U[d]=UNew;
    }
    //B
    _B.reset(grd);
    VariableSizedGridOp<scalar,DIM,Encoder>::copyFromGridNodal(grd,_B,_nodalSolidPhi);
    //grd
    _grd=grd;
  } else {
    WARNING("Focus region too small!")
  }
}
//getter
template <int DIM,typename Encoder>
ScalarField ShallowWaterSolver<DIM,Encoder>::getWS() const
{
  ScalarField W;
  VariableSizedGridOp<scalar,DIM,Encoder>::copyToGridCellCenter(_grd0,_grd,getW(),W);
  return W;
}
template <int DIM,typename Encoder>
ScalarField ShallowWaterSolver<DIM,Encoder>::getBS() const
{
  ScalarField B;
  VariableSizedGridOp<scalar,DIM,Encoder>::copyToGridNodal(_grd0,_grd,getB(),B);
  return B;
}
template <int DIM,typename Encoder>
const CellCenterData<scalar>& ShallowWaterSolver<DIM,Encoder>::getW() const
{
  return _U[0];
}
template <int DIM,typename Encoder>
const NodalData<scalar>& ShallowWaterSolver<DIM,Encoder>::getB() const
{
  return _B;
}
template <int DIM,typename Encoder>
scalar ShallowWaterSolver<DIM,Encoder>::getCDrag() const
{
  return _CDrag;
}
template <int DIM,typename Encoder>
void ShallowWaterSolver<DIM,Encoder>::setCDrag(scalar c)
{
  _CDrag=c;
}
template <int DIM,typename Encoder>
scalar ShallowWaterSolver<DIM,Encoder>::getCDragLiquid() const
{
  return _CDragLiquid;
}
template <int DIM,typename Encoder>
void ShallowWaterSolver<DIM,Encoder>::setCDragLiquid(scalar c)
{
  _CDragLiquid=c;
}
template <int DIM,typename Encoder>
scalar ShallowWaterSolver<DIM,Encoder>::getCLift() const
{
  return _CLift;
}
template <int DIM,typename Encoder>
void ShallowWaterSolver<DIM,Encoder>::setCLift(scalar c)
{
  _CLift=c;
}
template <int DIM,typename Encoder>
scalar ShallowWaterSolver<DIM,Encoder>::getTheta() const
{
  return _theta;
}
template <int DIM,typename Encoder>
void ShallowWaterSolver<DIM,Encoder>::setTheta(scalar t)
{
  _theta=t;
}
template <int DIM,typename Encoder>
Vec3 ShallowWaterSolver<DIM,Encoder>::getForceOnBody(sizeType bodyId) const
{
  return _forceTorque[bodyId].segment<3>(0);
}
template <int DIM,typename Encoder>
Vec3 ShallowWaterSolver<DIM,Encoder>::getTorqueOnBody(sizeType bodyId) const
{
  return _forceTorque[bodyId].segment<3>(3);
}
//solver
template <int DIM,typename Encoder>
sizeType ShallowWaterSolver<DIM,Encoder>::advance(const scalar& dt)
{
  _rigid->clearContactFlag();
  _forceTorque.clear();
  CellCenterData<scalar> RHS[DIM+1];
  CellCenterData<scalar> DU[DIM+1][DIM];
  CellCenterData<scalar> h,u[DIM];
  sizeType index=0;
  scalar t=0,limit;
  while(t < dt) {
    //sample source
    sampleSource();
    //compute RHS
    scalar g=-_rigid->getGravity().dot(Vec3::Unit(DIM));
    VariableSizedGridOpShallowWater<scalar,DIM,Encoder>::computeDU(_grd,_U,DU,_B,_theta);
    limit=VariableSizedGridOpShallowWater<scalar,DIM,Encoder>::computeRHS(_grd,_U,DU,RHS,_B,g);
    ASSERT(isFinite(RHS[0]._data))
    //compute timestep
    scalar substep=getSubstep(t,dt,limit);
    if(_rigid && _rigid->nrBody() > 0)
      _rigid->advanceVelocity(substep,Parent::getGlobalTime()+t);
    //update U
    for(sizeType D=0; D<=DIM; D++)
      _U[D]._data+=RHS[D]._data*substep;
    VariableSizedGridOpShallowWater<scalar,DIM,Encoder>::computehu(_grd,h,u,_U,_B);
    VariableSizedGridOpShallowWater<scalar,DIM,Encoder>::computeU(_grd,h,u,_U,_B);
    //compute rigid force
    {
      //save force
      if(_forceTorque.empty())
        _forceTorque.assign(_rigid->nrBody(),Vec6::Zero());
      for(sizeType i=0; i<_rigid->nrBody(); i++) {
        _forceTorque[i].segment<3>(0)-=_rigid->getBody(i)._force;
        _forceTorque[i].segment<3>(3)-=_rigid->getBody(i)._torque;
      }
      VariableSizedGridOpShallowWater<scalar,DIM,Encoder>::computeRigidForce(_grd,_U,h,*_rigid,_CDrag,_CDragLiquid,_CLift,_density,g,substep);
      //load force
      for(sizeType i=0; i<_rigid->nrBody(); i++) {
        _forceTorque[i].segment<3>(0)+=_rigid->getBody(i)._force;
        _forceTorque[i].segment<3>(3)+=_rigid->getBody(i)._torque;
      }
    }
    //advect particle
    if(_rigid && _rigid->nrBody() > 0)
      _rigid->advancePosition(substep);
    index++;
  }
  Parent::advanceTime(dt);
  return index;
}
template <int DIM,typename Encoder>
void ShallowWaterSolver<DIM,Encoder>::sampleSource()
{
  for(sizeType i=0; i<(sizeType)_sources.size(); i++)
    _sources[i]->sampleRegionSWE(_grd,_U,_B,_globalTime);
}
//instance
#define INSTANCE(DIM,TYPE,SPARSITY) \
template class ShallowWaterSolver<DIM,Encoder##TYPE<SPARSITY> >;
INSTANCE_ALL
