import pyFluidOpt as fo
import numpy as np
import math,random,os,shutil
import sys
sys.path.append('..')
sys.path.append('../Render3D/')
from rendering import Viewer
from renderer import Renderer3D
from shader import *
from entity import *
from time import time

def setTrans(T, v):
    for i in range(3):
        T(i,3,v[i])
def setRot(T, r):
    for i in range(3):
        for j in range(3):
            T(i,j,r(i,j))
if __name__ == '__main__':
    #setup
    sz=64.0
    sol=fo.SolverSWE2DConstant1()
    sol.initDomain(fo.BBox(fo.Vec3(0,0,0),fo.Vec3(3,3,0)),fo.Vec3(1.0/sz,1.0/sz,0),fo.Vec3i(1,0,0))
    #add solid
    rigid=fo.RigidSolver(3,1/sz)
    rigid.BB=fo.BBox(fo.Vec3(1/sz,1/sz,1/sz),fo.Vec3(3.0-1/sz,3.0-1/sz,3.0-1/sz))
    T=fo.Mat4()
    R=fo.Mat3()
    T.setIdentity()
    setTrans(T,fo.Vec3(1.5,1.5,-0.1))
    rigid.addSolid(fo.BoxGeomCell(T,3,fo.Vec3(2,2,0.1),0,False))
    setTrans(T,fo.Vec3(1.5,1.5, 0.1))
    R.fromLog(fo.Vec3(math.pi/6.0,0,0))
    setRot(T,R)
    rigid.addSolid(fo.BoxGeomCell(T,3,fo.Vec3(2,2,0.1),0,False))
    sol.initSolid(rigid)
    sol.initLiquidLevel(1.0,1)
    #rigid
    rigid.addBox2(fo.Vec3(0.1,0.1,0.3),fo.Vec3(1.5,2.5,2.5),fo.Vec3(random.random(),random.random(),random.random()))
    rigid.getBody(0).rho=500.0
    rigid.getBody(0).setP(fo.Vec3(5,0,0))
    #source
    s=fo.RandomSource()
    sf=fo.ImplicitRect(fo.Vec3(1.4,1.4,0.0),fo.Vec3(1.6,1.6,1.05),3,1.0/sz,fo.Vec3(0,0,0),0,True,False)
    s.addSource(sf)
    sol.addSource(s)
    #init
    sol.globalId=0
    sol.globalTime=0
    #sol.CDrag=0
    sol.CDragLiquid=15
    #sol.CLift=0
    #simulate
    sol.focusOn1(fo.BBox(fo.Vec3(0,0,0),fo.Vec3(1.5,2.0,0)))
    # if os.path.exists('SWE'):
    #     shutil.rmtree('SWE')
    # os.mkdir('SWE')
    # sol.getGrid().writeCellCenterVTK(sol.getW(),'SWE/W.vtk',True)
    # sol.getGrid().writeNodalVTK(sol.getB(),'SWE/B.vtk',True)

    renderer = Renderer3D(w=5, h=3, res=200, camera_pos=(0.5, 0.5, 1.3), world_up=(0, 0, 1))
    renderer.add_entity(Mesh(renderer.shader, sol, goodlooking=False, output_obj=True))

    for i in range(500):
        print('Simulating frame: %d!'%i)
        sol.advance(1e-2)
        sol.focusOn2(0,1)
        renderer.update()
        renderer.render()

