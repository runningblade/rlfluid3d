#include <Fluid/ScriptedSource.h>
#include <Fluid/SmokeSolver.h>
#include <CommonFile/RotationUtil.h>

USE_PRJ_NAMESPACE

#define SPARSITY 1
extern "C" {
  SmokeSolver<2,EncoderConstant<SPARSITY> >* solSmoke=NULL;
  boost::shared_ptr<RandomSource> s1Smoke,s2Smoke,sSmoke;
  VectorField featureSmoke;
  void destroySmoke()
  {
    if(solSmoke) {
      delete solSmoke;
      solSmoke=NULL;
    }
  }
  void createSmoke(scalar x,scalar y,scalar sz,scalar szShooter)
  {
    ASSERT(x > 1 && y > 1 && sz > 5)
    destroySmoke();
    //create Smoke
    solSmoke=new SmokeSolver<2,EncoderConstant<SPARSITY> >();
    solSmoke->initDomain(BBox<scalar>(Vec3(0.0f,0.0f,0.0f),Vec3(x,y,0.0f)),Vec3(1.0f/sz,1.0f/sz,0.0f));
    {
      boost::shared_ptr<RigidSolver> rigid(new RigidSolver(2,1/sz));
      rigid->setBB(BBox<scalar>(Vec3(-1/sz,-1/sz,0),Vec3(x+1/sz,y+1/sz,0)));
      solSmoke->initSolid(rigid);
    }
    solSmoke->setGlobalTime(0.0f);
    solSmoke->setGlobalId(0);
    solSmoke->setVelDamping(0);
    solSmoke->setDamping(0);
    //add source: 1
    {
      s1Smoke.reset(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitSphere(Vec3(x/2,szShooter,0.0f),szShooter,2,1.0f/sz,Vec3(0,0,0)));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(s1Smoke)->_sources.push_back(sf);
      s1Smoke->setRho(0.8f);
      s1Smoke->setT(20);
      solSmoke->addSource(s1Smoke);
    }
    //add source: 2
    {
      s2Smoke.reset(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitSphere(Vec3(x/2,szShooter,0.0f),szShooter,2,1.0f/sz,Vec3(0,0,0)));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(s2Smoke)->_sources.push_back(sf);
      s2Smoke->setRho(0.8f);
      s2Smoke->setT(20);
      solSmoke->addSource(s2Smoke);
    }
  }
  void createSmokeSingle(scalar x,scalar y,scalar sz,scalar szShooter)
  {
    ASSERT(x > 1 && y > 1 && sz > 5)
    destroySmoke();
    //create Smoke
    solSmoke=new SmokeSolver<2,EncoderConstant<SPARSITY> >();
    solSmoke->initDomain(BBox<scalar>(Vec3(0.0f,0.0f,0.0f),Vec3(x,y,0.0f)),Vec3(1.0f/sz,1.0f/sz,0.0f));
    {
      boost::shared_ptr<RigidSolver> rigid(new RigidSolver(2,1/sz));
      rigid->setBB(BBox<scalar>(Vec3(-1/sz,-1/sz,0),Vec3(x+1/sz,y+1/sz,0)));
      solSmoke->initSolid(rigid);
    }
    solSmoke->setGlobalTime(0.0f);
    solSmoke->setGlobalId(0);
    solSmoke->setVelDamping(0);
    solSmoke->setDamping(0);
    //add source: 1
    {
      sSmoke.reset(new RandomSource(1.0f));
      boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitSphere(Vec3(x/2,szShooter,0.0f),szShooter,2,1.0f/sz,Vec3(0,0,0)));
      boost::dynamic_pointer_cast<ImplicitFuncSource>(sSmoke)->_sources.push_back(sf);
      sSmoke->setRho(0.8f);
      sSmoke->setT(20);
      solSmoke->addSource(sSmoke);
    }
  }
  void addSmokeSolid(scalar cx,scalar cy,scalar x,scalar y)
  {
    if(solSmoke) {
      RigidSolver& rigid=solSmoke->getRigid();
      Mat4 T=Mat4::Identity();
      T.block<3,1>(0,3)=Vec3(cx,cy,0);
      boost::shared_ptr<StaticGeomCell> cell(new BoxGeomCell(T,2,Vec3(x,y,0),0.5f));
      rigid.addSolid(cell);
    }
  }
  void setRigidSmoke(scalar x,scalar y,scalar rad,scalar rho)
  {
    if(solSmoke) {
      RigidSolver& rigid=solSmoke->getRigid();
      if(rigid.nrBody() == 0)
        rigid.addBall(rad,Vec3(x,y,0));
      else rigid.getBody(0).setPos(Vec3(x,y,0));
      rigid.getBody(0).rho()=rho;
    }
  }
  void setRigidBoxSmoke(scalar x,scalar y,scalar rx,scalar ry,scalar rho,bool cross)
  {
    if(solSmoke) {
      RigidSolver& rigid=solSmoke->getRigid();
      if(rigid.nrBody() == 0) {
        if(cross)
          rigid.addCross(Vec3(rx,ry,0),Vec3(x,y,0),Vec3::Zero());
        else rigid.addBox(Vec3(rx,ry,0),Vec3(x,y,0),Vec3::Zero());
      } else rigid.getBody(0).setPos(Vec3(x,y,0));
      rigid.getBody(0).rho()=rho;
    }
  }
  void setRigidVelSmoke(scalar x,scalar y)
  {
    if(solSmoke) {
      RigidSolver& rigid=solSmoke->getRigid();
      if(rigid.nrBody() == 1)
        rigid.getBody(0).setP(Vec3(x,y,0)*rigid.getBody(0).getM());
    }
  }
  void getRigidPosSmoke(scalar* x,scalar* y)
  {
    if(solSmoke) {
      RigidSolver& rigid=solSmoke->getRigid();
      if(rigid.nrBody() > 0) {
        Vec3 pos=rigid.getBody(0).pos();
        *x=pos[0];
        *y=pos[1];
      }
    }
  }
  void getRigidRotSmoke(scalar r[4])
  {
    if(solSmoke) {
      RigidSolver& rigid=solSmoke->getRigid();
      if(rigid.nrBody() > 0) {
        Mat3 rot=rigid.getBody(0).rot();
        r[0]=rot(0,0);
        r[1]=rot(1,0);
        r[2]=rot(0,1);
        r[3]=rot(1,1);
      }
    }
  }
  void getRigidRotAngleSmoke(scalar* theta)
  {
    if(solSmoke) {
      RigidSolver& rigid=solSmoke->getRigid();
      if(rigid.nrBody() > 0)
        *theta=invExpW<scalar>(rigid.getBody(0).rot())[2];
    }
  }
  void getRigidVelSmoke(scalar* x,scalar* y)
  {
    if(solSmoke) {
      RigidSolver& rigid=solSmoke->getRigid();
      if(rigid.nrBody() > 0) {
        Vec3 vel=rigid.getBody(0).linSpd();
        *x=vel[0];
        *y=vel[1];
      }
    }
  }
  void getRigidRotVelSmoke(scalar* dtheta)
  {
    if(solSmoke) {
      RigidSolver& rigid=solSmoke->getRigid();
      if(rigid.nrBody() > 0)
        *dtheta=rigid.getBody(0).rotSpd()[2];
    }
  }
  void getRigidForceSmoke(scalar* force)
  {
    if(solSmoke) {
      RigidSolver& rigid=solSmoke->getRigid();
      if(rigid.nrBody() > 0) {
        force[0]=solSmoke->getForceOnBody(0)[0];
        force[1]=solSmoke->getForceOnBody(0)[1];
      }
    }
  }
  void getRigidTorqueSmoke(scalar* torque)
  {
    if(solSmoke) {
      RigidSolver& rigid=solSmoke->getRigid();
      if(rigid.nrBody() > 0) {
        torque[0]=solSmoke->getTorqueOnBody(0)[2];
      }
    }
  }
  void getRigidBBSmoke(scalar* minC,scalar* maxC)
  {
    minC[0]=minC[1]=maxC[0]=maxC[1]=0;
    if(solSmoke) {
      RigidSolver& rigid=solSmoke->getRigid();
      if(rigid.nrBody() > 0) {
        BBox<scalar> bb;
        rigid.getBody(0).getWorldBB(bb);
        minC[0]=bb._minC[0];
        minC[1]=bb._minC[1];
        maxC[0]=bb._maxC[0];
        maxC[1]=bb._maxC[1];
      }
    }
  }
  void transferSmoke(scalar dt,bool left,bool right,scalar* x,scalar dist,scalar spd)
  {
    Vec3 pos;
    if(solSmoke) {
      boost::shared_ptr<ImplicitSphere> ss1=boost::dynamic_pointer_cast<ImplicitSphere>(s1Smoke->_sources[0]);
      boost::shared_ptr<ImplicitSphere> ss2=boost::dynamic_pointer_cast<ImplicitSphere>(s2Smoke->_sources[0]);
      scalar dx=solSmoke->getGrid0()._cellSz0.maxCoeff();
      scalar width=solSmoke->getGrid0().getBBNodal().getExtent()[0];
      *x=max<scalar>(*x,(dist/2+dx));
      *x=min<scalar>(*x,width-(dist/2+dx));
      ss1->_ctr[0]=*x-dist/2;
      ss2->_ctr[0]=*x+dist/2;
      //turn the shooter
      {
        scalar rx,ry;
        getRigidPosSmoke(&rx,&ry);
        pos=Vec3(rx,ry,0);
      }
      ss1->_vel=(pos-ss1->_ctr);
      ss2->_vel=(pos-ss2->_ctr);
      ss1->_vel*=spd/max<scalar>(ss1->_vel.norm(),1E-6f);
      ss2->_vel*=spd/max<scalar>(ss2->_vel.norm(),1E-6f);
      ss1->reset(ss1->_ctr,ss1->_rad,2,ss1->_res,ss1->_vel);
      ss2->reset(ss2->_ctr,ss2->_rad,2,ss2->_res,ss2->_vel);
      //add source
      if(left)
        solSmoke->addSource(s1Smoke);
      else solSmoke->removeSource(s1Smoke);
      if(right)
        solSmoke->addSource(s2Smoke);
      else solSmoke->removeSource(s2Smoke);
      solSmoke->LiquidSolverPIC<2,EncoderConstant<SPARSITY> >::focusOn(0);
      solSmoke->advance(dt);
    }
  }
  void transferSmokeSingle(scalar dt, bool trigger, scalar *degree, scalar *x, scalar spd, bool* contact=NULL)
  {
    Vec3 pos;
    if(solSmoke) {
      boost::shared_ptr<ImplicitSphere> ss = boost::dynamic_pointer_cast<ImplicitSphere>(sSmoke->_sources[0]);
      scalar dx=solSmoke->getGrid0()._cellSz0.maxCoeff();
      scalar width=solSmoke->getGrid0().getBBNodal().getExtent()[0];
      // Pingchuan: in case of moving traspassing
      *x = max<scalar>(*x, dx);
      *x = min<scalar>(*x, width - dx);
      // Pingchuan: in case of ratating traspassing
      *degree = min<scalar>(*degree, 150.0);
      *degree = max<scalar>(*degree, 30.0);
      // Pingchuan: set the horizontal position
      // Vec3[0] == Vecf3.x
      ss->_ctr[0] = *x;
      //turn the shooter
      scalar degree_ = M_PI **degree / 180.0;
      ss->_vel = Vec3(cos(degree_), sin(degree_), 0);
      ss->_vel *= spd / max<scalar>(ss->_vel.norm(), 1E-6f);
      ss->reset(ss->_ctr, ss->_rad, 2, ss->_res, ss->_vel);
      //add source
      if (trigger)
        solSmoke->addSource(sSmoke);
      else solSmoke->removeSource(sSmoke);
      solSmoke->LiquidSolverPIC<2,EncoderConstant<SPARSITY> >::focusOn(0);
      solSmoke->advance(dt);
      if(contact) {
        *contact=false;
        RigidSolver& rigid=solSmoke->getRigid();
        if(rigid.nrBody() > 0)
          *contact=rigid.getBody(0).hasContactFlag();
      }
    }
  }
  //set sz > 0 to get a feature region around rigid body, sz indicates size of feature
  //set sz < 0 to get a feature of whole region
  void getFeatureSmoke(scalar sz,scalar* rho,scalar* minC,scalar* cellSz,int* nrPoint)
  {
    if(solSmoke) {
      if(!rho) {
        solSmoke->getFeature(featureSmoke,sz);
      } else {
        sizeType k=0;
        for(sizeType i=0; i<featureSmoke.getNrPoint().prod(); i++) {
          rho[k++]=featureSmoke.get(i)[0];
          rho[k++]=featureSmoke.get(i)[1];
        }
      }
      Vec3 pt=featureSmoke.getPt(Vec3i::Zero());
      minC[0]=pt[0];
      minC[1]=pt[1];
      cellSz[0]=featureSmoke.getCellSize()[0];
      cellSz[1]=featureSmoke.getCellSize()[1];
      nrPoint[0]=featureSmoke.getNrPoint()[0];
      nrPoint[1]=featureSmoke.getNrPoint()[1];
    } else {
      minC[0]=minC[1]=0;
      cellSz[0]=cellSz[1]=0;
      nrPoint[0]=nrPoint[1]=0;
    }
  }
  void getFeatureSmokeSimple(scalar sz,scalar* rho,int* nrPoint)
  {
    if(solSmoke) {
      if(!rho) {
        solSmoke->getFeature(featureSmoke,sz);
        nrPoint[0]=featureSmoke.getNrPoint()[0];
        nrPoint[1]=featureSmoke.getNrPoint()[1];
      } else {
        sizeType k=0;
        for(sizeType i=0; i<featureSmoke.getNrPoint().prod(); i++) {
          rho[k++]=featureSmoke.get(i)[0];
          rho[k++]=featureSmoke.get(i)[1];
        }
      }
    } else {
      nrPoint[0]=nrPoint[1]=0;
    }
  }
  void getRhoSmoke(scalar* rho,scalar* posX,scalar* posY,int* sz,int* stride,int* nrRho)
  {
    if(solSmoke) {
      const CellCenterData<scalar>& RHO=solSmoke->getRho();
      const VariableSizedGrid<scalar,2,EncoderConstant<SPARSITY> >& grd=solSmoke->getGrid();
      sz[0]=grd._nrPointCellCenter[0];
      sz[1]=grd._nrPointCellCenter[1];
      stride[0]=grd._strideCellCenter[0];
      stride[1]=grd._strideCellCenter[1];
      nrRho[0]=(int)grd._nrPointCellCenter.prod();
      if(rho)
        memcpy(rho,RHO._data.data(),sizeof(scalar)*nrRho[0]);
      if(posX)
        memcpy(posX,&(grd._x0CellCenters[0][0]),sizeof(scalar)*sz[0]);
      if(posY)
        memcpy(posY,&(grd._x0CellCenters[1][0]),sizeof(scalar)*sz[1]);
    } else {
      sz[0]=sz[1]=0;
      stride[0]=stride[1]=0;
      nrRho[0]=0;
    }
  }
  void setFade(scalar fade)
  {
    if(solSmoke) {
      solSmoke->setDamping(fade);
      solSmoke->setVelDamping(fade);
    }
  }
}
