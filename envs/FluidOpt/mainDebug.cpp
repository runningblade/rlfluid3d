#include <Fluid/VariableSizedGridOpShallowWater.h>
#include <Fluid/VariableSizedGridOpProject.h>
#include <Fluid/VariableSizedGridOp.h>
#include <Fluid/RigidSolver.h>
#include <CommonFile/solvers/ParallelVector.h>
#include <CommonFile/RotationUtil.h>
#include <CommonFile/ParticleCD.h>

USE_PRJ_NAMESPACE

//#define DEBUG_PARTICLECD
//#define DEBUG_GRID
//#define DEBUG_ITERATOR
//#define DEBUG_ADVECT
//#define DEBUG_EXTRAPOLATE
//#define DEBUG_WEIGHT
//#define DEBUG_PROJECT
//#define DEBUG_RIGID_PROJECT
//#define DEBUG_ROTATION
//#define DEBUG_ADJUST_VELOCITY
//#define DEBUG_BOTE_LUMP
//#define DEBUG_SWE
int main()
{
  //all tests
  RandEngine::useDeterministic();
  RandEngine::seed(1000);
#ifdef DEBUG_PARTICLECD
  debugParticleCollisionInterfaceAllDIM(10000);
#endif
#ifdef DEBUG_GRID
  while(true) {
    VariableSizedGrid<scalar,3,EncoderConstant<2> >::debugNodalAllWarp();
    VariableSizedGrid<scalar,3,EncoderConstant<2> >::debugCellCenterAllWarp();
    VariableSizedGrid<scalar,3,EncoderConstant<2> >::debugMACVelAllWarp();
    VariableSizedGrid<scalar,3,EncoderConstant<2> >::debugFocusAllWarp();

    VariableSizedGrid<scalar,3,EncoderOctree<2> >::debugNodalAllWarp();
    VariableSizedGrid<scalar,3,EncoderOctree<2> >::debugCellCenterAllWarp();
    VariableSizedGrid<scalar,3,EncoderOctree<2> >::debugMACVelAllWarp();
    VariableSizedGrid<scalar,3,EncoderOctree<2> >::debugFocusAllWarp();
  }
#endif
#ifdef DEBUG_ITERATOR
  while(true) {
    VariableSizedGrid<scalar,3,EncoderConstant<2> >::debugIteratorAllWarp();
    VariableSizedGrid<scalar,3,EncoderOctree<2> >::debugIteratorAllWarp();
  }
#endif
#ifdef DEBUG_ADVECT
  VariableSizedGridOp<scalar,2,EncoderConstant<2> >::debugAdvectAllWarp(true);
  VariableSizedGridOp<scalar,2,EncoderConstant<2> >::debugAdvectAllWarp(false);
#endif
#ifdef DEBUG_EXTRAPOLATE
  VariableSizedGridOp<scalar,2,EncoderConstant<2> >::debugExtrapolateAllWarp();
  //VariableSizedGridOp<scalar,2,EncoderOctree<2> >::debugExtrapolateAllWarp();
#endif
#ifdef DEBUG_WEIGHT
  VariableSizedGridOpProject<scalar,2,EncoderConstant<2> >::debugWeightAllWarp();
  VariableSizedGridOpProject<scalar,2,EncoderOctree<2> >::debugWeightAllWarp();
#endif
#ifdef DEBUG_PROJECT
  VariableSizedGridOpProject<scalar,2,EncoderConstant<2> >::debugProjectAllWarp();
  //VariableSizedGridOpProject<scalar,2,EncoderOctree<2> >::debugProjectAllWarp();
#endif
#ifdef DEBUG_RIGID_PROJECT
  VariableSizedGridOpProject<scalar,2,EncoderConstant<2> >::debugRigidProjectAllWarp();
#endif
#ifdef DEBUG_ROTATION
  debugExpWGrad<scalarD>();
  debugInvExpW<scalarD>();
  debugExpLogSE3<scalarD>();
  debugCalcFGrad<scalarD>();
  debugDerivSVD<scalarD>();
#endif
#ifdef DEBUG_ADJUST_VELOCITY
  RigidBody::debugAdjustVelocity();
#endif
#ifdef DEBUG_BOTE_LUMP
  VariableSizedGridOpProject<scalar,2,EncoderConstant<2> >::debugRigidProjectBOTELumpAllWarp();
#endif
#ifdef DEBUG_SWE
  VariableSizedGridOpShallowWater<scalar,2,EncoderConstant<2> >::debugSWEAllWarp();
#endif
  return 0;
}
