#include <Fluid/Utils.h>
#include <Fluid/SmokeSolver.h>
#include <Fluid/ScriptedSource.h>
#include <Fluid/VariableSizedGridOp.h>
#include <CommonFile/geom/StaticGeomCell.h>

USE_PRJ_NAMESPACE

void mainSmoke(bool BOTE)
{
  //add fluid
  scalar sz=32.0f;
  Vec3i warpMode(1,1,0);
  SmokeSolver<2,EncoderConstant<1> > sol;
  sol.initDomain(BBox<scalar>(Vec3(0.0f,0.0f,0.0f),Vec3(3.0f,4.0f,0.0f)),Vec3(1.0f/sz,1.0f/sz,0.0f),&warpMode);
  //add solid
  {
    boost::shared_ptr<RigidSolver> rigid(new RigidSolver(2,1/sz));
    rigid->setBB(BBox<scalar>(Vec3(1/sz,1/sz,0),Vec3(3.0f-1/sz,4.0f+100/sz,0)));
    Mat4 T=Mat4::Identity();
    T.block<3,1>(0,3)=Vec3(1.5f,2.5f,0);
    T.block<3,3>(0,0)=makeRotation<scalar>(Vec3(0,0,M_PI/4));
    boost::shared_ptr<StaticGeomCell> cell(new CapsuleGeomCell(T,2,0.05f,0.3f));
    rigid->addSolid(cell);
    sol.initSolid(rigid);
  }
  sol.setGlobalId(0);
  sol.setGlobalTime(0);
  //add source
  {
    boost::shared_ptr<RandomSource> s(new RandomSource(1.0f));
    boost::shared_ptr<SourceImplicitFunc> sf(new ImplicitSphere(Vec3(1.5f,0.25f,0.0f),0.1f,2,1.0f/sz,Vec3(0.0f,-10.0f,0.0f)));
    boost::dynamic_pointer_cast<ImplicitFuncSource>(s)->_sources.push_back(sf);
    s->setT(20);
    s->setRho(0.8f);
    sol.addSource(s);
  }
  //save load
  {
    boost::filesystem::ofstream os("smoke.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    sol.write(os,dat.get());
  }
  {
    sol=SmokeSolver<2,EncoderConstant<1> >();
    boost::filesystem::ifstream is("smoke.dat",ios::binary);
    boost::shared_ptr<IOData> dat=getIOData();
    sol.read(is,dat.get());
  }
  //simulate
  sol.focusOn(BBox<scalar>(Vec3(0,0,0),Vec3(1.5f,2.0f,0)));
  //sol.setUseVariational(false);
  sol.setUseBOTE(BOTE);
  recreate("smoke");
  sol.getRigid().writeMeshVTK("smoke/rigid.vtk");
  for(sizeType i=0; i<1500; i++) {
    INFOV("Simulating frame: %ld!",i)
    sol.advance(1E-2f);
    {
      std::ostringstream oss;
      oss << "smoke/rho" << i << ".vtk";
      VariableSizedGridOp<scalar,2,EncoderConstant<1> >::writeGridCellCenterVTK(oss.str(),sol.getGrid(),sol.getRho());
    }
    {
      std::ostringstream oss;
      oss << "smoke/T" << i << ".vtk";
      VariableSizedGridOp<scalar,2,EncoderConstant<1> >::writeGridCellCenterVTK(oss.str(),sol.getGrid(),sol.getT());
    }
  }
}
