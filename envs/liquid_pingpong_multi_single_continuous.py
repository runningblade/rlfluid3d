import math
import numpy as np
import ctypes as ct
import os

from pynput.keyboard import Listener
from env_utils import *

import gym
from gym import spaces
from gym.utils import seeding
from gym.utils import reraise

import sys
sys.path.append('..')
from baselines.ppo1.mlp_policy import MlpPolicy
import tensorflow as tf
import baselines.common.tf_util as U

# from rendering import Geom, _add_attrs

try:
    import pyglet
except ImportError as e:
    reraise(
        suffix="HINT: you can install pyglet directly via 'pip install pyglet'. But if you really just want "
               "to install all Gym dependencies and not have to think about it, 'pip install -e .[all]' or "
               "'pip install gym[all]' will do it.")

try:
    from pyglet.gl import *
except ImportError as e:
    reraise(prefix="Error occured while running `from pyglet.gl import *`",
            suffix="HINT: make sure you have OpenGL install. On Ubuntu, you can run 'apt-get install "
                   "python-opengl'. If you're running on a server, you may need a virtual frame buffer; "
                   "something like this should work: 'xvfb-run -s \"-screen 0 1400x900x24\" "
                   "python <your_script.py>'")


FPS = 50


class LiquidPingPongMultiSingleContinuous(gym.Env):
    # implementation
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': FPS
    }

    def __init__(self, w=5, h=6, radpp=0.15, radsh=0.12,  # geometry
                 fsh=30, fbb=200, max_omega=250, rhopp=1000,  # physics	# Yunsheng: fsh(10->30), fbb(10->30), max_omega(100->250), rhopp(1500->1000)
                 wp=100, wv=10, wsh=0.5, wl=1, max_step=1000,  # reward weights
                 resf=32, res=100,  # render resolution
                 debug=False, goodlooking=False):  # render when training
        # the rest
        self._seed()
        self.viewer   = None
        self.now_step = 0
        self.max_step = max_step
        self.res      = res
        self.debug    = debug
        # geometry
        self.w     = w
        self.h     = h
        self.radpp = radpp
        self.radsh = radsh
        self.resf  = resf
        # physics
        self.possh1     = w / 2
        self.velsh1     = 0.0
        self.theta1     = 90.0
        self.possh2     = w / 2
        self.velsh2     = 0.0
        self.theta2     = 90.0
        self.fsh       = fsh
        self.fbb       = fbb
        self.rhopp     = rhopp
        self.max_omega = max_omega
        # reward
        self.wp    = wp
        self.wv    = wv
        self.wsh   = wsh
        self.wl    = wl
        # render option
        self.goodlooking = goodlooking
        self.background = [(0, 0), (self.w, 0), (self.w, self.h), (0, self.h)]
        # fluid mesh info
        self.mesh_v_size = 0
        self.mesh_i_size = 0
        self.mesh_v = []
        self.mesh_i = []
        # state: pp x, pp y, pp velx, pp vely,
        # state: sh x, sh a, sh velx, shooter vela
        high = np.array([np.inf] * 7)
        self.observation_space = spaces.Box(-high, high)
        # action: nop, shoot, shoot left, shoot right, move left, move right
        # self.bb_speed_range  = np.array([0, 1])  # Pingchuan: speed of liquid
        self.bb_volume_range = np.array([-1, 1])  # Pingchuan: volume of liquid
        self.sh_degree_range = np.array([-1, 1])  # Pingchuan: degree of shooter
        self.sh_force_range  = np.array([-1, 1])  # Pingchuan: moving force of shooter
        action_range = np.array([self.bb_volume_range, self.sh_degree_range, self.sh_force_range])
        action_range = action_range.T
        self.action_space = spaces.Box(action_range[0, :], action_range[1, :])
        # c interface
        lib_path = os.path.join(os.path.split(os.path.realpath(__file__))[0], "FluidOpt-build/libFluid.so")
        self.fluid = ct.cdll.LoadLibrary(lib_path)
        liquid_func_declare(self.fluid)
        self._reset()

    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _destroy(self):
        self.fluid.destroyLiquid()

    def _reset(self):
        self._destroy()
        self.fluid.createLiquidSingle(self.w, self.h, self.resf, self.radsh, (ct.c_float*4)(1,1,1,1))
        self.fluid.setRigidLiquid(self.w / 2, self.h / 2, self.radpp, self.rhopp, 0)
        self.game_over  = False
        self.now_step   = 0
        self.possh1     = self.w / 2
        self.velsh1     = 0.0
        self.theta1     = 90.0
        self.possh2     = self.w / 2
        self.velsh2     = 0.0
        self.theta2     = 90.0
        
        # field size
        nrPoint = (ct.c_int * 2)() # w, h
        self.fluid.getFeatureLiquidSimple(ct.c_float(-1), ct.POINTER(ct.c_float)(), ct.byref(nrPoint))
        self.field_size = [nrPoint[1], nrPoint[0], 2] # input: h, w

        new_step = self._step(self.action_space.sample())
        return new_step[0], new_step[1]

    def sample_speed(self):
        nrPoint = (ct.c_int * 2)()
        self.fluid.getFeatureLiquidSimple(ct.c_float(-1), ct.POINTER(ct.c_float)(), ct.byref(nrPoint))
        rho = (ct.c_float * (nrPoint[0] * nrPoint[1] * 2))()
        self.fluid.getFeatureLiquidSimple(ct.c_float(-1), ct.byref(rho), ct.byref(nrPoint))
        speed = [rho[i] for i in range(nrPoint[0] * nrPoint[1] * 2)]
        return preprocess(speed, nrPoint[0], nrPoint[1])

    def _step(self, action):
        # ensure its a valid action
        low = self.action_space.low
        high = self.action_space.high
        action1, action2 = action[0], action[1]
        action1 = np.clip(action1, low, high)
        action2 = np.clip(action2, low, high)
        assert self.action_space.contains(action1), "%r (%s) invalid " % (action1, type(action1))
        assert self.action_space.contains(action2), "%r (%s) invalid " % (action2, type(action2))
        # record the steps
        self.now_step += 1
        # shoot_A = False
        # shoot_B = False
        if self.debug is True:  # in case u wanna c the training process visually
            self.render()
        # Action/Engine
        # action_MOVE     = action // 4
        # action_SHOOT    = action % 4
        bb_speed   = 1
        bb_volume1  = action1[0]
        sh_omega1   = action1[1]
        sh_force1   = action1[2]
        shoot1      = False
        last_possh1 = self.possh1
        bb_volume2  = action2[0]
        sh_omega2   = action2[1]
        sh_force2   = action2[2]
        shoot2      = False
        last_possh2 = self.possh2
        # Pingchuan: update velocity of shooter
        self.velsh1 += sh_force1 * self.fsh / FPS
        self.possh1 += self.velsh1 / FPS
        self.theta1 += sh_omega1 * self.max_omega / FPS
        self.velsh2 += sh_force2 * self.fsh / FPS
        self.possh2 += self.velsh2 / FPS
        self.theta2 += sh_omega2 * self.max_omega / FPS
        # print(self.theta)
        if np.random.rand() * 2 - 1 < bb_volume1:
            shoot1 = True
        if np.random.rand() * 2 - 1 < bb_volume2:
            shoot2 = True
        possh1_c    = (ct.c_float)(self.possh1)
        theta1_c    = (ct.c_float)(self.theta1)
        possh2_c    = (ct.c_float)(self.w - self.possh2)
        theta2_c    = (ct.c_float)(self.theta2 + 180.0)
        bb_speed_c = (ct.c_float)(self.fbb * bb_speed)
        mesh_v_size_c = (ct.c_int)(self.mesh_v_size)
        mesh_i_size_c = (ct.c_int)(self.mesh_i_size)
        self.fluid.transferLiquidMultiSingle(1 / FPS, True, shoot1, theta1_c, possh1_c, bb_speed_c, mesh_v_size_c, mesh_i_size_c, (ct.c_bool)())
        self.fluid.transferLiquidMultiSingle(1 / FPS, False, shoot2, theta2_c, possh2_c, bb_speed_c, mesh_v_size_c, mesh_i_size_c, (ct.c_bool)())
        self.possh1 = possh1_c.value
        self.theta1 = theta1_c.value
        self.velsh1 = (self.possh1 - last_possh1) * FPS
        self.possh2 = self.w - possh2_c.value
        self.theta2 = theta2_c.value - 180.0
        self.velsh2 = (self.possh2 - last_possh2) * FPS

        if self.goodlooking:
            self.mesh_v_size = mesh_v_size_c.value
            self.mesh_i_size = mesh_i_size_c.value
            self.mesh_v = [0.0] * self.mesh_v_size * 3
            self.mesh_i = [0] * self.mesh_i_size * 3
            if self.mesh_v_size > 0 and self.mesh_i_size > 0:
                mesh_v_c = (ct.c_float * (self.mesh_v_size * 3))(*self.mesh_v)
                mesh_i_c = (ct.c_int * (self.mesh_i_size * 3))(*self.mesh_i)
                self.fluid.getLiquidMesh(mesh_v_c, mesh_i_c)
                self.mesh_v = list(mesh_v_c)
                self.mesh_i = list(mesh_i_c)

        # State
        pospp_x = (ct.c_float)()
        pospp_y = (ct.c_float)()
        self.fluid.getRigidPosLiquid(pospp_x, pospp_y)
        velpp_x = (ct.c_float)()
        velpp_y = (ct.c_float)()
        self.fluid.getRigidVelLiquid(velpp_x, velpp_y)
        state1 = np.array([pospp_x.value, pospp_y.value, velpp_x.value, velpp_y.value, self.possh1, self.velsh1, self.theta1])
        state2 = np.array([self.w - pospp_x.value, self.h - pospp_y.value, -velpp_x.value, -velpp_y.value, self.possh2, self.velsh2, self.theta2])
        assert self.observation_space.contains(state1), "%r (%s) invalid " % (state1, type(state1))
        assert self.observation_space.contains(state2), "%r (%s) invalid " % (state2, type(state2))
        
        speed = self.sample_speed()

        # Reward
        reward1 = 0
        reward2 = 0 

        if pospp_y.value - self.radpp * 1.5 < 2 / self.resf:
            self.game_over = True
            reward1 = -1
            reward2 = 1

        if self.h - self.radpp * 1.5 - pospp_y.value < 2 / self.resf:
            self.game_over = True
            reward1 = 1
            reward2 = -1

        if self.now_step > self.max_step:
            self.game_over = True

        # Return
        return state1, state2, reward1, reward2, self.game_over, {'speed': speed}

    def _render(self, mode='human', close=False):
        if close:
            if self.viewer is not None:
                self.viewer.close()
                self.viewer = None
            return

        # renderer setup
        import rendering
        from rendering import Points, _add_attrs
        if self.viewer is None:
            self.viewer = rendering.Viewer(self.w * self.res, self.h * self.res)
            self.viewer.set_bounds(0, self.w, 0, self.h)

        # background
        if self.goodlooking:
            t = rendering.Transform(translation=(self.w/2, self.h/2))
            self.viewer.draw_image('texs/table_top.png', self.w, self.h, one_time=False, color=(1,1,1,1)).add_attr(t)
        else:
            self.viewer.draw_polygon(self.background, color=(0,0,0))

            # liquid particles
            nr_particle_c = (ct.c_int)()
            null_ptr = ct.POINTER(ct.c_float)()
            self.fluid.getParticlesLiquid(null_ptr, nr_particle_c)
            particle_cache_c = (ct.c_float * (nr_particle_c.value * 2))()
            self.fluid.getParticlesLiquid(particle_cache_c, nr_particle_c)
            pts = Points(particle_cache_c)
            _add_attrs(pts, {'color': (0.5, 0.9, 0.4)})
            self.viewer.add_onetime(pts)
            for i in range(nr_particle_c.value):
               t = rendering.Transform(translation = (particle_cache_c[i*2+0], particle_cache_c[i*2+1]))
               self.viewer.draw_circle(0.01, 20, color=(0.5, 0.9, 0.4)).add_attr(t)
        
        pospp_x = (ct.c_float)()
        pospp_y = (ct.c_float)()
        self.fluid.getRigidPosLiquid(pospp_x, pospp_y)

        if self.goodlooking:
            # pingpong
            scale_por = 1.1 + pospp_x.value/6/self.w
            t = rendering.Transform(translation=(pospp_x.value + self.radpp*(scale_por-1), pospp_y.value), scale=(scale_por, 1), 
                rotation=-np.arctan((self.h/2-pospp_y.value)/(self.w+pospp_x.value)))
            self.viewer.draw_circle(self.radpp, 20, color=(0.0, 0.0, 0.0, 0.3)).add_attr(t)
            t = rendering.Transform(translation=(pospp_x.value, pospp_y.value))
            self.viewer.draw_image('texs/pingpong.png', self.radpp * 2, self.radpp * 2, color=(1,1,1,1)).add_attr(t)
            # shooter
            t = rendering.Transform(translation=(self.possh1, self.radsh/2), rotation=(self.theta1-90.0)*np.pi/180.0)
            self.viewer.draw_image('texs/pipe_flip.png', self.radsh*2, self.radsh*2, color=(1,1,1,1)).add_attr(t)
            t = rendering.Transform(translation=(self.w - self.possh2, self.h - self.radsh/2), rotation=(self.theta2-270.0)*np.pi/180.0)
            self.viewer.draw_image('texs/pipe.png', self.radsh*2, self.radsh*2, color=(1,1,1,1)).add_attr(t)
            # mesh
            self.viewer.draw_mesh(self.mesh_v, self.mesh_i, color=(187/255, 1, 1, 0.3))
        else:
            # pingpong
            t = rendering.Transform(translation=(pospp_x.value, pospp_y.value))
            self.viewer.draw_circle(self.radpp, 20, color=(1, 1, 1)).add_attr(t)
            self.viewer.draw_circle(self.radpp, 20, color=(0.9, 0.9, 0.9), filled=False, linewidth=2).add_attr(t)
            # shooter
            t = rendering.Transform(translation=(self.possh1, self.radsh))
            self.viewer.draw_circle(self.radsh, 20, color=(0.5, 0.4, 0.9)).add_attr(t)
            self.viewer.draw_circle(self.radsh, 20, color=(0.3, 0.3, 0.5), filled=False, linewidth=2).add_attr(t)
            t = rendering.Transform(translation=(self.possh2, self.h - self.radsh))
            self.viewer.draw_circle(self.radsh, 20, color=(0.5, 0.4, 0.9)).add_attr(t)
            self.viewer.draw_circle(self.radsh, 20, color=(0.3, 0.3, 0.5), filled=False, linewidth=2).add_attr(t)
        # return
        return self.viewer.render(return_rgb_array=mode == 'rgb_array')

    def run_game_2p(self):
        Listener(
            on_press=on_press,
            on_release=on_release).start()
        s1, s2 = self.reset()
        total_reward = 0
        steps = 0
        bb_speed = 1
        bb_volume1 = -1
        sh_omega1 = 0
        sh_force1 = 0
        bb_volume2 = -1
        sh_omega2 = 0
        sh_force2 = 0
        while True:     # Yunsheng: fixed bugs in user control

            if keys['a'] is True:
                sh_force1 = -1
            elif keys['d'] is True:
                sh_force1 = 1
            else:
                sh_force1 = 0

            if keys['w'] is True:
                bb_volume1 = 1
            else:
                bb_volume1 = -1

            if keys['1'] is True:
                sh_omega1 = 1
            elif keys['2'] is True:
                sh_omega1 = -1
            else:
                sh_omega1 = 0

            if keys['j'] is True:
                sh_force2 = 1
            elif keys['l'] is True:
                sh_force2 = -1
            else:
                sh_force2 = 0

            if keys['i'] is True:
                bb_volume2 = 1
            else:
                bb_volume2 = -1

            if keys['9'] is True:
                sh_omega2 = 1
            elif keys['0'] is True:
                sh_omega2 = -1
            else:
                sh_omega2 = 0

            action1 = np.array([bb_volume1, sh_omega1, sh_force1])
            action2 = np.array([bb_volume2, sh_omega2, sh_force2])

            s1, s2, r1, r2, done, info = self.step([action1, action2])
            self.render()
            total_reward += r1
            if done:
                print(["{:+0.2f}".format(x) for x in s1])
                print("step {} total_reward {:+0.2f}".format(steps, total_reward))
            steps += 1
            if done:
                self.reset()
                total_reward = 0
                steps = 0
                bb_speed = 1
                bb_volume1 = -1
                sh_omega1 = 0
                sh_force1 = 0
                bb_volume2 = -1
                sh_omega2 = 0
                sh_force2 = 0


    def run_game_1p(self):
        Listener(
            on_press=on_press,
            on_release=on_release).start()
        s1, s2 = self.reset()
        total_reward = 0
        steps = 0
        bb_speed = 1
        bb_volume1 = -1
        sh_omega1 = 0
        sh_force1 = 0

        ob_space = self.observation_space
        ac_space = self.action_space

        def policy_fn(name, ob_space, ac_space):
            return MlpPolicy(name=name, ob_space=ob_space, ac_space=ac_space,
                         policy_hid_size=(128, 128, 64, 32), vf_hid_size=(128, 128, 64, 32), activation_policy=tf.nn.relu,
                         activation_vf=tf.nn.relu)

        pi = policy_fn("pi", ob_space, ac_space)
        U.load_state("../trpo/saved_model/trpo_model_lppmsc.ckpt")

        while True:     # Yunsheng: fixed bugs in user control

            if keys['a'] is True:
                sh_force1 = -1
            elif keys['d'] is True:
                sh_force1 = 1
            else:
                sh_force1 = 0

            if keys['w'] is True:
                bb_volume1 = 1
            else:
                bb_volume1 = -1

            if keys['1'] is True:
                sh_omega1 = 1
            elif keys['2'] is True:
                sh_omega1 = -1
            else:
                sh_omega1 = 0

            action1 = np.array([bb_volume1, sh_omega1, sh_force1])
            action2, _ = pi.act(True, s2)
            # action2 = self.action_space.sample()

            s1, s2, r1, r2, done, info = self.step([action1, action2])
            self.render()
            total_reward += r1
            if done:
                print(["{:+0.2f}".format(x) for x in s1])
                print("step {} total_reward {:+0.2f}".format(steps, total_reward))
            steps += 1
            if done:
                self.reset()
                total_reward = 0
                steps = 0
                bb_speed = 1
                bb_volume1 = -1
                sh_omega1 = 0
                sh_force1 = 0


if __name__ == "__main__":
    env = LiquidPingPongMultiSingleContinuous(goodlooking=True)
    env.run_game_2p()
