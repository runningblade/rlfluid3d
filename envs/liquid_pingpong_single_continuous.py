import math
import numpy as np
import ctypes as ct
import os

from pynput.keyboard import Listener
from env_utils import *

import gym
from gym import spaces
from gym.utils import seeding
from gym.utils import reraise
from fluidRender import *
from recorder import GameSingleRunner

FPS = 50


class LiquidPingPongSingleContinuous(gym.Env):
    # implementation
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': FPS
    }

    def __init__(self, w=5, h=3, radpp=0.35, radsh=0.12,  # geometry
                 fsh=30, fbb=20, max_omega=250, rhopp=700,  # physics   # Yunsheng: fsh(10->30), fbb(10->30), max_omega(100->250), rhopp(1500->1000)
                 wp=10, wv=2, wsh=1, wl=150, max_step=1000,  # reward weights
                 resf=32, res=100,  # render resolution
                 debug=False, render_option='p', **kwargs):  # render when training
        # the rest
        self.seed()
        self.viewers  = []
        self.now_step = 0
        self.max_step = max_step
        self.res      = res
        self.debug    = debug
        # geometry
        self.w     = w
        self.h     = h
        self.radpp = radpp
        self.radsh = radsh
        self.resf  = resf
        # physics
        self.possh     = w / 2
        self.velsh     = 0.0
        self.theta     = 90.0
        self.fsh       = fsh
        self.fbb       = fbb
        self.rhopp     = rhopp
        self.max_omega = max_omega
        # fluid mesh info
        self.mesh_v_size = 0
        self.mesh_i_size = 0
        self.mesh_v = []
        self.mesh_i = []
        # reward
        self.wp    = wp
        self.wv    = wv
        self.wsh   = wsh
        self.wl    = wl
        # rendering
        self.bg_obj = Background(self.w, self.h, '../envs/texs/grey.jpg', (0, 0, 0))
        self.rigid_obj = Ball(self.radpp * 2, self.radpp * 2, '../envs/texs/yellowball.png')
        self.sh_obj = Ball(self.radsh * 2, self.radsh * 2, '../envs/texs/pipe.png', (0.5, 0.4, 0.9))
        # state: pp x, pp y, pp velx, pp vely,
        # state: sh x, sh a, sh velx, shooter vela
        high = np.array([np.inf] * 9)
        self.observation_space = spaces.Box(-high, high, dtype=np.float32)
        # action: nop, shoot, shoot left, shoot right, move left, move right
        # self.bb_speed_range  = np.array([0, 1])  # Pingchuan: speed of liquid
        self.bb_volume_range = np.array([-1, 1])  # Pingchuan: volume of liquid
        self.sh_degree_range = np.array([-1, 1])  # Pingchuan: degree of shooter
        self.sh_force_range  = np.array([-1, 1])  # Pingchuan: moving force of shooter
        action_range = np.array([self.bb_volume_range, self.sh_degree_range, self.sh_force_range])  # (3, 2)
        action_range = action_range.T  # (2, 3)
        self.action_space = spaces.Box(action_range[0, :], action_range[1, :], dtype=np.float32)
        # c interface
        lib_path = os.path.join(os.path.split(os.path.realpath(__file__))[0], "FluidOpt-build/libFluid.so")
        self.fluid = ct.cdll.LoadLibrary(lib_path)
        liquid_func_declare(self.fluid)
        self.renderer = Renderer(render_option, 'liquid', self.fluid, self.w, self.h, self.res, self.bg_obj, self.rigid_obj, self.sh_obj)
        self.reset()

        self.keymap = [{'w': 1, 'else': -1}, {'left': 1, 'right': -1}, {'a': -1, 'd': 1}]

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def destroy(self):
        self.fluid.destroyLiquid()

    def reset(self):
        self.destroy()
        self.fluid.createLiquidSingle(self.w, self.h, self.resf, self.radsh, (ct.c_float*4)(1,1,1,1), False)
        self.fluid.setRigidLiquid(self.w / 2, self.h / 2, self.radpp, self.rhopp, GRAVITY)
        self.game_over  = False
        self.now_step   = 0
        self.possh      = self.w / 2
        self.velsh      = 0.0
        self.theta      = 90.0
        
        # field size
        nrPoint = (ct.c_int * 2)() # w, h
        self.fluid.getFeatureLiquidSimple(ct.c_float(-1), ct.POINTER(ct.c_float)(), ct.byref(nrPoint))
        self.field_size = [nrPoint[1], nrPoint[0], 2] # input: h, w
        return self.step(self.action_space.sample())[0]

    def sample_speed(self):
        nrPoint = (ct.c_int * 2)()
        self.fluid.getFeatureLiquidSimple(ct.c_float(-1), ct.POINTER(ct.c_float)(), ct.byref(nrPoint))
        rho = (ct.c_float * (nrPoint[0] * nrPoint[1] * 2))()
        self.fluid.getFeatureLiquidSimple(ct.c_float(-1), ct.byref(rho), ct.byref(nrPoint))
        speed = [rho[i] for i in range(nrPoint[0] * nrPoint[1] * 2)]
        return preprocess(speed, nrPoint[0], nrPoint[1])

    def step(self, action):
        # ensure its a valid action
        low = self.action_space.low
        high = self.action_space.high
        action = np.clip(action, low, high)
        assert self.action_space.contains(action), "%r (%s) invalid " % (action, type(action))
        # record the steps
        self.now_step += 1
        # shoot_A = False
        # shoot_B = False
        if self.debug is True:  # in case u wanna c the training process visually
            self.render()
        # Action/Engine
        # action_MOVE     = action // 4
        # action_SHOOT    = action % 4
        bb_speed   = 1
        bb_volume  = action[0]
        sh_omega   = action[1]
        sh_force   = action[2]
        shoot      = False
        last_possh = self.possh
        # Pingchuan: update velocity of shooter
        self.velsh += sh_force * self.fsh / FPS
        # self.velsh = np.clip(self.velsh, -4, 4)     # Yunsheng: velsh clip
        self.possh += self.velsh / FPS
        self.theta += sh_omega * self.max_omega / FPS
        # print(self.theta)
        if np.random.rand() * 2 - 1 < bb_volume:
            shoot = True
        possh_c    = (ct.c_float)(self.possh)
        theta_c    = (ct.c_float)(self.theta)
        bb_speed_c = (ct.c_float)(self.fbb * bb_speed)
        mesh_v_size_c = (ct.c_int)(self.mesh_v_size)
        mesh_i_size_c = (ct.c_int)(self.mesh_i_size)
        contact_c = (ct.c_bool)()
        self.fluid.transferLiquidSingle(1 / FPS, shoot, theta_c, possh_c, bb_speed_c, mesh_v_size_c, mesh_i_size_c, ct.c_bool(False), contact_c)
        self.possh = possh_c.value
        self.theta = theta_c.value
        self.velsh = (self.possh - last_possh) * FPS

        self.mesh_v_size = mesh_v_size_c.value
        self.mesh_i_size = mesh_i_size_c.value

        # State
        pospp_x = (ct.c_float)()
        pospp_y = (ct.c_float)()
        self.fluid.getRigidPosLiquid(pospp_x, pospp_y)
        velpp_x = (ct.c_float)()
        velpp_y = (ct.c_float)()
        self.fluid.getRigidVelLiquid(velpp_x, velpp_y)
        force_pp = (ct.c_float * 2)()
        self.fluid.getRigidForceLiquid(force_pp)
        force_pp[0] /= 5
        force_pp[1] /= 5
        state = np.array([
            pospp_x.value,
            pospp_y.value,
            velpp_x.value,
            velpp_y.value,
            force_pp[0],
            force_pp[1],
            self.possh,
            self.velsh,
            self.theta
        ])
        assert self.observation_space.contains(state), "%r (%s) invalid " % (state, type(state))
        
        speed = self.sample_speed()

        # Reward
        reward = 0

        coefficient = np.exp(-3 * np.sqrt(
            (state[0] - self.w / 2) * (state[0] - self.w / 2) +
            (state[1] - self.h / 2) * (state[1] - self.h / 2)
        ))
        reward += coefficient * self.wp

        coefficient = np.exp(-3 * np.sqrt(
            state[2] * state[2] +
            state[3] * state[3]
        ))
        reward += coefficient * self.wv

        reward += (1 - bb_volume) / 2 * self.wsh

        if contact_c.value:
            self.game_over = True

        if self.now_step > self.max_step:
            self.game_over = True

        # Return
        return state, reward, self.game_over, {'speed': speed}

    def render(self, mode='human', close=False):
        self.viewers, result = self.renderer.render(self.viewers, mode, close, [(self.possh, self.radsh / 2)], [self.theta], self.mesh_v_size, self.mesh_i_size)
        return result


if __name__ == "__main__":
    env = LiquidPingPongSingleContinuous()
    runner = GameSingleRunner(env)
    runner.run('h')
