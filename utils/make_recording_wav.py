# read json from 'results/saved_recording/jsons/' and save wav to 'results/saved_recording/wavs/'

from os import makedirs, listdir
import json
import sys
sys.path.append('../pysynth')
from argparse import ArgumentParser
from pysynth_b import make_wav

RESULT_PATH = '../results/'


def make_song(name, bpm):
	assert name.endswith('.json')
	with open(name, 'r') as f:
		song = json.load(f)
	save_path = RESULT_PATH + 'saved_recording/wavs/'
	makedirs(save_path, exist_ok=True)
	make_wav(tuple(song), fn=save_path + name.split('/')[-1].rstrip('.json') +'.wav', leg_stac=.7, bpm=bpm)


def main():
	parser = ArgumentParser(description='MAKE_MUSIC_WAV.')
	parser.add_argument('--name', type=str, default='_all')
	parser.add_argument('--bpm', type=int, default=120)
	args = parser.parse_args()
	name = args.name

	load_path = RESULT_PATH + 'saved_recording/jsons/'
	if name == '_all':
		for file_name in listdir(load_path):
			make_song(load_path + file_name, args.bpm)
	else:
		song = load_path + args.name + '.json'
		make_song(args.name, args.bpm)


if __name__ == '__main__':
	main()