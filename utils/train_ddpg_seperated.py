#!/usr/bin/env python
# noinspection PyUnresolvedReferences
import os
import sys

sys.path.append('..')
sys.path.append('../envs')
sys.path.append('../trpo')
sys.path.append('../ddpg')
from mpi4py import MPI
from baselines.common import set_global_seeds, tf_util as U
import os.path as osp
import gym
import logging
import argparse
from baselines import logger, bench

import signal
from trpo_cnn import TrpoCnn
import numpy as np

from env_funcs import *
from utils import *
from recorder import GameSingleRunner, GameDoubleRunner

# import ddpg as ddpg

from functools import partial

RESULT_PATH = '../results/rollout4_500_memory3000/'
MUSIC_PATH = './music_jsons/'
RECORD_PATH = RESULT_PATH + 'saved_recording/jsons/'

# Parameters

# CNN_PARAMS = {
#     'filter_sizes': [7, 7, 5, 5, 3, 3, 3],
#     'strides': [2, 2, 2, 2, 1, 1, 1],
#     'filter_nums': [64, 64, 128, 128, 256, 256, 256],
#     'pads': ["SAME", "SAME", "SAME", "SAME", "VALID", "VALID", "VALID"]
# }

CNN_PARAMS = {
    'filter_sizes': None,
    'strides': None,
    'filter_nums': None,
    'pads': None
}


def train(env_id, mode, seed, max_step,
          music, make_recording, load_ckpt, make_video,
          args):

    rank = MPI.COMM_WORLD.Get_rank()

    if rank != 0:
        logger.set_level(logger.DISABLED)
    workerseed = seed + 10000 * MPI.COMM_WORLD.Get_rank()
    set_global_seeds(workerseed)

    if make_recording:
        assert music != ''
        recording = RECORD_PATH + music + '.json'
    else:
        recording = None

    if music == '':
        music = None
    else:
        music = MUSIC_PATH + music + '.json'

    cnn_input = False
    cnn = None
    if mode in ['train', 'train_multi']:
        env = func[env_id](max_step=max_step, music=music)
    elif mode in ['train_cnn', 'train_multi_cnn']:
        env = func[env_id](max_step=max_step, music=music)
        cnn_input = True
    elif mode in ['play', 'enjoy', 'battle']:
        env = func[env_id](max_step=np.inf, music=music, recording=recording)
    elif mode in ['enjoy_cnn', 'battle_cnn']:
        env = func[env_id](max_step=np.inf, music=music, recording=recording)
        cnn_input = True
        cnn = TrpoCnn(env.field_size)
    elif mode == 'build_cnn':
        env = func[env_id](max_step=np.inf)
        cnn = TrpoCnn(env.field_size, filter_sizes=CNN_PARAMS['filter_sizes'], strides=CNN_PARAMS['strides'],
                      filter_nums=CNN_PARAMS['filter_nums'], pads=CNN_PARAMS['pads'])
    else:
        print('Mode argument error!')
        return

    if env_id.endswith('b'):  # be cautious when naming non-battle env
        import trpo_multi_mpi as trpo
        battle = True
        GameRunner = GameDoubleRunner
    else:
        import trpo_mpi as trpo
        battle = False
        GameRunner = GameSingleRunner


    def policy_fn():
        from models import Actor, Critic
        from ddpg_learner import DDPG
        critic = Critic(layer_norm=True)
        actor = Actor(env.action_space.shape[-1], layer_norm=True)
        return DDPG(actor, critic, None, env.observation_space.shape, env.action_space.shape)

    env.seed(workerseed)
    gym.logger.setLevel(logging.WARN)
    logger.info('rank {}: seed={}, logdir={}'.format(rank, workerseed, logger.get_dir()))


    finetune = False
    if mode.endswith('_cnn'):
        IDENTIFIER[0] += '_cnn'
        env_id += '_cnn'
        finetune = True

    if args.use_ddpg_shared:
        import ddpg_shared as ddpg
        logger.log("using shared")
    else:
        import ddpg_seperated as ddpg
        logger.log("using sepereted")

    if mode.startswith('train'):
        ddpg.learn(None, env, seed=workerseed, write_path=RESULT_PATH,
                   nb_epoch_cycles=args.nb_epoch_cycles,
                   nb_rollout_steps=args.nb_rollout_steps,
                   critic_lr=args.critic_lr, actor_lr=args.actor_lr,
                   nb_train_steps=args.nb_train_steps,
                   batch_size=args.batch_size,
                   memory_size=args.memory_size,
                   )

    elif mode == 'build_cnn':
        trpo.build_equal_cnn(env, policy_fn, cont=False, timesteps_per_batch=500,
                             max_iters=700, identifier=env_id, cnn=cnn, reward_list=reward_list)
    else:
        runner = GameRunner(env, policy_fn, max_timesteps=int(3e7), identifier=env_id,
                            record_video=make_video, cnn=cnn, network_trpo = False)
        run_option = {'play': 'h', 'enjoy': 'n', 'enjoy_cnn': 'n',
                      'battle': 'hn', 'battle_cnn': 'hn'}
        runner.run(run_option[mode])

    env.close()

def main():
    # global IDENTIFIER

    parser = argparse.ArgumentParser(description='TRAIN.')
    parser.add_argument('--mode', type=str, default='train')  # train or play
    parser.add_argument('--env_id', type=str, default='lppsc')  # choose env you like

    parser.add_argument('--load_ckpt', dest='load_ckpt', action='store_true')
    parser.set_defaults(load_ckpt=False)
    parser.add_argument('--make_video', dest='make_video', action='store_true')
    parser.set_defaults(make_video=False)

    parser.add_argument('--max_step', type=int, default=500)
    parser.add_argument('--rollout_length', type=int, default=500)   # no for ddpg
    parser.add_argument('--total_timesteps', type=int, default=None)  # not too big in this case

    parser.add_argument('--policy_size', type=int, nargs='+', default=(128, 64, 64, 32))
    parser.add_argument('--value_func_size', type=int, nargs='+', default=(128, 64, 64, 32))

    parser.add_argument('--activation_vf', type=str, default='relu')
    parser.add_argument('--activation_policy', type=str, default='relu')
    parser.add_argument('--seed', type=int, default=0)
    parser.add_argument('--log_dir', type=str, default=RESULT_PATH + 'logs/')

    parser.add_argument('--music', type=str, default='')
    parser.add_argument('--make_recording', dest='make_recording', action='store_true')
    parser.set_defaults(make_recording=False)

    parser.add_argument('--actor-lr', type=float, default=1e-5)
    parser.add_argument('--critic-lr', type=float, default=1e-3)

    parser.add_argument('--batch-size', type=int, default=64)  # per MPI worker 64
    parser.add_argument('--gamma', type=float, default=0.99)
    parser.add_argument('--reward-scale', type=float, default=1.)
    parser.add_argument('--clip-norm', type=float, default=None)
    parser.add_argument('--nb-epochs', type=int, default=None)  # with default settings, perform 1M steps total
    parser.add_argument('--nb-epoch-cycles', type=int, default=2)  # 20
    parser.add_argument('--nb-train-steps', type=int, default=50)  # per epoch cycle and MPI worker
    parser.add_argument('--nb-eval-steps', type=int, default=100)  # per epoch cycle and MPI worker
    parser.add_argument('--nb-rollout-steps', type=int, default=500)  # per epoch cycle and MPI worker
    parser.add_argument('--noise-type', type=str, default='adaptive-param_0.2')  # choices are adaptive-param_xx, ou_xx, normal_xx, none
    parser.add_argument('--num-timesteps', type=int, default=None)  # ????sha wan yi useless
    parser.add_argument('--memory-size', type=int, default=3000)

    parser.add_argument('--use_ddpg_shared', dest='use_ddpg_shared', action='store_true')
    parser.set_defaults(use_ddpg_shared=False)

    args = parser.parse_args()

    assert args.env_id in func
    IDENTIFIER[0] = args.env_id
    if args.load_ckpt or args.mode in ['play', 'enjoy', 'enjoy_cnn', 'battle', 'battle_cnn', 'build_cnn']:
        params = load_params(args.env_id)
        args.max_step = params['max_step']
        args.rollout_length = params['rollout_length']
        args.total_timesteps = params['total_timesteps']
        args.policy_size = tuple(params['policy_size'])
        args.value_func_size = tuple(params['value_func_size'])
    else:
        params = {'max_step': args.max_step,
                  'rollout_length': args.rollout_length,
                  'total_timesteps': args.total_timesteps,
                  'policy_size': args.policy_size,
                  'value_func_size': args.value_func_size,
                  'actor_lr': args.actor_lr,
                  'critic_lr': args.critic_lr,
                  'nb_epoch_cycles': args.nb_epoch_cycles,
                  'nb_rollout_steps': args.nb_rollout_steps,
                  'nb_train_steps': args.nb_train_steps,
                  'batch_size': args.batch_size,
                  'memory_size': args.memory_size,
                  'use_ddpg_shared' : args.use_ddpg_shared,
                  }
        save_params(params, args.env_id, RESULT_PATH=RESULT_PATH)

    activation_map = {'relu': tf.nn.relu, 'leaky_relu': U.lrelu, 'tanh': tf.nn.tanh}

    logger.configure(dir=args.log_dir)


    activation_policy = activation_map[args.activation_policy]
    activation_vf = activation_map[args.activation_vf]

    # set para in default is more comfortable for me
    train(args.env_id, args.mode, args.seed, args.max_step,
          args.music, args.make_recording, args.load_ckpt, args.make_video,
          args)


if __name__ == '__main__':
    main()

