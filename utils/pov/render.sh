#!/bin/sh
if [ "$#" -ne 2 ]; then
    echo 'Wrong argument number, expect 2'
    exit 1
fi

type=$1
batch_size=$2

result_path=../../results/pov
inis_path=$result_path/inis/$type
mkdir $inis_path -p

img_path=$result_path/img/$type
rm -rf $img_path
mkdir $img_path

for ((i=0;i<200;i+=$batch_size))
do
    rm -f $inis_path/scene$(($i/$batch_size)).ini
    echo '+H720
+W1280

Input_File_Name=scene_'$type'.pov
Output_File_Name="'$img_path'/scene"
Initial_Frame='$(($i))'
Final_Frame='$(($i+$batch_size-1))'
Initial_Clock=0
Final_Clock=0

Antialias=On
Antialias_Threshold=0.2
Antialias_Depth=3' >> $inis_path/scene$(($i/$batch_size)).ini
    gnome-terminal -x bash -c 'povray '$inis_path'/scene'$(($i/$batch_size))'.ini'
done
