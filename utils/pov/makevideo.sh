#!/bin/sh
if [ "$#" -ne 1 ]; then
    echo 'Wrong argument number, expect 1'
    exit 1
fi

TYPE=$1
RESULT_PATH=../../results/pov
mkdir $RESULT_PATH/video -p
ffmpeg -framerate 25 -i $RESULT_PATH/img/$TYPE/scene%d.png -c:v libx264 -profile:v high -crf 20 -pix_fmt yuv420p $RESULT_PATH/video/$TYPE.mp4
