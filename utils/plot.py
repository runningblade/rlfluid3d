# read json from 'results/saved_model/plot_jsons/' and save png to 'results/saved_model/plot_images'

from utils import plot_rewards
from argparse import ArgumentParser
from os import listdir

RESULT_PATH = '../results/'

PATH = RESULT_PATH + 'saved_model/plot_jsons/'
POSTFIX = '.json'

parser = ArgumentParser(description='PLOT.')
parser.add_argument('--name', type=str, default='_all')
args = parser.parse_args()
name = args.name

if name == '_all':
    for file_name in listdir(PATH):
        assert file_name.endswith(POSTFIX)
        plot_rewards(PATH + file_name)
else:
    plot_rewards(PATH + args.name + POSTFIX)
