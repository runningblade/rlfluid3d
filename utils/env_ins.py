import sys
sys.path.append('../envs')
from scenes import *

def get_swe1(*args, **kwargs):
    src_num = kwargs['src_num']
    target_threshold = kwargs['target_threshold']
    return SWEMulti1Way3DRgd(
        seed=100,
        debug=kwargs['debug'],
        max_step=500,
        res=200,
        warp_on=False,
        rgd_rho=100,
        reward=SWE2DReward(
            win_factor=100,
            lose_factor=1,
            target_threshold=target_threshold
            ),
        game_over=SWE2DGameOver(
            target_threshold=target_threshold
            ),
        obs_num=12+src_num,
        act_num=2*src_num,
        w=1.5,
        h=2.5,
        d=1,
        sz=64,
        rgd_w=0.1,
        rgd_h=0.1,
        rgd_d=0.1,
        rgd_x=0.3,
        rgd_y=0.25,
        rgd_z=0.4,
        target_x_range=[0.225, 1.275],
        target_y_range=[1.5, 2.25],
        target_z_range=[0.4, 0.5],
        target_threshold=target_threshold,
        rgd_type='ball',
        trap_h=0.6,
        trap_t=0.2,
        src_num=src_num,
        c_d=200,
        c_dl=5,
        c_l=10,
        sink_h=0.5,
        src_def_h=0.4,
        f_mv=0.1,
        f_sh=1,
        water_d=0.37,
        vtk=kwargs['vtk'],
        pov=kwargs['pov']
    )


def get_swe2(*args, **kwargs):
    src_num = kwargs['src_num']
    target_threshold = kwargs['target_threshold']
    return SWEMulti2Way3DRgd(
        seed=100,
        debug=kwargs['debug'],
        max_step=500,
        res=200,
        warp_on=False,
        rgd_rho=100,
        reward=SWE2DReward(
            win_factor=100,
            lose_factor=1,
            target_threshold=target_threshold
            ),
        game_over=SWE2DGameOver(
            target_threshold=target_threshold
            ),
        obs_num=12+src_num*2,
        act_num=4*src_num,
        w=1,
        h=2,
        d=2,
        sz=64,
        rgd_w=0.1,
        rgd_h=0.1,
        rgd_d=0.1,
        rgd_x=0.3,
        rgd_y=0.25,
        rgd_z=0.6,
        target_x_range=[0.3, 0.7],
        target_y_range=[1.2, 1.5],
        target_z_range=[0.4, 0.5],
        target_threshold=target_threshold,
        rgd_type='ball',
        trap_h=0.5,
        trap_t=0.2,
        src_num=src_num,
        c_d=200,
        c_dl=5,
        c_l=10,
        sink_h=0.5,
        src_def_h=0.4,
        f_mv=0.1,
        f_sh=1,
        water_d=0.37,
        vtk=kwargs['vtk'],
        pov=kwargs['pov']
    )
