import numpy as np
import matplotlib as mpl
import scipy.stats

import scipy as scp
# import sys
# sys.path.append('/scratch/tdb40/sandbox/GPflow/gpflow')
import gpflow
import gpflow.multioutput.kernels as mk
import gpflow.multioutput.features as mf

import pdb
import matplotlib.pyplot as plt
import tensorflow as tf
import matplotlib.ticker as ticker

# import sgpr
import osgpr

import logging
logging.getLogger().setLevel(logging.INFO)


# gpflow.multioutput.conditionals.logger.level = logging.DEBUG
# gpflow.multioutput.features.logger.level = logging.DEBUG

def init_Z(cur_Z, new_X, use_old_Z=False):
    if use_old_Z:
        Z = np.copy(cur_Z)
    else:
        M = cur_Z.shape[0]
        M_old = int(0.7 * M)
        M_new = M - M_old
        old_Z = cur_Z[np.random.permutation(M)[0:M_old], :]
        new_Z = new_X[np.random.permutation(new_X.shape[0])[0:M_new], :]
        Z = np.vstack((old_Z, new_Z))
    return Z

def make_feed(param, value):
    return {param.unconstrained_tensor : param.transform.backward(value)}

class Surr_env(object):
    def __init__(self, X, Y, M=500, alpha=0.5, disp=False):
        self.M = M
        self.P = Y.shape[1]
        # q_mu = np.zeros((M, self.P)).reshape(M * self.P, 1)
        # q_sqrt = np.eye(M * self.P).reshape(1, M * self.P, M * self.P)

        Z = X[np.random.permutation(X.shape[0])[0:M]]

        kern_list = [gpflow.kernels.RBF(X.shape[1], variance=0.001, lengthscales=0.8) for _ in range(Y.shape[1])]
        self.kern = mk.SeparateIndependentMok(kern_list)
        self.feat = mf.SharedIndependentMof(gpflow.features.InducingPoints(Z))
        # self.sgpr_model = sgpr.SGPR_PEP(X, Y, self.kern, Z=Z, alpha=alpha)
        self.svgp_model = gpflow.models.SVGP(X.copy(), Y.copy(), self.kern, gpflow.likelihoods.Gaussian(),
                                             feat=self.feat, minibatch_size=50)
                                            # Z=Z, minibatch_size=50)
        # for var in tf.trainable_variables():
        #     print(var.name)
        # w = tf.summary.FileWriter('./tensorboard_log', tf.get_default_session().graph)
        # w.close()

        self.opt = gpflow.train.ScipyOptimizer()
        self.opt.minimize(self.svgp_model, disp=disp)
        self.opt = self.opt.optimizer  # important

        self.sess = self.svgp_model.enquire_session()
        self.ssgp_model = None


    def update(self, X, Y, alpha=0.5, disp=False):
        # if self.ssgp_model is None:
        #     Z_old = self.svgp_model.feature.Z.value
        #     Z_new = init_Z(Z_old, X)
        #     mu, su = self.svgp_model.predict_f_full_cov(Z_old)
        #     Kaa = self.svgp_model.kern.compute_K_symm(Z_old)
        #     self.ssgp_model = osgpr.OSGPR_PEP(X, Y, self.svgp_model.kern, mu, su, Kaa, Z_old, Z_new, alpha)
        #     self.ssgp_model.likelihood.variance = self.svgp_model.likelihood.variance.value
        # else:
        #     Z_old = self.ssgp_model.Z.value
        #     Z_new = init_Z(Z_old, X)
        #     self.ssgp_model.X = X
        #     self.ssgp_model.Y = Y
        #     mu, su = self.ssgp_model.predict_f_full_cov(Z_old)
        #     Kaa = self.ssgp_model.kern.compute_K_symm(Z_old)
        #     self.ssgp_model.mu_old = mu
        #     self.ssgp_model.Su_old = su
        #     self.ssgp_model.Kaa_old = Kaa
        #     self.ssgp_model.Z_old = Z_old
        #     self.ssgp_model.Z = Z_new
        # self.opt.minimize(self.ssgp_model, disp=disp)

        Z_old = self.svgp_model.feature.feat.Z.value
        Z_new = init_Z(Z_old, X)
        self.svgp_model.feature.feat.Z = Z_new

        self.opt.minimize(self.sess)


    def step(self, X):
        if self.ssgp_model is None:
            result = self.svgp_model.predict_f(X)
        else:
            result = self.ssgp_model.predict_f(X)
        return result
