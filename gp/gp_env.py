import GPy
import numpy as np
from sklearn import preprocessing
from paramz.transformations import Logistic

def init_Z(cur_Z, new_X, use_old_Z=False):
    if use_old_Z:
        Z = np.copy(cur_Z)
    else:
        M = cur_Z.shape[0]
        M_old = int(0.7 * M)
        M_new = M - M_old
        old_Z = cur_Z[np.random.permutation(M)[0:M_old], :]
        new_Z = new_X[np.random.permutation(new_X.shape[0])[0:M_new], :]
        Z = np.vstack((old_Z, new_Z))
    return Z

class Surr_env(object):
    def __init__(self, X, Y):
        self.normalizer = preprocessing.StandardScaler().fit(X)
        self.D = X.shape[1]
        self.P = Y.shape[1]
        trans_x = self.normalizer.transform(X)
        # k = GPy.kern.Matern32(X.shape[0], lengthscale=0.1, ARD=True)
        # self.model = GPy.models.GPCoregionalizedRegression(trans_x, Y, kernel=k)
        # # self.model = GPy.models.GPMultioutRegression(trans_x, Y, self.P, Z=trans_x, num_inducing=(self.P, self.P))
        # self.model.optimize(verbose=True)
        self.model = []
        for i in range(Y.shape[1]):
            k = GPy.kern.RBF(X.shape[1], variance=1.0, lengthscale=0.1, ARD=True)
            gp = GPy.models.SparseGPRegression(trans_x, Y[:, i].reshape(-1,1), k, Z=trans_x, name='SVGP'+str(i), normalizer=True)
            gp.likelihood.constrain(Logistic(0, 1e2))
            gp.likelihood = 0.01
            self.model.append(gp)
            print(gp)
            gp.optimize(messages=True)
        # print(self.model)
        self.Z = trans_x


    def update(self, X, Y):
        trans_x = self.normalizer(X)
        Z_new = init_Z(self.Z, trans_x)
        self.Z = Z_new
        for i in range(self.P):
            self.model[i].X = trans_x
            self.model[i].Y = Y[:, i]
            self.model[i].parameters_changed()
            self.model[i].optimize(messages=True)

    def step(self, X):
        trans_x = self.normalizer.transform(X)
        result = self.model[0].predict(trans_x)
        for i in range(self.P-1):
            r = self.model[i+1].predict(trans_x)
            result = np.hstack(result, r)
        return result
